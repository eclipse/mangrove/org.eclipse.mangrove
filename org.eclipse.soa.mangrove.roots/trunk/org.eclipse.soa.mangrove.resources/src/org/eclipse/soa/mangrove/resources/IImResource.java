/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.resources;

public interface IImResource {

	public static final String DATA_SOURCE_IM_RESOURCE_TYPE = "datasource";
	
	
	public String getId();
	
	public String getResourceType();
	
	public String[] getPropertyNames();
	
	public String[] getPropertyLabels();
	
	public String getProperty(String propertyName);
	
	public void setProperty(String propertyName, String propertyValue);
	
	public void delProperty(String propertyName);
	
	
	
}
