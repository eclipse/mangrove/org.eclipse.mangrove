/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.resources.ui.providers;



import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.soa.mangrove.resources.IImResource;
import org.eclipse.swt.graphics.Image;

public class IImResourceLabelProvider implements ITableLabelProvider
{
	
	public IImResourceLabelProvider(){
	}
	
	public String getColumnText(Object element, int column_index) {
		if (element instanceof IImResource) {
			 IImResource imResource =(IImResource)element;
			 
			 if ((imResource.getPropertyNames()[column_index]).equalsIgnoreCase("Password"))
				 return "*****";
			 return imResource.getProperty(imResource.getPropertyNames()[column_index]);	
			 
		 }else{
			 return "";
		 }	 
			 
  }

  public void addListener(ILabelProviderListener ilabelproviderlistener)
  {
  }

  public void dispose()
  {
  }

  public boolean isLabelProperty(Object obj, String s)
  {
    return false;
  }

  public void removeListener(ILabelProviderListener ilabelproviderlistener)
  {
  }

  public Image getColumnImage(Object element, int column_index)
  {
      return null;
    
  }
}

