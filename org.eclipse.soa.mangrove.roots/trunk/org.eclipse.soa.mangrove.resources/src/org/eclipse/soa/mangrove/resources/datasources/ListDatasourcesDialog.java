/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.resources.datasources;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.soa.mangrove.resources.IImResource;
import org.eclipse.soa.mangrove.resources.IImResourceConfigurator;
import org.eclipse.soa.mangrove.resources.IImResourceImporter;
import org.eclipse.soa.mangrove.resources.ImResourcesActivator;
import org.eclipse.soa.mangrove.resources.ui.providers.IImResourceContentProvider;
import org.eclipse.soa.mangrove.resources.ui.providers.IImResourceLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class ListDatasourcesDialog extends ImTitleAreaDialog {
	
    private Composite aComposite = null;
	
    private SashForm internalSash  = null; 
    
    private Table tableDatasources = null;
    private TableViewer tbvDatasources = null; 
    private List<IImResource> theDatasources = null;
    private CCombo exportersCombo = null;
    private CCombo importersCombo = null;

    
		
	public ListDatasourcesDialog(Shell parentShell) {
		
		super(parentShell);

		this.theDatasources = new ArrayList<IImResource>();
		

	}
	
	
				

	/**
	   * @see org.eclipse.jface.window.Window#create() We complete the dialog with
	   *      a title and a message
	   */
	  public void create() {
		super.create();
		setTitle("IntermediateModel Datasources Repository ");
	    setMessage(" Datasource Definitions / Import / Export  ");
	   
	  }

	 
	  

	@Override
	protected Control createContents(Composite parent) {
		Control ctrl = super.createContents(parent);
		return ctrl;
	}

	protected Control createDialogArea(Composite parent) {
		
        this.internalSash = new SashForm(parent,  SWT.VERTICAL | SWT.FLAT);
        this.internalSash.setLayoutData(new GridData(GridData.FILL_BOTH));
       	
    	final ListDatasourcesDialog theDialog = this;
    	
    	this.theDatasources = ImResourcesActivator.getDataSources(); 
    	
        this.tableDatasources = new Table(internalSash, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);
        this.tableDatasources.setLinesVisible(true);
        this.tableDatasources.setHeaderVisible(true);
        TableColumn column = null;
		column = new TableColumn(tableDatasources, SWT.LEFT);
		column.setText("Name");
		column.setWidth(200);
		column = new TableColumn(tableDatasources, SWT.LEFT);
		column.setText("Driver");
		column.setWidth(200);
		column = new TableColumn(tableDatasources, SWT.LEFT);
		column.setText("Url");
		column.setWidth(200);
		column = new TableColumn(tableDatasources, SWT.LEFT);
		column.setText("User");
		column.setWidth(200);
		column = new TableColumn(tableDatasources, SWT.LEFT);
		column.setText("Password");
		column.setWidth(100);
		
        this.tbvDatasources = new TableViewer(tableDatasources);
        tbvDatasources.setContentProvider(new IImResourceContentProvider());
        tbvDatasources.setLabelProvider(new IImResourceLabelProvider());
        tbvDatasources.setInput(this.theDatasources);    
    	
        this.tableDatasources.addKeyListener(new KeyListener(){
        	public void keyPressed(org.eclipse.swt.events.KeyEvent e) {
        		//System.out.println("KeyCode =" +  e.keyCode);
        		if (e.keyCode == 127){
        			TableItem[] tis = theDialog.tableDatasources.getSelection();
        			DataSourceImResource selected = (DataSourceImResource)tis[0].getData();
        			boolean confirm = MessageDialog.openConfirm(
    						getShell(),
    						"Intermediate Model",
    						"This will remove datasource "+ selected.getId()+ "from IM Resources are you sure ? ");
        			if (confirm){
        				ImResourcesActivator.deleteDatasource(selected);
        				theDialog.tbvDatasources.setInput(ImResourcesActivator.getDataSources());
        				theDialog.tbvDatasources.refresh();
        			}
        			
        		}
        	};
        	public void keyReleased(org.eclipse.swt.events.KeyEvent e) {
        		
        	};
        	
        });
       
       
        
        aComposite = new Composite(internalSash, SWT.NONE);
		final GridLayout layout = new GridLayout();
    	layout.marginWidth = 15;
    	layout.marginHeight = 10;
    	layout.numColumns = 2;
    	aComposite.setLayout(layout);
    	aComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    	
    	String imgAddDatasourcePath = "icons" + File.separator + "database_add.gif";
    	Image addDatasorceImage = ImResourcesActivator.getImageDescriptor(
    			imgAddDatasourcePath).createImage();
    	
    	
    	
    	
    	
    	// 1 - row
    	
    	
    	
    	CLabel aLabel = new CLabel(aComposite, SWT.LEFT);
        aLabel.setText(" Add a new datasource ");
    	GridData data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	aLabel.setLayoutData(data);
    	
    	Button btnAddSpagicDatasource = new Button(aComposite, SWT.FLAT);
    	btnAddSpagicDatasource.setImage(addDatasorceImage);
    	btnAddSpagicDatasource.setText(" Add Datasource ");
    	data = new GridData(GridData.FILL_HORIZONTAL);

    	data.horizontalSpan = 1;
    	btnAddSpagicDatasource.setLayoutData(data);
    	
    	// 2 - row
    	importersCombo = new CCombo(aComposite, SWT.BORDER | SWT.FLAT );
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	
    	String names[] = ImResourcesActivator.getIImResourceImportersNames();
    	for (int i=0; i < names.length; i++){
    		importersCombo.add(names[i]);
    	}
    	
    	data.horizontalSpan = 1;
    	importersCombo.setLayoutData(data);
    	
    	Button btnImport= new Button(aComposite, SWT.FLAT);
    	//btnImport.setImage(impSmxImage);
    	btnImport.setText(" Import ");
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	btnImport.setLayoutData(data);
    	
    	// 3 - row
    	exportersCombo = new CCombo(aComposite, SWT.BORDER | SWT.FLAT );
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	
    	String names2[] = ImResourcesActivator.getIImResourceConfiguratorsNames();
    	for (int i=0; i < names2.length; i++){
    		exportersCombo.add(names2[i]);
    	}
    	
    	data.horizontalSpan = 1;
    	exportersCombo.setLayoutData(data);
    	
    	Button btnExport = new Button(aComposite, SWT.FLAT);
    	//btnExport.setImage(exportJNDIImage);
    	btnExport.setText(" Export ");
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	btnExport.setLayoutData(data);

    	
    	
    	btnExport.addListener(SWT.Selection,new Listener() {
        	public void handleEvent(Event event) {
        		String resourceConfiguratorName = exportersCombo.getText();
        		IImResourceConfigurator resCfg = ImResourcesActivator.getIImResourceConfiguratorByName(resourceConfiguratorName);
        		List<IImResource> selectedDataSources = new ArrayList<IImResource>();
        		
        		TableItem[] selectedItems = theDialog.tableDatasources.getSelection();
        		if (selectedItems == null || selectedItems.length == 0){
        			MessageDialog.openInformation(getShell(), "ImResource", " Select Local Resourcse to Export ");
        			return;
        		}
        		for (int i = 0; i < selectedItems.length; i++) {
        			selectedDataSources.add((IImResource)selectedItems[i].getData());
        		}
        		if (selectedDataSources != null && selectedDataSources.size() > 0){
        			if (resCfg.isDeploySupported()){
        				String deploymentResult = resCfg.deploy(selectedDataSources,IImResource.DATA_SOURCE_IM_RESOURCE_TYPE);
        				MessageDialog.openInformation(getShell(), "ImResource", deploymentResult);
            			return;
        			}else {
        				ViewConfigurationDialog viewConfigurationDialog = new ViewConfigurationDialog(getShell(), resCfg.getConfiguration(selectedDataSources,IImResource.DATA_SOURCE_IM_RESOURCE_TYPE));
        				viewConfigurationDialog.open();
        			}
        		}
        	
        	}
    	});
    	
    	
    	
    	
    	btnAddSpagicDatasource.addListener(SWT.Selection,new Listener() {
        	public void handleEvent(Event event) {
        		DetailDatasourceDialog dsDialog = new DetailDatasourceDialog(getShell());
        		dsDialog.open();
        		DataSourceImResource dsBean = dsDialog.getDatasourceBean();
        		if (dsBean != null){
        			ImResourcesActivator.addDataSource(dsBean);
        			theDialog.tbvDatasources.setInput(ImResourcesActivator.getDataSources());
    				theDialog.tbvDatasources.refresh();
        		}
        	}
        });
        

    	btnImport.addListener(SWT.Selection, 
    		new Listener() {
            	public void handleEvent(Event event) {
            		try{
            			String importerName = importersCombo.getText();
                		IImResourceImporter resImporter = ImResourcesActivator.getIImResourceImporterByName(importerName);
            			List<IImResource> dsListFromImporter = resImporter.importResources(IImResource.DATA_SOURCE_IM_RESOURCE_TYPE);

                		if (dsListFromImporter == null || dsListFromImporter.size() == 0){
                			return;
                		}
                		
            			SelectImResourcesDialog selectionDsDialog = new SelectImResourcesDialog(getShell(), "Datasources List from ServiceMix",dsListFromImporter);
            			selectionDsDialog.open();
            			List<IImResource> choosedResources = selectionDsDialog.getSelectedImResources();
                		if (choosedResources != null){
                			
                			for (IImResource res : choosedResources){
                				if (ImResourcesActivator.getImResourceByIdAndType(res.getId(), DataSourceImResource.DATA_SOURCE_IM_RESOURCE_TYPE) != null){
                					if (MessageDialog.openConfirm(getShell(), "Spagic", "Datasource ["+res.getId()+"] is already defined in Spagic Studio, Update with configurations found in running smx ?")){
                						ImResourcesActivator.deleteDatasource(res);
                						ImResourcesActivator.addDataSource(res);
                					}
                				}else{
                					ImResourcesActivator.addDataSource(res);
                				}
                			}
                			theDialog.tbvDatasources.setInput(ImResourcesActivator.getDataSources());
            				theDialog.tbvDatasources.refresh();
                		}
            		}catch (Exception e) {
						MessageDialog.openError(getShell(),"Spagic","Cannot contact Spagic Server to get datasourceList");
					}
            	}
            });
    	
    	
    	internalSash.setWeights(new int[] {60,40});
        return parent;
	}

	
	protected void okPressed() {
		super.okPressed();
		
	}
	



	protected void createButtonsForButtonBar(Composite parent) {
		
		
	}
	
}
