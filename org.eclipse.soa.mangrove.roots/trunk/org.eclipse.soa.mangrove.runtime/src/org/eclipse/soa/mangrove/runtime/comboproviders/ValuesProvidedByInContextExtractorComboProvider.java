/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.comboproviders;

import java.util.Map;

public class ValuesProvidedByInContextExtractorComboProvider extends AbstractComboProvider {

	public ComboEntries getComboEntries() {
		return comboEntries;
	}
	public ValuesProvidedByInContextExtractorComboProvider(Map<String, String> params){
		this.params = params;
		this.comboEntries = new ComboEntries();
	}
	
	private ComboEntries comboEntries = null;
	
	
	
	public void setLabels(String[] labels){
		
		comboEntries.setLabels(labels);
	}
	
	public void setValues(String[] values){
		comboEntries.setValues(values);
	}



		
	
}
