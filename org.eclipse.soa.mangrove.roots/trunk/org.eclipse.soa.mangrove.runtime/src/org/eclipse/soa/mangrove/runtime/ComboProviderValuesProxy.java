/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.soa.mangrove.util.ImLogger;

public class ComboProviderValuesProxy {

	private static final String ATT_ID = "id";
	private static final String ATT_NAME = "name";
	private static final String ATT_CLASS = "class";
	private static final String ATT_TARGET_COMBO = "target-combo";


	private final IConfigurationElement configElement;

	private final String id;
	private final String name;
	private final String targetCombo;


	public ComboProviderValuesProxy(IConfigurationElement configElement) {
		this.configElement = configElement;

		id = getAttribute(configElement, ATT_ID, null);
		name = getAttribute(configElement, ATT_NAME, id);
		targetCombo = getAttribute(configElement, ATT_TARGET_COMBO, id);
		
		// Make sure that class is defined,
		// but don't load it
		getAttribute(configElement, ATT_CLASS, null);
	}

	private static String getAttribute(IConfigurationElement configElem,
			String name, String defaultValue) {
		String value = configElem.getAttribute(name);
		if (value != null)
			return value;
		if (defaultValue != null)
			return defaultValue;
		throw new IllegalArgumentException("Missing " + name + " attribute");
	}
	
	public IComboProviderValues getComboProviderValue(){
		try{
			IComboProviderValues comboProviderValues = (IComboProviderValues) configElement.createExecutableExtension(ATT_CLASS);
			return comboProviderValues;
		}catch (Exception e) {
			ImLogger.error(ImRuntimeActivator.PLUGIN_ID, e.getMessage(), e);
			return null;
		}
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getTargetCombo() {
		return targetCombo;
	}

}
