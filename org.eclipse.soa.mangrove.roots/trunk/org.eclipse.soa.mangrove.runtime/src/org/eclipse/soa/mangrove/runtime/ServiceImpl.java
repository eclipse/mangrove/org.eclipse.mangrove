/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import java.util.ArrayList;
import java.util.List;

public class ServiceImpl implements IService {
	
	private String name = null;
	private String description = null;
	private List<String>  serviceBindingsName = null;
	private String emfServiceEntity = null;
	
	private String registeringBundleId = null;
	private String iconPath = null;
	
	public ServiceImpl(String name, String description, String bindingsList){
		this.name = name;
		this.description = description;
		
		String[] strArr = bindingsList.split(",");
		serviceBindingsName = new ArrayList<String>();
		
		for (int i = 0; i < strArr.length; i++){
			serviceBindingsName.add(strArr[i].trim());
		} // --
	
	} // --
	
	public String getDescription() {
	
		return this.description;
	
	}

	public String getName() {
		
		return this.name;
	
	}
	
	public List<String> getServiceBindingsName() {
	
		return this.serviceBindingsName;
	
	}

	public String getEmfServiceEntity() {
		return emfServiceEntity;
	}

	public void setEmfServiceEntity(String emfServiceEntity) {
		this.emfServiceEntity = emfServiceEntity;
	}

	public String getIconPath() {
		return this.iconPath;
	}

	public String getRegisteringBundleId() {
		return this.registeringBundleId;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;	
	}
	
	public void setRegisteringBundleId(String registeringBundleId) {
		this.registeringBundleId= registeringBundleId;
	}
}
