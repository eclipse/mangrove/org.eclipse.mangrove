/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.property.listeners;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.soa.mangrove.runtime.IProperty;

public class NumberedMapHandlerListener implements IPropertyListener {

	public void handleUpdate(String sourcePropertyValue, String dependentPropertyName, IProperty destinationServiceBindingProperty, EAnnotation ea ) {
		
		
		// 
		// In this implementation the property that has changes is a number that determines the
		// size of the map identified by dependentPropertyName
		// 
		int changedValueInt = Integer.valueOf(sourcePropertyValue);
		if (changedValueInt >= 0){
			//System.out.println(" Cannot handle a number of value less then 0");
			String basePropertyName = null;
			for (int i = 0; i < changedValueInt; i++) {
				basePropertyName = dependentPropertyName + "["+i+"]";
				String propertyToFoundInEAnnotation = basePropertyName+"."+destinationServiceBindingProperty.getMapFieldList().get(0);
				
				// -------------------------------------------------------
				// If the eAnnotation contains already just the value
				// keep it
				// --------------------------------------------------------
				
				if (ea.getDetails().get(propertyToFoundInEAnnotation) == null){
					String destPropertyNameInAnnotation = null;	
					//--------------------------------------------------------
					// Write a "Map Entry" in the eAnnotation
					// -------------------------------------------------------
					for (String propOfMap : destinationServiceBindingProperty.getMapFieldList()) {
						
						

							destPropertyNameInAnnotation = basePropertyName + "." + propOfMap;
							ea.getDetails().put(destPropertyNameInAnnotation, "");

					}
				}else{
					// -----------
					// -----------
				}
			}
			
			
			// ------------------------------------------------------------------------------
			//
			// Se ho rimosso delle property Devo Gestirlo
			// 
			// ------------------------------------------------------------------------------
			
			String idx = null;
			Integer idxInt = null;
			List<String> keyToRemove = new ArrayList<String>();
			
			for (String s : ea.getDetails().keySet() ){
				
				if (s.startsWith(dependentPropertyName)){
					idx = s.substring(s.indexOf("[") +1, s.indexOf("]"));
					idxInt = Integer.valueOf(idx);
					
					if (idxInt >= changedValueInt)
						keyToRemove.add(s);
			
				} // -- end if (s.startsWith(dependentPropertyName))
			
			} // -- end for (String s : ea.getDetails().keySet() )
			
			for ( String s : keyToRemove){
			
				ea.getDetails().removeKey(s);
			
			}
			
		}// end if (changedValueInt > 0)
	}
}
