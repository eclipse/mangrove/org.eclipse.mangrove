/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.comboproviders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.soa.mangrove.runtime.IAnnotationDependentComboProviderValues;
import org.eclipse.soa.mangrove.runtime.IComboProviderValues;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;

public class AnnotationDependentComboProvider extends AbstractComboProvider {
	
	protected EAnnotation eAnnotation = null;
	
	public AnnotationDependentComboProvider(Map<String, String> params){
		
		this.params = params;
		
	}

	public ComboEntries getComboEntries() {
		
		String comboName = params.get("comboName");
		
		List<IComboProviderValues> listComboProviderValues = ImRuntimeActivator.getExternalPluginsComboProviderValues(comboName);
				
		ComboEntries aComboEntries = null;
		List<String> tmpLabels = new ArrayList<String>();
		List<String> tmpValues = new ArrayList<String>();
		
		for (IComboProviderValues aComboProviderValues : listComboProviderValues ){
			if (aComboProviderValues instanceof IAnnotationDependentComboProviderValues){
				((IAnnotationDependentComboProviderValues)aComboProviderValues).setAnnotation(eAnnotation);
			}
			aComboEntries = aComboProviderValues.getComboEntries();
			tmpLabels.addAll(Arrays.asList(aComboEntries.getLabels()));
			tmpValues.addAll(Arrays.asList(aComboEntries.getValues()));
		}
		
		ComboEntries ce = new ComboEntries();
		String[] labels = new String[tmpLabels.size()];
		String[] values = new String[tmpValues.size()];
		ce.setLabels(tmpLabels.toArray(labels));
		ce.setValues(tmpValues.toArray(values));
		return ce;
	}

	public String getConfigurationParameterValue(String parameterName) {
		// The StaticComboProvider have not parameters
		return "";
	}
	

	
	
	public void setAnnotation(EAnnotation ea){
		this.eAnnotation = ea;
	}
}
