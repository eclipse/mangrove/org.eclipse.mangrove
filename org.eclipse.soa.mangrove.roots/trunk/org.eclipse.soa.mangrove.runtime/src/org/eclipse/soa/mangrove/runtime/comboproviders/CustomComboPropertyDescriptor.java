/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.comboproviders;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.ComboBoxPropertyDescriptor;

public class CustomComboPropertyDescriptor  extends ComboBoxPropertyDescriptor {
	
	    private String[] mData;
	    private String[] mDisplay;
	    private int mDefault;

	    public CustomComboPropertyDescriptor(String id, String displayName, ComboEntries entries, int default1)
	    {
	        
	    	super(id, displayName, new String[0]);
//	    	System.out.println(" Display Name " + displayName);
	        mData = entries.getValues();
	        mDisplay = entries.getLabels();
	        mDefault = default1;

	        setLabelProvider(new LabelProvider(){
	            public String getText(Object element)
	            {
	                if(element instanceof String) {
	                	if (mData != null){
	                		for(int i=0; i<mData.length; i++) {
	                			if(mData[i].equals(element)) {
	                				return mDisplay[i];
	                			}
	                		}
	                	}
	                	if (mDisplay != null){
	                		return mDisplay[mDefault];
	                	}
	                }
	                return super.getText(element);
	            }
	        });
	    }

	    public CellEditor createPropertyEditor(Composite parent)
	    {
	        return new ComboBoxCellEditor(parent,mDisplay,SWT.READ_ONLY) {
	            protected Object doGetValue()
	            {
	                Integer i = (Integer)super.doGetValue();
	                return mData[i.intValue()];
	            }

	            protected void doSetValue(Object value)
	            {
	                int val = mDefault;
	                for(int i=0; i<mData.length; i++) {
	                    if(mData[i].equals(value)) {
	                        val = i;
	                        break;
	                    }
	                }
	                super.doSetValue(new Integer(val));
	            }
	        };
	    }

}
