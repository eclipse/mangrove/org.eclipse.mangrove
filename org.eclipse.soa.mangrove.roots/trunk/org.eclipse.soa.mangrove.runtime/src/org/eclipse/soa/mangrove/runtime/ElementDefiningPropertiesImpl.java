/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ElementDefiningPropertiesImpl implements IElementDefiningProperties {
	
	private String name = null;
	private String runtimeId = null;
	private String registeringBundleId = null;
	private String iconPath = null;
	
	
	private List<IProperty> properties;
	private Map<String, IProperty> propertyMap = null;
	
	

	public String getRuntimeId() {
		return runtimeId;
	}

	public void setRuntimeId(String runtimeId) {
		this.runtimeId = runtimeId;
	}
	
	public ElementDefiningPropertiesImpl(String name, String runtimeID) {
		super();
		this.name = name;
		this.propertyMap = new HashMap<String, IProperty>();
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {	
		return "Element With Properties ["+name+"]";
	}

	

	public List<IProperty> getDefinedProperties() {
		return properties;
	}

	public void setDefinedProperties(
			List<IProperty> properties) {
		this.properties = properties;
		this.propertyMap.clear();
		
		for (IProperty prop : properties){
			propertyMap.put(prop.getName(), prop);
		}
	}

	public IProperty getDefinedPropertyByName(
			String propertyName) {

		return propertyMap.get(propertyName);
	}

	public String getIconPath() {
		return this.iconPath;
	}

	public String getRegisteringBundleId() {
		return this.registeringBundleId;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;	
	}
	
	public void setRegisteringBundleId(String registeringBundleId) {
		this.registeringBundleId= registeringBundleId;
		
	}
	

  
}
