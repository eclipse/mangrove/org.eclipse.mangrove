/**

    Copyright 2007, 2009 Engineering Ingegneria Informatica S.p.A.

    This file is part of Spagic.

    Spagic is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    any later version.

    Spagic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
**/
package org.eclipse.soa.mangrove.runtime;

import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.soa.mangrove.BasicProperty;
import org.eclipse.soa.mangrove.ImFactory;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.Property;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.ui.actions.WorkspaceModifyOperation;


public class VersionImWorkspaceOperationAction extends WorkspaceModifyOperation {
	
	
	private IFile imFile = null;
	private String newVersion = null;
	
	public IFile getImFile() {
		return imFile;
	}

	public void setImFile(IFile imFile) {
		this.imFile = imFile;
	}


	public VersionImWorkspaceOperationAction(IFile imFile, String newVersion){
		this.imFile = imFile;
		this.newVersion = newVersion;
	}
	
	
	
	protected void execute(IProgressMonitor monitor)
    {	
		try{
			ResourceSet resourceSetIM = new ResourceSetImpl();
			ImFactory stpModelfactory = ImFactory.eINSTANCE;
			URI uri = URI.createPlatformResourceURI(imFile.getFullPath().toString(),true);
		
			// Force Package Registration
			ImPackage imPackage = ImPackage.eINSTANCE;
		
			// Load On Demand = true 
			Resource resourceIM = resourceSetIM.getResource(uri, true);
		
		
			StpIntermediateModel im = (StpIntermediateModel)resourceIM.getContents().get(0);

			for (org.eclipse.soa.mangrove.Process p : im.getProcessCollection().getProcesses()){
				Property propertyVersion = p.getProperties().get(ImConstants.PROCESS_VERSION);
				if (propertyVersion != null){
					((BasicProperty)propertyVersion).setValue(newVersion);
				}else{
					propertyVersion  = stpModelfactory.createBasicProperty();
					propertyVersion.setKey(ImConstants.PROCESS_VERSION);
					((BasicProperty)propertyVersion).setValue(newVersion);
					p.getProperties().put(ImConstants.PROCESS_VERSION, propertyVersion);
				}
			}
			resourceIM.save(Collections.EMPTY_MAP);
		}catch (Throwable e) {
			ImLogger.error("Exception Versioning IM ", e);
			throw new RuntimeException("Exception Versioning IM", e);
		}
		
		
    }
	
	
	

	public String getNewVersion() {
		return newVersion;
	}

	public void setNewVersion(String newVersion) {
		this.newVersion = newVersion;
	}	

	
	
}
