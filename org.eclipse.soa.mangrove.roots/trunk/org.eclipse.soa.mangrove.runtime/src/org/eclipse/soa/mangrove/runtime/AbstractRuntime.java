/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import java.io.File;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;
import org.eclipse.soa.mangrove.runtime.property.listeners.IPropertyListener;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;





public abstract class AbstractRuntime implements IRuntime {
	
	

	public String id = null;
	public String name = null;
	public String version=null; 
	public String nsprefix=null;
	public String nsuri=null;
	public Map<String, IServiceBinding> serviceBindings = null;
	public Map<String, IComboProvider> comboProviders = null;
	private String runtimeSpecific = null;
	
	
	private IProcessProperties processProperties = null;
	
	Map<String,String> runtimeSpecificForExtension = null;
	
	public List<IExpression> expressions = null;
	
	public List<IService> services = null;
	
	private Map<String,String> dnDMatchingProperties = null;
	
	private String bundleId = null;
	
	public AbstractRuntime(){
		super();
	}
	
	public AbstractRuntime(String bundleId){
		super();
		this.bundleId = bundleId;
	}
	
	public String getId() {
	
		return id;
	
	}
	
	public void setId(String id) {
	
		this.id = id;
	
	}
	public String getVersion() {
	
		return version;
	
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	
	}
	public String getNsprefix() {
		return nsprefix;
	}
	public void setNsprefix(String nsprefix) {
		this.nsprefix = nsprefix;
	}
	public String getNsuri() {
		return nsuri;
	}
	public void setNsuri(String nsuri) {
		this.nsuri = nsuri;
	}
	
	public Map<String, IServiceBinding>  getServiceBindings(){
		return this.serviceBindings;
	}
	
	
	public void init(File f){
		try{
			
			// Parse the XML as a W3C document.
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(f);
			initBaseProperties(document);
			initComboProviders(document);
			initProcessProperties(document);
			initBindings(document);
			initServices(document);
			initExpressions(document);
			initRuntimeSpecific(document);
			initDnDMatchingProperties(document);
		}catch(Exception ex){
			// -----------------
			// 
			// -----------------
			ex.printStackTrace();
		}
	}
	
	
	
	
	public void initRuntimeSpecific(Document document) throws Exception {
		String xPath ="/runtime/runtime-specific";
		Node node = selectSingleNode(document, xPath);
		if (node != null){
			this.runtimeSpecific = getStringFromNode(node);
		}else{
			StringBuffer sb = new StringBuffer();
			sb.append("<runtime-specific>\n");
			sb.append("</runtime-specific>\n");
			this.runtimeSpecific =  sb.toString();
		}
		
	}
	
	
	public void addRuntimeSpecificForExtension(String extensionId, String extensionRuntimeSpecific){
		if (runtimeSpecificForExtension == null){
			runtimeSpecificForExtension = new HashMap<String, String>();
		}
		runtimeSpecificForExtension.put(extensionId, extensionRuntimeSpecific);
	}
	
	/*
	public void mergeRuntimeSpecificSection(String toMerge) {
		try{
			Document originalDocument = getDocumentFromString(runtimeSpecific);
			Document toMergeDocument = getDocumentFromString(toMerge);
			NodeList nl = toMergeDocument.getChildNodes();
			
			for (int i=0; i < nl.getLength(); i++){
				Element tmp =(Element)nl.item(i);
				Node toCopy = originalDocument.importNode(tmp, true);
				originalDocument.getDocumentElement().appendChild(toCopy);
			}
			System.out.println(" ================== Old Runtime Specific ================== " );
			System.out.println(runtimeSpecific);
			this.runtimeSpecific = getStringFromNode(originalDocument.getDocumentElement());
			System.out.println(" ================== New Runtime Specific ================== " );
			System.out.println(runtimeSpecific);
		}catch (Exception e) {
			e.printStackTrace();
			
		}
	}

	
	private Document getDocumentFromString( String str ) throws Exception {
		DocumentBuilder builder =
			DocumentBuilderFactory.newInstance().newDocumentBuilder();
		org.xml.sax.InputSource inStream = new org.xml.sax.InputSource();
		 
		inStream.setCharacterStream(new java.io.StringReader(str));
	    Document doc = builder.parse(inStream);
	    return doc;
	}
	*/
	
	public String getRuntimeSpecific() {
		
		return this.runtimeSpecific;
	}

	public Map<String, String> getDnDMatchingProperties() {
		
		return this.dnDMatchingProperties;
	}
	
	

	private String getStringFromNode(Node node) throws Exception {
		OutputFormat format = new OutputFormat();
		String XML_HEADER_DEFAULT_ENCODING = "ISO-8859-1";
        format.setEncoding(XML_HEADER_DEFAULT_ENCODING);
        format.setIndenting(true);
        format.setIndent(3);
        format.setLineWidth(0);
        format.setLineSeparator("\n");
        format.setPreserveEmptyAttributes(true);
        StringWriter result = new StringWriter();
        
       
      
        XMLSerializer serializer = new XMLSerializer(result, format);         
            switch (node.getNodeType()) {
               case Node.DOCUMENT_NODE:               
                  serializer.serialize((Document) node);
                  break;
               case Node.ELEMENT_NODE:
                  serializer.serialize((Element) node);
                  break;
               case Node.DOCUMENT_FRAGMENT_NODE:
                  serializer.serialize((DocumentFragment) node);
                  break;
            }
        System.out.print(result.toString());
        return result.toString();
		
	}
	
	
	
	public void initBaseProperties(Document document) throws Exception {
		
		String xPath ="/runtime";
		Node node = selectSingleNode(document, xPath);
		
		this.name = getAttributeValue(node,"name");
		this.id =  getAttributeValue(node,"id");
		this.nsprefix = getAttributeValue(node,"nsprefix");
		this.nsuri = getAttributeValue(node,"nsuri");
		this.version = getAttributeValue(node,"version");
		
	}
	
	protected IServiceBinding createServiceBindingFromNode(Node aNode){
		IServiceBinding sb =  new ServiceBindingImpl(getAttributeValue(aNode,"name"), getId());
		sb.setRegisteringBundleId(this.bundleId);
		String icon = getAttributeValue(aNode,"icon");
		
		if ((icon != null) && (icon.trim().length() > 0))
			sb.setIconPath(icon);
		return sb;
		
	} // ---
		
	protected IExpression createExpressionFromNode(Node aNode){
		IExpression expr = new ExpressionImpl(getAttributeValue(aNode,"name"), getId());
		expr.setType(getAttributeValue(aNode,"type"));
		expr.setRegisteringBundleId(this.bundleId);
		String icon = getAttributeValue(aNode,"icon");
		
		if ((icon != null) && (icon.trim().length() > 0))
			expr.setIconPath(icon);
		return expr;
	} // ---
	
	
	public void initDnDMatchingProperties(Document document) throws Exception {
	
		String xPath ="/runtime/dnd-file-property-handler/match";
		NodeList nodes = selectNodes(document, xPath);
		
		
		
		this.dnDMatchingProperties = new HashMap<String, String>();
		
		// Process the elements in the nodelist
		Node n = null;
		for (int i=0; i<nodes.getLength(); i++) {
            // Get element
            n = (Node)nodes.item(i);
            dnDMatchingProperties.put(getAttributeValue(n, "extension"), getAttributeValue(n, "propertyName")); 
            

        }
		
		
		
		
	}
	public void initExpressions(Document document) throws Exception {
		String xPath ="/runtime/expressions/expression";
		NodeList nodes = selectNodes(document, xPath);
		
		
		
		this.expressions = new ArrayList<IExpression>();
		
		// Process the elements in the nodelist
		Node n = null;
		for (int i=0; i<nodes.getLength(); i++) {
            // Get element
            n = (Node)nodes.item(i);
            this.expressions.add(createExpressionFromNode(n));
        }
		
		
		
		for (IExpression expression : this.expressions){
			populateExpressionProperties( expression, document);
		}	
	}
	
	public void initServices(Document document) throws Exception {
		String xPath ="/runtime/im-services/im-service";
		NodeList nodes = selectNodes(document, xPath); 
		
		this.services = new ArrayList<IService>();
		IService aService = null;
		String name = null;
		String icon = null;
		Node n = null;
		for (int i=0; i<nodes.getLength(); i++) {
			n = nodes.item(i);
			name = getAttributeValue(n, "name");
			icon = getAttributeValue(n, "icon");
			aService = new ServiceImpl(name, "Description for "+name, getAttributeValue(n, "bindings"));
			aService.setEmfServiceEntity(getAttributeValue(n, "emf-service-entity"));
			aService.setRegisteringBundleId(this.bundleId);
			if ( (icon !=  null) && (icon.trim().length() > 0) )
				aService.setIconPath(icon);
			this.services.add(aService);
		} 
	
	}
	
	
	public void initProcessProperties(Document document) throws Exception {
		
		this.processProperties =  new ProcessPropertiesImpl(getId());
		processProperties.setRegisteringBundleId(this.bundleId);
		
		String xPathProp ="/runtime/process-properties/property";
		List<IProperty> serviceBindingProperties = selectProperties(document, xPathProp);
		processProperties.setDefinedProperties(serviceBindingProperties);
		
	}
	
	public void initBindings(Document document) throws Exception {
		

		
		String xPath ="/runtime/service-bindings/service-binding";
		NodeList nodes = selectNodes(document, xPath); 
		
		
		this.serviceBindings = new HashMap<String, IServiceBinding>();
		Node n = null;
		for (int i=0; i<nodes.getLength(); i++) {
			n = nodes.item(i);
			this.serviceBindings.put(getAttributeValue(n,"name"), createServiceBindingFromNode(n));
		} // --
		
		for (IServiceBinding sb : this.serviceBindings.values()){
			populateServiceBindingProperties( sb, document);
		}
	}

	protected void initComboProviders(Document document) throws Exception {
		this.comboProviders = new HashMap<String, IComboProvider>();
		String xPath ="/runtime/combo-providers/combo-provider";
		NodeList nodes = selectNodes(document, xPath); 
		
		Map initParamsForComboProvider = null;
		String comboProviderName = null;
		String comboProviderType = null;
		
		Class providerClass = null;
		Constructor constructor = null;
		Class[] constructorParameterTypes = new Class[1];
		constructorParameterTypes[0] = Map.class;
		Map parametersOfThisCombo = null;
		IComboProvider comboProvider = null;
		
		Node n = null;
		for (int i=0; i<nodes.getLength(); i++) {
			n = nodes.item(i);
			comboProviderName = getAttributeValue(n,"name");
			comboProviderType = getAttributeValue(n,"type");
			
			NodeList nodesOfComboProviderParameters = selectNodes(document, "/runtime/combo-providers/combo-provider[@name='"+comboProviderName+"']/combo-provider-parameter");
			parametersOfThisCombo = new HashMap();
			Node n2 = null;
			for (int j=0; j < nodesOfComboProviderParameters.getLength(); j++) {
				n2 = nodesOfComboProviderParameters.item(j);
				parametersOfThisCombo.put(getAttributeValue(n2,"name"), getAttributeValue(n2,"value"));
			}
			try {
//				System.out.println(" Instantiating Combo Provider ["+comboProviderType+"]");
				providerClass = Class.forName(comboProviderType);
				constructor = providerClass.getConstructor(constructorParameterTypes);
				comboProvider = (IComboProvider)constructor.newInstance(parametersOfThisCombo);
				this.comboProviders.put(comboProviderName, comboProvider);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public IComboProvider getNamedComboProvider(String name){
		
		return this.comboProviders.get(name);
	
	}
	
	protected List<IProperty> selectProperties(Document document, String xPath) throws Exception {
		NodeList nodes = selectNodes(document, xPath); 
		IProperty aProperty = null;
		List<IProperty>  propertyConfigs = new ArrayList<IProperty>();
		
		Node n = null;
		for (int i=0; i<nodes.getLength(); i++) {
			n = nodes.item(i);
			
			aProperty = new PropertyImpl();
			aProperty.setLabel(getAttributeValue(n,"label")); 
			aProperty.setName(getAttributeValue(n,"name")); 
			aProperty.setDefaultValue(getAttributeValue(n,"defaultValue"));
			
			aProperty.setRequired(getAttributeValue(n,"required"));
			String visibleCondition = (getAttributeValue(n,"visibleCondition"));
			if (visibleCondition != null && visibleCondition.trim().length() > 0){
				aProperty.setVisibleUnderCondition(true);
				aProperty.setVisibleCondition(visibleCondition);
		    }else{
		    	aProperty.setVisibleUnderCondition(false);
		    }
			
			String uiCategory = (getAttributeValue(n,"uicategory"));
			if (uiCategory != null && uiCategory.trim().length() > 0){
				aProperty.setUiCategoratory(uiCategory);
			}
			aProperty.setEditable(Boolean.valueOf(getAttributeValue(n,"editable")));
			aProperty.setPropertyEditor(getAttributeValue(n,"propertyEditor"));
			aProperty.setMapKey(getAttributeValue(n,"mapKey"));
			aProperty.setMapFields(getAttributeValue(n,"mapFields"));
			aProperty.setMapFieldsEditors(getAttributeValue(n,"mapFieldsEditors"));
			aProperty.setDependentProperty(getAttributeValue(n,"dependentProperty"));
			
			if (aProperty.isCombo()){
				String comboName = getAttributeValue(n,"comboProvider");
				aProperty.setComboProviderName(comboName);
				IComboProvider comboProvider = getNamedComboProvider(getAttributeValue(n,"comboProvider"));
				if (comboProvider != null){
//					System.out.println("Setting ["+comboProvider.getClass().getName()+"] ComboProvider on Property ["+aProperty.getName()+"]");
					aProperty.setComboProvider(comboProvider);
				}	
			} // -- if (aProperty.isCombo()) 
			
			String modifyListener = getAttributeValue(n,"modifyListener");
			if ((modifyListener != null) && (modifyListener.trim().length() > 0)){
				try{
//					System.out.println("Setting listener ["+modifyListener+"] on Property ["+aProperty.getName()+"]");
					IPropertyListener propertyListener =(IPropertyListener)Class.forName(modifyListener).newInstance();
					aProperty.setPropertyListener(propertyListener);
				}catch(Exception e){
					ImLogger.error(ImRuntimeActivator.PLUGIN_ID, e.getMessage(), e);
				}

			}
			
			propertyConfigs.add(aProperty);
		}
		return propertyConfigs;
	}
	
	protected void populateServiceBindingProperties(IServiceBinding sb, Document document) throws Exception {
		
		String xPath ="/runtime/service-bindings/service-binding[@name='"+sb.getName()+"']/property";
		List<IProperty> serviceBindingProperties = selectProperties(document, xPath);
		sb.setDefinedProperties(serviceBindingProperties);
	
	}
	
	protected void populateExpressionProperties(IExpression expression, Document document) throws Exception {
	
		String xPath ="/runtime/expressions/expression[@name='"+expression.getName()+"']/property";
		List<IProperty> expressionProperties = selectProperties(document,xPath);
		expression.setDefinedProperties(expressionProperties);
	
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<IService> getServices() {
		
		return services;
	
	}
	
	public List<IExpression> getExpressions(){
		return this.expressions;
	}

	public IExpression getExpressionWithName(String expressionName) {
		for (IExpression expr : this.expressions){
			if (expr.getName().equalsIgnoreCase(expressionName))
				return expr;
		}
		return null;
	}
	
	private static Node selectSingleNode(Node document, String xPath) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
        return (Node)xpath.evaluate(xPath, document, XPathConstants.NODE);
	}
	
	private static NodeList selectNodes(Node document, String xPath) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
        return (NodeList)xpath.evaluate(xPath, document, XPathConstants.NODESET);
	}
	
	private static String getAttributeValue(Node n, String attributeName){
		try{
			Node attributeNode = n.getAttributes().getNamedItem(attributeName); 
			return (attributeNode != null ? attributeNode.getNodeValue() : "");
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public String getExtendedRuntimeId() {
		return null;
	}

	public boolean isRuntimeExtension() {

		return false;
	}

	public Map<String, String> getRuntimeSpecificForExtension() {
		return runtimeSpecificForExtension;
	}

	public void setRuntimeSpecificForExtension(
			Map<String, String> runtimeSpecificForExtension) {
		this.runtimeSpecificForExtension = runtimeSpecificForExtension;
	}
	public Map<String, IComboProvider> getComboProviders(){
		return this.comboProviders;
	}

	public IProcessProperties getProcessProperties() {
		// TODO Auto-generated method stub
		return this.processProperties;
	}

}
	
