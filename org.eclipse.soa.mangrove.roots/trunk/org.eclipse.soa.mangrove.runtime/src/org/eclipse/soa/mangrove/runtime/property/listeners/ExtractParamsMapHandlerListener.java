/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.property.listeners;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IProperty;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.runtime.comboproviders.ComboEntries;
import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;

public class ExtractParamsMapHandlerListener implements IPropertyListener {
	private String matchRegexp = "\\$\\w+";
	public static String PLACE_HOLDER_SIGN = "$".intern();
	
	public void handleUpdate(String sourcePropertyValue, String dependentPropertyName, IProperty destinationServiceBindingProperty, EAnnotation ea ) {
		
		// 
		// In this implementation the property that has changes is a string 
		// where parameters are identified by string line $paramName
		//
		String runtimeId = ea.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
		IRuntime imRuntime = ImRuntimeActivator.getRuntime(runtimeId);
		List<String> parametersList = getStringParameterNames(sourcePropertyValue);
		
		//Collections.reverse(parametersList);

		if (parametersList.size() >= 0){
			//System.out.println(" Cannot handle a number of value less then 0");
			String basePropertyName = null;
			for (String aParamName : parametersList) {
				basePropertyName = dependentPropertyName + "["+aParamName+"]";
				String propertyToFoundInEAnnotation = basePropertyName+"."+destinationServiceBindingProperty.getMapFieldList().get(0);
				
				// -------------------------------------------------------------------------------
				// If the eAnnotation contains already just the value
				// keep it
				// -------------------------------------------------------------------------------
				
				if (ea.getDetails().get(propertyToFoundInEAnnotation) == null){
					String destPropertyNameInAnnotation = null;	
					
					//--------------------------------------------------------------------------------------
					// Write a "Map Entry" in the eAnnotation
					// -------------------------------------------------------------------------------------
					
					Map<String, String> fieldEditorsMap = destinationServiceBindingProperty.getMapFieldsEditorsMap();
					
					String fieldEditor = null;  
					for (String propOfMap : destinationServiceBindingProperty.getMapFieldList()) {
					
						fieldEditor = fieldEditorsMap.get(propOfMap);
						String defaultValue = null;
						if (fieldEditor != null && fieldEditor.startsWith("combo")){
							String comboName = fieldEditor.substring(fieldEditor.indexOf("(") + 1, fieldEditor.indexOf(")"));
							IComboProvider comboProvider = imRuntime.getNamedComboProvider(comboName);
							ComboEntries ce = comboProvider.getComboEntries();
							defaultValue = ce.getValues()[0];
						}
						destPropertyNameInAnnotation = basePropertyName + "." + propOfMap;
						if (propOfMap.equalsIgnoreCase(destinationServiceBindingProperty.getMapKey()))
							ea.getDetails().put(destPropertyNameInAnnotation, aParamName);	
						else		
							ea.getDetails().put(destPropertyNameInAnnotation, (defaultValue != null ? defaultValue : ""));
					
					}
				}else{
					// -----------
					// -----------
				}
			}
			
			
			// ------------------------------------------------------------------------------
			//
			// Se ho rimosso delle property Devo Gestirlo
			// 
			// ------------------------------------------------------------------------------
			
			String parName = null;

			List<String> keyToRemove = new ArrayList<String>();
			
			for (String s : ea.getDetails().keySet() ){
				
				if (s.startsWith(dependentPropertyName)){
					parName = s.substring(s.indexOf("[") +1, s.indexOf("]"));
					
					
					if (!parametersList.contains(parName))
						keyToRemove.add(s);
			
				} // -- end if (s.startsWith(dependentPropertyName))
			
			} // -- end for (String s : ea.getDetails().keySet() )
			
			for ( String s : keyToRemove){
			
				ea.getDetails().removeKey(s);
			
			}
			
		}// end if (changedValueInt > 0)
	}
	
	public List<String> getStringParameterNames(String propertyValue) {
        Pattern pattern = Pattern.compile(matchRegexp);
        Matcher matcher = 
        	pattern.matcher(propertyValue);
        String placeHolder;
        List<String> toReturn = new ArrayList<String>();
		for (int idx = 1; matcher.find(); idx++) {
			placeHolder = matcher.group();
			placeHolder = placeHolder.substring(
				placeHolder.indexOf(PLACE_HOLDER_SIGN)+1);
			toReturn.add(placeHolder);
		}
		return toReturn;
	}
}
