/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.soa.mangrove.util.ImLogger;

public class RuntimeDescriptorProxy {

	private static final String ATT_ID = "id";
	private static final String ATT_NAME = "name";
	private static final String ATT_CLASS = "class";
	
	
	private static final String ATT_IS_RUNTIME_EXTENSION = "isRuntimeExtension";
	private static final String ATT_ID_EXTENDED_RUNTIME = "idExtendedRuntime";


	private final IConfigurationElement configElement;

	private final String id;
	private final String name;
	private final String isRuntimeExtension;
	private final String idExtendedRuntime;

	public RuntimeDescriptorProxy(IConfigurationElement configElement) {
		this.configElement = configElement;

		id = getAttribute(configElement, ATT_ID, null);
		name = getAttribute(configElement, ATT_NAME, id);
		
		isRuntimeExtension = getAttribute(configElement, ATT_IS_RUNTIME_EXTENSION, "false");
		idExtendedRuntime = getAttribute(configElement, ATT_ID_EXTENDED_RUNTIME, "none");
		
		// Make sure that class is defined,
		// but don't load it
		getAttribute(configElement, ATT_CLASS, null);
		
	}

	private static String getAttribute(IConfigurationElement configElem,
			String name, String defaultValue) {
		String value = configElem.getAttribute(name);
		if (value != null)
			return value;
		if (defaultValue != null)
			return defaultValue;
		throw new IllegalArgumentException("Missing " + name + " attribute");
	}
	
	public IRuntime getRuntime(){
		try{
			IRuntime runtime = (IRuntime) configElement.createExecutableExtension(ATT_CLASS);
			return runtime;
		}catch (Exception e) {
			ImLogger.error(ImRuntimeActivator.PLUGIN_ID, e.getMessage(), e);
			return null;
		}
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getIsRuntimeExtension() {
		return isRuntimeExtension;
	}

	public String getIdExtendedRuntime() {
		return idExtendedRuntime;
	}

}
