 /*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.property.listeners;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IProperty;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.IServiceBinding;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;

public class SwitchCreateDeleteDependentPropertiesListener implements IPropertyListener {

	public void handleUpdate(String sourcePropertyValue, String dependentPropertyName, IProperty destinationServiceBindingProperty, EAnnotation ea ) {
		// 
		// In this implementation if the sourcePropertyValue is true the dependent properties will be
		// created otherwise it will be deleted
		//
		
		Boolean booleanSourcePropertyValue = Boolean.valueOf(sourcePropertyValue);
		EMap<String, String> details = ea.getDetails();
		String runtimeID = details.get(ImConstants.IM_POOL_RUNTIME_ID);
		String serviceName = details.get(ImConstants.IM_SERVICE_NAME);
		String serviceBindingName = details.get(ImConstants.IM_SERVICE_BINDING_NAME);

		
		IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeID);
		IServiceBinding serviceBinding = runtime.getServiceBindings().get(serviceBindingName);
			
		String[] splitParts = dependentPropertyName.split(";");
		String dependentPropertiesWhenIsTrueString = null;
		String dependentPropertiesWhenIsFalseString = null;
		String propPart = null;
		for (String s : splitParts){
			propPart = s.substring(s.indexOf("(") + 1, s.indexOf(")"));
			if (s.startsWith("if-is-true("))
				dependentPropertiesWhenIsTrueString = propPart;
			else if (s.startsWith("if-is-false"))
				dependentPropertiesWhenIsFalseString = propPart;
		}
		
		String[] dependentPropertiesWhenIsTrue = (dependentPropertiesWhenIsTrueString != null ? dependentPropertiesWhenIsTrueString.split(",") : new String[0]);
		String[] dependentPropertiesWhenIsFalse = (dependentPropertiesWhenIsFalseString != null ? dependentPropertiesWhenIsFalseString.split(",") : new String[0]);
		if (booleanSourcePropertyValue){
			IProperty aProperty = null;
			for (String s : dependentPropertiesWhenIsTrue){
				s = s.trim();
				aProperty = serviceBinding.getDefinedPropertyByName(s);
				if (aProperty != null){
					String value = (aProperty.getDefaultValue() != null ? aProperty.getDefaultValue() : ""); 
					details.put(aProperty.getName(),value);
				}else{
//					System.out.println("ServiceBinding ["+serviceBindingName+"] does not define a property called ["+s+"]");
				}
			}
			for (String s : dependentPropertiesWhenIsFalse){
				s = s.trim();
				details.remove(s);
			}
		}else{
			IProperty aProperty = null;
			for (String s : dependentPropertiesWhenIsTrue){
				s = s.trim();
				details.remove(s);
			}
			for (String s : dependentPropertiesWhenIsFalse){
				s = s.trim();
				aProperty = serviceBinding.getDefinedPropertyByName(s);
				String value = (aProperty.getDefaultValue() != null ? aProperty.getDefaultValue() : ""); 
				details.put(aProperty.getName(),value);
			}
		}
		
		
	}
}
