/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;
import org.eclipse.soa.mangrove.util.ImLogger;

import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class ImRuntimeActivator extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.soa.mangrove.runtime";
	public static final String RUNTIME_DESCRIPTOR_TAG = "runtime-descriptor";
	public static final String COMBO_PROVIDER_VALUES_TAG = "combo-values-provider";

	
	private static Map<String, RuntimeDescriptorProxy> runtimeDescriptorProxies = null;
	private static Map<String, String> runtimeNameToId  = null;
	private static List<String> runtimeNames = null;
	private static Map<String, IRuntime> instantiatedRuntime = null;
	
	
	private static Map<String, ComboProviderValuesProxy> comboProviderValuesProxies = null;
	private static Map<String, IComboProviderValues> instantiatedComboProvidersValues = null;
	
	
	// The shared instance
	private static ImRuntimeActivator plugin;
	
	/**
	 * The constructor
	 */
	public ImRuntimeActivator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ImRuntimeActivator getDefault() {
		return plugin;
	}
	
	

	public static void initRuntimeProxies() {
		if ((runtimeDescriptorProxies != null) 
				&& (runtimeNames != null) && 
				(runtimeNameToId != null)) {
			return;
		}
		try{
			IExtensionRegistry extensionPointRegistry   = Platform.getExtensionRegistry ();
			IExtensionPoint    extensionPoint = extensionPointRegistry.getExtensionPoint (PLUGIN_ID, "runtimedescriptor");

			IExtension[] extensions = extensionPoint.getExtensions();
			
			

			runtimeDescriptorProxies = new HashMap<String, RuntimeDescriptorProxy>();
			runtimeNames = new ArrayList<String>();
			runtimeNameToId = new HashMap<String, String>();
			instantiatedRuntime = new HashMap<String, IRuntime>();

			
			for (int i = 0; i < extensions.length; i++) {
				IConfigurationElement[] configElements = extensions[i]
					.getConfigurationElements();
				for (int j = 0; j < configElements.length; j++) {
					RuntimeDescriptorProxy proxy = parseRuntimeDescriptorProxy(
						configElements[j]);
					if (proxy != null){
						runtimeDescriptorProxies.put(proxy.getId(), proxy);
						runtimeNames.add(proxy.getName());
						runtimeNameToId.put(proxy.getName(), proxy.getId());
					}
				}
			}
			return;
		}catch (Throwable t) {
			ImLogger.error(ImRuntimeActivator.PLUGIN_ID, t.getMessage(), t);
			runtimeDescriptorProxies = null;
			runtimeNames = null;
			runtimeNameToId = null;
		}
	}
	
	
	public static void initComboProviderValuesProxies() {
		if (comboProviderValuesProxies != null) 
			return;
		
		try{
			IExtensionRegistry extensionPointRegistry   = Platform.getExtensionRegistry();
			IExtensionPoint    extensionPoint = extensionPointRegistry.getExtensionPoint (PLUGIN_ID, "combovaluesprovider");

			IExtension[] extensions = extensionPoint.getExtensions();
			
			comboProviderValuesProxies = new HashMap<String, ComboProviderValuesProxy>();
			instantiatedComboProvidersValues = new HashMap<String, IComboProviderValues>();

			
			for (int i = 0; i < extensions.length; i++) {
				IConfigurationElement[] configElements = extensions[i]
					.getConfigurationElements();
				for (int j = 0; j < configElements.length; j++) {
					ComboProviderValuesProxy proxy = parseComboProviderValuesProxies(
						configElements[j]);
					if (proxy != null){
						comboProviderValuesProxies.put(proxy.getName(), proxy);
					}
				}
			}
			return;
		}catch (Throwable t) {
			ImLogger.error(ImRuntimeActivator.PLUGIN_ID, t.getMessage(), t);
			comboProviderValuesProxies = null;
			instantiatedComboProvidersValues = null;
		}
	}
	
	private static ComboProviderValuesProxy parseComboProviderValuesProxies(IConfigurationElement configElement) {
		if (!configElement.getName().equals(COMBO_PROVIDER_VALUES_TAG))
			return null;
		try {
			return new ComboProviderValuesProxy(configElement);
		}
		catch (Exception e) {
			ImLogger.error(ImRuntimeActivator.PLUGIN_ID, e.getMessage(), e);
			return null;
			
		}
	}
	
	private static RuntimeDescriptorProxy parseRuntimeDescriptorProxy(IConfigurationElement configElement) {
		if (!configElement.getName().equals(RUNTIME_DESCRIPTOR_TAG))
			return null;
		try {
			return new RuntimeDescriptorProxy(configElement);
		}
		catch (Exception e) {
			ImLogger.error(ImRuntimeActivator.PLUGIN_ID, e.getMessage(), e);
			return null;
			
		}
	}
	
	public static IRuntime getRuntime(String runtimeId){
		initRuntimeProxies();
		IRuntime runtime = instantiatedRuntime.get(runtimeId);
		if (runtime == null){
			RuntimeDescriptorProxy proxy = runtimeDescriptorProxies.get(runtimeId);
			runtime = proxy.getRuntime();
			
			IRuntime runtimeExtension = null;
			for (RuntimeDescriptorProxy rdProxy : runtimeDescriptorProxies.values()){
				  if ((Boolean.valueOf(rdProxy.getIsRuntimeExtension())) 
					 && (rdProxy.getIdExtendedRuntime().equalsIgnoreCase(runtimeId))){
					 
					    runtimeExtension = instantiatedRuntime.get(rdProxy.getId());
					    if (runtimeExtension == null){
					    	runtimeExtension = rdProxy.getRuntime();
					    	instantiatedRuntime.put(rdProxy.getId(), runtimeExtension);
					    }
					    // copy service bindings 
					    for (IServiceBinding aServiceBinding : runtimeExtension.getServiceBindings().values())
					    	runtime.getServiceBindings().put(aServiceBinding.getName(), aServiceBinding);
					    
					    // copy services
					    for (IService aService : runtimeExtension.getServices())
					    	runtime.getServices().add(aService);
					    
					    // copy expressions
					    for (IExpression aExpression : runtimeExtension.getExpressions())
					    	runtime.getExpressions().add(aExpression);
					   
					    // copy combo providers
					    for (String comboProviderName : runtimeExtension.getComboProviders().keySet()){
					    	runtime.getComboProviders().put(comboProviderName, runtimeExtension.getComboProviders().get(comboProviderName));
					    }
						
					    for (String runtimeDndPropertyKey : runtimeExtension.getDnDMatchingProperties().keySet()){
					    	runtime.getDnDMatchingProperties().put(runtimeDndPropertyKey, runtimeExtension.getDnDMatchingProperties().get(runtimeDndPropertyKey));
					    }
					    
					    runtime.addRuntimeSpecificForExtension(rdProxy.getId(), runtimeExtension.getRuntimeSpecific()); 
					
				  }
			}
			instantiatedRuntime.put(runtimeId, runtime);
		}
		return runtime;
	}

	/**
	 * 
	 * @param comboName
	 * @return Get all IComboProviderValues that match the combo named "comboName"
	 */
	public static List<IComboProviderValues> getExternalPluginsComboProviderValues(String comboName){
		initComboProviderValuesProxies();
		ComboProviderValuesProxy comboProviderValuesProxy = null;
		List<IComboProviderValues> foundComboProviderValues = new ArrayList<IComboProviderValues>();;
		IComboProviderValues aComboProviderValues = null;
		for (String name : comboProviderValuesProxies.keySet()){
			comboProviderValuesProxy = comboProviderValuesProxies.get(name);
			// If this combo provider target this combo
			if (comboProviderValuesProxy.getTargetCombo().equalsIgnoreCase(comboName)){
				aComboProviderValues = instantiatedComboProvidersValues.get(name);
				if (aComboProviderValues == null){
					aComboProviderValues = comboProviderValuesProxy.getComboProviderValue();
					instantiatedComboProvidersValues.put(name, aComboProviderValues);
				}
				foundComboProviderValues.add(aComboProviderValues);
			}
		}
		return foundComboProviderValues;
	}
	
	public static String getIdForRuntimeName(String runtimeName){
		initRuntimeProxies();
		return runtimeNameToId.get(runtimeName);
	}
	
	
	public static String[] getRuntimeNames(){
		initRuntimeProxies();
		String[] resultingNames = null;
		List <String> arrRuntimeNames = new ArrayList<String>();
		
		
		String runtimeName = null;
		String runtimeId = null;
		RuntimeDescriptorProxy runtimeProxy = null;
		for (int i = 0; i < runtimeNames.size(); i++ ){
			  runtimeName = runtimeNames.get(i);
			  runtimeId = runtimeNameToId.get(runtimeName);
			  runtimeProxy = runtimeDescriptorProxies.get(runtimeId);
			  if (! Boolean.valueOf(runtimeProxy.getIsRuntimeExtension())){
				  arrRuntimeNames.add(runtimeName);
			  }else{
//				  System.out.println(" Runtime ID: ["+runtimeId+"] Name: ["+runtimeName+"]");
			  }
		}
		resultingNames = new String[arrRuntimeNames.size()];
		resultingNames =  arrRuntimeNames.toArray(resultingNames);
		return resultingNames;
	}
}
