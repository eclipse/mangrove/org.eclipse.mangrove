/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;
import org.eclipse.soa.mangrove.runtime.property.listeners.IPropertyListener;



public class PropertyImpl implements IProperty {
	
	private String label = null; 
	private String name = null; 
	private String required = "false";
	private String defaultValue = null; 
	private boolean editable = true;
	private String propertyEditor = null;
	private String mapKey = null;
	private String mapFields = null;
	private IComboProvider comboProvider = null;
	private IPropertyListener propertyListener = null;
	private String dependentProperty = null;
	private String uiCategoratory = null;
	private String comboProviderName = null;

	private boolean visibleUnderCondition = false;
	private String visibleCondition = null;
	
	private String mapFieldsEditors = null;
	private Map<String,String> mapFieldsEditorsMap = null;
	
	public PropertyImpl(){
		super();
	}
	
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public String getPropertyEditor() {
		return propertyEditor;
	}
	public void setPropertyEditor(String propertyEditor) {
		this.propertyEditor = propertyEditor;
	}


	public boolean isRequired() {
		return required.equalsIgnoreCase("true");
	}


	public void setRequired(String required) {
		this.required = required;
	}


	public boolean isEditable() {
		return editable;
	}


	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	


	public boolean isCombo() {
		return this.propertyEditor.equalsIgnoreCase("combo");
	}


	public boolean isText() {
		// TODO Auto-generated method stub
		return this.propertyEditor.equalsIgnoreCase("text");
	}


	public List<String> getMapFieldList() {
		
		if ( isMap() && this.mapFields != null ){
			List<String> mapFieldList = new ArrayList<String>();
			
			String[] fieldsArray = mapFields.split(",");
			
			for (String field : fieldsArray){
				mapFieldList.add(field.trim());
			}
			
			return mapFieldList;
			
		}else{
			
			return null;
		
		}
	}


	public boolean isMap() {
		return this.propertyEditor.equalsIgnoreCase("map");
	}


	public String getMapKey() {
		return mapKey;
	}


	public void setMapKey(String mapKey) {
		this.mapKey = mapKey;
	}


	public String getMapFields() {
		return mapFields;
	}


	public void setMapFields(String mapFields) {
		this.mapFields = mapFields;
	}


	public IPropertyListener getPropertyListener() {
		
		return this.propertyListener;
	}

	public IComboProvider getComboProvider(){
		return comboProvider;
	}

	public void setComboProvider(IComboProvider provider) {
		this.comboProvider = provider;
		
	}


	public void setPropertyListener(IPropertyListener propertyListener) {
		this.propertyListener = propertyListener;
		
	}


	public String getDependentProperty() {
		return dependentProperty;
	}


	public void setDependentProperty(String dependentProperty) {
		this.dependentProperty = dependentProperty;
	}


	


	
	public boolean isVisibleUnderCondition() {
		return visibleUnderCondition;
	}


	public void setVisibleUnderCondition(boolean visibleUnderCondition) {
		this.visibleUnderCondition = visibleUnderCondition;
	}


	public String getVisibleCondition() {
		return visibleCondition;
	}


	public void setVisibleCondition(String visibleCondition) {
		this.visibleCondition = visibleCondition;
	}

	public void setMapFieldsEditors(String mapFieldsEditors){
		this.mapFieldsEditors = mapFieldsEditors;
		if (isMap())
		loadFieldEditorMap();
	}
	
	public String getMapFieldsEditors(){
		return this.mapFieldsEditors;
	}
	
	// <mapField, mapFieldEditor>  Key -> one specified in mapField attribute
	public Map<String, String> getMapFieldsEditorsMap(){
		return this.mapFieldsEditorsMap;
	}
	
	protected void loadFieldEditorMap(){
		
		if (mapFieldsEditors != null && mapFieldsEditors.trim().length() > 0){
			this.mapFieldsEditorsMap = new HashMap<String, String>();
			String[] arrayEditorsMap = mapFieldsEditors.split(",");
		
			String[] parts = null;

			for (String entry : arrayEditorsMap){
				entry= entry.trim();
				parts = entry.split("=");
				mapFieldsEditorsMap.put(parts[0], parts[1]);

			}
		}
	}


	public String getUiCategoratory() {
		return uiCategoratory;
	}


	public void setUiCategoratory(String uiCategoratory) {
		this.uiCategoratory = uiCategoratory;
	}


	public String getComboProviderName() {
		return comboProviderName;
	}


	public void setComboProviderName(String comboProviderName) {
		this.comboProviderName = comboProviderName;
	}
	
	


	


	
	
	
	
	
	
	
}
