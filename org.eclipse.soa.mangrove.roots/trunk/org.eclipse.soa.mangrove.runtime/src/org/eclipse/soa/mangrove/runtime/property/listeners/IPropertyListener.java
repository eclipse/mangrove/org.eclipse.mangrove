/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.property.listeners;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.soa.mangrove.runtime.IProperty;

public interface IPropertyListener {
	
	public void handleUpdate(String changedValue, String dependentPropertyName, IProperty property, EAnnotation ea);
}
