/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.comboproviders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.soa.mangrove.resources.IImResource;
import org.eclipse.soa.mangrove.resources.ImResourcesActivator;
import org.eclipse.soa.mangrove.resources.datasources.DataSourceImResource;



public class ImResourceDataSourcesComboProvider extends AbstractComboProvider {
	
	
	
	public ImResourceDataSourcesComboProvider(Map<String, String> params){
		
		this.params = params;
		
	}

	public ComboEntries getComboEntries() {
		ComboEntries ce = new ComboEntries();
		
		List<IImResource> resourcesList =  ImResourcesActivator.getImResourcesWithType(DataSourceImResource.DATA_SOURCE_IM_RESOURCE_TYPE);
		List<String> rsList = new ArrayList<String>();
		for (IImResource res : resourcesList){
			rsList.add(res.getId());
		}
		
		String [] arr = new String[rsList.size()];
		arr = rsList.toArray(arr);
		ce.setLabels(arr);
		ce.setValues(arr);
		return ce;
	}

	public String getConfigurationParameterValue(String parameterName) {
		// The StaticComboProvider have not parameters
		return "";
	}
	
	
	
}
