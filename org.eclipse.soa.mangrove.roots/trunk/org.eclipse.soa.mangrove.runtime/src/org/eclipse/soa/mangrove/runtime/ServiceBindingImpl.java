/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ServiceBindingImpl extends ElementDefiningPropertiesImpl implements IServiceBinding {
	
	
	
	public ServiceBindingImpl(String name, String runtimeId) {
		super(name, runtimeId);
	
	}

	

	@Override
	public String toString() {	
		return "Binding["+getName()+"]";
	}



	

	
	

	

	

  
}
