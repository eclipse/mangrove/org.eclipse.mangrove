/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.comboproviders;

import java.util.Map;

public class StaticComboProvider extends AbstractComboProvider {
	
	private ComboEntries staticValues = null;
	
	public StaticComboProvider(Map<String, String> params){
		
		this.params = params;
		
		String comboValues = (String)params.get("comboValues");
		String[] tokens = comboValues.split(";");
		
		for (int i=0; i < tokens.length; i++){
			tokens[i] = tokens[i].trim();
		}
		this.staticValues = new ComboEntries();
		this.staticValues.setLabels(tokens);
		this.staticValues.setValues(tokens);
		
	}

	public ComboEntries getComboEntries() {
	
		return staticValues;
	}

	public String getConfigurationParameterValue(String parameterName) {
		// The StaticComboProvider have not parameters
		return "";
	}
	
	
}
