/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;

public interface IRuntime {

	public String getId();

	public void setId(String id);

	public String getVersion();

	public void setVersion(String version);

	public String getNsprefix();

	public void setNsprefix(String nsprefix);

	public String getNsuri();

	public void setNsuri(String nsuri);
	
	public List<IService> getServices();
	
	public Map<String, IServiceBinding> getServiceBindings();

	public List<IExpression> getExpressions();
	
	public void init(File f);

	public IComboProvider getNamedComboProvider(String name);

	public String getName();

	public void setName(String name);

	public IExpression getExpressionWithName(String expressionName);
	
	public String getRuntimeSpecific();
	
	// Get the list of properties that could be filled with a DnD
	public Map<String, String> getDnDMatchingProperties();
	
	public IProcessProperties getProcessProperties();
	
	/**
	 * 
	 * @return false if this is defining an "Im Runtime"
	 * @return true if this is defining an extension to an existing runtime
	 */
	public boolean isRuntimeExtension();
	
	/**
	 * 
	 * @return the id of extended runtime, if this is defining a runtime extension, null otherwise
	 */
	public String getExtendedRuntimeId();
	
	
	public void addRuntimeSpecificForExtension(String extensionId, String extensionRuntimeSpecific);
	
	public Map<String, String> getRuntimeSpecificForExtension();
	
	public void setRuntimeSpecificForExtension(Map<String, String> runtimeSpecificForExtension);
	
	public Map<String, IComboProvider> getComboProviders();

	
}
