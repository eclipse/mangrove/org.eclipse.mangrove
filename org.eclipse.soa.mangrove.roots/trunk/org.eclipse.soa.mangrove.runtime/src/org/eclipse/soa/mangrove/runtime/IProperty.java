/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime;

import java.util.List;
import java.util.Map;

import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;
import org.eclipse.soa.mangrove.runtime.property.listeners.IPropertyListener;


public interface IProperty {
	
	public String getUiCategoratory();
	
	public void setUiCategoratory(String uiCategoratory);
	
	public String getLabel();

	public void setLabel(String label);

	public String getName();

	public void setName(String name);	

	public String getDefaultValue();

	public void setDefaultValue(String defaultValue);

	public String getPropertyEditor();

	public void setPropertyEditor(String propertyEditor);
	
	public boolean isRequired();

	public void setRequired(String required);

	public boolean isEditable();

	public void setEditable(boolean editable);
	
	public boolean isCombo();
	
	public boolean isText();
	
	public boolean isMap();
	
	public IComboProvider getComboProvider();
	
	public void setComboProvider(IComboProvider provider);
	
	public IPropertyListener getPropertyListener();
	
	public void setPropertyListener(IPropertyListener provider);
	
	public List<String> getMapFieldList();
	
	public void setMapFieldsEditors(String mapFieldsEditors);
	
	public String getMapFieldsEditors();
	
	// <mapField, mapFieldEditor>  Key -> one specified in mapField attribute
	public Map<String, String> getMapFieldsEditorsMap();
	
	public String getMapKey();
	
	public void setMapKey(String mapKey);
	
	public String getMapFields();
	
	public void setMapFields(String mapFields);
	
	public String getDependentProperty();
	
	public void setDependentProperty(String property);
	
	public boolean isVisibleUnderCondition();

	public void setVisibleUnderCondition(boolean visibleUnderCondition);
	
	public String getVisibleCondition();
	
	public void setVisibleCondition(String visibleCondition);
	
	public void setComboProviderName(String comboProviderName);
	
	public String getComboProviderName();
	
	
	
	 
}
