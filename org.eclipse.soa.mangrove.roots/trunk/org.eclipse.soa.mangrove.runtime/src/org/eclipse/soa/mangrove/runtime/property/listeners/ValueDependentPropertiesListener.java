/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.runtime.property.listeners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IProperty;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.IServiceBinding;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;

public class ValueDependentPropertiesListener implements IPropertyListener {

	public void handleUpdate(String sourcePropertyValue, String dependentPropertyName, IProperty destinationServiceBindingProperty, EAnnotation ea ) {
		
		
		// 
		// In this implementation if the sourcePropertyValue is true the dependent properties will be
		// created otherwise it will be deleted
		//
		
		
		EMap<String, String> details = ea.getDetails();
		String runtimeID = details.get(ImConstants.IM_POOL_RUNTIME_ID);
		String serviceName = details.get(ImConstants.IM_SERVICE_NAME);
		String serviceBindingName = details.get(ImConstants.IM_SERVICE_BINDING_NAME);

		
		IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeID);
		IServiceBinding serviceBinding = runtime.getServiceBindings().get(serviceBindingName);
			
		
		String[] clauses = dependentPropertyName.split(";");
		

		String[] onValuePropertyNames = null;
		
		String[] tok = null;
		IProperty aProperty = null;
		String pNames = null;
		for (String clause : clauses){
			 clause = clause.trim();
			 tok = clause.split("->");
			 List<String> candidateValues = new ArrayList<String>();
			 if (tok[0].startsWith("onValues(")){
				 String arrValues = tok[0].substring("onValues(".length());
				 arrValues = arrValues.substring(1, arrValues.length() - 1);
				 String[] splitterArr = arrValues.split(",");
				 candidateValues = Arrays.asList(splitterArr);
			 }else{
				 candidateValues.add(tok[0].substring("onValue(".length()));
			 }
			
			 pNames = tok[1].substring(0, tok[1].length() - 1);
			 onValuePropertyNames = pNames.split(",");
			 for (String s : onValuePropertyNames){
				 aProperty = serviceBinding.getDefinedPropertyByName(s);
				 //if (sourcePropertyValue.equalsIgnoreCase(candidateValue)){
				 if (candidateValues.contains(sourcePropertyValue)){
					 String value = (aProperty.getDefaultValue() != null ? aProperty.getDefaultValue() : ""); 
					 if (details.get(aProperty.getName()) == null){ 
						 details.put(aProperty.getName(),value);
					 }
				 }else{
					 details.remove(s);
				 }
			 }
		}
	}
}
