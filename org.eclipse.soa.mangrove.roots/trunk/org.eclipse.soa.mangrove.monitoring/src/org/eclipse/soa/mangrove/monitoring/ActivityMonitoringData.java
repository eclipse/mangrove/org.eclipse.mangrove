package org.eclipse.soa.mangrove.monitoring;

/**
 * @author Adrian Mos
 * Holder for monitoring data for BPMS activities
 */
public class ActivityMonitoringData {
	public String getProcName() {
		return procName;
	}
	public void setProcName(String procName) {
		this.procName = procName;
	}
	public String getProcID() {
		return procID;
	}
	public void setProcID(String procID) {
		this.procID = procID;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getGrantedRole() {
		return grantedRole;
	}
	public void setGrantedRole(String grantedRole) {
		this.grantedRole = grantedRole;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public long getLastExecTime() {
		return lastExecTime;
	}
	public void setLastExecTime(long l) {
		this.lastExecTime = l;
	}
	public long getAvgExecTime() {
		return avgExecTime;
	}
	public void setAvgExecTime(long l) {
		this.avgExecTime = l;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	private String procName;
	private String procID;
	private String activityName;
	private String grantedRole;
	private String user;
	private long lastExecTime;
	private long avgExecTime;
	private String timeStamp;
}
