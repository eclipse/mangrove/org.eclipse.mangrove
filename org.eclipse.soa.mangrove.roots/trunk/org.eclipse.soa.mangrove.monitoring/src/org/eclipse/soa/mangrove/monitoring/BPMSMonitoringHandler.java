package org.eclipse.soa.mangrove.monitoring;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;

/**
 * @author Adrian Mos
 * Helper class to deal with monitoring data from BPMS
 */
public class BPMSMonitoringHandler {

	String activityFileName = "C:\\XRCE_PushMonitoring_FileStore\\monitoring_Activity.info";
	String processFileName = "C:\\XRCE_PushMonitoring_FileStore\\monitoring_Process.info";
	String soaFileName = "C:\\XRCE_PushMonitoring_FileStore\\monitoring_Activity_SOA.info";
	String dslActivityMapFileName = "C:\\XRCE_PushMonitoring_FileStore\\dsl_bpms_activity_map.info";
	private Path activityFile;
	private Path processFile;
	private Path dslActivityFile;
	private HashMap<String, DSLActivityandServiceMap> conceptsActivitiesAndServiceMap; //maps the concepts in the DSL to the activity names and the webservice that together achieve its functionality
	private HashMap<String, ActivityMonitoringData> activityExecMap; //maps activity names composed of ProcessName+ActivityName to activity execution details
	private HashMap<String, Vector<Long>> activityExecHistoryMap; //maps activity names composed of ProcessName+ActivityName to activity execution history 
	private HashMap<String, ProcessMonitoringData> processExecMap; //maps activity names composed of ProcessName+ActivityName to activity execution details
	private HashMap<String, Vector<Long>> processExecHistoryMap; //maps activity names composed of ProcessName+ActivityName to activity execution history
	private HashMap<String, Long> serviceLastExecTime = new HashMap<String, Long>()
				{{put("CRM Update",new Long(8368)); put("Postage Carriers",new Long(10349)); put("Insurance Provider", new Long(9797));}};

	
	
	public HashMap<String, Long> getServiceLastExecTime() {
		return serviceLastExecTime;
	}

	public BPMSMonitoringHandler() {
		activityFile = Paths.get(activityFileName);
		processFile = Paths.get(processFileName);
		dslActivityFile = Paths.get(dslActivityMapFileName);
		initializeActivityDataStructures();
		initializeProcessDataStructures();
		initializeDSLDataStructures();
		try {
			processActivityMonitoringFile();
			processProcessMonitoringFile();
			processDSLActivityFile();
		} catch (IOException e) {
			System.err.println("Could not process the monitoring file at " + activityFileName);
		}
		try {
			processProcessMonitoringFile();
		} catch (IOException e) {
			System.err.println("Could not process the monitoring file at " + processFileName);
		}
		
	}
	
	private void processDSLActivityFile() throws IOException {
		Scanner s = new Scanner(dslActivityFile);
		while (s.hasNextLine()){
			DSLActivityandServiceMap data = processMappingLine(s.nextLine());
			if (null!=data){
				this.conceptsActivitiesAndServiceMap.put(data.getConceptName(), data);
			}
		}
		s.close();
		//this.printDataStructures();
	}

	private DSLActivityandServiceMap processMappingLine(String nextLine) {
		//System.out.println("Processing Mapping Line: " + nextLine);
		DSLActivityandServiceMap data = new DSLActivityandServiceMap();
		Scanner ls = new Scanner(nextLine);
		ls.useDelimiter("::");
		try{
			if (ls.hasNext()){
				data.setConceptName(ls.next());
				//System.out.println("Concept Name: " + data.getConceptName());

				if(ls.hasNext()){
					// add the list of activities
					Vector<String> activityNames = new Vector<String>();
					Scanner aNames = new Scanner(ls.next());
					aNames.useDelimiter(";");
					while(aNames.hasNext()) activityNames.add(aNames.next());
					data.setActivityNames(activityNames);
					aNames.close();
				}
				
				if(ls.hasNext()) data.setServiceName(ls.next());
			}
		} catch (Exception e){
			data = null;
		}

		ls.close();
		return data;
	}
	
	private void processProcessMonitoringFile() throws IOException {
		Scanner s = new Scanner(processFile);
		initializeProcessDataStructures();
		while (s.hasNextLine()){
			ProcessMonitoringData data = processProcessLine (s.nextLine());
			if (null!=data){
				processExecMap.put(data.getProcessName(), data);
				//System.out.println("Processed PROCESS: " + data.getProcessName());
			}
		}
		s.close();
		//this.printDataStructures();
		
	}

	private ProcessMonitoringData processProcessLine(String nextLine) {
		ProcessMonitoringData data = new ProcessMonitoringData();
		Scanner ls = new Scanner(nextLine);
		ls.useDelimiter("::");
		try{
			if (ls.hasNext()){
				data.setProcessName(ls.next());
				data.setProcID(ls.next());
				
				// add the list of activities
				Vector<String> activityNames = new Vector<String>();
				Scanner aNames = new Scanner(ls.next());
				aNames.useDelimiter(";");
				while(aNames.hasNext()) activityNames.add(aNames.next());
				data.setPerformedActivityNames(activityNames);
				aNames.close();
				
				data.setUser(ls.next());
				data.setLastExecTime(ls.nextLong());
				data.setAvgExecTime(ls.nextLong());
				data.setTimeStamp(ls.next());
				
				String key = data.getProcessName();
				Vector<Long> v;
				if (processExecHistoryMap.containsKey(key)) v = processExecHistoryMap.get(key);
				else v = new Vector<Long>();
				v.add(data.getLastExecTime());
				processExecHistoryMap.put(key, v);
			}
		} catch (Exception e){
			data = null;
		}

		ls.close();
		return data;
	}

	private void initializeActivityDataStructures(){
		activityExecMap = new HashMap<String, ActivityMonitoringData>();
		activityExecHistoryMap = new HashMap<String, Vector<Long>>();
	}
	
	private void initializeProcessDataStructures(){
		processExecMap = new HashMap<String, ProcessMonitoringData>();
		processExecHistoryMap = new HashMap<String, Vector<Long>>();		
	}
	
	private void initializeDSLDataStructures(){
		conceptsActivitiesAndServiceMap = new HashMap<String, DSLActivityandServiceMap>();	
	}
	
	public static void main(String[] args) {
		BPMSMonitoringHandler h = new BPMSMonitoringHandler();
		//h.printDataStructures();
		try {
			ActivityMonitoringData d = h.findLatestActivityMonitoringData("Shipment_Process-Hardware", "FillaPostlabel");
			if (null!=d) System.out.println("FOUND Latest Activity Data: " + d.getAvgExecTime());
			Vector v = h.getActivityExecutionTimes("Shipment_Process-Hardware", "FillaPostlabel");
			System.out.println(v);
			
			ProcessMonitoringData p = h.findLatestProcessMonitoringData("Shipment_Process-Hardware");
			if (null!=p) {
				System.out.println("FOUND Latest Data: " + p.getAvgExecTime());
				Vector act = p.getPerformedActivityNames();
				System.out.println("Executed activities for last instance " + p.getProcID() + ": " + act);
			}
			Vector vp = h.getProcessExecutionTimes("Shipment_Process-Hardware");
			System.out.println(vp);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void processActivityMonitoringFile() throws IOException{
		Scanner s = new Scanner(activityFile);
		initializeActivityDataStructures();
		while (s.hasNextLine()){
			ActivityMonitoringData data = processActivityLine (s.nextLine());
			if (null!=data){
				//System.out.println(data.getActivityName() + " executed in " + data.getLastExecTime() + " at " + data.getTimeStamp());
				activityExecMap.put(data.getProcName() + "::" + data.getActivityName(), data);
			}
		}
		s.close();
		//this.printDataStructures();
	}
	
	private void printDataStructures(){
		System.out.println("Printing Activity TO Execution Data HashMap");
		for (String key : activityExecMap.keySet()){
			System.out.println("Activity: " + key + " :: "+ activityExecMap.get(key));
		}
		
		System.out.println("Printing Activity TO Execution History HashMap");
		for (String key : activityExecHistoryMap.keySet()){
			System.out.println("Activity: " + key + " :: "+ activityExecHistoryMap.get(key));
		}

		System.out.println("Printing Concept TO Activities and Service Map");
		for (String key : conceptsActivitiesAndServiceMap.keySet()){
			System.out.println("Concept: " + key + " :: "+ conceptsActivitiesAndServiceMap.get(key));
		}
		
	}

	/**
	 * @param processName
	 * @param activityName
	 * @return the object containing execution details for the last execution of this activity regardless of process instance
	 * @throws IOException
	 */
	public ActivityMonitoringData findLatestActivityMonitoringData(String processName, String activityName) throws IOException{
		ActivityMonitoringData data = null;
		String key = processName + "::" + activityName; //our convention, used also when storing values in the map
		this.processActivityMonitoringFile(); //updates the contents of the map (only the last values will be stored) due to the nature of the map
		if (activityExecMap.containsKey(key)) data = activityExecMap.get(key);
		return data;
	}
	
	public Vector<Long> getActivityExecutionTimes(String processName, String activityName) {
		//System.out.println("LOOKING FOR EXECUTION TIMES FOR ACTIVITY: " + activityName + " IN PROCESS: " + processName);
		Vector<Long> v = null;
		String key = processName + "::" + activityName; //our convention, used also when storing values in the map
		if (activityExecHistoryMap.containsKey(key)) v = activityExecHistoryMap.get(key);
		//System.out.println("FOUND these values: " + v);
		return v;
	}
	
	public ProcessMonitoringData findLatestProcessMonitoringData(String processName) throws IOException{
		ProcessMonitoringData data = null;
		this.processProcessMonitoringFile(); //updates the contents of the map (only the last values will be stored) due to the nature of the map
		if (processExecMap.containsKey(processName)) data = processExecMap.get(processName);
		return data;
	}
	
	public Vector<Long> getProcessExecutionTimes(String processName) {
		//System.out.println("LOOKING FOR EXECUTION TIMES FOR PROCESS: " + processName);
		Vector<Long> v = null;
		if (processExecHistoryMap.containsKey(processName)) v = processExecHistoryMap.get(processName);
		//System.out.println("FOUND these values: " + v);
		return v;
	}
	
	public DSLActivityandServiceMap getConceptActivitiesAndServiceMap(String conceptName) {
		DSLActivityandServiceMap m = null;
		if ((null!=conceptName) && conceptsActivitiesAndServiceMap.containsKey(conceptName)){
			m = conceptsActivitiesAndServiceMap.get(conceptName);
		}
		return m;
	}
	
	private ActivityMonitoringData processActivityLine(String nextLine) {
		ActivityMonitoringData data = new ActivityMonitoringData();
		Scanner ls = new Scanner(nextLine);
		ls.useDelimiter("::");
		try{
			if (ls.hasNext()){
				data.setProcName(ls.next());
				data.setProcID(ls.next());
				data.setActivityName(ls.next());
				data.setGrantedRole(ls.next());
				data.setUser(ls.next());
				data.setLastExecTime(ls.nextLong());
				data.setAvgExecTime(ls.nextLong());
				data.setTimeStamp(ls.next());
				
				String key = data.getProcName() + "::" + data.getActivityName(); //our convention, used also when storing values in the map
				Vector<Long> v;
				if (activityExecHistoryMap.containsKey(key)) v = activityExecHistoryMap.get(key);
				else v = new Vector<Long>();
				v.add(data.getLastExecTime());
				activityExecHistoryMap.put(key, v);
			}
		} catch (Exception e){
			data = null;
		}

		ls.close();
		return data;
	}
	
}
