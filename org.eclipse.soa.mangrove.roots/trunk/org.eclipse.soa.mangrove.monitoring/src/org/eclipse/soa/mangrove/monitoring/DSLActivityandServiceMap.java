package org.eclipse.soa.mangrove.monitoring;

import java.util.Vector;

public class DSLActivityandServiceMap {

	public String getConceptName() {
		return conceptName;
	}
	public void setConceptName(String conceptName) {
		this.conceptName = conceptName;
	}
	public Vector<String> getActivityNames() {
		return activityNames;
	}
	public void setActivityNames(Vector<String> activityNames) {
		this.activityNames = activityNames;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	private String conceptName;
	private Vector<String> activityNames;
	private String serviceName;
	
}
