package org.eclipse.soa.mangrove.monitoring;

import java.util.Vector;

/**
 * @author Adrian Mos
 * Holder for monitoring data for BPMS processes
 */
public class ProcessMonitoringData {
	public String getProcessName() {
		return procName;
	}
	public void setProcessName(String procName) {
		this.procName = procName;
	}
	public String getProcID() {
		return procID;
	}
	public void setProcID(String procID) {
		this.procID = procID;
	}
	public Vector<String> getPerformedActivityNames() {
		return performedActivityNames;
	}
	public void setPerformedActivityNames(Vector<String> performedAactivityNames) {
		this.performedActivityNames = performedAactivityNames;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public long getLastExecTime() {
		return lastExecTime;
	}
	public void setLastExecTime(long lastExecTime) {
		this.lastExecTime = lastExecTime;
	}
	public long getAvgExecTime() {
		return avgExecTime;
	}
	public void setAvgExecTime(long avgExecTime) {
		this.avgExecTime = avgExecTime;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	private String procName;
	private String procID;
	private Vector<String> performedActivityNames;
	private String user;
	private long lastExecTime;
	private long avgExecTime;
	private String timeStamp;
}
