/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.resources;

import java.util.List;

/**
 * This is the interface 
 * implement to get the configuration section for a set of resource in a particular runtime
 * 
 * @author zoppello
 *
 */
public interface IImResourceConfigurator {

	public boolean isDeploySupported();
	
	public String getConfiguration(List<IImResource> resourceList, String resourceType);
	
	public String deploy(List<IImResource> resourceList, String resourceType);

}
