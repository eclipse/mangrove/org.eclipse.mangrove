/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.resources.ui.providers;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.soa.mangrove.resources.IImResource;


	public class IImResourceContentProvider  implements IStructuredContentProvider {
		
			
			
			public IImResourceContentProvider(){
				 super(); 
				
			}
		  
		
			public Object[] getElements(Object element){
				List<IImResource> c = (List<IImResource>) (element); 
				Object[] arr = c.toArray();
				return arr;
			}	

			public void dispose()
			{
				
			}

			public void inputChanged(Viewer viewer, Object old_object, Object new_object)
			{
				
			}
		  
		  
	}

