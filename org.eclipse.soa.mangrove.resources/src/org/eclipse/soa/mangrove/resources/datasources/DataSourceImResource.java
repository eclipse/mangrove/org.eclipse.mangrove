/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.resources.datasources;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.soa.mangrove.resources.IImResource;

public class DataSourceImResource implements IImResource {
	
	private String id = null;
	private Map<String, String> datasourceProperties = null;
	
	public static String DATASOURCE_NAME = "Name";
	public static String DATASOURCE_URL = "Url";
	public static String DATASOURCE_DRIVER = "Driver";
	public static String DATASOURCE_USER = "User";
	public static String DATASOURCE_PASSWORD = "Password";
	

	public DataSourceImResource(String id) {
		this.id = id;
		datasourceProperties = new HashMap<String, String>();
		datasourceProperties.put(DATASOURCE_NAME, id);
	}
		

	public void delProperty(String propertyName) {
		datasourceProperties.remove(propertyName);
	}

	public String getId() {
		return id;
	}

	public String getProperty(String propertyName) {
		
		return datasourceProperties.get(propertyName);
	}

	public String[] getPropertyLabels() {
		
		String[] labels = new String[5];
		
		labels[0] = DATASOURCE_NAME;
		labels[1] = DATASOURCE_DRIVER;
		labels[2] = DATASOURCE_URL; 
		
		labels[3] = DATASOURCE_USER;
		labels[4] = DATASOURCE_PASSWORD;
		
		return labels;
	}

	public String[] getPropertyNames() {
		String[] names = new String[5];
		
		names[0] = DATASOURCE_NAME;
		names[1] = DATASOURCE_DRIVER;
		names[2] = DATASOURCE_URL;
		names[3] = DATASOURCE_USER;
		names[4] = DATASOURCE_PASSWORD;
		
		return names;
	}

	public String getResourceType() {
		return IImResource.DATA_SOURCE_IM_RESOURCE_TYPE;
	}

	public void setProperty(String propertyName, String propertyValue) {
		datasourceProperties.put(propertyName, propertyValue);
	}
	
	

	
	
	
}
