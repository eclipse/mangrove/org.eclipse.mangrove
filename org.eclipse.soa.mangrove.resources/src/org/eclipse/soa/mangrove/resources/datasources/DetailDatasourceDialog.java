/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.resources.datasources;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class DetailDatasourceDialog extends ImTitleAreaDialog {

//	final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	

    private Composite aComposite = null;
    private CLabel dsNameLabel = null;
    private Text dsNameTextField = null;
    
    private CLabel dsjdbcUrlLabel = null;
    private Text dsjdbcUrlTextField = null;
    
    private CLabel dsjdbcDriverLabel = null;
    private Text dsjdbcDriverTextField = null;
    
    private CLabel dsjdbcUserLabel = null;
    private Text dsjdbcUserTextField = null;
    
    private CLabel dsjdbcPasswordLabel = null;
    private Text dsjdbcPasswordTextField = null;
    
    private DataSourceImResource datasourceBean = null;
    
    

    
    
	public DetailDatasourceDialog(Shell parentShell) {
		super(parentShell);

	}
	
	
				

	/**
	   * @see org.eclipse.jface.window.Window#create() We complete the dialog with
	   *      a title and a message
	   */
	  public void create() {
		super.create();
	    
	    setTitle("New Datasource");
	    setMessage("New Datasource");
	    
	  }

	 
	  

	@Override
	protected Control createContents(Composite parent) {
		Control ctrl = super.createContents(parent);
		return ctrl;
	}

	protected Control createDialogArea(Composite parent) {
		
		aComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
    	layout.marginWidth = 15;
    	layout.marginHeight = 10;
    	layout.numColumns = 5;
    	aComposite.setLayout(layout);
    	aComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    	
    	
    	
// Riga 1
    	dsNameLabel = new CLabel(aComposite, SWT.RIGHT);
    	dsNameLabel.setText(" DataSource Name ");
    	dsNameTextField = new Text(aComposite, SWT.BORDER | SWT.FLAT );
    	GridData data = new GridData(GridData.FILL_HORIZONTAL);
    	data.widthHint = 200;
    	data.horizontalSpan = 4;
    	dsNameTextField.setLayoutData(data);    	
    	
    	
 // Riga 2
    	dsjdbcUrlLabel = new CLabel(aComposite, SWT.RIGHT);
    	dsjdbcUrlLabel.setText(" JDBC Url ");
    	dsjdbcUrlTextField = new Text(aComposite, SWT.BORDER | SWT.FLAT );
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.widthHint = 200;
    	data.horizontalSpan = 4;
    	dsjdbcUrlTextField.setLayoutData(data);
    	
// Riga 3
    	dsjdbcDriverLabel = new CLabel(aComposite, SWT.RIGHT);
    	dsjdbcDriverLabel.setText(" JDBC Driver Class Name ");
    	dsjdbcDriverTextField = new Text(aComposite, SWT.BORDER | SWT.FLAT );
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.widthHint = 200;
    	data.horizontalSpan = 4;
    	dsjdbcDriverTextField.setLayoutData(data);
    	
    	
//    	 Riga 3
    	dsjdbcUserLabel = new CLabel(aComposite, SWT.RIGHT);
    	dsjdbcUserLabel.setText(" User ");
    	dsjdbcUserTextField = new Text(aComposite, SWT.BORDER | SWT.FLAT );
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.widthHint = 200;
    	data.horizontalSpan = 4;
    	dsjdbcUserTextField.setLayoutData(data);
    	
//   	 Riga 3
    	dsjdbcPasswordLabel = new CLabel(aComposite, SWT.RIGHT);
    	dsjdbcPasswordLabel.setText(" Password ");
    	dsjdbcPasswordTextField = new Text(aComposite, SWT.BORDER | SWT.FLAT );
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.widthHint = 200;
    	data.horizontalSpan = 4;
    	dsjdbcPasswordTextField.setLayoutData(data);
    	
    		
        return parent; 
	
	}
	
	protected void okPressed() {
		
		this.datasourceBean = new DataSourceImResource(dsNameTextField.getText());
		datasourceBean.setProperty(DataSourceImResource.DATASOURCE_DRIVER, dsjdbcDriverTextField.getText());
		datasourceBean.setProperty(DataSourceImResource.DATASOURCE_URL,dsjdbcUrlTextField.getText());
		datasourceBean.setProperty(DataSourceImResource.DATASOURCE_USER,dsjdbcUserTextField.getText());
		datasourceBean.setProperty(DataSourceImResource.DATASOURCE_PASSWORD,dsjdbcPasswordTextField.getText());
												


		close();
	}




	public DataSourceImResource getDatasourceBean() {
		return datasourceBean;
	}
	
	
	
	




	
	
}
