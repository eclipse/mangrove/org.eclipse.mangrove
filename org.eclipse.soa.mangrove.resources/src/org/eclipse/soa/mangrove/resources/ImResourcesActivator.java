/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.soa.mangrove.resources.datasources.DataSourceImResource;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class ImResourcesActivator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.soa.mangrove.resources";

	// The shared instance
	private static ImResourcesActivator plugin;
	
	private static Properties resourceProperties = null;
	
	private static Map<String, IImResourceImporterProxy> imResourceImportersProxies = null;
	private static List<String> imResourceImportersNames = null;
	private static Map<String, IImResourceImporter> imResourceImportersInstantiated = null;
	
	private static Map<String, IImResourceConfiguratorProxy> imResourceConfiguratorsProxies = null;
	private static List<String> imResourceConfiguratorsNames = null;
	private static Map<String, IImResourceConfigurator> imResourceConfiguratorsInstantiated = null;
	
	public static final String IM_RESOURCE_IMPORTER_TAG = "im-resource-importer";	
	public static final String IM_RESOURCE_CONFIGURATOR_TAG = "im-resource-configurator";	
	
	/**
	 * The constructor
	 */
	public ImResourcesActivator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;	
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ImResourcesActivator getDefault() {
		return plugin;
	}
	
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	private static File getImResourcePropertiesFile(){
		IPath path = getDefault().getStateLocation();
		String imResourcesFileName = path.toOSString() + File.separator + "imresources.properties";
		File imResourcesFile = new File (imResourcesFileName);
		return imResourcesFile;
	}
	public static void loadResourceProperties(){
		File imResourcesFile = getImResourcePropertiesFile();
		resourceProperties = new Properties();
		if (!imResourcesFile.exists()){
			return;
		}
		FileInputStream fis = null;
		try{
			fis = new FileInputStream(imResourcesFile);
			resourceProperties.load(fis);
		}catch (Exception e) {
			ImLogger.error(PLUGIN_ID, e.getMessage(),e);
		}finally{
			if (fis != null)
				try{
					fis.close();
				}catch (IOException ioe) {
					ImLogger.error(PLUGIN_ID, ioe.getMessage(),ioe);
				}
		}
		
	}
	public static Properties getResourceProperties(){
		if (resourceProperties == null)
			loadResourceProperties();
		return resourceProperties;
	}
	
	public static void saveResourceProperties() {
		File imResourcesFile = getImResourcePropertiesFile();
		if (imResourcesFile.exists())
			imResourcesFile.delete();
		
		FileOutputStream fos = null;
		try{
			fos = new FileOutputStream(imResourcesFile);
			resourceProperties.store(fos, "Andrea");
		}catch (Exception e) {
			ImLogger.error(PLUGIN_ID, e.getMessage(),e);
		}finally{
			if (fos != null)
				try{
					fos.close();
				}catch (IOException ioe) {
					ImLogger.error(PLUGIN_ID, ioe.getMessage(),ioe);
				}
		}
	}
	
	
	
	
	
	
	
	public static void initImResourcesImportersProxy() {
		if ((imResourceImportersProxies != null) 
				&& (imResourceImportersNames != null)){
			return;
		}
		try{
			IExtensionRegistry extensionPointRegistry   = Platform.getExtensionRegistry ();
			IExtensionPoint    extensionPoint = extensionPointRegistry.getExtensionPoint (PLUGIN_ID, "imresourceimporter");

			IExtension[] extensions = extensionPoint.getExtensions();
			
			

			imResourceImportersProxies = new HashMap<String, IImResourceImporterProxy>();
			imResourceImportersNames = new ArrayList<String>();
			imResourceImportersInstantiated = new HashMap<String, IImResourceImporter>();

			
			for (int i = 0; i < extensions.length; i++) {
				IConfigurationElement[] configElements = extensions[i]
					.getConfigurationElements();
				for (int j = 0; j < configElements.length; j++) {
					IImResourceImporterProxy proxy = parseIImResourceImporterProxy(
						configElements[j]);
					if (proxy != null){
						imResourceImportersProxies.put(proxy.getName(), proxy);
						imResourceImportersNames.add(proxy.getName());
					}
				}
			}
			return;
		}catch (Throwable t) {
			ImLogger.error(ImResourcesActivator.PLUGIN_ID, t.getMessage(), t);
			imResourceImportersProxies = null;
			imResourceImportersNames = null;
		}
	}
	
	public static void initImResourceConfiguratorsProxy() {
		if ((imResourceConfiguratorsProxies != null) 
				&& (imResourceConfiguratorsNames != null)){
			return;
		}
		try{
			IExtensionRegistry extensionPointRegistry   = Platform.getExtensionRegistry ();
			IExtensionPoint    extensionPoint = extensionPointRegistry.getExtensionPoint (PLUGIN_ID, "imresourceconfigurator");

			IExtension[] extensions = extensionPoint.getExtensions();
			
			

			imResourceConfiguratorsProxies = new HashMap<String, IImResourceConfiguratorProxy>();
			imResourceConfiguratorsNames = new ArrayList<String>();
			imResourceConfiguratorsInstantiated = new HashMap<String,IImResourceConfigurator>();

			
			for (int i = 0; i < extensions.length; i++) {
				IConfigurationElement[] configElements = extensions[i]
					.getConfigurationElements();
				for (int j = 0; j < configElements.length; j++) {
					IImResourceConfiguratorProxy proxy = parseIImResourceConfiguratorProxy(
						configElements[j]);
					if (proxy != null){
						imResourceConfiguratorsProxies.put(proxy.getName(), proxy);
						imResourceConfiguratorsNames.add(proxy.getName());
					}
				}
			}
			return;
		}catch (Throwable t) {
			ImLogger.error(ImResourcesActivator.PLUGIN_ID, t.getMessage(), t);
			imResourceImportersProxies = null;
			imResourceImportersNames = null;
		}
	}
	
	private static IImResourceImporterProxy parseIImResourceImporterProxy(IConfigurationElement configElement) {
		if (!configElement.getName().equals(IM_RESOURCE_IMPORTER_TAG))
			return null;
		try {
			return new IImResourceImporterProxy(configElement);
		}
		catch (Exception e) {
			ImLogger.error(ImResourcesActivator.PLUGIN_ID, e.getMessage(), e);
			return null;
			
		}
	}
	
	private static IImResourceConfiguratorProxy parseIImResourceConfiguratorProxy(IConfigurationElement configElement) {
		if (!configElement.getName().equals(IM_RESOURCE_CONFIGURATOR_TAG))
			return null;
		try {
			return new IImResourceConfiguratorProxy(configElement);
		}
		catch (Exception e) {
			ImLogger.error(ImResourcesActivator.PLUGIN_ID, e.getMessage(), e);
			return null;
			
		}
	}

	public static String[] getIImResourceImportersNames(){
		initImResourcesImportersProxy();
		String[] names = new String[imResourceImportersNames.size()];
		for (int i = 0; i < names.length; i++ ){
			names[i] = imResourceImportersNames.get(i);
		}
		return names;
	}
	
	public static String[] getIImResourceConfiguratorsNames(){
		initImResourceConfiguratorsProxy();
		String[] names = new String[imResourceConfiguratorsNames.size()];
		for (int i = 0; i < names.length; i++ ){
			names[i] = imResourceConfiguratorsNames.get(i);
		}
		return names;
	}
	
	public static IImResourceImporter getIImResourceImporterByName(String importerName){
		initImResourcesImportersProxy();
		IImResourceImporter importer = imResourceImportersInstantiated.get(importerName);
		if (importer == null){
			IImResourceImporterProxy proxy = imResourceImportersProxies.get(importerName);
			importer = proxy.getIImResourceImporter();
			imResourceImportersInstantiated.put(importerName, importer);
		}
		return importer;
	}
	
	public static IImResourceConfigurator  getIImResourceConfiguratorByName(String configuratorName){
		initImResourceConfiguratorsProxy();
		IImResourceConfigurator configurator = imResourceConfiguratorsInstantiated.get(configuratorName);
		if (configurator == null){
			IImResourceConfiguratorProxy proxy = imResourceConfiguratorsProxies.get(configuratorName);
			configurator = proxy.getIImResourceConfigurator();
			imResourceConfiguratorsInstantiated.put(configuratorName, configurator);
		}
		return configurator;
	}
	
	
	// -- Resource repository management method
	public static void addDataSource(IImResource dsBean){
		
		Properties props = getResourceProperties();
		String name = "im.datasource."+dsBean.getId();
		props.setProperty(name, dsBean.getId());
		props.setProperty(name + ".property.jdbcDriver",dsBean.getProperty(DataSourceImResource.DATASOURCE_DRIVER));
		props.setProperty(name + ".property.jdbcUrl",dsBean.getProperty(DataSourceImResource.DATASOURCE_URL));
		props.setProperty(name + ".property.jdbcUser",dsBean.getProperty(DataSourceImResource.DATASOURCE_USER)); 
		props.setProperty(name + ".property.jdbcPassword",dsBean.getProperty(DataSourceImResource.DATASOURCE_PASSWORD));
		saveResourceProperties();
				
		
	}
	
	public static void deleteDatasource(IImResource dsBean){
		Properties props = getResourceProperties();
		String name = "im.datasource."+dsBean.getId();
		props.remove(name);
		props.remove(name + ".property.jdbcDriver");
		props.remove(name + ".property.jdbcUrl");
		props.remove(name + ".property.jdbcUser");
		props.remove(name + ".property.jdbcPassword");
		saveResourceProperties();
	}
	
	public static List<IImResource> getDataSources(){
		return ImResourcesActivator.getImResourcesWithType(DataSourceImResource.DATA_SOURCE_IM_RESOURCE_TYPE);
	}
	
	public static IImResource getImResourceByIdAndType(String resourceID, String resourceType){
		Properties props = getResourceProperties();
		DataSourceImResource dsBean = null;
		String idDs = null;
		for ( Object key : props.keySet()){
			String name = (String)key;
			if (name.startsWith("im."+resourceType+".") && (!name.contains("property"))){
				idDs = props.getProperty(name);
				
				if (idDs.equals(resourceID)){
					dsBean = new DataSourceImResource(props.getProperty(name));
					dsBean.setProperty(DataSourceImResource.DATASOURCE_DRIVER, props.getProperty(name + ".property.jdbcDriver"));
					dsBean.setProperty(DataSourceImResource.DATASOURCE_URL,props.getProperty(name + ".property.jdbcUrl"));
					dsBean.setProperty(DataSourceImResource.DATASOURCE_USER,props.getProperty(name + ".property.jdbcUser"));
					dsBean.setProperty(DataSourceImResource.DATASOURCE_PASSWORD,props.getProperty(name + ".property.jdbcPassword"));
				
					return dsBean;
					
				}
			}
		}
		return null;
	}
	
	public static List<IImResource> getImResourcesWithType(String resourceType){
		Properties props = getResourceProperties();
		
		List <IImResource> resources = new ArrayList<IImResource>();

	
		DataSourceImResource dsBean = null;
		String idDs = null;
		for ( Object key : props.keySet()){
			String name = (String)key;
			if (name.startsWith("im."+resourceType+".") && (!name.contains("property"))){
				idDs = props.getProperty(name);
				
				if (resourceType.equalsIgnoreCase("datasource")){
					dsBean = new DataSourceImResource(props.getProperty(name));
					dsBean.setProperty(DataSourceImResource.DATASOURCE_DRIVER, props.getProperty(name + ".property.jdbcDriver"));
					dsBean.setProperty(DataSourceImResource.DATASOURCE_URL,props.getProperty(name + ".property.jdbcUrl"));
					dsBean.setProperty(DataSourceImResource.DATASOURCE_USER,props.getProperty(name + ".property.jdbcUser"));
					dsBean.setProperty(DataSourceImResource.DATASOURCE_PASSWORD,props.getProperty(name + ".property.jdbcPassword"));
				
					resources.add(dsBean);
				}
			}
		}
		return resources;
	}
}
