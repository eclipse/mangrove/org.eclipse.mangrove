package org.eclipse.soa.mangrove.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.soa.mangrove.diagram.edit.commands.ProcessCollectionCreateCommand;
import org.eclipse.soa.mangrove.diagram.edit.commands.ServiceCollectionCreateCommand;
import org.eclipse.soa.mangrove.diagram.providers.IntermediateModelElementTypes;

/**
 * @generated
 */
public class StpIntermediateModelItemSemanticEditPolicy extends
		IntermediateModelBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public StpIntermediateModelItemSemanticEditPolicy() {
		super(IntermediateModelElementTypes.StpIntermediateModel_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (IntermediateModelElementTypes.ProcessCollection_2005 == req
				.getElementType()) {
			return getGEFWrapper(new ProcessCollectionCreateCommand(req));
		}
		if (IntermediateModelElementTypes.ServiceCollection_2006 == req
				.getElementType()) {
			return getGEFWrapper(new ServiceCollectionCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
