package org.eclipse.soa.mangrove.diagram.providers;

import org.eclipse.soa.mangrove.diagram.part.IntermediateModelDiagramEditorPlugin;

/**
 * @generated
 */
public class ElementInitializers {

	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	 * @generated
	 */
	public static ElementInitializers getInstance() {
		ElementInitializers cached = IntermediateModelDiagramEditorPlugin
				.getInstance().getElementInitializers();
		if (cached == null) {
			IntermediateModelDiagramEditorPlugin.getInstance()
					.setElementInitializers(cached = new ElementInitializers());
		}
		return cached;
	}
}
