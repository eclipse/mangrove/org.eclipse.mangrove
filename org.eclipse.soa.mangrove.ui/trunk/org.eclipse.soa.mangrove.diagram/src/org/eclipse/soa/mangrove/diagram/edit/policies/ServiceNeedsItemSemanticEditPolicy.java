package org.eclipse.soa.mangrove.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;
import org.eclipse.soa.mangrove.diagram.providers.IntermediateModelElementTypes;

/**
 * @generated
 */
public class ServiceNeedsItemSemanticEditPolicy extends
		IntermediateModelBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ServiceNeedsItemSemanticEditPolicy() {
		super(IntermediateModelElementTypes.ServiceNeeds_4011);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
