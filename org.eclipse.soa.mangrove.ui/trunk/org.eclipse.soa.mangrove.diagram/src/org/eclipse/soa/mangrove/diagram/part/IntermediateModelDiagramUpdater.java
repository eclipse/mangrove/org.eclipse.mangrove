package org.eclipse.soa.mangrove.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.Process;
import org.eclipse.soa.mangrove.ProcessCollection;
import org.eclipse.soa.mangrove.Service;
import org.eclipse.soa.mangrove.ServiceCollection;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.Transition;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessCollectionEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceCollectionEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceNeedsEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepServiceModelEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StpIntermediateModelEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.TransitionEditPart;
import org.eclipse.soa.mangrove.diagram.providers.IntermediateModelElementTypes;

/**
 * @generated
 */
public class IntermediateModelDiagramUpdater {

	/**
	 * @generated
	 */
	public static List<IntermediateModelNodeDescriptor> getSemanticChildren(
			View view) {
		switch (IntermediateModelVisualIDRegistry.getVisualID(view)) {
		case StpIntermediateModelEditPart.VISUAL_ID:
			return getStpIntermediateModel_1000SemanticChildren(view);
		case ProcessCollectionEditPart.VISUAL_ID:
			return getProcessCollection_2005SemanticChildren(view);
		case ServiceCollectionEditPart.VISUAL_ID:
			return getServiceCollection_2006SemanticChildren(view);
		case ProcessEditPart.VISUAL_ID:
			return getProcess_3007SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelNodeDescriptor> getStpIntermediateModel_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		StpIntermediateModel modelElement = (StpIntermediateModel) view
				.getElement();
		LinkedList<IntermediateModelNodeDescriptor> result = new LinkedList<IntermediateModelNodeDescriptor>();
		{
			ProcessCollection childElement = modelElement
					.getProcessCollection();
			int visualID = IntermediateModelVisualIDRegistry.getNodeVisualID(
					view, childElement);
			if (visualID == ProcessCollectionEditPart.VISUAL_ID) {
				result.add(new IntermediateModelNodeDescriptor(childElement,
						visualID));
			}
		}
		{
			ServiceCollection childElement = modelElement
					.getServiceCollection();
			int visualID = IntermediateModelVisualIDRegistry.getNodeVisualID(
					view, childElement);
			if (visualID == ServiceCollectionEditPart.VISUAL_ID) {
				result.add(new IntermediateModelNodeDescriptor(childElement,
						visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelNodeDescriptor> getProcessCollection_2005SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		ProcessCollection modelElement = (ProcessCollection) view.getElement();
		LinkedList<IntermediateModelNodeDescriptor> result = new LinkedList<IntermediateModelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getProcesses().iterator(); it
				.hasNext();) {
			Process childElement = (Process) it.next();
			int visualID = IntermediateModelVisualIDRegistry.getNodeVisualID(
					view, childElement);
			if (visualID == ProcessEditPart.VISUAL_ID) {
				result.add(new IntermediateModelNodeDescriptor(childElement,
						visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelNodeDescriptor> getServiceCollection_2006SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		ServiceCollection modelElement = (ServiceCollection) view.getElement();
		LinkedList<IntermediateModelNodeDescriptor> result = new LinkedList<IntermediateModelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getServices().iterator(); it
				.hasNext();) {
			Service childElement = (Service) it.next();
			int visualID = IntermediateModelVisualIDRegistry.getNodeVisualID(
					view, childElement);
			if (visualID == ServiceEditPart.VISUAL_ID) {
				result.add(new IntermediateModelNodeDescriptor(childElement,
						visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelNodeDescriptor> getProcess_3007SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		Process modelElement = (Process) view.getElement();
		LinkedList<IntermediateModelNodeDescriptor> result = new LinkedList<IntermediateModelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getSteps().iterator(); it.hasNext();) {
			Step childElement = (Step) it.next();
			int visualID = IntermediateModelVisualIDRegistry.getNodeVisualID(
					view, childElement);
			if (visualID == StepEditPart.VISUAL_ID) {
				result.add(new IntermediateModelNodeDescriptor(childElement,
						visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getContainedLinks(
			View view) {
		switch (IntermediateModelVisualIDRegistry.getVisualID(view)) {
		case StpIntermediateModelEditPart.VISUAL_ID:
			return getStpIntermediateModel_1000ContainedLinks(view);
		case ProcessCollectionEditPart.VISUAL_ID:
			return getProcessCollection_2005ContainedLinks(view);
		case ServiceCollectionEditPart.VISUAL_ID:
			return getServiceCollection_2006ContainedLinks(view);
		case ProcessEditPart.VISUAL_ID:
			return getProcess_3007ContainedLinks(view);
		case StepEditPart.VISUAL_ID:
			return getStep_3008ContainedLinks(view);
		case ServiceEditPart.VISUAL_ID:
			return getService_3009ContainedLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4009ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getIncomingLinks(
			View view) {
		switch (IntermediateModelVisualIDRegistry.getVisualID(view)) {
		case ProcessCollectionEditPart.VISUAL_ID:
			return getProcessCollection_2005IncomingLinks(view);
		case ServiceCollectionEditPart.VISUAL_ID:
			return getServiceCollection_2006IncomingLinks(view);
		case ProcessEditPart.VISUAL_ID:
			return getProcess_3007IncomingLinks(view);
		case StepEditPart.VISUAL_ID:
			return getStep_3008IncomingLinks(view);
		case ServiceEditPart.VISUAL_ID:
			return getService_3009IncomingLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4009IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getOutgoingLinks(
			View view) {
		switch (IntermediateModelVisualIDRegistry.getVisualID(view)) {
		case ProcessCollectionEditPart.VISUAL_ID:
			return getProcessCollection_2005OutgoingLinks(view);
		case ServiceCollectionEditPart.VISUAL_ID:
			return getServiceCollection_2006OutgoingLinks(view);
		case ProcessEditPart.VISUAL_ID:
			return getProcess_3007OutgoingLinks(view);
		case StepEditPart.VISUAL_ID:
			return getStep_3008OutgoingLinks(view);
		case ServiceEditPart.VISUAL_ID:
			return getService_3009OutgoingLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4009OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getStpIntermediateModel_1000ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getProcessCollection_2005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getServiceCollection_2006ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getProcess_3007ContainedLinks(
			View view) {
		Process modelElement = (Process) view.getElement();
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Transition_4009(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Service_Needs_4011(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getStep_3008ContainedLinks(
			View view) {
		Step modelElement = (Step) view.getElement();
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Step_ServiceModel_4010(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getService_3009ContainedLinks(
			View view) {
		Service modelElement = (Service) view.getElement();
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Service_Needs_4011(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getTransition_4009ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getProcessCollection_2005IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getServiceCollection_2006IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getProcess_3007IncomingLinks(
			View view) {
		Process modelElement = (Process) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Step_ServiceModel_4010(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_Service_Needs_4011(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getStep_3008IncomingLinks(
			View view) {
		Step modelElement = (Step) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Transition_4009(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getService_3009IncomingLinks(
			View view) {
		Service modelElement = (Service) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Step_ServiceModel_4010(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_Service_Needs_4011(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getTransition_4009IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getProcessCollection_2005OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getServiceCollection_2006OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getProcess_3007OutgoingLinks(
			View view) {
		Process modelElement = (Process) view.getElement();
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Service_Needs_4011(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getStep_3008OutgoingLinks(
			View view) {
		Step modelElement = (Step) view.getElement();
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Transition_4009(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Step_ServiceModel_4010(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getService_3009OutgoingLinks(
			View view) {
		Service modelElement = (Service) view.getElement();
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Service_Needs_4011(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<IntermediateModelLinkDescriptor> getTransition_4009OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<IntermediateModelLinkDescriptor> getContainedTypeModelFacetLinks_Transition_4009(
			Process container) {
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		for (Iterator<?> links = container.getTransitions().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Transition) {
				continue;
			}
			Transition link = (Transition) linkObject;
			if (TransitionEditPart.VISUAL_ID != IntermediateModelVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Step dst = link.getTarget();
			Step src = link.getSource();
			result.add(new IntermediateModelLinkDescriptor(src, dst, link,
					IntermediateModelElementTypes.Transition_4009,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<IntermediateModelLinkDescriptor> getIncomingTypeModelFacetLinks_Transition_4009(
			Step target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != ImPackage.eINSTANCE
					.getTransition_Target()
					|| false == setting.getEObject() instanceof Transition) {
				continue;
			}
			Transition link = (Transition) setting.getEObject();
			if (TransitionEditPart.VISUAL_ID != IntermediateModelVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Step src = link.getSource();
			result.add(new IntermediateModelLinkDescriptor(src, target, link,
					IntermediateModelElementTypes.Transition_4009,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<IntermediateModelLinkDescriptor> getIncomingFeatureModelFacetLinks_Step_ServiceModel_4010(
			Service target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == ImPackage.eINSTANCE
					.getStep_ServiceModel()) {
				result.add(new IntermediateModelLinkDescriptor(setting
						.getEObject(), target,
						IntermediateModelElementTypes.StepServiceModel_4010,
						StepServiceModelEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<IntermediateModelLinkDescriptor> getIncomingFeatureModelFacetLinks_Service_Needs_4011(
			Service target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == ImPackage.eINSTANCE
					.getService_Needs()) {
				result.add(new IntermediateModelLinkDescriptor(setting
						.getEObject(), target,
						IntermediateModelElementTypes.ServiceNeeds_4011,
						ServiceNeedsEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<IntermediateModelLinkDescriptor> getOutgoingTypeModelFacetLinks_Transition_4009(
			Step source) {
		Process container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Process) {
				container = (Process) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		for (Iterator<?> links = container.getTransitions().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Transition) {
				continue;
			}
			Transition link = (Transition) linkObject;
			if (TransitionEditPart.VISUAL_ID != IntermediateModelVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Step dst = link.getTarget();
			Step src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new IntermediateModelLinkDescriptor(src, dst, link,
					IntermediateModelElementTypes.Transition_4009,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<IntermediateModelLinkDescriptor> getOutgoingFeatureModelFacetLinks_Step_ServiceModel_4010(
			Step source) {
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		Service destination = source.getServiceModel();
		if (destination == null) {
			return result;
		}
		result.add(new IntermediateModelLinkDescriptor(source, destination,
				IntermediateModelElementTypes.StepServiceModel_4010,
				StepServiceModelEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<IntermediateModelLinkDescriptor> getOutgoingFeatureModelFacetLinks_Service_Needs_4011(
			Service source) {
		LinkedList<IntermediateModelLinkDescriptor> result = new LinkedList<IntermediateModelLinkDescriptor>();
		for (Iterator<?> destinations = source.getNeeds().iterator(); destinations
				.hasNext();) {
			Service destination = (Service) destinations.next();
			result.add(new IntermediateModelLinkDescriptor(source, destination,
					IntermediateModelElementTypes.ServiceNeeds_4011,
					ServiceNeedsEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */

		public List<IntermediateModelNodeDescriptor> getSemanticChildren(
				View view) {
			return IntermediateModelDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */

		public List<IntermediateModelLinkDescriptor> getContainedLinks(View view) {
			return IntermediateModelDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */

		public List<IntermediateModelLinkDescriptor> getIncomingLinks(View view) {
			return IntermediateModelDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */

		public List<IntermediateModelLinkDescriptor> getOutgoingLinks(View view) {
			return IntermediateModelDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
