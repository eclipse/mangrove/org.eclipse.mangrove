package org.eclipse.soa.mangrove.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessCollectionEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessCollectionPoolNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceCollectionEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceServiceNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StpIntermediateModelEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.TransitionEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class IntermediateModelVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "org.eclipse.soa.mangrove.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (StpIntermediateModelEditPart.MODEL_ID.equals(view.getType())) {
				return StpIntermediateModelEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				IntermediateModelDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (ImPackage.eINSTANCE.getStpIntermediateModel().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((StpIntermediateModel) domainElement)) {
			return StpIntermediateModelEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
				.getModelID(containerView);
		if (!StpIntermediateModelEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (StpIntermediateModelEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = StpIntermediateModelEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case StpIntermediateModelEditPart.VISUAL_ID:
			if (ImPackage.eINSTANCE.getProcessCollection().isSuperTypeOf(
					domainElement.eClass())) {
				return ProcessCollectionEditPart.VISUAL_ID;
			}
			if (ImPackage.eINSTANCE.getServiceCollection().isSuperTypeOf(
					domainElement.eClass())) {
				return ServiceCollectionEditPart.VISUAL_ID;
			}
			break;
		case ProcessCollectionEditPart.VISUAL_ID:
			if (ImPackage.eINSTANCE.getProcess().isSuperTypeOf(
					domainElement.eClass())) {
				return ProcessEditPart.VISUAL_ID;
			}
			break;
		case ServiceCollectionEditPart.VISUAL_ID:
			if (ImPackage.eINSTANCE.getService().isSuperTypeOf(
					domainElement.eClass())) {
				return ServiceEditPart.VISUAL_ID;
			}
			break;
		case ProcessEditPart.VISUAL_ID:
			if (ImPackage.eINSTANCE.getStep().isSuperTypeOf(
					domainElement.eClass())) {
				return StepEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
				.getModelID(containerView);
		if (!StpIntermediateModelEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (StpIntermediateModelEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = StpIntermediateModelEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case StpIntermediateModelEditPart.VISUAL_ID:
			if (ProcessCollectionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ServiceCollectionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ProcessCollectionEditPart.VISUAL_ID:
			if (ProcessCollectionPoolNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ProcessEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ServiceCollectionEditPart.VISUAL_ID:
			if (ServiceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ProcessEditPart.VISUAL_ID:
			if (ProcessNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (StepEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case StepEditPart.VISUAL_ID:
			if (StepNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ServiceEditPart.VISUAL_ID:
			if (ServiceServiceNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (ImPackage.eINSTANCE.getTransition().isSuperTypeOf(
				domainElement.eClass())) {
			return TransitionEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(StpIntermediateModel element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case StpIntermediateModelEditPart.VISUAL_ID:
			return false;
		case StepEditPart.VISUAL_ID:
		case ServiceEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */

		public int getVisualID(View view) {
			return org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */

		public String getModelID(View view) {
			return org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */

		public int getNodeVisualID(View containerView, EObject domainElement) {
			return org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */

		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */

		public boolean isCompartmentVisualID(int visualID) {
			return org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */

		public boolean isSemanticLeafVisualID(int visualID) {
			return org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
