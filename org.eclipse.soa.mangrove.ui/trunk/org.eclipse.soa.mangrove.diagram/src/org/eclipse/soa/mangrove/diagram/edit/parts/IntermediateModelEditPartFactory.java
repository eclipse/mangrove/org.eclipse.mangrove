package org.eclipse.soa.mangrove.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry;

/**
 * @generated
 */
public class IntermediateModelEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (IntermediateModelVisualIDRegistry.getVisualID(view)) {

			case StpIntermediateModelEditPart.VISUAL_ID:
				return new StpIntermediateModelEditPart(view);

			case ProcessCollectionEditPart.VISUAL_ID:
				return new ProcessCollectionEditPart(view);

			case ProcessCollectionPoolNameEditPart.VISUAL_ID:
				return new ProcessCollectionPoolNameEditPart(view);

			case ServiceCollectionEditPart.VISUAL_ID:
				return new ServiceCollectionEditPart(view);

			case ProcessEditPart.VISUAL_ID:
				return new ProcessEditPart(view);

			case ProcessNameEditPart.VISUAL_ID:
				return new ProcessNameEditPart(view);

			case StepEditPart.VISUAL_ID:
				return new StepEditPart(view);

			case StepNameEditPart.VISUAL_ID:
				return new StepNameEditPart(view);

			case ServiceEditPart.VISUAL_ID:
				return new ServiceEditPart(view);

			case ServiceServiceNameEditPart.VISUAL_ID:
				return new ServiceServiceNameEditPart(view);

			case TransitionEditPart.VISUAL_ID:
				return new TransitionEditPart(view);

			case StepServiceModelEditPart.VISUAL_ID:
				return new StepServiceModelEditPart(view);

			case ServiceNeedsEditPart.VISUAL_ID:
				return new ServiceNeedsEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
