package org.eclipse.soa.mangrove.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.soa.mangrove.Process;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.Transition;
import org.eclipse.soa.mangrove.diagram.edit.policies.IntermediateModelBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class TransitionReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public TransitionReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof Transition) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof Step && newEnd instanceof Step)) {
			return false;
		}
		Step target = getLink().getTarget();
		if (!(getLink().eContainer() instanceof Process)) {
			return false;
		}
		Process container = (Process) getLink().eContainer();
		return IntermediateModelBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistTransition_4009(container, getLink(), getNewSource(),
						target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof Step && newEnd instanceof Step)) {
			return false;
		}
		Step source = getLink().getSource();
		if (!(getLink().eContainer() instanceof Process)) {
			return false;
		}
		Process container = (Process) getLink().eContainer();
		return IntermediateModelBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistTransition_4009(container, getLink(), source,
						getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setSource(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTarget(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected Transition getLink() {
		return (Transition) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected Step getOldSource() {
		return (Step) oldEnd;
	}

	/**
	 * @generated
	 */
	protected Step getNewSource() {
		return (Step) newEnd;
	}

	/**
	 * @generated
	 */
	protected Step getOldTarget() {
		return (Step) oldEnd;
	}

	/**
	 * @generated
	 */
	protected Step getNewTarget() {
		return (Step) newEnd;
	}
}
