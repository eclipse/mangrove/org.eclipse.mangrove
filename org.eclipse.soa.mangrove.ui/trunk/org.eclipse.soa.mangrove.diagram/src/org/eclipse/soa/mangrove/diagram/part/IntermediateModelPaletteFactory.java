package org.eclipse.soa.mangrove.diagram.part;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.soa.mangrove.diagram.providers.IntermediateModelElementTypes;

/**
 * @generated
 */
public class IntermediateModelPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		cleanStandardTools(paletteRoot);
		paletteRoot.add(createMangrove1Group());
		paletteRoot.add(createTravelServices2Group());
		paletteRoot.add(createFinancialServices3Group());
	}

	/**
	 * Workaround for https://bugs.eclipse.org/bugs/show_bug.cgi?id=159289
	 * @generated
	 */
	private void cleanStandardTools(PaletteRoot paletteRoot) {
		for (Iterator it = paletteRoot.getChildren().iterator(); it.hasNext();) {
			PaletteEntry entry = (PaletteEntry) it.next();
			if (!"standardGroup".equals(entry.getId())) { //$NON-NLS-1$
				continue;
			}
			for (Iterator it2 = ((PaletteContainer) entry).getChildren()
					.iterator(); it2.hasNext();) {
				PaletteEntry entry2 = (PaletteEntry) it2.next();
				if ("zoomTool".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				} else if ("noteStack".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				} else if ("selectionTool".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				}
				if (paletteRoot.getDefaultEntry() == entry2) {
					paletteRoot.setDefaultEntry(null);
				}
			}
		}
	}

	/**
	 * Creates "mangrove" palette tool group
	 * @generated
	 */
	private PaletteContainer createMangrove1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Mangrove1Group_title);
		paletteContainer.setId("createMangrove1Group"); //$NON-NLS-1$
		paletteContainer.setDescription(Messages.Mangrove1Group_desc);
		paletteContainer.add(createStandardToolsTool());
		paletteContainer.add(new PaletteSeparator());
		paletteContainer.add(createProcessCollection3CreationTool());
		paletteContainer.add(createProcess4CreationTool());
		paletteContainer.add(createStep5CreationTool());
		paletteContainer.add(createTransition6CreationTool());
		paletteContainer.add(new PaletteSeparator());
		paletteContainer.add(createServiceCollection8CreationTool());
		paletteContainer.add(createService9CreationTool());
		paletteContainer.add(createServiceService10CreationTool());
		paletteContainer.add(new PaletteSeparator());
		paletteContainer.add(createStepServiceConnection12CreationTool());
		paletteContainer.add(new PaletteSeparator());
		return paletteContainer;
	}

	/**
	 * Creates "Travel Services" palette tool group
	 * @generated
	 */
	private PaletteContainer createTravelServices2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.TravelServices2Group_title);
		paletteContainer.setId("createTravelServices2Group"); //$NON-NLS-1$
		paletteContainer.add(createHotel1CreationTool());
		paletteContainer.add(createWeather2CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Financial Services" palette tool group
	 * @generated
	 */
	private PaletteContainer createFinancialServices3Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.FinancialServices3Group_title);
		paletteContainer.setId("createFinancialServices3Group"); //$NON-NLS-1$
		paletteContainer.setDescription(Messages.FinancialServices3Group_desc);
		paletteContainer.add(createCreditCard1CreationTool());
		paletteContainer.add(createShoppingCart2CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStandardToolsTool() {
		PanningSelectionToolEntry entry = new PanningSelectionToolEntry();
		entry.setId("createStandardToolsTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createProcessCollection3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ProcessCollection3CreationTool_title,
				Messages.ProcessCollection3CreationTool_desc,
				Collections
						.singletonList(IntermediateModelElementTypes.ProcessCollection_2005));
		entry.setId("createProcessCollection3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IntermediateModelElementTypes
				.getImageDescriptor(IntermediateModelElementTypes.ProcessCollection_2005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createProcess4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Process4CreationTool_title,
				Messages.Process4CreationTool_desc,
				Collections
						.singletonList(IntermediateModelElementTypes.Process_3007));
		entry.setId("createProcess4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IntermediateModelElementTypes
				.getImageDescriptor(IntermediateModelElementTypes.Process_3007));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStep5CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Step5CreationTool_title,
				Messages.Step5CreationTool_desc,
				Collections
						.singletonList(IntermediateModelElementTypes.Step_3008));
		entry.setId("createStep5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IntermediateModelElementTypes
				.getImageDescriptor(IntermediateModelElementTypes.Step_3008));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createTransition6CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Transition6CreationTool_title,
				Messages.Transition6CreationTool_desc,
				Collections
						.singletonList(IntermediateModelElementTypes.Transition_4009));
		entry.setId("createTransition6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IntermediateModelElementTypes
				.getImageDescriptor(IntermediateModelElementTypes.Transition_4009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createServiceCollection8CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ServiceCollection8CreationTool_title,
				Messages.ServiceCollection8CreationTool_desc,
				Collections
						.singletonList(IntermediateModelElementTypes.ServiceCollection_2006));
		entry.setId("createServiceCollection8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IntermediateModelElementTypes
				.getImageDescriptor(IntermediateModelElementTypes.ServiceCollection_2006));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createService9CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Service9CreationTool_title,
				Messages.Service9CreationTool_desc,
				Collections
						.singletonList(IntermediateModelElementTypes.Service_3009));
		entry.setId("createService9CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IntermediateModelElementTypes
				.getImageDescriptor(IntermediateModelElementTypes.Service_3009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createServiceService10CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ServiceService10CreationTool_title,
				Messages.ServiceService10CreationTool_desc,
				Collections
						.singletonList(IntermediateModelElementTypes.ServiceNeeds_4011));
		entry.setId("createServiceService10CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IntermediateModelElementTypes
				.getImageDescriptor(IntermediateModelElementTypes.ServiceNeeds_4011));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createStepServiceConnection12CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.StepServiceConnection12CreationTool_title,
				Messages.StepServiceConnection12CreationTool_desc,
				Collections
						.singletonList(IntermediateModelElementTypes.StepServiceModel_4010));
		entry.setId("createStepServiceConnection12CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(IntermediateModelElementTypes
				.getImageDescriptor(IntermediateModelElementTypes.StepServiceModel_4010));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createHotel1CreationTool() {
		ToolEntry entry = new ToolEntry(Messages.Hotel1CreationTool_title,
				null, null, null) {
		};
		entry.setId("createHotel1CreationTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createWeather2CreationTool() {
		ToolEntry entry = new ToolEntry(Messages.Weather2CreationTool_title,
				Messages.Weather2CreationTool_desc, null, null) {
		};
		entry.setId("createWeather2CreationTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCreditCard1CreationTool() {
		ToolEntry entry = new ToolEntry(Messages.CreditCard1CreationTool_title,
				null, null, null) {
		};
		entry.setId("createCreditCard1CreationTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createShoppingCart2CreationTool() {
		ToolEntry entry = new ToolEntry(
				Messages.ShoppingCart2CreationTool_title, null, null, null) {
		};
		entry.setId("createShoppingCart2CreationTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
