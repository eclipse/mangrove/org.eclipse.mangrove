package org.eclipse.soa.mangrove.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

	/**
	 * @generated
	 */
	public DiagramPrintingPreferencePage() {
		setPreferenceStore(IntermediateModelDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
