package org.eclipse.soa.mangrove.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.AppearancePreferencePage;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramAppearancePreferencePage extends AppearancePreferencePage {

	/**
	 * @generated
	 */
	public DiagramAppearancePreferencePage() {
		setPreferenceStore(IntermediateModelDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
