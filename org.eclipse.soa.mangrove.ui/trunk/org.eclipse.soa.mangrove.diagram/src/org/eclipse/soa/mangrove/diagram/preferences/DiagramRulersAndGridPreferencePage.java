package org.eclipse.soa.mangrove.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	 * @generated
	 */
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(IntermediateModelDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
