package org.eclipse.soa.mangrove.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String IntermediateModelCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String IntermediateModelCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String IntermediateModelCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String IntermediateModelCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String IntermediateModelCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String IntermediateModelDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String IntermediateModelNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String IntermediateModelElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Mangrove1Group_title;

	/**
	 * @generated
	 */
	public static String Mangrove1Group_desc;

	/**
	 * @generated
	 */
	public static String TravelServices2Group_title;

	/**
	 * @generated
	 */
	public static String FinancialServices3Group_title;

	/**
	 * @generated
	 */
	public static String FinancialServices3Group_desc;

	/**
	 * @generated
	 */
	public static String StandardToolsTool_title;

	/**
	 * @generated
	 */
	public static String ProcessCollection3CreationTool_title;

	/**
	 * @generated
	 */
	public static String ProcessCollection3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Process4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Process4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Step5CreationTool_title;

	/**
	 * @generated
	 */
	public static String Step5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Transition6CreationTool_title;

	/**
	 * @generated
	 */
	public static String Transition6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ServiceCollection8CreationTool_title;

	/**
	 * @generated
	 */
	public static String ServiceCollection8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Service9CreationTool_title;

	/**
	 * @generated
	 */
	public static String Service9CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ServiceService10CreationTool_title;

	/**
	 * @generated
	 */
	public static String ServiceService10CreationTool_desc;

	/**
	 * @generated
	 */
	public static String StepServiceConnection12CreationTool_title;

	/**
	 * @generated
	 */
	public static String StepServiceConnection12CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Hotel1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Weather2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Weather2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CreditCard1CreationTool_title;

	/**
	 * @generated
	 */
	public static String ShoppingCart2CreationTool_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StpIntermediateModel_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_4009_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_4009_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Service_3009_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Service_3009_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ServiceNeeds_4011_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ServiceNeeds_4011_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StepServiceModel_4010_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_StepServiceModel_4010_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Step_3008_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Step_3008_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Process_3007_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Process_3007_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String IntermediateModelModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String IntermediateModelModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
