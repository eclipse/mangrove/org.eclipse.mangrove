package org.eclipse.soa.mangrove.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry;

/**
 * @generated
 */
public class IntermediateModelNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 4013;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof IntermediateModelNavigatorItem) {
			IntermediateModelNavigatorItem item = (IntermediateModelNavigatorItem) element;
			return IntermediateModelVisualIDRegistry
					.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
