package org.eclipse.soa.mangrove.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessCollectionPoolNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceServiceNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepNameEditPart;
import org.eclipse.soa.mangrove.diagram.parsers.MessageFormatParser;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry;

/**
 * @generated
 */
public class IntermediateModelParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser processCollectionPoolName_5011Parser;

	/**
	 * @generated
	 */
	private IParser getProcessCollectionPoolName_5011Parser() {
		if (processCollectionPoolName_5011Parser == null) {
			EAttribute[] features = new EAttribute[] { ImPackage.eINSTANCE
					.getProcessCollection_PoolName() };
			MessageFormatParser parser = new MessageFormatParser(features);
			processCollectionPoolName_5011Parser = parser;
		}
		return processCollectionPoolName_5011Parser;
	}

	/**
	 * @generated
	 */
	private IParser processName_5010Parser;

	/**
	 * @generated
	 */
	private IParser getProcessName_5010Parser() {
		if (processName_5010Parser == null) {
			EAttribute[] features = new EAttribute[] { ImPackage.eINSTANCE
					.getProcess_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			processName_5010Parser = parser;
		}
		return processName_5010Parser;
	}

	/**
	 * @generated
	 */
	private IParser stepName_5009Parser;

	/**
	 * @generated
	 */
	private IParser getStepName_5009Parser() {
		if (stepName_5009Parser == null) {
			EAttribute[] features = new EAttribute[] { ImPackage.eINSTANCE
					.getStep_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			stepName_5009Parser = parser;
		}
		return stepName_5009Parser;
	}

	/**
	 * @generated
	 */
	private IParser serviceServiceName_5012Parser;

	/**
	 * @generated
	 */
	private IParser getServiceServiceName_5012Parser() {
		if (serviceServiceName_5012Parser == null) {
			EAttribute[] features = new EAttribute[] { ImPackage.eINSTANCE
					.getService_ServiceName() };
			MessageFormatParser parser = new MessageFormatParser(features);
			serviceServiceName_5012Parser = parser;
		}
		return serviceServiceName_5012Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case ProcessCollectionPoolNameEditPart.VISUAL_ID:
			return getProcessCollectionPoolName_5011Parser();
		case ProcessNameEditPart.VISUAL_ID:
			return getProcessName_5010Parser();
		case StepNameEditPart.VISUAL_ID:
			return getStepName_5009Parser();
		case ServiceServiceNameEditPart.VISUAL_ID:
			return getServiceServiceName_5012Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(IntermediateModelVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(IntermediateModelVisualIDRegistry
					.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (IntermediateModelElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
