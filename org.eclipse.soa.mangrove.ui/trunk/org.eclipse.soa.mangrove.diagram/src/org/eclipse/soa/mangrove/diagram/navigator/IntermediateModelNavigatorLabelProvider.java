package org.eclipse.soa.mangrove.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessCollectionEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessCollectionPoolNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceCollectionEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceNeedsEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceServiceNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepNameEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepServiceModelEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StpIntermediateModelEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.TransitionEditPart;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelDiagramEditorPlugin;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelVisualIDRegistry;
import org.eclipse.soa.mangrove.diagram.providers.IntermediateModelElementTypes;
import org.eclipse.soa.mangrove.diagram.providers.IntermediateModelParserProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

/**
 * @generated
 */
public class IntermediateModelNavigatorLabelProvider extends LabelProvider
		implements ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		IntermediateModelDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		IntermediateModelDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof IntermediateModelNavigatorItem
				&& !isOwnView(((IntermediateModelNavigatorItem) element)
						.getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof IntermediateModelNavigatorGroup) {
			IntermediateModelNavigatorGroup group = (IntermediateModelNavigatorGroup) element;
			return IntermediateModelDiagramEditorPlugin.getInstance()
					.getBundledImage(group.getIcon());
		}

		if (element instanceof IntermediateModelNavigatorItem) {
			IntermediateModelNavigatorItem navigatorItem = (IntermediateModelNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (IntermediateModelVisualIDRegistry.getVisualID(view)) {
		case StpIntermediateModelEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://eclipse.org/soa/mangrove?StpIntermediateModel", IntermediateModelElementTypes.StpIntermediateModel_1000); //$NON-NLS-1$
		case TransitionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://eclipse.org/soa/mangrove?Transition", IntermediateModelElementTypes.Transition_4009); //$NON-NLS-1$
		case ProcessCollectionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://eclipse.org/soa/mangrove?ProcessCollection", IntermediateModelElementTypes.ProcessCollection_2005); //$NON-NLS-1$
		case ServiceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://eclipse.org/soa/mangrove?Service", IntermediateModelElementTypes.Service_3009); //$NON-NLS-1$
		case ServiceNeedsEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://eclipse.org/soa/mangrove?Service?needs", IntermediateModelElementTypes.ServiceNeeds_4011); //$NON-NLS-1$
		case StepServiceModelEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://eclipse.org/soa/mangrove?Step?serviceModel", IntermediateModelElementTypes.StepServiceModel_4010); //$NON-NLS-1$
		case StepEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://eclipse.org/soa/mangrove?Step", IntermediateModelElementTypes.Step_3008); //$NON-NLS-1$
		case ProcessEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://eclipse.org/soa/mangrove?Process", IntermediateModelElementTypes.Process_3007); //$NON-NLS-1$
		case ServiceCollectionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://eclipse.org/soa/mangrove?ServiceCollection", IntermediateModelElementTypes.ServiceCollection_2006); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = IntermediateModelDiagramEditorPlugin
				.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null
				&& elementType != null
				&& IntermediateModelElementTypes
						.isKnownElementType(elementType)) {
			image = IntermediateModelElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof IntermediateModelNavigatorGroup) {
			IntermediateModelNavigatorGroup group = (IntermediateModelNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof IntermediateModelNavigatorItem) {
			IntermediateModelNavigatorItem navigatorItem = (IntermediateModelNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (IntermediateModelVisualIDRegistry.getVisualID(view)) {
		case StpIntermediateModelEditPart.VISUAL_ID:
			return getStpIntermediateModel_1000Text(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4009Text(view);
		case ProcessCollectionEditPart.VISUAL_ID:
			return getProcessCollection_2005Text(view);
		case ServiceEditPart.VISUAL_ID:
			return getService_3009Text(view);
		case ServiceNeedsEditPart.VISUAL_ID:
			return getServiceNeeds_4011Text(view);
		case StepServiceModelEditPart.VISUAL_ID:
			return getStepServiceModel_4010Text(view);
		case StepEditPart.VISUAL_ID:
			return getStep_3008Text(view);
		case ProcessEditPart.VISUAL_ID:
			return getProcess_3007Text(view);
		case ServiceCollectionEditPart.VISUAL_ID:
			return getServiceCollection_2006Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getStpIntermediateModel_1000Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getTransition_4009Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getProcessCollection_2005Text(View view) {
		IParser parser = IntermediateModelParserProvider.getParser(
				IntermediateModelElementTypes.ProcessCollection_2005, view
						.getElement() != null ? view.getElement() : view,
				IntermediateModelVisualIDRegistry
						.getType(ProcessCollectionPoolNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			IntermediateModelDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5011); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getService_3009Text(View view) {
		IParser parser = IntermediateModelParserProvider.getParser(
				IntermediateModelElementTypes.Service_3009,
				view.getElement() != null ? view.getElement() : view,
				IntermediateModelVisualIDRegistry
						.getType(ServiceServiceNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			IntermediateModelDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5012); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getServiceNeeds_4011Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getStepServiceModel_4010Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getStep_3008Text(View view) {
		IParser parser = IntermediateModelParserProvider.getParser(
				IntermediateModelElementTypes.Step_3008,
				view.getElement() != null ? view.getElement() : view,
				IntermediateModelVisualIDRegistry
						.getType(StepNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			IntermediateModelDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getProcess_3007Text(View view) {
		IParser parser = IntermediateModelParserProvider.getParser(
				IntermediateModelElementTypes.Process_3007,
				view.getElement() != null ? view.getElement() : view,
				IntermediateModelVisualIDRegistry
						.getType(ProcessNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			IntermediateModelDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getServiceCollection_2006Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return StpIntermediateModelEditPart.MODEL_ID
				.equals(IntermediateModelVisualIDRegistry.getModelID(view));
	}

}
