package org.eclipse.soa.mangrove.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessCollectionEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceCollectionEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.ServiceNeedsEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepServiceModelEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StpIntermediateModelEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.TransitionEditPart;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelDiagramEditorPlugin;
import org.eclipse.swt.graphics.Image;

/**
 * @generated
 */
public class IntermediateModelElementTypes {

	/**
	 * @generated
	 */
	private IntermediateModelElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static ImageRegistry imageRegistry;

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType StpIntermediateModel_1000 = getElementType("org.eclipse.soa.mangrove.diagram.StpIntermediateModel_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ProcessCollection_2005 = getElementType("org.eclipse.soa.mangrove.diagram.ProcessCollection_2005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ServiceCollection_2006 = getElementType("org.eclipse.soa.mangrove.diagram.ServiceCollection_2006"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Process_3007 = getElementType("org.eclipse.soa.mangrove.diagram.Process_3007"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Step_3008 = getElementType("org.eclipse.soa.mangrove.diagram.Step_3008"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Service_3009 = getElementType("org.eclipse.soa.mangrove.diagram.Service_3009"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Transition_4009 = getElementType("org.eclipse.soa.mangrove.diagram.Transition_4009"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType StepServiceModel_4010 = getElementType("org.eclipse.soa.mangrove.diagram.StepServiceModel_4010"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ServiceNeeds_4011 = getElementType("org.eclipse.soa.mangrove.diagram.ServiceNeeds_4011"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element) {
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(
			ENamedElement element) {
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract()) {
				element = eContainingClass;
			} else if (eType instanceof EClass
					&& !((EClass) eType).isAbstract()) {
				element = eType;
			}
		}
		if (element instanceof EClass) {
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract()) {
				return IntermediateModelDiagramEditorPlugin.getInstance()
						.getItemImageDescriptor(
								eClass.getEPackage().getEFactoryInstance()
										.create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null) {
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		if (image == null) {
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(StpIntermediateModel_1000,
					ImPackage.eINSTANCE.getStpIntermediateModel());

			elements.put(ProcessCollection_2005,
					ImPackage.eINSTANCE.getProcessCollection());

			elements.put(ServiceCollection_2006,
					ImPackage.eINSTANCE.getServiceCollection());

			elements.put(Process_3007, ImPackage.eINSTANCE.getProcess());

			elements.put(Step_3008, ImPackage.eINSTANCE.getStep());

			elements.put(Service_3009, ImPackage.eINSTANCE.getService());

			elements.put(Transition_4009, ImPackage.eINSTANCE.getTransition());

			elements.put(StepServiceModel_4010,
					ImPackage.eINSTANCE.getStep_ServiceModel());

			elements.put(ServiceNeeds_4011,
					ImPackage.eINSTANCE.getService_Needs());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(StpIntermediateModel_1000);
			KNOWN_ELEMENT_TYPES.add(ProcessCollection_2005);
			KNOWN_ELEMENT_TYPES.add(ServiceCollection_2006);
			KNOWN_ELEMENT_TYPES.add(Process_3007);
			KNOWN_ELEMENT_TYPES.add(Step_3008);
			KNOWN_ELEMENT_TYPES.add(Service_3009);
			KNOWN_ELEMENT_TYPES.add(Transition_4009);
			KNOWN_ELEMENT_TYPES.add(StepServiceModel_4010);
			KNOWN_ELEMENT_TYPES.add(ServiceNeeds_4011);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case StpIntermediateModelEditPart.VISUAL_ID:
			return StpIntermediateModel_1000;
		case ProcessCollectionEditPart.VISUAL_ID:
			return ProcessCollection_2005;
		case ServiceCollectionEditPart.VISUAL_ID:
			return ServiceCollection_2006;
		case ProcessEditPart.VISUAL_ID:
			return Process_3007;
		case StepEditPart.VISUAL_ID:
			return Step_3008;
		case ServiceEditPart.VISUAL_ID:
			return Service_3009;
		case TransitionEditPart.VISUAL_ID:
			return Transition_4009;
		case StepServiceModelEditPart.VISUAL_ID:
			return StepServiceModel_4010;
		case ServiceNeedsEditPart.VISUAL_ID:
			return ServiceNeeds_4011;
		}
		return null;
	}

}
