package org.eclipse.soa.mangrove.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.DiagramsPreferencePage;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramGeneralPreferencePage extends DiagramsPreferencePage {

	/**
	 * @generated
	 */
	public DiagramGeneralPreferencePage() {
		setPreferenceStore(IntermediateModelDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
