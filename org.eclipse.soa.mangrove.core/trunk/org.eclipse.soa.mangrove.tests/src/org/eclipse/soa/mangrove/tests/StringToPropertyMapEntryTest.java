/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.soa.mangrove.tests;

import java.util.Map;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.soa.mangrove.ImFactory;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.Property;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>String To Property Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StringToPropertyMapEntryTest extends TestCase {

	/**
	 * The fixture for this String To Property Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map.Entry<String, Property> fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StringToPropertyMapEntryTest.class);
	}

	/**
	 * Constructs a new String To Property Map Entry test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringToPropertyMapEntryTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this String To Property Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Map.Entry<String, Property> fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this String To Property Map Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map.Entry<String, Property> getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void setUp() throws Exception {
		setFixture((Map.Entry<String, Property>)ImFactory.eINSTANCE.create(ImPackage.Literals.STRING_TO_PROPERTY_MAP_ENTRY));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //StringToPropertyMapEntryTest
