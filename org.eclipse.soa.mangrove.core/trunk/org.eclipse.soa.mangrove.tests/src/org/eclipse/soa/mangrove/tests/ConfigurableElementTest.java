/*******************************************************************************
 * Copyright (c) {2007-2008} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tests;

import junit.framework.TestCase;

import org.eclipse.soa.mangrove.ConfigurableElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Configurable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ConfigurableElementTest extends TestCase {

	/**
	 * The fixture for this Configurable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurableElement fixture = null;

	/**
	 * Constructs a new Configurable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurableElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Configurable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ConfigurableElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Configurable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurableElement getFixture() {
		return fixture;
	}

} //ConfigurableElementTest
