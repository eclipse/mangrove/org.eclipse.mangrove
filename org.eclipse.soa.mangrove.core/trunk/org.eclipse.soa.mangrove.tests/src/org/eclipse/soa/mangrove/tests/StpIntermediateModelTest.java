/*******************************************************************************
 * Copyright (c) {2007-2008} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.soa.mangrove.ImFactory;
import org.eclipse.soa.mangrove.StpIntermediateModel;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Stp Intermediate Model</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StpIntermediateModelTest extends ConfigurableElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StpIntermediateModelTest.class);
	}

	/**
	 * Constructs a new Stp Intermediate Model test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StpIntermediateModelTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Stp Intermediate Model test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected StpIntermediateModel getFixture() {
		return (StpIntermediateModel)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ImFactory.eINSTANCE.createStpIntermediateModel());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //StpIntermediateModelTest
