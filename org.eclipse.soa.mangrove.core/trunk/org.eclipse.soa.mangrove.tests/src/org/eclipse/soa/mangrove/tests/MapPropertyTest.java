/*******************************************************************************
 * Copyright (c) {2007-2008} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tests;

import junit.textui.TestRunner;

import org.eclipse.soa.mangrove.ImFactory;
import org.eclipse.soa.mangrove.MapProperty;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Map Property</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MapPropertyTest extends PropertyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MapPropertyTest.class);
	}

	/**
	 * Constructs a new Map Property test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapPropertyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Map Property test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected MapProperty getFixture() {
		return (MapProperty)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ImFactory.eINSTANCE.createMapProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MapPropertyTest
