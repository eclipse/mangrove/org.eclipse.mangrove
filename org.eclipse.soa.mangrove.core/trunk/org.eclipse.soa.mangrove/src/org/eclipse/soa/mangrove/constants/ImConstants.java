/*******************************************************************************
 * Copyright (c) {2007 - 2008} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.constants;

public interface ImConstants {
	
	//
	// Data for TECHNOLOGY_ANNOTATION
	// TECHNOLOGY_ANNOTATION ARE IN THE BPMN POOL ( Intermediate Model Process )
	//
	public static final String TECHNOLOGY_ANNOTATION = "technology";
	public static final String IM_POOL_RUNTIME_ID = "im.pool.runtimeID";
	public static final String TECHNOLOGY_NAME = "TechnologyName";
	public static final String IM_PROCESS_TECHNOLOGY = "im.process.technology";

	//
	// Data for PARAMETERS_ANNOTATION
	// PARAMETERS_ANNOTATION ARE IN THE BPMN POOL ( Intermediate Model Process )
	//
	public static final String PARAMETERS_ANNOTATION = "parameters";

	//
	// Data for IM_ANNOTATION
	// IM_ANNOTATION ARE IN THE BPMN ACTIVITY OBJECTS ( Intermediate Model Steps )
	//
	// This annotation is the annotation that bind a bpmn object to a step with a particular 
	// im service/im service binding
	//  
	//
	public static final String IM_ANNOTATION = "im";
	public static final String IM_SERVICE_NAME = "im.servicename";
	public static final String IM_SERVICE_BINDING_NAME = "im.servicebindingname";
	

	//
	// Data for IM_TRANSITION_ANNOTATION
	// IM_TRANSITION_ANNOTATION ARE IN THE BPMN EDGES
	//
	public static final String IM_TRANSITION_ANNOTATION = "imtransition";
	public static final String IM_TRANSITION_CONDITION_NAME = "im.transition.conditionname";
	public static final String IM_TRANSITION_TYPE = "im.transition.type";
	public static final String IM_TRANSITION_TYPE_EXPRESSION_CONDITION = "ExpressionCondition";
	public static final String IM_TRANSITION_TYPE_PROPERTY_CONDITION = "PropertyCondition";
	public static final String IM_TRANSITION_TYPE_EVENT_CONDITION = "EventCondition";
	
	
	//
	// Data for IM_OBSERVABLE_ATTRIBUTE_ANNOTATION
	// IM_OBSERVABLE_ATTRIBUTE ANNOTATION ARE IN BPMN DATA OBJECTS
	//
	public static final String IM_OBSERVABLE_ATTRIBUTE = "imobsattribute";
	public static final String IM_OBSERVABLE_ATTRIBUTE_LIST_SIZE = "im.obsattribute.list.size";
	public static final String IM_OBSERVABLE_ATTRIBUTE_ID = "im.obsattribute[{0}].id"; // The relevant data id in the database
	public static final String IM_OBSERVABLE_ATTRIBUTE_NAME = "im.obsattribute[{0}].name"; // The relavant data name of the database
	public static final String IM_OBSERVABLE_ATTRIBUTE_RULE_ID = "im.obsattribute[{0}].ruleid"; // The rule to associate with the endpoint
	public static final String IM_OBSERVABLE_ATTRIBUTE_RULE_NAME = "im.obsattribute[{0}].rulename";
	public static final String IM_OBSERVABLE_ATTRIBUTE_RULE_EXPRESSION="im.obsattribute[{0}].ruleexpression"; // The rule expression
	public static final String IM_OBSERVABLE_ATTRIBUTE_RULE_LANGUAGE = "im.obsattribute[{0}].rulelanguage";  // The rule language
	public static final String IM_OBSERVABLE_ATTRIBUTE_RULE_SUBJECT_TO_CONDITION = "im.obsattribute[{0}].flagrulesubjecttocondition";
	public static final String IM_OBSERVABLE_ATTRIBUTE_CONDITION_EXPRESSION="im.obsattribute[{0}].conditionruleexpression"; // The rule expression
	public static final String IM_OBSERVABLE_ATTRIBUTE_CONDITION_EXPRESSION_LANGUAGE = "im.obsattribute[{0}].conditionrulelanguage"; 
	
	//
	// The name of the property that give us a PROCESS VERSION
	//
	public static final String IM_VERSION_ANNOTATION = "im.version";
		
	//
	// The name of the property of the service binding that bind a servicebinding to a particular runtimeID
	//
	public static final String IM_STEP_BINDING_RUNTIME_ID = "im.stepbinding.runtimeID";
	
	//
	// Process properties
	//
	public static final String PROCESS_VERSION = "im.process.version";
	public static final String IM_PROCESS_PARAMETERS = "im.process.parameters";
	public static final String MONITORING_TYPE = "im.process.monitoringType";
	
	// Monitoring types
	public static final String MONITORING_TYPE_ASYNCHRONOUS = "ASYNCHRONOUS";
	public static final String MONITORING_TYPE_SYNCHRONOUS = "SYNCHRONOUS";
	
	//
	// Iter properties
	//
	public static final String ITER_VERSION = "im.iter.version";

	public static final String IM_ITER_ANNOTATION = "im.iter";
	public static final String IM_ITER_NAME = "im.iter.name";
	public static final String IM_ITER_RULE_FOR_DETERMINATION = "im.iter.rule";
	public static final String IM_ITER_RULE_FOR_DETERMINATION_LANGUAGE = "im.iter.rule.language";
	public static final String IM_ITER_FIRST_PROCESS_NAME = "im.iter.firstprocess.name";
	
	public static final String IM_ITER_OBSERVABLE_ATTRIBUTE_LIST_SIZE  = "im.iter.obsattribute.list.size";
	public static final String IM_ITER_OBSERVABLE_ATTRIBUTE_ID  = "im.iter.obsattribute[{0}].id";
	public static final String IM_ITER_OBSERVABLE_ATTRIBUTE_NAME  = "im.iter.obsattribute[{0}].name";

	
	public static final String IM_ITER_OBSERVABLE_ATTRIBUTE_MAP = "im.iter.observableattribute.map";
	
	
	public static final String SERVICE_SPLIT_CONTROL = "SplitControl";
	public static final String SERVICE_JOIN_CONTROL = "JoinControl";
	public static final String SERVICE_ROUTER_CONTROL = "RouterControl";
	public static final String SERVICE_ITERATION_CONTROL = "IterationControl";
	public static final String SERVICE_GENERIC = "ServiceGeneric";
	
	public static final String SERVICE_ENTITY_PROPERTY_NAME = "im.serviceentity";
	
	
	public static String IS_STEP_INTERMEDIATE_EVENT = "im.isStepIntermediateEvent";
	public static String IS_STEP_START_EVENT = "im.isStepStartEvent";
	public static String IS_STEP_END_EVENT = "im.isStepEndEvent";
	public static String IS_EXCLUSIVE_GATEWAY = "im.isExclusiveGateway";
	public static String IS_PARALLEL_GATEWAY = "im.isParallelGateway";
	
	public static String IS_STEP_SUBPROCESS = "im.isStepSubprocess";
	public static String SUBPROCESS_PROPERTY = "SubProcess";
	
	public static final String HAS_STEP_CONDITIONAL_EXIT_FLAG = "has_conditional_exit";
	
	public static final String PROPERTY_NAMESPACE = "namespace";


}
