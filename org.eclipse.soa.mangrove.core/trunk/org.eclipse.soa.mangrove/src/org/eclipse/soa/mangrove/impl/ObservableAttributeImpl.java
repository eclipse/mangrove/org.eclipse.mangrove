/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.soa.mangrove.ExtractDataRule;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.ObservableAttribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Observable Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ObservableAttributeImpl#getIdAttribute <em>Id Attribute</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ObservableAttributeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ObservableAttributeImpl#getObservableAttributeExtractRule <em>Observable Attribute Extract Rule</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ObservableAttributeImpl extends EObjectImpl implements ObservableAttribute {
	/**
	 * The default value of the '{@link #getIdAttribute() <em>Id Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_ATTRIBUTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIdAttribute() <em>Id Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdAttribute()
	 * @generated
	 * @ordered
	 */
	protected String idAttribute = ID_ATTRIBUTE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getObservableAttributeExtractRule() <em>Observable Attribute Extract Rule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservableAttributeExtractRule()
	 * @generated
	 * @ordered
	 */
	protected ExtractDataRule observableAttributeExtractRule;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObservableAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImPackage.Literals.OBSERVABLE_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIdAttribute() {
		return idAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdAttribute(String newIdAttribute) {
		String oldIdAttribute = idAttribute;
		idAttribute = newIdAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.OBSERVABLE_ATTRIBUTE__ID_ATTRIBUTE, oldIdAttribute, idAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.OBSERVABLE_ATTRIBUTE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtractDataRule getObservableAttributeExtractRule() {
		return observableAttributeExtractRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObservableAttributeExtractRule(ExtractDataRule newObservableAttributeExtractRule, NotificationChain msgs) {
		ExtractDataRule oldObservableAttributeExtractRule = observableAttributeExtractRule;
		observableAttributeExtractRule = newObservableAttributeExtractRule;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE, oldObservableAttributeExtractRule, newObservableAttributeExtractRule);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObservableAttributeExtractRule(ExtractDataRule newObservableAttributeExtractRule) {
		if (newObservableAttributeExtractRule != observableAttributeExtractRule) {
			NotificationChain msgs = null;
			if (observableAttributeExtractRule != null)
				msgs = ((InternalEObject)observableAttributeExtractRule).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE, null, msgs);
			if (newObservableAttributeExtractRule != null)
				msgs = ((InternalEObject)newObservableAttributeExtractRule).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE, null, msgs);
			msgs = basicSetObservableAttributeExtractRule(newObservableAttributeExtractRule, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE, newObservableAttributeExtractRule, newObservableAttributeExtractRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE:
				return basicSetObservableAttributeExtractRule(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ImPackage.OBSERVABLE_ATTRIBUTE__ID_ATTRIBUTE:
				return getIdAttribute();
			case ImPackage.OBSERVABLE_ATTRIBUTE__NAME:
				return getName();
			case ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE:
				return getObservableAttributeExtractRule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ImPackage.OBSERVABLE_ATTRIBUTE__ID_ATTRIBUTE:
				setIdAttribute((String)newValue);
				return;
			case ImPackage.OBSERVABLE_ATTRIBUTE__NAME:
				setName((String)newValue);
				return;
			case ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE:
				setObservableAttributeExtractRule((ExtractDataRule)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ImPackage.OBSERVABLE_ATTRIBUTE__ID_ATTRIBUTE:
				setIdAttribute(ID_ATTRIBUTE_EDEFAULT);
				return;
			case ImPackage.OBSERVABLE_ATTRIBUTE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE:
				setObservableAttributeExtractRule((ExtractDataRule)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ImPackage.OBSERVABLE_ATTRIBUTE__ID_ATTRIBUTE:
				return ID_ATTRIBUTE_EDEFAULT == null ? idAttribute != null : !ID_ATTRIBUTE_EDEFAULT.equals(idAttribute);
			case ImPackage.OBSERVABLE_ATTRIBUTE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ImPackage.OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE:
				return observableAttributeExtractRule != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (idAttribute: ");
		result.append(idAttribute);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ObservableAttributeImpl
