/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Observable Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.ObservableAttribute#getIdAttribute <em>Id Attribute</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ObservableAttribute#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ObservableAttribute#getObservableAttributeExtractRule <em>Observable Attribute Extract Rule</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getObservableAttribute()
 * @model
 * @generated
 */
public interface ObservableAttribute extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Attribute</em>' attribute.
	 * @see #setIdAttribute(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getObservableAttribute_IdAttribute()
	 * @model
	 * @generated
	 */
	String getIdAttribute();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ObservableAttribute#getIdAttribute <em>Id Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Attribute</em>' attribute.
	 * @see #getIdAttribute()
	 * @generated
	 */
	void setIdAttribute(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getObservableAttribute_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ObservableAttribute#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Observable Attribute Extract Rule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observable Attribute Extract Rule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observable Attribute Extract Rule</em>' containment reference.
	 * @see #setObservableAttributeExtractRule(ExtractDataRule)
	 * @see org.eclipse.soa.mangrove.ImPackage#getObservableAttribute_ObservableAttributeExtractRule()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ExtractDataRule getObservableAttributeExtractRule();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ObservableAttribute#getObservableAttributeExtractRule <em>Observable Attribute Extract Rule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observable Attribute Extract Rule</em>' containment reference.
	 * @see #getObservableAttributeExtractRule()
	 * @generated
	 */
	void setObservableAttributeExtractRule(ExtractDataRule value);

} // ObservableAttribute
