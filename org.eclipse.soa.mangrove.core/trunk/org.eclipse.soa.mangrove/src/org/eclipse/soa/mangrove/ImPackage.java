/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.soa.mangrove.ImFactory
 * @model kind="package"
 * @generated
 */
public interface ImPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mangrove";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://eclipse.org/soa/mangrove";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mangrove";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ImPackage eINSTANCE = org.eclipse.soa.mangrove.impl.ImPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ConfigurableElementImpl <em>Configurable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ConfigurableElementImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getConfigurableElement()
	 * @generated
	 */
	int CONFIGURABLE_ELEMENT = 21;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_ELEMENT__PROPERTIES = 0;

	/**
	 * The number of structural features of the '<em>Configurable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ServiceImpl <em>Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ServiceImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getService()
	 * @generated
	 */
	int SERVICE = 2;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__PROPERTIES = CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Needs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__NEEDS = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is owned</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__IS_OWNED = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Service Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__SERVICE_NAME = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Service Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__SERVICE_TYPE = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__BINDINGS = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE__ENDPOINTS = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FEATURE_COUNT = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ProcessImpl <em>Process</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ProcessImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getProcess()
	 * @generated
	 */
	int PROCESS = 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__PROPERTIES = SERVICE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Needs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__NEEDS = SERVICE__NEEDS;

	/**
	 * The feature id for the '<em><b>Is owned</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__IS_OWNED = SERVICE__IS_OWNED;

	/**
	 * The feature id for the '<em><b>Service Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__SERVICE_NAME = SERVICE__SERVICE_NAME;

	/**
	 * The feature id for the '<em><b>Service Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__SERVICE_TYPE = SERVICE__SERVICE_TYPE;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__BINDINGS = SERVICE__BINDINGS;

	/**
	 * The feature id for the '<em><b>Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__ENDPOINTS = SERVICE__ENDPOINTS;

	/**
	 * The feature id for the '<em><b>Steps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__STEPS = SERVICE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__NAME = SERVICE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__DESCRIPTION = SERVICE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__TRANSITIONS = SERVICE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Process Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__PROCESS_VARIABLES = SERVICE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Process</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_FEATURE_COUNT = SERVICE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.StepImpl <em>Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.StepImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getStep()
	 * @generated
	 */
	int STEP = 1;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP__PROPERTIES = CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Service Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP__SERVICE_MODEL = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP__NAME = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP__DESCRIPTION = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Source Transitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP__SOURCE_TRANSITIONS = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Target Transitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP__TARGET_TRANSITIONS = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Stepbindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP__STEPBINDINGS = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Observable Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP__OBSERVABLE_ATTRIBUTES = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_FEATURE_COUNT = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ServiceClassificationImpl <em>Service Classification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ServiceClassificationImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getServiceClassification()
	 * @generated
	 */
	int SERVICE_CLASSIFICATION = 3;

	/**
	 * The feature id for the '<em><b>Service Classification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CLASSIFICATION__SERVICE_CLASSIFICATION = 0;

	/**
	 * The number of structural features of the '<em>Service Classification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CLASSIFICATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.TransitionImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 4;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__PROPERTIES = CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ATTRIBUTES = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.OwnerImpl <em>Owner</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.OwnerImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getOwner()
	 * @generated
	 */
	int OWNER = 5;

	/**
	 * The feature id for the '<em><b>Owns</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OWNER__OWNS = 0;

	/**
	 * The number of structural features of the '<em>Owner</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OWNER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.TransitionUnderConditionImpl <em>Transition Under Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.TransitionUnderConditionImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getTransitionUnderCondition()
	 * @generated
	 */
	int TRANSITION_UNDER_CONDITION = 6;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_UNDER_CONDITION__PROPERTIES = TRANSITION__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_UNDER_CONDITION__ATTRIBUTES = TRANSITION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_UNDER_CONDITION__SOURCE = TRANSITION__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_UNDER_CONDITION__TARGET = TRANSITION__TARGET;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_UNDER_CONDITION__CONDITION = TRANSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Transition Under Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_UNDER_CONDITION_FEATURE_COUNT = TRANSITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ObservableAttributeImpl <em>Observable Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ObservableAttributeImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getObservableAttribute()
	 * @generated
	 */
	int OBSERVABLE_ATTRIBUTE = 7;

	/**
	 * The feature id for the '<em><b>Id Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ATTRIBUTE__ID_ATTRIBUTE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ATTRIBUTE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Observable Attribute Extract Rule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE = 2;

	/**
	 * The number of structural features of the '<em>Observable Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ATTRIBUTE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.PropertyImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 8;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__KEY = 0;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ProcessCollectionImpl <em>Process Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ProcessCollectionImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getProcessCollection()
	 * @generated
	 */
	int PROCESS_COLLECTION = 9;

	/**
	 * The feature id for the '<em><b>Pool Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_COLLECTION__POOL_NAME = 0;

	/**
	 * The feature id for the '<em><b>Processes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_COLLECTION__PROCESSES = 1;

	/**
	 * The number of structural features of the '<em>Process Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_COLLECTION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ServiceBindingImpl <em>Service Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ServiceBindingImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getServiceBinding()
	 * @generated
	 */
	int SERVICE_BINDING = 10;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_BINDING__PROPERTIES = CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_BINDING__NAME = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_BINDING__DESCRIPTION = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Service Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_BINDING_FEATURE_COUNT = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ControlServiceImpl <em>Control Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ControlServiceImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getControlService()
	 * @generated
	 */
	int CONTROL_SERVICE = 11;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_SERVICE__PROPERTIES = SERVICE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Needs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_SERVICE__NEEDS = SERVICE__NEEDS;

	/**
	 * The feature id for the '<em><b>Is owned</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_SERVICE__IS_OWNED = SERVICE__IS_OWNED;

	/**
	 * The feature id for the '<em><b>Service Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_SERVICE__SERVICE_NAME = SERVICE__SERVICE_NAME;

	/**
	 * The feature id for the '<em><b>Service Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_SERVICE__SERVICE_TYPE = SERVICE__SERVICE_TYPE;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_SERVICE__BINDINGS = SERVICE__BINDINGS;

	/**
	 * The feature id for the '<em><b>Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_SERVICE__ENDPOINTS = SERVICE__ENDPOINTS;

	/**
	 * The number of structural features of the '<em>Control Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_SERVICE_FEATURE_COUNT = SERVICE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.RouterControlImpl <em>Router Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.RouterControlImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getRouterControl()
	 * @generated
	 */
	int ROUTER_CONTROL = 12;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTER_CONTROL__PROPERTIES = CONTROL_SERVICE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Needs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTER_CONTROL__NEEDS = CONTROL_SERVICE__NEEDS;

	/**
	 * The feature id for the '<em><b>Is owned</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTER_CONTROL__IS_OWNED = CONTROL_SERVICE__IS_OWNED;

	/**
	 * The feature id for the '<em><b>Service Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTER_CONTROL__SERVICE_NAME = CONTROL_SERVICE__SERVICE_NAME;

	/**
	 * The feature id for the '<em><b>Service Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTER_CONTROL__SERVICE_TYPE = CONTROL_SERVICE__SERVICE_TYPE;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTER_CONTROL__BINDINGS = CONTROL_SERVICE__BINDINGS;

	/**
	 * The feature id for the '<em><b>Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTER_CONTROL__ENDPOINTS = CONTROL_SERVICE__ENDPOINTS;

	/**
	 * The number of structural features of the '<em>Router Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROUTER_CONTROL_FEATURE_COUNT = CONTROL_SERVICE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.SplitControlImpl <em>Split Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.SplitControlImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getSplitControl()
	 * @generated
	 */
	int SPLIT_CONTROL = 13;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPLIT_CONTROL__PROPERTIES = CONTROL_SERVICE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Needs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPLIT_CONTROL__NEEDS = CONTROL_SERVICE__NEEDS;

	/**
	 * The feature id for the '<em><b>Is owned</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPLIT_CONTROL__IS_OWNED = CONTROL_SERVICE__IS_OWNED;

	/**
	 * The feature id for the '<em><b>Service Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPLIT_CONTROL__SERVICE_NAME = CONTROL_SERVICE__SERVICE_NAME;

	/**
	 * The feature id for the '<em><b>Service Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPLIT_CONTROL__SERVICE_TYPE = CONTROL_SERVICE__SERVICE_TYPE;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPLIT_CONTROL__BINDINGS = CONTROL_SERVICE__BINDINGS;

	/**
	 * The feature id for the '<em><b>Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPLIT_CONTROL__ENDPOINTS = CONTROL_SERVICE__ENDPOINTS;

	/**
	 * The number of structural features of the '<em>Split Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPLIT_CONTROL_FEATURE_COUNT = CONTROL_SERVICE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.JoinControlImpl <em>Join Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.JoinControlImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getJoinControl()
	 * @generated
	 */
	int JOIN_CONTROL = 14;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_CONTROL__PROPERTIES = CONTROL_SERVICE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Needs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_CONTROL__NEEDS = CONTROL_SERVICE__NEEDS;

	/**
	 * The feature id for the '<em><b>Is owned</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_CONTROL__IS_OWNED = CONTROL_SERVICE__IS_OWNED;

	/**
	 * The feature id for the '<em><b>Service Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_CONTROL__SERVICE_NAME = CONTROL_SERVICE__SERVICE_NAME;

	/**
	 * The feature id for the '<em><b>Service Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_CONTROL__SERVICE_TYPE = CONTROL_SERVICE__SERVICE_TYPE;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_CONTROL__BINDINGS = CONTROL_SERVICE__BINDINGS;

	/**
	 * The feature id for the '<em><b>Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_CONTROL__ENDPOINTS = CONTROL_SERVICE__ENDPOINTS;

	/**
	 * The number of structural features of the '<em>Join Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_CONTROL_FEATURE_COUNT = CONTROL_SERVICE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.IterationControlImpl <em>Iteration Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.IterationControlImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getIterationControl()
	 * @generated
	 */
	int ITERATION_CONTROL = 15;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_CONTROL__PROPERTIES = CONTROL_SERVICE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Needs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_CONTROL__NEEDS = CONTROL_SERVICE__NEEDS;

	/**
	 * The feature id for the '<em><b>Is owned</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_CONTROL__IS_OWNED = CONTROL_SERVICE__IS_OWNED;

	/**
	 * The feature id for the '<em><b>Service Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_CONTROL__SERVICE_NAME = CONTROL_SERVICE__SERVICE_NAME;

	/**
	 * The feature id for the '<em><b>Service Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_CONTROL__SERVICE_TYPE = CONTROL_SERVICE__SERVICE_TYPE;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_CONTROL__BINDINGS = CONTROL_SERVICE__BINDINGS;

	/**
	 * The feature id for the '<em><b>Endpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_CONTROL__ENDPOINTS = CONTROL_SERVICE__ENDPOINTS;

	/**
	 * The number of structural features of the '<em>Iteration Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_CONTROL_FEATURE_COUNT = CONTROL_SERVICE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.StpIntermediateModelImpl <em>Stp Intermediate Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.StpIntermediateModelImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getStpIntermediateModel()
	 * @generated
	 */
	int STP_INTERMEDIATE_MODEL = 16;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STP_INTERMEDIATE_MODEL__PROPERTIES = CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Process Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Servicebindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STP_INTERMEDIATE_MODEL__SERVICEBINDINGS = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Service Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Stp Intermediate Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STP_INTERMEDIATE_MODEL_FEATURE_COUNT = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ServiceCollectionImpl <em>Service Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ServiceCollectionImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getServiceCollection()
	 * @generated
	 */
	int SERVICE_COLLECTION = 17;

	/**
	 * The feature id for the '<em><b>Services</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_COLLECTION__SERVICES = 0;

	/**
	 * The number of structural features of the '<em>Service Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_COLLECTION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.BasicPropertyImpl <em>Basic Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.BasicPropertyImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getBasicProperty()
	 * @generated
	 */
	int BASIC_PROPERTY = 18;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROPERTY__KEY = PROPERTY__KEY;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROPERTY__VALUE = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Basic Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.MapPropertyImpl <em>Map Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.MapPropertyImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getMapProperty()
	 * @generated
	 */
	int MAP_PROPERTY = 19;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_PROPERTY__KEY = PROPERTY__KEY;

	/**
	 * The feature id for the '<em><b>Value</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_PROPERTY__VALUE = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Map Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ContractImpl <em>Contract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ContractImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getContract()
	 * @generated
	 */
	int CONTRACT = 20;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT__KEY = PROPERTY__KEY;

	/**
	 * The number of structural features of the '<em>Contract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.StringToPropertyMapEntryImpl <em>String To Property Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.StringToPropertyMapEntryImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getStringToPropertyMapEntry()
	 * @generated
	 */
	int STRING_TO_PROPERTY_MAP_ENTRY = 22;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_PROPERTY_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_PROPERTY_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To Property Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_PROPERTY_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.EndpointImpl <em>Endpoint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.EndpointImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getEndpoint()
	 * @generated
	 */
	int ENDPOINT = 23;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT__PROPERTIES = CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Endpointbinding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT__ENDPOINTBINDING = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT__URI = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Endpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT_FEATURE_COUNT = CONFIGURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.Condition <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.Condition
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 24;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.PropertyConditionImpl <em>Property Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.PropertyConditionImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getPropertyCondition()
	 * @generated
	 */
	int PROPERTY_CONDITION = 25;

	/**
	 * The feature id for the '<em><b>Property Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_CONDITION__PROPERTY_NAME = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_CONDITION__OPERATOR = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Property Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_CONDITION__PROPERTY_VALUE = CONDITION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Property Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_CONDITION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ExpressionConditionImpl <em>Expression Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ExpressionConditionImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getExpressionCondition()
	 * @generated
	 */
	int EXPRESSION_CONDITION = 26;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_CONDITION__EXPRESSION = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_CONDITION__EXPRESSION_LANGUAGE = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Expression Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_CONDITION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl <em>Extract Data Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getExtractDataRule()
	 * @generated
	 */
	int EXTRACT_DATA_RULE = 27;

	/**
	 * The feature id for the '<em><b>Id Rule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_DATA_RULE__ID_RULE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_DATA_RULE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Flag Evaluate Under Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_DATA_RULE__FLAG_EVALUATE_UNDER_CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_DATA_RULE__CONDITION = 3;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_DATA_RULE__EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_DATA_RULE__EXPRESSION_LANGUAGE = 5;

	/**
	 * The feature id for the '<em><b>Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_DATA_RULE__VARIABLE_NAME = 6;

	/**
	 * The number of structural features of the '<em>Extract Data Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_DATA_RULE_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.eclipse.soa.mangrove.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.soa.mangrove.impl.VariableImpl
	 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 28;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Process <em>Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process</em>'.
	 * @see org.eclipse.soa.mangrove.Process
	 * @generated
	 */
	EClass getProcess();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.Process#getSteps <em>Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Steps</em>'.
	 * @see org.eclipse.soa.mangrove.Process#getSteps()
	 * @see #getProcess()
	 * @generated
	 */
	EReference getProcess_Steps();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Process#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.soa.mangrove.Process#getName()
	 * @see #getProcess()
	 * @generated
	 */
	EAttribute getProcess_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Process#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.soa.mangrove.Process#getDescription()
	 * @see #getProcess()
	 * @generated
	 */
	EAttribute getProcess_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.Process#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see org.eclipse.soa.mangrove.Process#getTransitions()
	 * @see #getProcess()
	 * @generated
	 */
	EReference getProcess_Transitions();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.Process#getProcessVariables <em>Process Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Process Variables</em>'.
	 * @see org.eclipse.soa.mangrove.Process#getProcessVariables()
	 * @see #getProcess()
	 * @generated
	 */
	EReference getProcess_ProcessVariables();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Step <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Step</em>'.
	 * @see org.eclipse.soa.mangrove.Step
	 * @generated
	 */
	EClass getStep();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.soa.mangrove.Step#getServiceModel <em>Service Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Service Model</em>'.
	 * @see org.eclipse.soa.mangrove.Step#getServiceModel()
	 * @see #getStep()
	 * @generated
	 */
	EReference getStep_ServiceModel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Step#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.soa.mangrove.Step#getName()
	 * @see #getStep()
	 * @generated
	 */
	EAttribute getStep_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Step#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.soa.mangrove.Step#getDescription()
	 * @see #getStep()
	 * @generated
	 */
	EAttribute getStep_Description();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.soa.mangrove.Step#getSourceTransitions <em>Source Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source Transitions</em>'.
	 * @see org.eclipse.soa.mangrove.Step#getSourceTransitions()
	 * @see #getStep()
	 * @generated
	 */
	EReference getStep_SourceTransitions();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.soa.mangrove.Step#getTargetTransitions <em>Target Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target Transitions</em>'.
	 * @see org.eclipse.soa.mangrove.Step#getTargetTransitions()
	 * @see #getStep()
	 * @generated
	 */
	EReference getStep_TargetTransitions();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.soa.mangrove.Step#getStepbindings <em>Stepbindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Stepbindings</em>'.
	 * @see org.eclipse.soa.mangrove.Step#getStepbindings()
	 * @see #getStep()
	 * @generated
	 */
	EReference getStep_Stepbindings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.Step#getObservableAttributes <em>Observable Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Observable Attributes</em>'.
	 * @see org.eclipse.soa.mangrove.Step#getObservableAttributes()
	 * @see #getStep()
	 * @generated
	 */
	EReference getStep_ObservableAttributes();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Service <em>Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service</em>'.
	 * @see org.eclipse.soa.mangrove.Service
	 * @generated
	 */
	EClass getService();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.soa.mangrove.Service#getNeeds <em>Needs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Needs</em>'.
	 * @see org.eclipse.soa.mangrove.Service#getNeeds()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_Needs();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.soa.mangrove.Service#getIs_owned <em>Is owned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Is owned</em>'.
	 * @see org.eclipse.soa.mangrove.Service#getIs_owned()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_Is_owned();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Service#getServiceName <em>Service Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Service Name</em>'.
	 * @see org.eclipse.soa.mangrove.Service#getServiceName()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_ServiceName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Service#getServiceType <em>Service Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Service Type</em>'.
	 * @see org.eclipse.soa.mangrove.Service#getServiceType()
	 * @see #getService()
	 * @generated
	 */
	EAttribute getService_ServiceType();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.soa.mangrove.Service#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bindings</em>'.
	 * @see org.eclipse.soa.mangrove.Service#getBindings()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_Bindings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.Service#getEndpoints <em>Endpoints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Endpoints</em>'.
	 * @see org.eclipse.soa.mangrove.Service#getEndpoints()
	 * @see #getService()
	 * @generated
	 */
	EReference getService_Endpoints();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ServiceClassification <em>Service Classification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Classification</em>'.
	 * @see org.eclipse.soa.mangrove.ServiceClassification
	 * @generated
	 */
	EClass getServiceClassification();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.ServiceClassification#getServiceClassification <em>Service Classification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Service Classification</em>'.
	 * @see org.eclipse.soa.mangrove.ServiceClassification#getServiceClassification()
	 * @see #getServiceClassification()
	 * @generated
	 */
	EReference getServiceClassification_ServiceClassification();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see org.eclipse.soa.mangrove.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.Transition#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see org.eclipse.soa.mangrove.Transition#getAttributes()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Attributes();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.soa.mangrove.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.soa.mangrove.Transition#getSource()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.soa.mangrove.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.soa.mangrove.Transition#getTarget()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Owner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Owner</em>'.
	 * @see org.eclipse.soa.mangrove.Owner
	 * @generated
	 */
	EClass getOwner();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.soa.mangrove.Owner#getOwns <em>Owns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Owns</em>'.
	 * @see org.eclipse.soa.mangrove.Owner#getOwns()
	 * @see #getOwner()
	 * @generated
	 */
	EReference getOwner_Owns();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.TransitionUnderCondition <em>Transition Under Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition Under Condition</em>'.
	 * @see org.eclipse.soa.mangrove.TransitionUnderCondition
	 * @generated
	 */
	EClass getTransitionUnderCondition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.soa.mangrove.TransitionUnderCondition#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.soa.mangrove.TransitionUnderCondition#getCondition()
	 * @see #getTransitionUnderCondition()
	 * @generated
	 */
	EReference getTransitionUnderCondition_Condition();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ObservableAttribute <em>Observable Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Observable Attribute</em>'.
	 * @see org.eclipse.soa.mangrove.ObservableAttribute
	 * @generated
	 */
	EClass getObservableAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ObservableAttribute#getIdAttribute <em>Id Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Attribute</em>'.
	 * @see org.eclipse.soa.mangrove.ObservableAttribute#getIdAttribute()
	 * @see #getObservableAttribute()
	 * @generated
	 */
	EAttribute getObservableAttribute_IdAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ObservableAttribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.soa.mangrove.ObservableAttribute#getName()
	 * @see #getObservableAttribute()
	 * @generated
	 */
	EAttribute getObservableAttribute_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.soa.mangrove.ObservableAttribute#getObservableAttributeExtractRule <em>Observable Attribute Extract Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Observable Attribute Extract Rule</em>'.
	 * @see org.eclipse.soa.mangrove.ObservableAttribute#getObservableAttributeExtractRule()
	 * @see #getObservableAttribute()
	 * @generated
	 */
	EReference getObservableAttribute_ObservableAttributeExtractRule();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.eclipse.soa.mangrove.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Property#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.eclipse.soa.mangrove.Property#getKey()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Key();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ProcessCollection <em>Process Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Collection</em>'.
	 * @see org.eclipse.soa.mangrove.ProcessCollection
	 * @generated
	 */
	EClass getProcessCollection();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ProcessCollection#getPoolName <em>Pool Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pool Name</em>'.
	 * @see org.eclipse.soa.mangrove.ProcessCollection#getPoolName()
	 * @see #getProcessCollection()
	 * @generated
	 */
	EAttribute getProcessCollection_PoolName();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.ProcessCollection#getProcesses <em>Processes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Processes</em>'.
	 * @see org.eclipse.soa.mangrove.ProcessCollection#getProcesses()
	 * @see #getProcessCollection()
	 * @generated
	 */
	EReference getProcessCollection_Processes();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ServiceBinding <em>Service Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Binding</em>'.
	 * @see org.eclipse.soa.mangrove.ServiceBinding
	 * @generated
	 */
	EClass getServiceBinding();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ServiceBinding#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.soa.mangrove.ServiceBinding#getName()
	 * @see #getServiceBinding()
	 * @generated
	 */
	EAttribute getServiceBinding_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ServiceBinding#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.soa.mangrove.ServiceBinding#getDescription()
	 * @see #getServiceBinding()
	 * @generated
	 */
	EAttribute getServiceBinding_Description();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ControlService <em>Control Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Service</em>'.
	 * @see org.eclipse.soa.mangrove.ControlService
	 * @generated
	 */
	EClass getControlService();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.RouterControl <em>Router Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Router Control</em>'.
	 * @see org.eclipse.soa.mangrove.RouterControl
	 * @generated
	 */
	EClass getRouterControl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.SplitControl <em>Split Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Split Control</em>'.
	 * @see org.eclipse.soa.mangrove.SplitControl
	 * @generated
	 */
	EClass getSplitControl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.JoinControl <em>Join Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Join Control</em>'.
	 * @see org.eclipse.soa.mangrove.JoinControl
	 * @generated
	 */
	EClass getJoinControl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.IterationControl <em>Iteration Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iteration Control</em>'.
	 * @see org.eclipse.soa.mangrove.IterationControl
	 * @generated
	 */
	EClass getIterationControl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.StpIntermediateModel <em>Stp Intermediate Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stp Intermediate Model</em>'.
	 * @see org.eclipse.soa.mangrove.StpIntermediateModel
	 * @generated
	 */
	EClass getStpIntermediateModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.soa.mangrove.StpIntermediateModel#getProcessCollection <em>Process Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Process Collection</em>'.
	 * @see org.eclipse.soa.mangrove.StpIntermediateModel#getProcessCollection()
	 * @see #getStpIntermediateModel()
	 * @generated
	 */
	EReference getStpIntermediateModel_ProcessCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.StpIntermediateModel#getServicebindings <em>Servicebindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Servicebindings</em>'.
	 * @see org.eclipse.soa.mangrove.StpIntermediateModel#getServicebindings()
	 * @see #getStpIntermediateModel()
	 * @generated
	 */
	EReference getStpIntermediateModel_Servicebindings();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.soa.mangrove.StpIntermediateModel#getServiceCollection <em>Service Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Service Collection</em>'.
	 * @see org.eclipse.soa.mangrove.StpIntermediateModel#getServiceCollection()
	 * @see #getStpIntermediateModel()
	 * @generated
	 */
	EReference getStpIntermediateModel_ServiceCollection();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ServiceCollection <em>Service Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Collection</em>'.
	 * @see org.eclipse.soa.mangrove.ServiceCollection
	 * @generated
	 */
	EClass getServiceCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.soa.mangrove.ServiceCollection#getServices <em>Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Services</em>'.
	 * @see org.eclipse.soa.mangrove.ServiceCollection#getServices()
	 * @see #getServiceCollection()
	 * @generated
	 */
	EReference getServiceCollection_Services();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.BasicProperty <em>Basic Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Property</em>'.
	 * @see org.eclipse.soa.mangrove.BasicProperty
	 * @generated
	 */
	EClass getBasicProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.BasicProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.soa.mangrove.BasicProperty#getValue()
	 * @see #getBasicProperty()
	 * @generated
	 */
	EAttribute getBasicProperty_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.MapProperty <em>Map Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map Property</em>'.
	 * @see org.eclipse.soa.mangrove.MapProperty
	 * @generated
	 */
	EClass getMapProperty();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.soa.mangrove.MapProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Value</em>'.
	 * @see org.eclipse.soa.mangrove.MapProperty#getValue()
	 * @see #getMapProperty()
	 * @generated
	 */
	EReference getMapProperty_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Contract <em>Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contract</em>'.
	 * @see org.eclipse.soa.mangrove.Contract
	 * @generated
	 */
	EClass getContract();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ConfigurableElement <em>Configurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configurable Element</em>'.
	 * @see org.eclipse.soa.mangrove.ConfigurableElement
	 * @generated
	 */
	EClass getConfigurableElement();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.soa.mangrove.ConfigurableElement#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Properties</em>'.
	 * @see org.eclipse.soa.mangrove.ConfigurableElement#getProperties()
	 * @see #getConfigurableElement()
	 * @generated
	 */
	EReference getConfigurableElement_Properties();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Property Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To Property Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDefault="" keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueType="org.eclipse.soa.mangrove.Property" valueContainment="true"
	 * @generated
	 */
	EClass getStringToPropertyMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToPropertyMapEntry()
	 * @generated
	 */
	EAttribute getStringToPropertyMapEntry_Key();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToPropertyMapEntry()
	 * @generated
	 */
	EReference getStringToPropertyMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Endpoint <em>Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Endpoint</em>'.
	 * @see org.eclipse.soa.mangrove.Endpoint
	 * @generated
	 */
	EClass getEndpoint();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.soa.mangrove.Endpoint#getEndpointbinding <em>Endpointbinding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Endpointbinding</em>'.
	 * @see org.eclipse.soa.mangrove.Endpoint#getEndpointbinding()
	 * @see #getEndpoint()
	 * @generated
	 */
	EReference getEndpoint_Endpointbinding();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Endpoint#getURI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URI</em>'.
	 * @see org.eclipse.soa.mangrove.Endpoint#getURI()
	 * @see #getEndpoint()
	 * @generated
	 */
	EAttribute getEndpoint_URI();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see org.eclipse.soa.mangrove.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.PropertyCondition <em>Property Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Condition</em>'.
	 * @see org.eclipse.soa.mangrove.PropertyCondition
	 * @generated
	 */
	EClass getPropertyCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.PropertyCondition#getPropertyName <em>Property Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Name</em>'.
	 * @see org.eclipse.soa.mangrove.PropertyCondition#getPropertyName()
	 * @see #getPropertyCondition()
	 * @generated
	 */
	EAttribute getPropertyCondition_PropertyName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.PropertyCondition#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see org.eclipse.soa.mangrove.PropertyCondition#getOperator()
	 * @see #getPropertyCondition()
	 * @generated
	 */
	EAttribute getPropertyCondition_Operator();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.PropertyCondition#getPropertyValue <em>Property Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Value</em>'.
	 * @see org.eclipse.soa.mangrove.PropertyCondition#getPropertyValue()
	 * @see #getPropertyCondition()
	 * @generated
	 */
	EAttribute getPropertyCondition_PropertyValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ExpressionCondition <em>Expression Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Condition</em>'.
	 * @see org.eclipse.soa.mangrove.ExpressionCondition
	 * @generated
	 */
	EClass getExpressionCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ExpressionCondition#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see org.eclipse.soa.mangrove.ExpressionCondition#getExpression()
	 * @see #getExpressionCondition()
	 * @generated
	 */
	EAttribute getExpressionCondition_Expression();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ExpressionCondition#getExpressionLanguage <em>Expression Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression Language</em>'.
	 * @see org.eclipse.soa.mangrove.ExpressionCondition#getExpressionLanguage()
	 * @see #getExpressionCondition()
	 * @generated
	 */
	EAttribute getExpressionCondition_ExpressionLanguage();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.ExtractDataRule <em>Extract Data Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extract Data Rule</em>'.
	 * @see org.eclipse.soa.mangrove.ExtractDataRule
	 * @generated
	 */
	EClass getExtractDataRule();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ExtractDataRule#getIdRule <em>Id Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Rule</em>'.
	 * @see org.eclipse.soa.mangrove.ExtractDataRule#getIdRule()
	 * @see #getExtractDataRule()
	 * @generated
	 */
	EAttribute getExtractDataRule_IdRule();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ExtractDataRule#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.soa.mangrove.ExtractDataRule#getName()
	 * @see #getExtractDataRule()
	 * @generated
	 */
	EAttribute getExtractDataRule_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ExtractDataRule#isFlagEvaluateUnderCondition <em>Flag Evaluate Under Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Flag Evaluate Under Condition</em>'.
	 * @see org.eclipse.soa.mangrove.ExtractDataRule#isFlagEvaluateUnderCondition()
	 * @see #getExtractDataRule()
	 * @generated
	 */
	EAttribute getExtractDataRule_FlagEvaluateUnderCondition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.soa.mangrove.ExtractDataRule#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.soa.mangrove.ExtractDataRule#getCondition()
	 * @see #getExtractDataRule()
	 * @generated
	 */
	EReference getExtractDataRule_Condition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ExtractDataRule#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see org.eclipse.soa.mangrove.ExtractDataRule#getExpression()
	 * @see #getExtractDataRule()
	 * @generated
	 */
	EAttribute getExtractDataRule_Expression();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ExtractDataRule#getExpressionLanguage <em>Expression Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression Language</em>'.
	 * @see org.eclipse.soa.mangrove.ExtractDataRule#getExpressionLanguage()
	 * @see #getExtractDataRule()
	 * @generated
	 */
	EAttribute getExtractDataRule_ExpressionLanguage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.ExtractDataRule#getVariableName <em>Variable Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable Name</em>'.
	 * @see org.eclipse.soa.mangrove.ExtractDataRule#getVariableName()
	 * @see #getExtractDataRule()
	 * @generated
	 */
	EAttribute getExtractDataRule_VariableName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.soa.mangrove.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see org.eclipse.soa.mangrove.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.soa.mangrove.Variable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.soa.mangrove.Variable#getName()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Name();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ImFactory getImFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ProcessImpl <em>Process</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ProcessImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getProcess()
		 * @generated
		 */
		EClass PROCESS = eINSTANCE.getProcess();

		/**
		 * The meta object literal for the '<em><b>Steps</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS__STEPS = eINSTANCE.getProcess_Steps();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS__NAME = eINSTANCE.getProcess_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS__DESCRIPTION = eINSTANCE.getProcess_Description();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS__TRANSITIONS = eINSTANCE.getProcess_Transitions();

		/**
		 * The meta object literal for the '<em><b>Process Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS__PROCESS_VARIABLES = eINSTANCE.getProcess_ProcessVariables();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.StepImpl <em>Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.StepImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getStep()
		 * @generated
		 */
		EClass STEP = eINSTANCE.getStep();

		/**
		 * The meta object literal for the '<em><b>Service Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STEP__SERVICE_MODEL = eINSTANCE.getStep_ServiceModel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STEP__NAME = eINSTANCE.getStep_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STEP__DESCRIPTION = eINSTANCE.getStep_Description();

		/**
		 * The meta object literal for the '<em><b>Source Transitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STEP__SOURCE_TRANSITIONS = eINSTANCE.getStep_SourceTransitions();

		/**
		 * The meta object literal for the '<em><b>Target Transitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STEP__TARGET_TRANSITIONS = eINSTANCE.getStep_TargetTransitions();

		/**
		 * The meta object literal for the '<em><b>Stepbindings</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STEP__STEPBINDINGS = eINSTANCE.getStep_Stepbindings();

		/**
		 * The meta object literal for the '<em><b>Observable Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STEP__OBSERVABLE_ATTRIBUTES = eINSTANCE.getStep_ObservableAttributes();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ServiceImpl <em>Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ServiceImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getService()
		 * @generated
		 */
		EClass SERVICE = eINSTANCE.getService();

		/**
		 * The meta object literal for the '<em><b>Needs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__NEEDS = eINSTANCE.getService_Needs();

		/**
		 * The meta object literal for the '<em><b>Is owned</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__IS_OWNED = eINSTANCE.getService_Is_owned();

		/**
		 * The meta object literal for the '<em><b>Service Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE__SERVICE_NAME = eINSTANCE.getService_ServiceName();

		/**
		 * The meta object literal for the '<em><b>Service Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE__SERVICE_TYPE = eINSTANCE.getService_ServiceType();

		/**
		 * The meta object literal for the '<em><b>Bindings</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__BINDINGS = eINSTANCE.getService_Bindings();

		/**
		 * The meta object literal for the '<em><b>Endpoints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE__ENDPOINTS = eINSTANCE.getService_Endpoints();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ServiceClassificationImpl <em>Service Classification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ServiceClassificationImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getServiceClassification()
		 * @generated
		 */
		EClass SERVICE_CLASSIFICATION = eINSTANCE.getServiceClassification();

		/**
		 * The meta object literal for the '<em><b>Service Classification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_CLASSIFICATION__SERVICE_CLASSIFICATION = eINSTANCE.getServiceClassification_ServiceClassification();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.TransitionImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__ATTRIBUTES = eINSTANCE.getTransition_Attributes();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SOURCE = eINSTANCE.getTransition_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET = eINSTANCE.getTransition_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.OwnerImpl <em>Owner</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.OwnerImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getOwner()
		 * @generated
		 */
		EClass OWNER = eINSTANCE.getOwner();

		/**
		 * The meta object literal for the '<em><b>Owns</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OWNER__OWNS = eINSTANCE.getOwner_Owns();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.TransitionUnderConditionImpl <em>Transition Under Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.TransitionUnderConditionImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getTransitionUnderCondition()
		 * @generated
		 */
		EClass TRANSITION_UNDER_CONDITION = eINSTANCE.getTransitionUnderCondition();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION_UNDER_CONDITION__CONDITION = eINSTANCE.getTransitionUnderCondition_Condition();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ObservableAttributeImpl <em>Observable Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ObservableAttributeImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getObservableAttribute()
		 * @generated
		 */
		EClass OBSERVABLE_ATTRIBUTE = eINSTANCE.getObservableAttribute();

		/**
		 * The meta object literal for the '<em><b>Id Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSERVABLE_ATTRIBUTE__ID_ATTRIBUTE = eINSTANCE.getObservableAttribute_IdAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSERVABLE_ATTRIBUTE__NAME = eINSTANCE.getObservableAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Observable Attribute Extract Rule</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVABLE_ATTRIBUTE__OBSERVABLE_ATTRIBUTE_EXTRACT_RULE = eINSTANCE.getObservableAttribute_ObservableAttributeExtractRule();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.PropertyImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__KEY = eINSTANCE.getProperty_Key();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ProcessCollectionImpl <em>Process Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ProcessCollectionImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getProcessCollection()
		 * @generated
		 */
		EClass PROCESS_COLLECTION = eINSTANCE.getProcessCollection();

		/**
		 * The meta object literal for the '<em><b>Pool Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_COLLECTION__POOL_NAME = eINSTANCE.getProcessCollection_PoolName();

		/**
		 * The meta object literal for the '<em><b>Processes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_COLLECTION__PROCESSES = eINSTANCE.getProcessCollection_Processes();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ServiceBindingImpl <em>Service Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ServiceBindingImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getServiceBinding()
		 * @generated
		 */
		EClass SERVICE_BINDING = eINSTANCE.getServiceBinding();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE_BINDING__NAME = eINSTANCE.getServiceBinding_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE_BINDING__DESCRIPTION = eINSTANCE.getServiceBinding_Description();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ControlServiceImpl <em>Control Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ControlServiceImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getControlService()
		 * @generated
		 */
		EClass CONTROL_SERVICE = eINSTANCE.getControlService();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.RouterControlImpl <em>Router Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.RouterControlImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getRouterControl()
		 * @generated
		 */
		EClass ROUTER_CONTROL = eINSTANCE.getRouterControl();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.SplitControlImpl <em>Split Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.SplitControlImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getSplitControl()
		 * @generated
		 */
		EClass SPLIT_CONTROL = eINSTANCE.getSplitControl();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.JoinControlImpl <em>Join Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.JoinControlImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getJoinControl()
		 * @generated
		 */
		EClass JOIN_CONTROL = eINSTANCE.getJoinControl();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.IterationControlImpl <em>Iteration Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.IterationControlImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getIterationControl()
		 * @generated
		 */
		EClass ITERATION_CONTROL = eINSTANCE.getIterationControl();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.StpIntermediateModelImpl <em>Stp Intermediate Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.StpIntermediateModelImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getStpIntermediateModel()
		 * @generated
		 */
		EClass STP_INTERMEDIATE_MODEL = eINSTANCE.getStpIntermediateModel();

		/**
		 * The meta object literal for the '<em><b>Process Collection</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION = eINSTANCE.getStpIntermediateModel_ProcessCollection();

		/**
		 * The meta object literal for the '<em><b>Servicebindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STP_INTERMEDIATE_MODEL__SERVICEBINDINGS = eINSTANCE.getStpIntermediateModel_Servicebindings();

		/**
		 * The meta object literal for the '<em><b>Service Collection</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION = eINSTANCE.getStpIntermediateModel_ServiceCollection();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ServiceCollectionImpl <em>Service Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ServiceCollectionImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getServiceCollection()
		 * @generated
		 */
		EClass SERVICE_COLLECTION = eINSTANCE.getServiceCollection();

		/**
		 * The meta object literal for the '<em><b>Services</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_COLLECTION__SERVICES = eINSTANCE.getServiceCollection_Services();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.BasicPropertyImpl <em>Basic Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.BasicPropertyImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getBasicProperty()
		 * @generated
		 */
		EClass BASIC_PROPERTY = eINSTANCE.getBasicProperty();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_PROPERTY__VALUE = eINSTANCE.getBasicProperty_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.MapPropertyImpl <em>Map Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.MapPropertyImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getMapProperty()
		 * @generated
		 */
		EClass MAP_PROPERTY = eINSTANCE.getMapProperty();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAP_PROPERTY__VALUE = eINSTANCE.getMapProperty_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ContractImpl <em>Contract</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ContractImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getContract()
		 * @generated
		 */
		EClass CONTRACT = eINSTANCE.getContract();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ConfigurableElementImpl <em>Configurable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ConfigurableElementImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getConfigurableElement()
		 * @generated
		 */
		EClass CONFIGURABLE_ELEMENT = eINSTANCE.getConfigurableElement();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURABLE_ELEMENT__PROPERTIES = eINSTANCE.getConfigurableElement_Properties();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.StringToPropertyMapEntryImpl <em>String To Property Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.StringToPropertyMapEntryImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getStringToPropertyMapEntry()
		 * @generated
		 */
		EClass STRING_TO_PROPERTY_MAP_ENTRY = eINSTANCE.getStringToPropertyMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_PROPERTY_MAP_ENTRY__KEY = eINSTANCE.getStringToPropertyMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_TO_PROPERTY_MAP_ENTRY__VALUE = eINSTANCE.getStringToPropertyMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.EndpointImpl <em>Endpoint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.EndpointImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getEndpoint()
		 * @generated
		 */
		EClass ENDPOINT = eINSTANCE.getEndpoint();

		/**
		 * The meta object literal for the '<em><b>Endpointbinding</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENDPOINT__ENDPOINTBINDING = eINSTANCE.getEndpoint_Endpointbinding();

		/**
		 * The meta object literal for the '<em><b>URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENDPOINT__URI = eINSTANCE.getEndpoint_URI();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.Condition <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.Condition
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.PropertyConditionImpl <em>Property Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.PropertyConditionImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getPropertyCondition()
		 * @generated
		 */
		EClass PROPERTY_CONDITION = eINSTANCE.getPropertyCondition();

		/**
		 * The meta object literal for the '<em><b>Property Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_CONDITION__PROPERTY_NAME = eINSTANCE.getPropertyCondition_PropertyName();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_CONDITION__OPERATOR = eINSTANCE.getPropertyCondition_Operator();

		/**
		 * The meta object literal for the '<em><b>Property Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_CONDITION__PROPERTY_VALUE = eINSTANCE.getPropertyCondition_PropertyValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ExpressionConditionImpl <em>Expression Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ExpressionConditionImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getExpressionCondition()
		 * @generated
		 */
		EClass EXPRESSION_CONDITION = eINSTANCE.getExpressionCondition();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION_CONDITION__EXPRESSION = eINSTANCE.getExpressionCondition_Expression();

		/**
		 * The meta object literal for the '<em><b>Expression Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION_CONDITION__EXPRESSION_LANGUAGE = eINSTANCE.getExpressionCondition_ExpressionLanguage();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl <em>Extract Data Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getExtractDataRule()
		 * @generated
		 */
		EClass EXTRACT_DATA_RULE = eINSTANCE.getExtractDataRule();

		/**
		 * The meta object literal for the '<em><b>Id Rule</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRACT_DATA_RULE__ID_RULE = eINSTANCE.getExtractDataRule_IdRule();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRACT_DATA_RULE__NAME = eINSTANCE.getExtractDataRule_Name();

		/**
		 * The meta object literal for the '<em><b>Flag Evaluate Under Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRACT_DATA_RULE__FLAG_EVALUATE_UNDER_CONDITION = eINSTANCE.getExtractDataRule_FlagEvaluateUnderCondition();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTRACT_DATA_RULE__CONDITION = eINSTANCE.getExtractDataRule_Condition();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRACT_DATA_RULE__EXPRESSION = eINSTANCE.getExtractDataRule_Expression();

		/**
		 * The meta object literal for the '<em><b>Expression Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRACT_DATA_RULE__EXPRESSION_LANGUAGE = eINSTANCE.getExtractDataRule_ExpressionLanguage();

		/**
		 * The meta object literal for the '<em><b>Variable Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRACT_DATA_RULE__VARIABLE_NAME = eINSTANCE.getExtractDataRule_VariableName();

		/**
		 * The meta object literal for the '{@link org.eclipse.soa.mangrove.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.soa.mangrove.impl.VariableImpl
		 * @see org.eclipse.soa.mangrove.impl.ImPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE__NAME = eINSTANCE.getVariable_Name();

	}

} //ImPackage
