/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.soa.mangrove.Contract;
import org.eclipse.soa.mangrove.ImPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contract</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ContractImpl extends PropertyImpl implements Contract {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContractImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImPackage.Literals.CONTRACT;
	}

} //ContractImpl
