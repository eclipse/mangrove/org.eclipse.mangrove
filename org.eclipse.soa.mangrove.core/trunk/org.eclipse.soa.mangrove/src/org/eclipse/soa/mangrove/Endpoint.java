/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Endpoint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.Endpoint#getEndpointbinding <em>Endpointbinding</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Endpoint#getURI <em>URI</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getEndpoint()
 * @model
 * @generated
 */
public interface Endpoint extends ConfigurableElement {
	/**
	 * Returns the value of the '<em><b>Endpointbinding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Endpointbinding</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Endpointbinding</em>' reference.
	 * @see #setEndpointbinding(ServiceBinding)
	 * @see org.eclipse.soa.mangrove.ImPackage#getEndpoint_Endpointbinding()
	 * @model
	 * @generated
	 */
	ServiceBinding getEndpointbinding();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Endpoint#getEndpointbinding <em>Endpointbinding</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Endpointbinding</em>' reference.
	 * @see #getEndpointbinding()
	 * @generated
	 */
	void setEndpointbinding(ServiceBinding value);

	/**
	 * Returns the value of the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URI</em>' attribute.
	 * @see #setURI(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getEndpoint_URI()
	 * @model
	 * @generated
	 */
	String getURI();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Endpoint#getURI <em>URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>URI</em>' attribute.
	 * @see #getURI()
	 * @generated
	 */
	void setURI(String value);

} // Endpoint
