/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Classification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.ServiceClassification#getServiceClassification <em>Service Classification</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getServiceClassification()
 * @model
 * @generated
 */
public interface ServiceClassification extends EObject {
	/**
	 * Returns the value of the '<em><b>Service Classification</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.Service}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Classification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Classification</em>' containment reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getServiceClassification_ServiceClassification()
	 * @model containment="true" volatile="true"
	 * @generated
	 */
	EList<Service> getServiceClassification();

} // ServiceClassification
