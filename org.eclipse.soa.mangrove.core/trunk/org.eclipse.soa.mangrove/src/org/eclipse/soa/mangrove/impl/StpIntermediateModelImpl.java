/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.ProcessCollection;
import org.eclipse.soa.mangrove.ServiceBinding;
import org.eclipse.soa.mangrove.ServiceCollection;
import org.eclipse.soa.mangrove.StpIntermediateModel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stp Intermediate Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StpIntermediateModelImpl#getProcessCollection <em>Process Collection</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StpIntermediateModelImpl#getServicebindings <em>Servicebindings</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StpIntermediateModelImpl#getServiceCollection <em>Service Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StpIntermediateModelImpl extends ConfigurableElementImpl implements StpIntermediateModel {
	/**
	 * The cached value of the '{@link #getProcessCollection() <em>Process Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessCollection()
	 * @generated
	 * @ordered
	 */
	protected ProcessCollection processCollection;

	/**
	 * The cached value of the '{@link #getServicebindings() <em>Servicebindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServicebindings()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceBinding> servicebindings;

	/**
	 * The cached value of the '{@link #getServiceCollection() <em>Service Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceCollection()
	 * @generated
	 * @ordered
	 */
	protected ServiceCollection serviceCollection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StpIntermediateModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImPackage.Literals.STP_INTERMEDIATE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessCollection getProcessCollection() {
		return processCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessCollection(ProcessCollection newProcessCollection, NotificationChain msgs) {
		ProcessCollection oldProcessCollection = processCollection;
		processCollection = newProcessCollection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION, oldProcessCollection, newProcessCollection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessCollection(ProcessCollection newProcessCollection) {
		if (newProcessCollection != processCollection) {
			NotificationChain msgs = null;
			if (processCollection != null)
				msgs = ((InternalEObject)processCollection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION, null, msgs);
			if (newProcessCollection != null)
				msgs = ((InternalEObject)newProcessCollection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION, null, msgs);
			msgs = basicSetProcessCollection(newProcessCollection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION, newProcessCollection, newProcessCollection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceBinding> getServicebindings() {
		if (servicebindings == null) {
			servicebindings = new EObjectContainmentEList<ServiceBinding>(ServiceBinding.class, this, ImPackage.STP_INTERMEDIATE_MODEL__SERVICEBINDINGS);
		}
		return servicebindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceCollection getServiceCollection() {
		return serviceCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetServiceCollection(ServiceCollection newServiceCollection, NotificationChain msgs) {
		ServiceCollection oldServiceCollection = serviceCollection;
		serviceCollection = newServiceCollection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION, oldServiceCollection, newServiceCollection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceCollection(ServiceCollection newServiceCollection) {
		if (newServiceCollection != serviceCollection) {
			NotificationChain msgs = null;
			if (serviceCollection != null)
				msgs = ((InternalEObject)serviceCollection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION, null, msgs);
			if (newServiceCollection != null)
				msgs = ((InternalEObject)newServiceCollection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION, null, msgs);
			msgs = basicSetServiceCollection(newServiceCollection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION, newServiceCollection, newServiceCollection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION:
				return basicSetProcessCollection(null, msgs);
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICEBINDINGS:
				return ((InternalEList<?>)getServicebindings()).basicRemove(otherEnd, msgs);
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION:
				return basicSetServiceCollection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION:
				return getProcessCollection();
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICEBINDINGS:
				return getServicebindings();
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION:
				return getServiceCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION:
				setProcessCollection((ProcessCollection)newValue);
				return;
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICEBINDINGS:
				getServicebindings().clear();
				getServicebindings().addAll((Collection<? extends ServiceBinding>)newValue);
				return;
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION:
				setServiceCollection((ServiceCollection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION:
				setProcessCollection((ProcessCollection)null);
				return;
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICEBINDINGS:
				getServicebindings().clear();
				return;
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION:
				setServiceCollection((ServiceCollection)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ImPackage.STP_INTERMEDIATE_MODEL__PROCESS_COLLECTION:
				return processCollection != null;
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICEBINDINGS:
				return servicebindings != null && !servicebindings.isEmpty();
			case ImPackage.STP_INTERMEDIATE_MODEL__SERVICE_COLLECTION:
				return serviceCollection != null;
		}
		return super.eIsSet(featureID);
	}

} //StpIntermediateModelImpl
