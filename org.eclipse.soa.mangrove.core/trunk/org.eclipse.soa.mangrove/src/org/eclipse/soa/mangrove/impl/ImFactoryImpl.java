/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.soa.mangrove.BasicProperty;
import org.eclipse.soa.mangrove.Contract;
import org.eclipse.soa.mangrove.ControlService;
import org.eclipse.soa.mangrove.Endpoint;
import org.eclipse.soa.mangrove.ExpressionCondition;
import org.eclipse.soa.mangrove.ExtractDataRule;
import org.eclipse.soa.mangrove.ImFactory;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.IterationControl;
import org.eclipse.soa.mangrove.JoinControl;
import org.eclipse.soa.mangrove.MapProperty;
import org.eclipse.soa.mangrove.ObservableAttribute;
import org.eclipse.soa.mangrove.Owner;
import org.eclipse.soa.mangrove.ProcessCollection;
import org.eclipse.soa.mangrove.Property;
import org.eclipse.soa.mangrove.PropertyCondition;
import org.eclipse.soa.mangrove.RouterControl;
import org.eclipse.soa.mangrove.Service;
import org.eclipse.soa.mangrove.ServiceBinding;
import org.eclipse.soa.mangrove.ServiceClassification;
import org.eclipse.soa.mangrove.ServiceCollection;
import org.eclipse.soa.mangrove.SplitControl;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.Transition;
import org.eclipse.soa.mangrove.TransitionUnderCondition;
import org.eclipse.soa.mangrove.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ImFactoryImpl extends EFactoryImpl implements ImFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ImFactory init() {
		try {
			ImFactory theImFactory = (ImFactory)EPackage.Registry.INSTANCE.getEFactory("http://eclipse.org/soa/mangrove"); 
			if (theImFactory != null) {
				return theImFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ImFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ImPackage.PROCESS: return createProcess();
			case ImPackage.STEP: return createStep();
			case ImPackage.SERVICE: return createService();
			case ImPackage.SERVICE_CLASSIFICATION: return createServiceClassification();
			case ImPackage.TRANSITION: return createTransition();
			case ImPackage.OWNER: return createOwner();
			case ImPackage.TRANSITION_UNDER_CONDITION: return createTransitionUnderCondition();
			case ImPackage.OBSERVABLE_ATTRIBUTE: return createObservableAttribute();
			case ImPackage.PROPERTY: return createProperty();
			case ImPackage.PROCESS_COLLECTION: return createProcessCollection();
			case ImPackage.SERVICE_BINDING: return createServiceBinding();
			case ImPackage.CONTROL_SERVICE: return createControlService();
			case ImPackage.ROUTER_CONTROL: return createRouterControl();
			case ImPackage.SPLIT_CONTROL: return createSplitControl();
			case ImPackage.JOIN_CONTROL: return createJoinControl();
			case ImPackage.ITERATION_CONTROL: return createIterationControl();
			case ImPackage.STP_INTERMEDIATE_MODEL: return createStpIntermediateModel();
			case ImPackage.SERVICE_COLLECTION: return createServiceCollection();
			case ImPackage.BASIC_PROPERTY: return createBasicProperty();
			case ImPackage.MAP_PROPERTY: return createMapProperty();
			case ImPackage.CONTRACT: return createContract();
			case ImPackage.STRING_TO_PROPERTY_MAP_ENTRY: return (EObject)createStringToPropertyMapEntry();
			case ImPackage.ENDPOINT: return createEndpoint();
			case ImPackage.PROPERTY_CONDITION: return createPropertyCondition();
			case ImPackage.EXPRESSION_CONDITION: return createExpressionCondition();
			case ImPackage.EXTRACT_DATA_RULE: return createExtractDataRule();
			case ImPackage.VARIABLE: return createVariable();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.soa.mangrove.Process createProcess() {
		ProcessImpl process = new ProcessImpl();
		return process;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Step createStep() {
		StepImpl step = new StepImpl();
		return step;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service createService() {
		ServiceImpl service = new ServiceImpl();
		return service;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceClassification createServiceClassification() {
		ServiceClassificationImpl serviceClassification = new ServiceClassificationImpl();
		return serviceClassification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Owner createOwner() {
		OwnerImpl owner = new OwnerImpl();
		return owner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionUnderCondition createTransitionUnderCondition() {
		TransitionUnderConditionImpl transitionUnderCondition = new TransitionUnderConditionImpl();
		return transitionUnderCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObservableAttribute createObservableAttribute() {
		ObservableAttributeImpl observableAttribute = new ObservableAttributeImpl();
		return observableAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessCollection createProcessCollection() {
		ProcessCollectionImpl processCollection = new ProcessCollectionImpl();
		return processCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceBinding createServiceBinding() {
		ServiceBindingImpl serviceBinding = new ServiceBindingImpl();
		return serviceBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlService createControlService() {
		ControlServiceImpl controlService = new ControlServiceImpl();
		return controlService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RouterControl createRouterControl() {
		RouterControlImpl routerControl = new RouterControlImpl();
		return routerControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SplitControl createSplitControl() {
		SplitControlImpl splitControl = new SplitControlImpl();
		return splitControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JoinControl createJoinControl() {
		JoinControlImpl joinControl = new JoinControlImpl();
		return joinControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IterationControl createIterationControl() {
		IterationControlImpl iterationControl = new IterationControlImpl();
		return iterationControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StpIntermediateModel createStpIntermediateModel() {
		StpIntermediateModelImpl stpIntermediateModel = new StpIntermediateModelImpl();
		return stpIntermediateModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceCollection createServiceCollection() {
		ServiceCollectionImpl serviceCollection = new ServiceCollectionImpl();
		return serviceCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicProperty createBasicProperty() {
		BasicPropertyImpl basicProperty = new BasicPropertyImpl();
		return basicProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MapProperty createMapProperty() {
		MapPropertyImpl mapProperty = new MapPropertyImpl();
		return mapProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Contract createContract() {
		ContractImpl contract = new ContractImpl();
		return contract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, Property> createStringToPropertyMapEntry() {
		StringToPropertyMapEntryImpl stringToPropertyMapEntry = new StringToPropertyMapEntryImpl();
		return stringToPropertyMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Endpoint createEndpoint() {
		EndpointImpl endpoint = new EndpointImpl();
		return endpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyCondition createPropertyCondition() {
		PropertyConditionImpl propertyCondition = new PropertyConditionImpl();
		return propertyCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionCondition createExpressionCondition() {
		ExpressionConditionImpl expressionCondition = new ExpressionConditionImpl();
		return expressionCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtractDataRule createExtractDataRule() {
		ExtractDataRuleImpl extractDataRule = new ExtractDataRuleImpl();
		return extractDataRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable createVariable() {
		VariableImpl variable = new VariableImpl();
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImPackage getImPackage() {
		return (ImPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ImPackage getPackage() {
		return ImPackage.eINSTANCE;
	}

} //ImFactoryImpl
