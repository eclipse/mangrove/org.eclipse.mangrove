/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Split Control</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getSplitControl()
 * @model
 * @generated
 */
public interface SplitControl extends ControlService {
} // SplitControl
