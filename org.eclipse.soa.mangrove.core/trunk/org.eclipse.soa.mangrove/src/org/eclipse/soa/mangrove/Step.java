/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.Step#getServiceModel <em>Service Model</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Step#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Step#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Step#getSourceTransitions <em>Source Transitions</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Step#getTargetTransitions <em>Target Transitions</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Step#getStepbindings <em>Stepbindings</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Step#getObservableAttributes <em>Observable Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getStep()
 * @model
 * @generated
 */
public interface Step extends ConfigurableElement {
	/**
	 * Returns the value of the '<em><b>Service Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Model</em>' reference.
	 * @see #setServiceModel(Service)
	 * @see org.eclipse.soa.mangrove.ImPackage#getStep_ServiceModel()
	 * @model
	 * @generated
	 */
	Service getServiceModel();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Step#getServiceModel <em>Service Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Service Model</em>' reference.
	 * @see #getServiceModel()
	 * @generated
	 */
	void setServiceModel(Service value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getStep_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Step#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getStep_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Step#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Source Transitions</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.Transition}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.soa.mangrove.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Transitions</em>' reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getStep_SourceTransitions()
	 * @see org.eclipse.soa.mangrove.Transition#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<Transition> getSourceTransitions();

	/**
	 * Returns the value of the '<em><b>Target Transitions</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.Transition}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.soa.mangrove.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Transitions</em>' reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getStep_TargetTransitions()
	 * @see org.eclipse.soa.mangrove.Transition#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<Transition> getTargetTransitions();

	/**
	 * Returns the value of the '<em><b>Stepbindings</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.ServiceBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stepbindings</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stepbindings</em>' reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getStep_Stepbindings()
	 * @model
	 * @generated
	 */
	EList<ServiceBinding> getStepbindings();

	/**
	 * Returns the value of the '<em><b>Observable Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.ObservableAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observable Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observable Attributes</em>' containment reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getStep_ObservableAttributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ObservableAttribute> getObservableAttributes();

} // Step
