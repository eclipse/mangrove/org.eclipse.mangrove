/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.Transition#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Transition#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Transition#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends ConfigurableElement {
	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.ObservableAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getTransition_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ObservableAttribute> getAttributes();

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.soa.mangrove.Step#getSourceTransitions <em>Source Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Step)
	 * @see org.eclipse.soa.mangrove.ImPackage#getTransition_Source()
	 * @see org.eclipse.soa.mangrove.Step#getSourceTransitions
	 * @model opposite="sourceTransitions" required="true"
	 * @generated
	 */
	Step getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Transition#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Step value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.soa.mangrove.Step#getTargetTransitions <em>Target Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Step)
	 * @see org.eclipse.soa.mangrove.ImPackage#getTransition_Target()
	 * @see org.eclipse.soa.mangrove.Step#getTargetTransitions
	 * @model opposite="targetTransitions" required="true"
	 * @generated
	 */
	Step getTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Transition#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Step value);

} // Transition
