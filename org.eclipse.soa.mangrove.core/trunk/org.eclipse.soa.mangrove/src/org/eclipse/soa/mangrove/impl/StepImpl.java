/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.ObservableAttribute;
import org.eclipse.soa.mangrove.Service;
import org.eclipse.soa.mangrove.ServiceBinding;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StepImpl#getServiceModel <em>Service Model</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StepImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StepImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StepImpl#getSourceTransitions <em>Source Transitions</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StepImpl#getTargetTransitions <em>Target Transitions</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StepImpl#getStepbindings <em>Stepbindings</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.StepImpl#getObservableAttributes <em>Observable Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StepImpl extends ConfigurableElementImpl implements Step {
	/**
	 * The cached value of the '{@link #getServiceModel() <em>Service Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceModel()
	 * @generated
	 * @ordered
	 */
	protected Service serviceModel;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSourceTransitions() <em>Source Transitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> sourceTransitions;

	/**
	 * The cached value of the '{@link #getTargetTransitions() <em>Target Transitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> targetTransitions;

	/**
	 * The cached value of the '{@link #getStepbindings() <em>Stepbindings</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepbindings()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceBinding> stepbindings;

	/**
	 * The cached value of the '{@link #getObservableAttributes() <em>Observable Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservableAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<ObservableAttribute> observableAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImPackage.Literals.STEP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service getServiceModel() {
		if (serviceModel != null && serviceModel.eIsProxy()) {
			InternalEObject oldServiceModel = (InternalEObject)serviceModel;
			serviceModel = (Service)eResolveProxy(oldServiceModel);
			if (serviceModel != oldServiceModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ImPackage.STEP__SERVICE_MODEL, oldServiceModel, serviceModel));
			}
		}
		return serviceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Service basicGetServiceModel() {
		return serviceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceModel(Service newServiceModel) {
		Service oldServiceModel = serviceModel;
		serviceModel = newServiceModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.STEP__SERVICE_MODEL, oldServiceModel, serviceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.STEP__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.STEP__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getSourceTransitions() {
		if (sourceTransitions == null) {
			sourceTransitions = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this, ImPackage.STEP__SOURCE_TRANSITIONS, ImPackage.TRANSITION__SOURCE);
		}
		return sourceTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTargetTransitions() {
		if (targetTransitions == null) {
			targetTransitions = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this, ImPackage.STEP__TARGET_TRANSITIONS, ImPackage.TRANSITION__TARGET);
		}
		return targetTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceBinding> getStepbindings() {
		if (stepbindings == null) {
			stepbindings = new EObjectResolvingEList<ServiceBinding>(ServiceBinding.class, this, ImPackage.STEP__STEPBINDINGS);
		}
		return stepbindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObservableAttribute> getObservableAttributes() {
		if (observableAttributes == null) {
			observableAttributes = new EObjectContainmentEList<ObservableAttribute>(ObservableAttribute.class, this, ImPackage.STEP__OBSERVABLE_ATTRIBUTES);
		}
		return observableAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ImPackage.STEP__SOURCE_TRANSITIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSourceTransitions()).basicAdd(otherEnd, msgs);
			case ImPackage.STEP__TARGET_TRANSITIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTargetTransitions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ImPackage.STEP__SOURCE_TRANSITIONS:
				return ((InternalEList<?>)getSourceTransitions()).basicRemove(otherEnd, msgs);
			case ImPackage.STEP__TARGET_TRANSITIONS:
				return ((InternalEList<?>)getTargetTransitions()).basicRemove(otherEnd, msgs);
			case ImPackage.STEP__OBSERVABLE_ATTRIBUTES:
				return ((InternalEList<?>)getObservableAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ImPackage.STEP__SERVICE_MODEL:
				if (resolve) return getServiceModel();
				return basicGetServiceModel();
			case ImPackage.STEP__NAME:
				return getName();
			case ImPackage.STEP__DESCRIPTION:
				return getDescription();
			case ImPackage.STEP__SOURCE_TRANSITIONS:
				return getSourceTransitions();
			case ImPackage.STEP__TARGET_TRANSITIONS:
				return getTargetTransitions();
			case ImPackage.STEP__STEPBINDINGS:
				return getStepbindings();
			case ImPackage.STEP__OBSERVABLE_ATTRIBUTES:
				return getObservableAttributes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ImPackage.STEP__SERVICE_MODEL:
				setServiceModel((Service)newValue);
				return;
			case ImPackage.STEP__NAME:
				setName((String)newValue);
				return;
			case ImPackage.STEP__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ImPackage.STEP__SOURCE_TRANSITIONS:
				getSourceTransitions().clear();
				getSourceTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case ImPackage.STEP__TARGET_TRANSITIONS:
				getTargetTransitions().clear();
				getTargetTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case ImPackage.STEP__STEPBINDINGS:
				getStepbindings().clear();
				getStepbindings().addAll((Collection<? extends ServiceBinding>)newValue);
				return;
			case ImPackage.STEP__OBSERVABLE_ATTRIBUTES:
				getObservableAttributes().clear();
				getObservableAttributes().addAll((Collection<? extends ObservableAttribute>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ImPackage.STEP__SERVICE_MODEL:
				setServiceModel((Service)null);
				return;
			case ImPackage.STEP__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ImPackage.STEP__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ImPackage.STEP__SOURCE_TRANSITIONS:
				getSourceTransitions().clear();
				return;
			case ImPackage.STEP__TARGET_TRANSITIONS:
				getTargetTransitions().clear();
				return;
			case ImPackage.STEP__STEPBINDINGS:
				getStepbindings().clear();
				return;
			case ImPackage.STEP__OBSERVABLE_ATTRIBUTES:
				getObservableAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ImPackage.STEP__SERVICE_MODEL:
				return serviceModel != null;
			case ImPackage.STEP__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ImPackage.STEP__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ImPackage.STEP__SOURCE_TRANSITIONS:
				return sourceTransitions != null && !sourceTransitions.isEmpty();
			case ImPackage.STEP__TARGET_TRANSITIONS:
				return targetTransitions != null && !targetTransitions.isEmpty();
			case ImPackage.STEP__STEPBINDINGS:
				return stepbindings != null && !stepbindings.isEmpty();
			case ImPackage.STEP__OBSERVABLE_ATTRIBUTES:
				return observableAttributes != null && !observableAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}

} //StepImpl
