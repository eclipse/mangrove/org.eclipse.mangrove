/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stp Intermediate Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.StpIntermediateModel#getProcessCollection <em>Process Collection</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.StpIntermediateModel#getServicebindings <em>Servicebindings</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.StpIntermediateModel#getServiceCollection <em>Service Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getStpIntermediateModel()
 * @model
 * @generated
 */
public interface StpIntermediateModel extends ConfigurableElement {
	/**
	 * Returns the value of the '<em><b>Process Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Collection</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Collection</em>' containment reference.
	 * @see #setProcessCollection(ProcessCollection)
	 * @see org.eclipse.soa.mangrove.ImPackage#getStpIntermediateModel_ProcessCollection()
	 * @model containment="true"
	 * @generated
	 */
	ProcessCollection getProcessCollection();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.StpIntermediateModel#getProcessCollection <em>Process Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Collection</em>' containment reference.
	 * @see #getProcessCollection()
	 * @generated
	 */
	void setProcessCollection(ProcessCollection value);

	/**
	 * Returns the value of the '<em><b>Servicebindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.ServiceBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Servicebindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Servicebindings</em>' containment reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getStpIntermediateModel_Servicebindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<ServiceBinding> getServicebindings();

	/**
	 * Returns the value of the '<em><b>Service Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Collection</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Collection</em>' containment reference.
	 * @see #setServiceCollection(ServiceCollection)
	 * @see org.eclipse.soa.mangrove.ImPackage#getStpIntermediateModel_ServiceCollection()
	 * @model containment="true"
	 * @generated
	 */
	ServiceCollection getServiceCollection();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.StpIntermediateModel#getServiceCollection <em>Service Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Service Collection</em>' containment reference.
	 * @see #getServiceCollection()
	 * @generated
	 */
	void setServiceCollection(ServiceCollection value);

} // StpIntermediateModel
