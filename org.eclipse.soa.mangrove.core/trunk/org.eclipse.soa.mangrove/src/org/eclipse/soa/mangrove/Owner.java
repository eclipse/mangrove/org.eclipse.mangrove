/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Owner</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.Owner#getOwns <em>Owns</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getOwner()
 * @model
 * @generated
 */
public interface Owner extends EObject {
	/**
	 * Returns the value of the '<em><b>Owns</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.Service}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.soa.mangrove.Service#getIs_owned <em>Is owned</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owns</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owns</em>' reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getOwner_Owns()
	 * @see org.eclipse.soa.mangrove.Service#getIs_owned
	 * @model opposite="is_owned"
	 * @generated
	 */
	EList<Service> getOwns();

} // Owner
