/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Collection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.ProcessCollection#getPoolName <em>Pool Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ProcessCollection#getProcesses <em>Processes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getProcessCollection()
 * @model
 * @generated
 */
public interface ProcessCollection extends EObject {
	/**
	 * Returns the value of the '<em><b>Pool Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pool Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pool Name</em>' attribute.
	 * @see #setPoolName(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getProcessCollection_PoolName()
	 * @model
	 * @generated
	 */
	String getPoolName();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ProcessCollection#getPoolName <em>Pool Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pool Name</em>' attribute.
	 * @see #getPoolName()
	 * @generated
	 */
	void setPoolName(String value);

	/**
	 * Returns the value of the '<em><b>Processes</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.Process}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processes</em>' containment reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getProcessCollection_Processes()
	 * @model containment="true"
	 * @generated
	 */
	EList<org.eclipse.soa.mangrove.Process> getProcesses();

} // ProcessCollection
