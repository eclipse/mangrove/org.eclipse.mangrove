/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extract Data Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.ExtractDataRule#getIdRule <em>Id Rule</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ExtractDataRule#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ExtractDataRule#isFlagEvaluateUnderCondition <em>Flag Evaluate Under Condition</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ExtractDataRule#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ExtractDataRule#getExpression <em>Expression</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ExtractDataRule#getExpressionLanguage <em>Expression Language</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.ExtractDataRule#getVariableName <em>Variable Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getExtractDataRule()
 * @model
 * @generated
 */
public interface ExtractDataRule extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Rule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Rule</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Rule</em>' attribute.
	 * @see #setIdRule(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getExtractDataRule_IdRule()
	 * @model
	 * @generated
	 */
	String getIdRule();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ExtractDataRule#getIdRule <em>Id Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Rule</em>' attribute.
	 * @see #getIdRule()
	 * @generated
	 */
	void setIdRule(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getExtractDataRule_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ExtractDataRule#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Flag Evaluate Under Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flag Evaluate Under Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flag Evaluate Under Condition</em>' attribute.
	 * @see #setFlagEvaluateUnderCondition(boolean)
	 * @see org.eclipse.soa.mangrove.ImPackage#getExtractDataRule_FlagEvaluateUnderCondition()
	 * @model
	 * @generated
	 */
	boolean isFlagEvaluateUnderCondition();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ExtractDataRule#isFlagEvaluateUnderCondition <em>Flag Evaluate Under Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Flag Evaluate Under Condition</em>' attribute.
	 * @see #isFlagEvaluateUnderCondition()
	 * @generated
	 */
	void setFlagEvaluateUnderCondition(boolean value);

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(ExpressionCondition)
	 * @see org.eclipse.soa.mangrove.ImPackage#getExtractDataRule_Condition()
	 * @model containment="true"
	 * @generated
	 */
	ExpressionCondition getCondition();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ExtractDataRule#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(ExpressionCondition value);

	/**
	 * Returns the value of the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' attribute.
	 * @see #setExpression(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getExtractDataRule_Expression()
	 * @model
	 * @generated
	 */
	String getExpression();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ExtractDataRule#getExpression <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' attribute.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(String value);

	/**
	 * Returns the value of the '<em><b>Expression Language</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Language</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Language</em>' attribute.
	 * @see #setExpressionLanguage(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getExtractDataRule_ExpressionLanguage()
	 * @model default=""
	 * @generated
	 */
	String getExpressionLanguage();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ExtractDataRule#getExpressionLanguage <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression Language</em>' attribute.
	 * @see #getExpressionLanguage()
	 * @generated
	 */
	void setExpressionLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Name</em>' attribute.
	 * @see #setVariableName(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getExtractDataRule_VariableName()
	 * @model
	 * @generated
	 */
	String getVariableName();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.ExtractDataRule#getVariableName <em>Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable Name</em>' attribute.
	 * @see #getVariableName()
	 * @generated
	 */
	void setVariableName(String value);

} // ExtractDataRule
