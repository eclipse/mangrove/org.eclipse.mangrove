/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.soa.mangrove.ExpressionCondition;
import org.eclipse.soa.mangrove.ExtractDataRule;
import org.eclipse.soa.mangrove.ImPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extract Data Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl#getIdRule <em>Id Rule</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl#isFlagEvaluateUnderCondition <em>Flag Evaluate Under Condition</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl#getExpression <em>Expression</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl#getExpressionLanguage <em>Expression Language</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ExtractDataRuleImpl#getVariableName <em>Variable Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExtractDataRuleImpl extends EObjectImpl implements ExtractDataRule {
	/**
	 * The default value of the '{@link #getIdRule() <em>Id Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdRule()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_RULE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIdRule() <em>Id Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdRule()
	 * @generated
	 * @ordered
	 */
	protected String idRule = ID_RULE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isFlagEvaluateUnderCondition() <em>Flag Evaluate Under Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFlagEvaluateUnderCondition()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FLAG_EVALUATE_UNDER_CONDITION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFlagEvaluateUnderCondition() <em>Flag Evaluate Under Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFlagEvaluateUnderCondition()
	 * @generated
	 * @ordered
	 */
	protected boolean flagEvaluateUnderCondition = FLAG_EVALUATE_UNDER_CONDITION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected ExpressionCondition condition;

	/**
	 * The default value of the '{@link #getExpression() <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPRESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExpression() <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected String expression = EXPRESSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getExpressionLanguage() <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressionLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPRESSION_LANGUAGE_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getExpressionLanguage() <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressionLanguage()
	 * @generated
	 * @ordered
	 */
	protected String expressionLanguage = EXPRESSION_LANGUAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getVariableName() <em>Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableName()
	 * @generated
	 * @ordered
	 */
	protected static final String VARIABLE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVariableName() <em>Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableName()
	 * @generated
	 * @ordered
	 */
	protected String variableName = VARIABLE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtractDataRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImPackage.Literals.EXTRACT_DATA_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIdRule() {
		return idRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdRule(String newIdRule) {
		String oldIdRule = idRule;
		idRule = newIdRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.EXTRACT_DATA_RULE__ID_RULE, oldIdRule, idRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.EXTRACT_DATA_RULE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFlagEvaluateUnderCondition() {
		return flagEvaluateUnderCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFlagEvaluateUnderCondition(boolean newFlagEvaluateUnderCondition) {
		boolean oldFlagEvaluateUnderCondition = flagEvaluateUnderCondition;
		flagEvaluateUnderCondition = newFlagEvaluateUnderCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.EXTRACT_DATA_RULE__FLAG_EVALUATE_UNDER_CONDITION, oldFlagEvaluateUnderCondition, flagEvaluateUnderCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionCondition getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(ExpressionCondition newCondition, NotificationChain msgs) {
		ExpressionCondition oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ImPackage.EXTRACT_DATA_RULE__CONDITION, oldCondition, newCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(ExpressionCondition newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ImPackage.EXTRACT_DATA_RULE__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ImPackage.EXTRACT_DATA_RULE__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.EXTRACT_DATA_RULE__CONDITION, newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression(String newExpression) {
		String oldExpression = expression;
		expression = newExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.EXTRACT_DATA_RULE__EXPRESSION, oldExpression, expression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpressionLanguage() {
		return expressionLanguage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpressionLanguage(String newExpressionLanguage) {
		String oldExpressionLanguage = expressionLanguage;
		expressionLanguage = newExpressionLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.EXTRACT_DATA_RULE__EXPRESSION_LANGUAGE, oldExpressionLanguage, expressionLanguage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVariableName() {
		return variableName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariableName(String newVariableName) {
		String oldVariableName = variableName;
		variableName = newVariableName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.EXTRACT_DATA_RULE__VARIABLE_NAME, oldVariableName, variableName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ImPackage.EXTRACT_DATA_RULE__CONDITION:
				return basicSetCondition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ImPackage.EXTRACT_DATA_RULE__ID_RULE:
				return getIdRule();
			case ImPackage.EXTRACT_DATA_RULE__NAME:
				return getName();
			case ImPackage.EXTRACT_DATA_RULE__FLAG_EVALUATE_UNDER_CONDITION:
				return isFlagEvaluateUnderCondition();
			case ImPackage.EXTRACT_DATA_RULE__CONDITION:
				return getCondition();
			case ImPackage.EXTRACT_DATA_RULE__EXPRESSION:
				return getExpression();
			case ImPackage.EXTRACT_DATA_RULE__EXPRESSION_LANGUAGE:
				return getExpressionLanguage();
			case ImPackage.EXTRACT_DATA_RULE__VARIABLE_NAME:
				return getVariableName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ImPackage.EXTRACT_DATA_RULE__ID_RULE:
				setIdRule((String)newValue);
				return;
			case ImPackage.EXTRACT_DATA_RULE__NAME:
				setName((String)newValue);
				return;
			case ImPackage.EXTRACT_DATA_RULE__FLAG_EVALUATE_UNDER_CONDITION:
				setFlagEvaluateUnderCondition((Boolean)newValue);
				return;
			case ImPackage.EXTRACT_DATA_RULE__CONDITION:
				setCondition((ExpressionCondition)newValue);
				return;
			case ImPackage.EXTRACT_DATA_RULE__EXPRESSION:
				setExpression((String)newValue);
				return;
			case ImPackage.EXTRACT_DATA_RULE__EXPRESSION_LANGUAGE:
				setExpressionLanguage((String)newValue);
				return;
			case ImPackage.EXTRACT_DATA_RULE__VARIABLE_NAME:
				setVariableName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ImPackage.EXTRACT_DATA_RULE__ID_RULE:
				setIdRule(ID_RULE_EDEFAULT);
				return;
			case ImPackage.EXTRACT_DATA_RULE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ImPackage.EXTRACT_DATA_RULE__FLAG_EVALUATE_UNDER_CONDITION:
				setFlagEvaluateUnderCondition(FLAG_EVALUATE_UNDER_CONDITION_EDEFAULT);
				return;
			case ImPackage.EXTRACT_DATA_RULE__CONDITION:
				setCondition((ExpressionCondition)null);
				return;
			case ImPackage.EXTRACT_DATA_RULE__EXPRESSION:
				setExpression(EXPRESSION_EDEFAULT);
				return;
			case ImPackage.EXTRACT_DATA_RULE__EXPRESSION_LANGUAGE:
				setExpressionLanguage(EXPRESSION_LANGUAGE_EDEFAULT);
				return;
			case ImPackage.EXTRACT_DATA_RULE__VARIABLE_NAME:
				setVariableName(VARIABLE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ImPackage.EXTRACT_DATA_RULE__ID_RULE:
				return ID_RULE_EDEFAULT == null ? idRule != null : !ID_RULE_EDEFAULT.equals(idRule);
			case ImPackage.EXTRACT_DATA_RULE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ImPackage.EXTRACT_DATA_RULE__FLAG_EVALUATE_UNDER_CONDITION:
				return flagEvaluateUnderCondition != FLAG_EVALUATE_UNDER_CONDITION_EDEFAULT;
			case ImPackage.EXTRACT_DATA_RULE__CONDITION:
				return condition != null;
			case ImPackage.EXTRACT_DATA_RULE__EXPRESSION:
				return EXPRESSION_EDEFAULT == null ? expression != null : !EXPRESSION_EDEFAULT.equals(expression);
			case ImPackage.EXTRACT_DATA_RULE__EXPRESSION_LANGUAGE:
				return EXPRESSION_LANGUAGE_EDEFAULT == null ? expressionLanguage != null : !EXPRESSION_LANGUAGE_EDEFAULT.equals(expressionLanguage);
			case ImPackage.EXTRACT_DATA_RULE__VARIABLE_NAME:
				return VARIABLE_NAME_EDEFAULT == null ? variableName != null : !VARIABLE_NAME_EDEFAULT.equals(variableName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (idRule: ");
		result.append(idRule);
		result.append(", name: ");
		result.append(name);
		result.append(", flagEvaluateUnderCondition: ");
		result.append(flagEvaluateUnderCondition);
		result.append(", expression: ");
		result.append(expression);
		result.append(", expressionLanguage: ");
		result.append(expressionLanguage);
		result.append(", variableName: ");
		result.append(variableName);
		result.append(')');
		return result.toString();
	}

} //ExtractDataRuleImpl
