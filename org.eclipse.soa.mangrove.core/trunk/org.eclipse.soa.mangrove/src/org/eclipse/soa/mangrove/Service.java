/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.Service#getNeeds <em>Needs</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Service#getIs_owned <em>Is owned</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Service#getServiceName <em>Service Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Service#getServiceType <em>Service Type</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Service#getBindings <em>Bindings</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.Service#getEndpoints <em>Endpoints</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.soa.mangrove.ImPackage#getService()
 * @model
 * @generated
 */
public interface Service extends ConfigurableElement {
	/**
	 * Returns the value of the '<em><b>Needs</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.Service}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Needs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Needs</em>' reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getService_Needs()
	 * @model resolveProxies="false"
	 * @generated
	 */
	EList<Service> getNeeds();

	/**
	 * Returns the value of the '<em><b>Is owned</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.soa.mangrove.Owner#getOwns <em>Owns</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is owned</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is owned</em>' reference.
	 * @see #setIs_owned(Owner)
	 * @see org.eclipse.soa.mangrove.ImPackage#getService_Is_owned()
	 * @see org.eclipse.soa.mangrove.Owner#getOwns
	 * @model opposite="owns" required="true"
	 * @generated
	 */
	Owner getIs_owned();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Service#getIs_owned <em>Is owned</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is owned</em>' reference.
	 * @see #getIs_owned()
	 * @generated
	 */
	void setIs_owned(Owner value);

	/**
	 * Returns the value of the '<em><b>Service Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Name</em>' attribute.
	 * @see #setServiceName(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getService_ServiceName()
	 * @model
	 * @generated
	 */
	String getServiceName();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Service#getServiceName <em>Service Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Service Name</em>' attribute.
	 * @see #getServiceName()
	 * @generated
	 */
	void setServiceName(String value);

	/**
	 * Returns the value of the '<em><b>Service Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Type</em>' attribute.
	 * @see #setServiceType(String)
	 * @see org.eclipse.soa.mangrove.ImPackage#getService_ServiceType()
	 * @model
	 * @generated
	 */
	String getServiceType();

	/**
	 * Sets the value of the '{@link org.eclipse.soa.mangrove.Service#getServiceType <em>Service Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Service Type</em>' attribute.
	 * @see #getServiceType()
	 * @generated
	 */
	void setServiceType(String value);

	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.ServiceBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindings</em>' reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getService_Bindings()
	 * @model
	 * @generated
	 */
	EList<ServiceBinding> getBindings();

	/**
	 * Returns the value of the '<em><b>Endpoints</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.soa.mangrove.Endpoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Endpoints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Endpoints</em>' containment reference list.
	 * @see org.eclipse.soa.mangrove.ImPackage#getService_Endpoints()
	 * @model containment="true"
	 * @generated
	 */
	EList<Endpoint> getEndpoints();

} // Service
