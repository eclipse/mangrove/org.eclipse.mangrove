/*******************************************************************************
 * Copyright (c) {2007 - 2008} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Status;
import org.eclipse.soa.mangrove.ImActivator;

public class ImLogger {

	//
	// Pass pluginID
	public static void info(String msg) {
	      info(ImActivator.PLUGIN_ID, msg);
	}
	
	public static void warning(String msg) {
	    warning(ImActivator.PLUGIN_ID, msg);
	}
	
	public static void warning(String msg, Throwable e) {
		warning(ImActivator.PLUGIN_ID, msg, e); 
	}
		
	public static void error(String msg) {
		error(ImActivator.PLUGIN_ID, msg);
	}

	public static void error(String msg, Throwable e) {
		error(ImActivator.PLUGIN_ID, msg, e);
	}	
	
	// Pass pluginID
	public static  void info(String pluginID, String msg) {
	      ImActivator.getDefault().getLog().log(new Status(Status.INFO, pluginID, Status.INFO, msg, null));
	}
	
	public static  void warning(String pluginID, String msg) {
	    warning(pluginID, msg, null);
	}
	
	public static  void warning(String pluginID, String msg, Throwable e) {
	      ImActivator.getDefault().getLog().log(new Status(Status.WARNING, pluginID, Status.WARNING, msg, getCause(e)));
	}
	
	public static  void error(String pluginID, String msg) {
	      error(pluginID, msg, null);
	}
	
	public static void error(String pluginID, String msg, Throwable e) {
	      ImActivator.getDefault().getLog().log(new Status(Status.ERROR, pluginID, Status.ERROR, msg, getCause(e)));
	}

    /**
     * This class must not be called outside this class
     * Utility method to get the cause of an exception.
     * @param exception : the exception to analyse
     * @return Throwable
     */
    public static Throwable getCause(final Throwable exception) {
        // Figure out which exception should
    	//actually be logged -- if the given exception is
        // a wrapper, unwrap it
        Throwable cause = null;
        if (exception != null) {
            if (exception instanceof CoreException) {
                // Workaround: CoreException contains
            	//a cause, but does not actually implement getCause().
                // If we get a CoreException, we need to
            	//manually unpack the cause. Otherwise, use
                // the general-purpose mechanism.
            	//Remove this branch if CoreException ever implements
                // a correct getCause() method.
                CoreException ce = (CoreException) exception;
                cause = ce.getStatus().getException();
            } else {
            	// use reflect instead of a direct call
            	//to getCause(), to allow compilation against
            	//JCL Foundation (bug 80053)
            	try {
            		Method causeMethod =
            			exception.getClass().getMethod(
            					"getCause", new Class[0]);
            		Object o = causeMethod.invoke(
            				exception, new Object[0]);
            		if (o instanceof Throwable) {
            			cause = (Throwable) o;
            		}
            	} catch (NoSuchMethodException e) {
            		//ignore
					cause = null;
            	} catch (IllegalArgumentException e) {
            		// ignore
					cause = null;
				} catch (IllegalAccessException e) {
            		// ignore
					cause = null;
				} catch (InvocationTargetException e) {
            		// ignore
					cause = null;
				}
            }
            if (cause == null) {
                cause = exception;
            }
        }
        return cause;
    }
}
