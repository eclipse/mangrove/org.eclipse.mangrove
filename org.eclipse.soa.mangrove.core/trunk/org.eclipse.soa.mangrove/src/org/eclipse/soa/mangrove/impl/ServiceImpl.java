/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.soa.mangrove.Endpoint;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.Owner;
import org.eclipse.soa.mangrove.Service;
import org.eclipse.soa.mangrove.ServiceBinding;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ServiceImpl#getNeeds <em>Needs</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ServiceImpl#getIs_owned <em>Is owned</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ServiceImpl#getServiceName <em>Service Name</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ServiceImpl#getServiceType <em>Service Type</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ServiceImpl#getBindings <em>Bindings</em>}</li>
 *   <li>{@link org.eclipse.soa.mangrove.impl.ServiceImpl#getEndpoints <em>Endpoints</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServiceImpl extends ConfigurableElementImpl implements Service {
	/**
	 * The cached value of the '{@link #getNeeds() <em>Needs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNeeds()
	 * @generated
	 * @ordered
	 */
	protected EList<Service> needs;

	/**
	 * The cached value of the '{@link #getIs_owned() <em>Is owned</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIs_owned()
	 * @generated
	 * @ordered
	 */
	protected Owner is_owned;

	/**
	 * The default value of the '{@link #getServiceName() <em>Service Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceName()
	 * @generated
	 * @ordered
	 */
	protected static final String SERVICE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getServiceName() <em>Service Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceName()
	 * @generated
	 * @ordered
	 */
	protected String serviceName = SERVICE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getServiceType() <em>Service Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceType()
	 * @generated
	 * @ordered
	 */
	protected static final String SERVICE_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getServiceType() <em>Service Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceType()
	 * @generated
	 * @ordered
	 */
	protected String serviceType = SERVICE_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBindings() <em>Bindings</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceBinding> bindings;

	/**
	 * The cached value of the '{@link #getEndpoints() <em>Endpoints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndpoints()
	 * @generated
	 * @ordered
	 */
	protected EList<Endpoint> endpoints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImPackage.Literals.SERVICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Service> getNeeds() {
		if (needs == null) {
			needs = new EObjectEList<Service>(Service.class, this, ImPackage.SERVICE__NEEDS);
		}
		return needs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Owner getIs_owned() {
		if (is_owned != null && is_owned.eIsProxy()) {
			InternalEObject oldIs_owned = (InternalEObject)is_owned;
			is_owned = (Owner)eResolveProxy(oldIs_owned);
			if (is_owned != oldIs_owned) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ImPackage.SERVICE__IS_OWNED, oldIs_owned, is_owned));
			}
		}
		return is_owned;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Owner basicGetIs_owned() {
		return is_owned;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIs_owned(Owner newIs_owned, NotificationChain msgs) {
		Owner oldIs_owned = is_owned;
		is_owned = newIs_owned;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ImPackage.SERVICE__IS_OWNED, oldIs_owned, newIs_owned);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIs_owned(Owner newIs_owned) {
		if (newIs_owned != is_owned) {
			NotificationChain msgs = null;
			if (is_owned != null)
				msgs = ((InternalEObject)is_owned).eInverseRemove(this, ImPackage.OWNER__OWNS, Owner.class, msgs);
			if (newIs_owned != null)
				msgs = ((InternalEObject)newIs_owned).eInverseAdd(this, ImPackage.OWNER__OWNS, Owner.class, msgs);
			msgs = basicSetIs_owned(newIs_owned, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.SERVICE__IS_OWNED, newIs_owned, newIs_owned));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceName(String newServiceName) {
		String oldServiceName = serviceName;
		serviceName = newServiceName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.SERVICE__SERVICE_NAME, oldServiceName, serviceName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getServiceType() {
		return serviceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceType(String newServiceType) {
		String oldServiceType = serviceType;
		serviceType = newServiceType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ImPackage.SERVICE__SERVICE_TYPE, oldServiceType, serviceType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceBinding> getBindings() {
		if (bindings == null) {
			bindings = new EObjectResolvingEList<ServiceBinding>(ServiceBinding.class, this, ImPackage.SERVICE__BINDINGS);
		}
		return bindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Endpoint> getEndpoints() {
		if (endpoints == null) {
			endpoints = new EObjectContainmentEList<Endpoint>(Endpoint.class, this, ImPackage.SERVICE__ENDPOINTS);
		}
		return endpoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ImPackage.SERVICE__IS_OWNED:
				if (is_owned != null)
					msgs = ((InternalEObject)is_owned).eInverseRemove(this, ImPackage.OWNER__OWNS, Owner.class, msgs);
				return basicSetIs_owned((Owner)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ImPackage.SERVICE__IS_OWNED:
				return basicSetIs_owned(null, msgs);
			case ImPackage.SERVICE__ENDPOINTS:
				return ((InternalEList<?>)getEndpoints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ImPackage.SERVICE__NEEDS:
				return getNeeds();
			case ImPackage.SERVICE__IS_OWNED:
				if (resolve) return getIs_owned();
				return basicGetIs_owned();
			case ImPackage.SERVICE__SERVICE_NAME:
				return getServiceName();
			case ImPackage.SERVICE__SERVICE_TYPE:
				return getServiceType();
			case ImPackage.SERVICE__BINDINGS:
				return getBindings();
			case ImPackage.SERVICE__ENDPOINTS:
				return getEndpoints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ImPackage.SERVICE__NEEDS:
				getNeeds().clear();
				getNeeds().addAll((Collection<? extends Service>)newValue);
				return;
			case ImPackage.SERVICE__IS_OWNED:
				setIs_owned((Owner)newValue);
				return;
			case ImPackage.SERVICE__SERVICE_NAME:
				setServiceName((String)newValue);
				return;
			case ImPackage.SERVICE__SERVICE_TYPE:
				setServiceType((String)newValue);
				return;
			case ImPackage.SERVICE__BINDINGS:
				getBindings().clear();
				getBindings().addAll((Collection<? extends ServiceBinding>)newValue);
				return;
			case ImPackage.SERVICE__ENDPOINTS:
				getEndpoints().clear();
				getEndpoints().addAll((Collection<? extends Endpoint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ImPackage.SERVICE__NEEDS:
				getNeeds().clear();
				return;
			case ImPackage.SERVICE__IS_OWNED:
				setIs_owned((Owner)null);
				return;
			case ImPackage.SERVICE__SERVICE_NAME:
				setServiceName(SERVICE_NAME_EDEFAULT);
				return;
			case ImPackage.SERVICE__SERVICE_TYPE:
				setServiceType(SERVICE_TYPE_EDEFAULT);
				return;
			case ImPackage.SERVICE__BINDINGS:
				getBindings().clear();
				return;
			case ImPackage.SERVICE__ENDPOINTS:
				getEndpoints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ImPackage.SERVICE__NEEDS:
				return needs != null && !needs.isEmpty();
			case ImPackage.SERVICE__IS_OWNED:
				return is_owned != null;
			case ImPackage.SERVICE__SERVICE_NAME:
				return SERVICE_NAME_EDEFAULT == null ? serviceName != null : !SERVICE_NAME_EDEFAULT.equals(serviceName);
			case ImPackage.SERVICE__SERVICE_TYPE:
				return SERVICE_TYPE_EDEFAULT == null ? serviceType != null : !SERVICE_TYPE_EDEFAULT.equals(serviceType);
			case ImPackage.SERVICE__BINDINGS:
				return bindings != null && !bindings.isEmpty();
			case ImPackage.SERVICE__ENDPOINTS:
				return endpoints != null && !endpoints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (serviceName: ");
		result.append(serviceName);
		result.append(", serviceType: ");
		result.append(serviceType);
		result.append(')');
		return result.toString();
	}

} //ServiceImpl
