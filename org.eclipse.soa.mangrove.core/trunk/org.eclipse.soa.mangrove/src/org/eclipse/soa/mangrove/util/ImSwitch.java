/*******************************************************************************
 * Copyright (c) {2007} {INRIA and Engineering}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    {Adrian Mos (INRIA) and Andrea Zoppello (Engineering)} - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.util;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.soa.mangrove.BasicProperty;
import org.eclipse.soa.mangrove.Condition;
import org.eclipse.soa.mangrove.ConfigurableElement;
import org.eclipse.soa.mangrove.Contract;
import org.eclipse.soa.mangrove.ControlService;
import org.eclipse.soa.mangrove.Endpoint;
import org.eclipse.soa.mangrove.ExpressionCondition;
import org.eclipse.soa.mangrove.ExtractDataRule;
import org.eclipse.soa.mangrove.ImPackage;
import org.eclipse.soa.mangrove.IterationControl;
import org.eclipse.soa.mangrove.JoinControl;
import org.eclipse.soa.mangrove.MapProperty;
import org.eclipse.soa.mangrove.ObservableAttribute;
import org.eclipse.soa.mangrove.Owner;
import org.eclipse.soa.mangrove.ProcessCollection;
import org.eclipse.soa.mangrove.Property;
import org.eclipse.soa.mangrove.PropertyCondition;
import org.eclipse.soa.mangrove.RouterControl;
import org.eclipse.soa.mangrove.Service;
import org.eclipse.soa.mangrove.ServiceBinding;
import org.eclipse.soa.mangrove.ServiceClassification;
import org.eclipse.soa.mangrove.ServiceCollection;
import org.eclipse.soa.mangrove.SplitControl;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.Transition;
import org.eclipse.soa.mangrove.TransitionUnderCondition;
import org.eclipse.soa.mangrove.Variable;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.soa.mangrove.ImPackage
 * @generated
 */
public class ImSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ImPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImSwitch() {
		if (modelPackage == null) {
			modelPackage = ImPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ImPackage.PROCESS: {
				org.eclipse.soa.mangrove.Process process = (org.eclipse.soa.mangrove.Process)theEObject;
				T result = caseProcess(process);
				if (result == null) result = caseService(process);
				if (result == null) result = caseConfigurableElement(process);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.STEP: {
				Step step = (Step)theEObject;
				T result = caseStep(step);
				if (result == null) result = caseConfigurableElement(step);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.SERVICE: {
				Service service = (Service)theEObject;
				T result = caseService(service);
				if (result == null) result = caseConfigurableElement(service);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.SERVICE_CLASSIFICATION: {
				ServiceClassification serviceClassification = (ServiceClassification)theEObject;
				T result = caseServiceClassification(serviceClassification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.TRANSITION: {
				Transition transition = (Transition)theEObject;
				T result = caseTransition(transition);
				if (result == null) result = caseConfigurableElement(transition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.OWNER: {
				Owner owner = (Owner)theEObject;
				T result = caseOwner(owner);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.TRANSITION_UNDER_CONDITION: {
				TransitionUnderCondition transitionUnderCondition = (TransitionUnderCondition)theEObject;
				T result = caseTransitionUnderCondition(transitionUnderCondition);
				if (result == null) result = caseTransition(transitionUnderCondition);
				if (result == null) result = caseConfigurableElement(transitionUnderCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.OBSERVABLE_ATTRIBUTE: {
				ObservableAttribute observableAttribute = (ObservableAttribute)theEObject;
				T result = caseObservableAttribute(observableAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.PROPERTY: {
				Property property = (Property)theEObject;
				T result = caseProperty(property);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.PROCESS_COLLECTION: {
				ProcessCollection processCollection = (ProcessCollection)theEObject;
				T result = caseProcessCollection(processCollection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.SERVICE_BINDING: {
				ServiceBinding serviceBinding = (ServiceBinding)theEObject;
				T result = caseServiceBinding(serviceBinding);
				if (result == null) result = caseConfigurableElement(serviceBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.CONTROL_SERVICE: {
				ControlService controlService = (ControlService)theEObject;
				T result = caseControlService(controlService);
				if (result == null) result = caseService(controlService);
				if (result == null) result = caseConfigurableElement(controlService);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.ROUTER_CONTROL: {
				RouterControl routerControl = (RouterControl)theEObject;
				T result = caseRouterControl(routerControl);
				if (result == null) result = caseControlService(routerControl);
				if (result == null) result = caseService(routerControl);
				if (result == null) result = caseConfigurableElement(routerControl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.SPLIT_CONTROL: {
				SplitControl splitControl = (SplitControl)theEObject;
				T result = caseSplitControl(splitControl);
				if (result == null) result = caseControlService(splitControl);
				if (result == null) result = caseService(splitControl);
				if (result == null) result = caseConfigurableElement(splitControl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.JOIN_CONTROL: {
				JoinControl joinControl = (JoinControl)theEObject;
				T result = caseJoinControl(joinControl);
				if (result == null) result = caseControlService(joinControl);
				if (result == null) result = caseService(joinControl);
				if (result == null) result = caseConfigurableElement(joinControl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.ITERATION_CONTROL: {
				IterationControl iterationControl = (IterationControl)theEObject;
				T result = caseIterationControl(iterationControl);
				if (result == null) result = caseControlService(iterationControl);
				if (result == null) result = caseService(iterationControl);
				if (result == null) result = caseConfigurableElement(iterationControl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.STP_INTERMEDIATE_MODEL: {
				StpIntermediateModel stpIntermediateModel = (StpIntermediateModel)theEObject;
				T result = caseStpIntermediateModel(stpIntermediateModel);
				if (result == null) result = caseConfigurableElement(stpIntermediateModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.SERVICE_COLLECTION: {
				ServiceCollection serviceCollection = (ServiceCollection)theEObject;
				T result = caseServiceCollection(serviceCollection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.BASIC_PROPERTY: {
				BasicProperty basicProperty = (BasicProperty)theEObject;
				T result = caseBasicProperty(basicProperty);
				if (result == null) result = caseProperty(basicProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.MAP_PROPERTY: {
				MapProperty mapProperty = (MapProperty)theEObject;
				T result = caseMapProperty(mapProperty);
				if (result == null) result = caseProperty(mapProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.CONTRACT: {
				Contract contract = (Contract)theEObject;
				T result = caseContract(contract);
				if (result == null) result = caseProperty(contract);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.CONFIGURABLE_ELEMENT: {
				ConfigurableElement configurableElement = (ConfigurableElement)theEObject;
				T result = caseConfigurableElement(configurableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.STRING_TO_PROPERTY_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<String, Property> stringToPropertyMapEntry = (Map.Entry<String, Property>)theEObject;
				T result = caseStringToPropertyMapEntry(stringToPropertyMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.ENDPOINT: {
				Endpoint endpoint = (Endpoint)theEObject;
				T result = caseEndpoint(endpoint);
				if (result == null) result = caseConfigurableElement(endpoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.CONDITION: {
				Condition condition = (Condition)theEObject;
				T result = caseCondition(condition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.PROPERTY_CONDITION: {
				PropertyCondition propertyCondition = (PropertyCondition)theEObject;
				T result = casePropertyCondition(propertyCondition);
				if (result == null) result = caseCondition(propertyCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.EXPRESSION_CONDITION: {
				ExpressionCondition expressionCondition = (ExpressionCondition)theEObject;
				T result = caseExpressionCondition(expressionCondition);
				if (result == null) result = caseCondition(expressionCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.EXTRACT_DATA_RULE: {
				ExtractDataRule extractDataRule = (ExtractDataRule)theEObject;
				T result = caseExtractDataRule(extractDataRule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ImPackage.VARIABLE: {
				Variable variable = (Variable)theEObject;
				T result = caseVariable(variable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcess(org.eclipse.soa.mangrove.Process object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStep(Step object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseService(Service object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Classification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Classification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceClassification(ServiceClassification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransition(Transition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Owner</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Owner</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOwner(Owner object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition Under Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition Under Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransitionUnderCondition(TransitionUnderCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Observable Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Observable Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObservableAttribute(ObservableAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProperty(Property object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Collection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Collection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessCollection(ProcessCollection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceBinding(ServiceBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Service</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlService(ControlService object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Router Control</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Router Control</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRouterControl(RouterControl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Split Control</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Split Control</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSplitControl(SplitControl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Join Control</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Join Control</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJoinControl(JoinControl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iteration Control</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iteration Control</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIterationControl(IterationControl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stp Intermediate Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stp Intermediate Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStpIntermediateModel(StpIntermediateModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Collection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Collection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceCollection(ServiceCollection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicProperty(BasicProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Map Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Map Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMapProperty(MapProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contract</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contract</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContract(Contract object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configurable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configurable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigurableElement(ConfigurableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To Property Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To Property Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToPropertyMapEntry(Map.Entry<String, Property> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Endpoint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Endpoint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEndpoint(Endpoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCondition(Condition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyCondition(PropertyCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionCondition(ExpressionCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extract Data Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extract Data Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtractDataRule(ExtractDataRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ImSwitch
