/*******************************************************************************
 * Copyright (c) {2008} INRIA
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (INRIA) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.util;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.soa.mangrove.ImFactory;
import org.eclipse.soa.mangrove.Process;
import org.eclipse.soa.mangrove.ProcessCollection;
import org.eclipse.soa.mangrove.Service;
import org.eclipse.soa.mangrove.ServiceCollection;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.Transition;

/**
 * Contains operations for creating elements in the Intermediate Model and writing it to disk
 * An instance of this class corresponds to an instance of the IM
 * @author Adrian Mos
 */
public class IMHandler {
    private StpIntermediateModel stpIM = null; //the top level element of the Intermediate Model EMF instance
    private ProcessCollection processCollection;
    private ServiceCollection serviceCollection;
    private ImFactory imFactory;
    private HashMap<Step, Process> stepsToProcesses = new HashMap<Step, Process>();

    /**
     * Instantiates the STP-IM and performs other set-up operations  
     */
    public IMHandler() {
        imFactory = ImFactory.eINSTANCE;
        stpIM = imFactory.createStpIntermediateModel();
        //create the IM collections (for processes and services)
        //each process or service created in the future will be added to these accordingly
        processCollection = imFactory.createProcessCollection();
        serviceCollection = imFactory.createServiceCollection();
        stpIM.setProcessCollection(processCollection);
        stpIM.setServiceCollection(serviceCollection);
    }

    /**
     * Instantiates the STP-IM from a .im file  
     * @param uri the file to load the IM from
     */
    public IMHandler(URI uri){
        
    }
    
    public StpIntermediateModel getStpIM() {
        return stpIM;
    }
    
    /**
     * creates and returns a Process element in the IM
     * @param name the process name
     * @return the Process EMF element
     */
    public Process createProcess(String name){
        Process p = imFactory.createProcess();
        p.setName(name);
        processCollection.getProcesses().add(p); //add it to the IM collection
        
        return p;
    }
    
    /**
     * creates and returns a Service element in the IM
     * @param name the service name
     * @return the Service EMF element
     */
    public Service createService(String name){
        Service s = imFactory.createService();
        s.setServiceName(name);
        serviceCollection.getServices().add(s); //add it to the IM collection
        return s;
    }

    /**
     * creates a process step and registers it with the given process
     * @param stepName the name of the step to create
     * @param parent the parent to attach the step to
     * @return the newly created step
     */
    public Step createStep(String stepName, Process parent){
        Step s = imFactory.createStep();
        s.setName(stepName);
        parent.getSteps().add(s);
        this.stepsToProcesses.put(s, parent);
        return s;
    }
    
    public Transition connectSteps(Step source, Step target){
    	Transition t = imFactory.createTransition();
    	t.setSource(source);
    	t.setTarget(target);
    	source.getSourceTransitions().add(t);
    	target.getTargetTransitions().add(t);
    	Process parent = this.stepsToProcesses.get(source);
    	parent.getTransitions().add(t);
    	return t;
    }
    
    /**
     * will create a file and save the IM instance in it
     * @param uri the URI of the file to save to
     * @throws IOException if the save operation does not succeed
     */
    public void persistIM(URI uri) throws IOException {
        //System.out.println("Attempting to persist the IM in " + uri);

        ResourceSet rs = new ResourceSetImpl();

        Resource resource = rs.createResource(uri);
        resource.getContents().add(this.stpIM);
        
        resource.save(Collections.EMPTY_MAP);
    }
    
}
