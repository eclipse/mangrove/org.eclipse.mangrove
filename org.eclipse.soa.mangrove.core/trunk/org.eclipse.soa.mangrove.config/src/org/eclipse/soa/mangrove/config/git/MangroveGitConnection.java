package org.eclipse.soa.mangrove.config.git;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

public class MangroveGitConnection {

	public static void main(String[] args) throws MissingObjectException, IOException {
		File gitWorkDir = new File("/Users/amos/git/config-mangrove/");
		Git git = Git.open(gitWorkDir);
		Repository repo = git.getRepository();

		ObjectId lastCommitId = repo.resolve(Constants.HEAD);

		RevWalk revWalk = new RevWalk(repo);
		RevCommit commit = revWalk.parseCommit(lastCommitId);

		RevTree tree = commit.getTree();

		TreeWalk treeWalk = new TreeWalk(repo);
		treeWalk.addTree(tree);
		treeWalk.setRecursive(true);
		treeWalk.setFilter(PathFilter.create("econ15.conf"));
		if (!treeWalk.next()) {
			System.out.println("Nothing found!");
			return;
		}

		ObjectId objectId = treeWalk.getObjectId(0);
		ObjectLoader loader = repo.open(objectId);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		loader.copyTo(out);
		System.out.println("econ15.conf:\n" + out.toString());

	}

}
