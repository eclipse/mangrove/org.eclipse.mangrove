package org.eclipse.soa.mangrove.config.preferences;

import java.io.File;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.soa.mangrove.config.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.GIT_REPO, System.getProperty("user.home")+File.pathSeparator+"git"+File.pathSeparator+"mangrove-config");
		store.setDefault(PreferenceConstants.JMX_ADDRESS, "localhost:9292");
	}

}
