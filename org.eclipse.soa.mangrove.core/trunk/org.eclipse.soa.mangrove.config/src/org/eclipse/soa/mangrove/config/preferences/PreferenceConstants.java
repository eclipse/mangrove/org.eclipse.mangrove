package org.eclipse.soa.mangrove.config.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String GIT_REPO = "git";

	public static final String P_BOOLEAN = "booleanPreference";

	public static final String P_CHOICE = "choicePreference";

	public static final String JMX_ADDRESS = "jmx";
	
}
