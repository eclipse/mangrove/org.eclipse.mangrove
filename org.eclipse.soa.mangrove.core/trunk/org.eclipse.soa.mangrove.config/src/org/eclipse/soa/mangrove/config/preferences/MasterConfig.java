package org.eclipse.soa.mangrove.config.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.Preferences;

public class MasterConfig {

	public static final String CONCEPT_ACTIVITY_FILE = "concept-activity.map";
	public static final String PROCESS_ACTIVITY_FILE = "processes-activities.map";
	public static final String SERVICE_ID_FILE = "services-id-names.map";
	
	//singleton
	private static MasterConfig instance = null;
	protected MasterConfig(){
	}
	public static MasterConfig getInstance(){
		if(null==instance) instance = new MasterConfig();
		return instance;
	}
	
	//properties
	public String getMangroveGitRepoLocation() {
		String defaultValue = System.getenv("MANGROVE_GIT_REPO");
		Preferences preferences = InstanceScope.INSTANCE.getNode("org.eclipse.soa.mangrove.config");
		String prefValue = preferences.get(PreferenceConstants.GIT_REPO, defaultValue);
		return prefValue;
	}
}
