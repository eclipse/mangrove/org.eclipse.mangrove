/*******************************************************************************
 * Copyright (c) {2013} Xerox Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (Xerox Corp.) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.out.bpmn2.popup.actions;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.soa.mangrove.out.bpmn2.transform.Mangrove2BPMN2Transformer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class GenerateBPMN2Action implements IObjectActionDelegate {

    private IFile selectedIMFile = null;
    private Mangrove2BPMN2Transformer bpmn2Transformer;
    private Shell shell;

	public GenerateBPMN2Action() {
		super();
        shell = new Shell();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
        if (null != this.selectedIMFile) {
            //load the IM from the selected file (full path includes the workspace location)
            IPath location = Platform.getLocation();
            //System.out.println("Workspace Location: " + location.toFile().getAbsolutePath());
            URI uri = URI.createFileURI(location.toString() + this.selectedIMFile.getFullPath().toString());
            IContainer container = this.selectedIMFile.getParent();
            this.bpmn2Transformer = new Mangrove2BPMN2Transformer(shell, uri);
            this.bpmn2Transformer.createBPMN2fromCore(container);
        }
	}

    /**
     * @see IActionDelegate#selectionChanged(IAction, ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
        this.selectedIMFile = ((IFile) ((StructuredSelection) selection).getFirstElement());
    }

}
