/*******************************************************************************
 * Copyright (c) {2013} Xerox Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (Xerox Corp.) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.out.bpmn2.transform;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.soa.mangrove.Process;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.Transition;
import org.eclipse.soa.mangrove.branches.common.Core2AnyTransformer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.bpmn2.*;

public class Mangrove2BPMN2Transformer extends Core2AnyTransformer {

	private BPMN2Handler bpmn2Handler;
	private HashMap<Step, FlowNode> stepsToBPMN;
	
	public Mangrove2BPMN2Transformer(Shell shell, URI uri) {
		super(shell, uri);
		this.bpmn2Handler = new BPMN2Handler();
		stepsToBPMN = new HashMap<Step, FlowNode>();
	}

	public void createBPMN2fromCore(IContainer container) {
		
		//this version creates a single BPMN2 Process with multiple lanes
		org.eclipse.bpmn2.Process p = bpmn2Handler.createProcess("Generated from Mangrove");
		StartEvent start = bpmn2Handler.createStartEvent(p);		
		//one Lane Set to contain the several lanes
		LaneSet laneSet = bpmn2Handler.createLaneSet(p, "Mangrove-Generated LaneSet");
		
		//for each Mangrove process, create a new lane in BPMN2 and add it to the lane set
		for(Process imProc : im.getProcessCollection().getProcesses()){
			Lane lane = bpmn2Handler.createLane(laneSet, imProc.getName());
			
			//now add all the steps in the Mangrove process as elements of the lane
			for(Step step : imProc.getSteps()){
				
				//first create the element as part of the BPMN2 process
				FlowNode node = null;
				if(step.getName().startsWith("IF ")) node = bpmn2Handler.createExGateway(p, step.getName());
				else if(imProc.getName().equals("SYSTEM")) {
					String serviceName = null;
					if(null!=step.getServiceModel()) serviceName = step.getServiceModel().getServiceName();
					//System.out.println("ServiceName = " + serviceName);
					node = bpmn2Handler.createServiceTask(p, step.getName(), serviceName);
				}
				else node = bpmn2Handler.createManualTask(p, step.getName());
				//now add the element to the lane
				//System.out.println("Added node " + node.getName() + " to lane: " + lane.getName());
				lane.getFlowNodeRefs().add(node);
				//now save the mapping for later on when creating the sequence flows
				stepsToBPMN.put(step, node);
				//System.out.println("Adding mapping " + step + " - " + node);
			}
			
			if(lane.getFlowNodeRefs().isEmpty()) laneSet.getLanes().remove(lane);
		}
		//bpmn2Handler.connectElements(p, start, end);

		//create all the BPMN sequence flows
		Iterator<Step> stepsIter = stepsToBPMN.keySet().iterator();
		while(stepsIter.hasNext()){
			Step s = stepsIter.next();
			System.out.println("Processing step " + s.getName());
			
			EList<Transition> transitions = s.getSourceTransitions();
			Iterator<Transition> transIter = transitions.iterator();
			while (transIter.hasNext()){
				Transition t = transIter.next();
				System.out.println("Processing Transition " + t + "with source step: " + t.getSource() + " - target = " + t.getTarget());
				FlowNode bStart = stepsToBPMN.get(s);
				FlowNode bEnd = stepsToBPMN.get(t.getTarget());
				bpmn2Handler.connectElements(p, bStart, bEnd);
			}
		}
		
		//to finish up, the BPMN2 file must be persisted in the workspace 
        try {
            URI bpmn2URI = uri.trimFileExtension().appendFileExtension("bpmn");
            bpmn2Handler.persistBPMN2File(bpmn2URI);
            container.refreshLocal(2, null); //refreshes the workspace so that the saved file is visible
        } catch (IOException e) {
            displayIMError("Could not save the BPMN2 Composite file", e);
        } catch (CoreException e) {
            displayIMError("Could not refresh the workspace", e);
        }
	
	}

}
