/*******************************************************************************
 * Copyright (c) {2013} Xerox Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (Xerox Corp.) - initial API and implementation
 *******************************************************************************/

package org.eclipse.soa.mangrove.out.bpmn2.transform;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.bpmn2.*;
import org.eclipse.bpmn2.Process;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * Creates handler methods for dealing with the BPMN2 Metamodel
 * @author Adrian Mos
 * */
public class BPMN2Handler {

	private DocumentRoot bpmn2Root;
	private Definitions definitions;
	private Bpmn2Factory bpmn2Factory;
	
	public BPMN2Handler() {
		bpmn2Factory = Bpmn2Factory.eINSTANCE;
		bpmn2Root = bpmn2Factory.createDocumentRoot();
		definitions = bpmn2Factory.createDefinitions();
		bpmn2Root.setDefinitions(definitions);
	}
	
	public Process createProcess(String name){
		Process p = bpmn2Factory.createProcess();
		p.setName(name);
		p.setId(name.replaceAll("\\s", "") + "_ID"); //sets a generated ID
		bpmn2Root.getDefinitions().getRootElements().add(p);
		return p;
	}
	
	public StartEvent createStartEvent(Process p){
		StartEvent se = bpmn2Factory.createStartEvent();
		se.setName("Generated Start Event");
		p.getFlowElements().add(se);
		return se;
	}
	
	public EndEvent createEndEvent(Process p){
		EndEvent ee = bpmn2Factory.createEndEvent();
		ee.setName("Generated End Event");
		p.getFlowElements().add(ee);
		return ee;
	}
	
	public SequenceFlow connectElements (Process p, FlowNode source, FlowNode target){
		SequenceFlow flow = bpmn2Factory.createSequenceFlow();
		p.getFlowElements().add(flow);
		flow.setSourceRef(source);
		flow.setTargetRef(target);
		return flow;
	}
	
	public Task createTask(Process p, String name){
		Task task = bpmn2Factory.createTask();
		task.setName(name);
		task.setId(name.replaceAll("\\s", ""));
		p.getFlowElements().add(task);
		return task;
	}
	
	public ServiceTask createServiceTask(Process p, String name, String serviceName){
		ServiceTask task = bpmn2Factory.createServiceTask();
		task.setName(name);
		task.setId(name.replaceAll("\\s", "") + "_" + EcoreUtil.generateUUID());
		task.setImplementation(serviceName);
		p.getFlowElements().add(task);
		return task;
	}
	
	public ManualTask createManualTask(Process p, String name){
		ManualTask task = bpmn2Factory.createManualTask();
		task.setName(name);
		task.setId(name.replaceAll("\\s", "") + "_" + EcoreUtil.generateUUID());
		p.getFlowElements().add(task);
		return task;
	}
	
	public ExclusiveGateway createExGateway(Process p, String name){
		ExclusiveGateway task = bpmn2Factory.createExclusiveGateway();
		task.setName(name);
		task.setId(name.replaceAll("\\s", "") + "_" + EcoreUtil.generateUUID());
		p.getFlowElements().add(task);
		return task;
	}

	public LaneSet createLaneSet(Process p, String name){
		LaneSet set = bpmn2Factory.createLaneSet();
		set.setId(name.replaceAll("\\s", ""));
		set.setName(name);
		p.getLaneSets().add(set);
		return set;
	}
	
	public Lane createLane(LaneSet set, String name){
		Lane lane = bpmn2Factory.createLane();
		lane.setId(name.replaceAll("\\s", name));
		lane.setName(name);
		set.getLanes().add(lane);
		
		return lane;
	}
	
	
	public void persistBPMN2File(URI uri) throws IOException{
        ResourceSet rs = new ResourceSetImpl();

        Resource resource = rs.createResource(uri, "org.eclipse.bpmn2.content-type.xml");
        resource.getContents().add(this.definitions);
        
        resource.save(Collections.EMPTY_MAP);
	}
	
}