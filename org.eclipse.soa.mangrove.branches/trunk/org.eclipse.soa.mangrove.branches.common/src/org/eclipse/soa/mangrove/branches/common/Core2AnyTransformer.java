/*******************************************************************************
 * Copyright (c) {2013} Xerox Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (Xerox Corp.) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.branches.common;

import java.io.IOException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.soa.mangrove.util.*;

/**
 * The main class driving the transformation process from the
 * Mangrove Core to any other meta-model. This class needs to be extended with logic specific to the target meta-model 
 * 
 * @author Adrian Mos
 */
public abstract class Core2AnyTransformer {
    
    private IMReader imReader = new IMReader(); //helper object for reading IM serialized instances
    private Shell shell; // the SWT shell that will be the parent of different widgets created here
	protected URI uri;
	protected StpIntermediateModel im;
	
    /**
     * @param shell
     */
    public Core2AnyTransformer(Shell shell, URI uri) {
        super();
        this.shell = shell;
        this.uri = uri;
        //get the root element of the IM
        try {
            im = imReader.loadIM(uri);
        } catch (Throwable t) {
            displayIMError("Could not obtain the IM instance contents", t);
            return;
        }
    }
    
    
    /**
     * Display an error message window for IM-related operations
     * @param msg the error message
     */
    protected void displayIMError(String msg, Throwable t) {
        IStatus iStatus = null;
        if (null != t)
            iStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, t.getLocalizedMessage(), t);
        else
            iStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, msg);
        ErrorDialog.openError(shell, "Intermediate Model Generation Error", msg, iStatus);

    }

}
