package org.eclipse.soa.mangrove.in.bpmn2im;

import java.util.List;

import org.eclipse.stp.bpmn.BpmnDiagram;

public interface IBPMNProcessor {

	public boolean isApplicable(BpmnDiagram bpmnDiagram);
	public BpmnDiagram process(BpmnDiagram bpmnDiagram) throws Exception;
	public List<String> selectPools(BpmnDiagram bpmnDiagram) throws Exception;
}
