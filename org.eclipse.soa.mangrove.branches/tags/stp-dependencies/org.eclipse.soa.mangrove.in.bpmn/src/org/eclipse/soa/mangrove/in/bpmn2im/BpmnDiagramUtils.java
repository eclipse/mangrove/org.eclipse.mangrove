package org.eclipse.soa.mangrove.in.bpmn2im;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.stp.bpmn.BpmnDiagram;

public class BpmnDiagramUtils {
	public static Resource getResourceFromIFile(IFile iFile){
		// Read the bpmn emf model
		ResourceSet resourceSetBpmn = new ResourceSetImpl();
		ImLogger.info(" BPMN File Full Path  " + iFile.getFullPath().toString());
		
		URI uri = URI.createPlatformResourceURI(iFile.getFullPath().toString(),true);
	
		ImLogger.info(Bpmn2ImActivator.PLUGIN_ID," URI FileString " + uri.toFileString());
		ImLogger.info(Bpmn2ImActivator.PLUGIN_ID," URI PlatformString " + uri.toPlatformString(false));
		ImLogger.info(Bpmn2ImActivator.PLUGIN_ID," URI To File " + uri.toString());
		

		Resource resourceBpmn = resourceSetBpmn.getResource(uri, true);	
		
		return resourceBpmn;
	}
	
	public static BpmnDiagram getBpmnDiagramFromResource(Resource resourceBpmn){
		
		BpmnDiagram bpmnDiagram= (BpmnDiagram)resourceBpmn.getContents().get(0);
		return bpmnDiagram;
	
	}
}
