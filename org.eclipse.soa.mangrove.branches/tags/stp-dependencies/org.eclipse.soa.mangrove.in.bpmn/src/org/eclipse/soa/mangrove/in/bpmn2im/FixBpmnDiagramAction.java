package org.eclipse.soa.mangrove.in.bpmn2im;

import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.ActivityType;
import org.eclipse.stp.bpmn.BpmnDiagram;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.stp.bpmn.SequenceEdge;
import org.eclipse.stp.bpmn.SubProcess;
import org.eclipse.stp.bpmn.Vertex;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

public class FixBpmnDiagramAction extends WorkspaceModifyOperation {
	
	private String basicPoolName = "Process";
	private int poolNamesCounter = 0;
	
	private String basicStepName = "Step";
	private int stepNamesCounter = 0;
	
	private String basicStartStep = "Start";
	private int startStepNamesCounter = 0;
	
	private String basicEndStep = "End";
	private int endStepNamesCounter = 0;
	
	private String basicExclusiveStepName = "Router";
	private int exclusiveStepNamesCounter = 0;
	
	private String basicParallelStepName = "Parallel";
	private int parallelStepNamesCounter = 0;
	
	private String basicLoopStepName = "Loop";
	private int loopStepNamesCounter = 0;
	
	private String basicSubProcessName = "SubProcess";
	private int subPocessNamesCounter = 0;
	
	private IFile bpmnFile = null;

	
	private int numberOfFixedError = 0;
	List<String> selectedPools = null;
	
	
	public FixBpmnDiagramAction(IFile bpmnFile){

		this.bpmnFile = bpmnFile;
		this.numberOfFixedError = 0;
		
	}
	
	public FixBpmnDiagramAction(IFile bpmnFile, List<String> selectedPools){
		
		this.bpmnFile = bpmnFile;
		this.numberOfFixedError = 0;
		this.selectedPools = selectedPools;
	
	}
	
	public void resetCounter(){

		poolNamesCounter = 0;
		stepNamesCounter = 0;
		startStepNamesCounter = 0;
		endStepNamesCounter = 0;
		exclusiveStepNamesCounter = 0;
		parallelStepNamesCounter = 0;
		loopStepNamesCounter = 0;
		subPocessNamesCounter = 0;
		
	}
	
	protected void execute(IProgressMonitor monitor)
    {	
			
		try{
    	 		resetCounter();
    	 		
    	 		Resource resourceBpmn = BpmnDiagramUtils.getResourceFromIFile(bpmnFile);
    			BpmnDiagram bpmnDiagram = BpmnDiagramUtils.getBpmnDiagramFromResource(resourceBpmn);
    			
    			int workingUnits = (10 * selectedPools.size()) + 20; 		        											    
    			monitor.beginTask(" Validate Fix/Bpmn Diagram",workingUnits);

    			try{
    				numberOfFixedError = validateFixBpmnDiagramForIntermediateModel(bpmnDiagram, selectedPools, monitor);
    			}catch (BpmnImValidationException e) {
    				ImLogger.error(Bpmn2ImActivator.PLUGIN_ID, "BPM-IM-GenerationException", e);
    				return;
    			}
    			
    			if (numberOfFixedError > 0){
    				resourceBpmn.save(Collections.EMPTY_MAP);
    			}
    			
			}catch(Exception x) {
    			
				throw new RuntimeException(x);
			
			}finally{
			
				if (monitor != null) 
    	 			monitor.done();
			
			} 	
    }
	
	
	
	public int validateFixBpmnDiagramForIntermediateModel(BpmnDiagram bpmnDiagram, List<String> selectedPools, IProgressMonitor monitor) throws BpmnImValidationException{
		
		int numberOfFixedErrors = 0;
		
		
		monitor.subTask(" Validating/Fixing BPMN Diagram ");
		EAnnotation technologyAnnotation = null;
		for (Pool aPool : bpmnDiagram.getPools()){
			
			if (selectedPools != null && !selectedPools.contains(aPool.getID())){
				continue;
			}
			
			
			if (!isValidString(aPool.getName())){
				ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Pool with ID ["+aPool.getID()+"] do not have a Name ");
				poolNamesCounter++;
				ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Fixing error setting name ["+basicPoolName + poolNamesCounter+"] on  Pool with ID ["+aPool.getID()+"] do not have a Name ");
				aPool.setName(basicPoolName + poolNamesCounter);
				numberOfFixedErrors++;
			}
			if ( aPool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION) == null){
				ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Pool with ID ["+aPool.getID()+"] and Name ["+aPool.getName()+"] is not bund to any technology it will result in abstract IM Process");
			}
			
			technologyAnnotation = null;
			technologyAnnotation = aPool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
			if (technologyAnnotation == null){
				ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Pool with ID ["+aPool.getID()+"] and Name ["+aPool.getName()+"] do not have a technology annotation it will result in an abtsract im model  ");
			}else {
				String runtimeId = technologyAnnotation.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
				if ((runtimeId != null) && (runtimeId.equalsIgnoreCase("AbstractModel"))){
					ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Pool with ID ["+aPool.getID()+"] and Name ["+aPool.getName()+"] have AbstractModel result in an abtsract im model  ");
				}
			}
			
		}
		
		
		for (Pool aPool : bpmnDiagram.getPools()){
			
			if (selectedPools != null && !selectedPools.contains(aPool.getID())){
				continue;
			}
			monitor.subTask(" Validating/Fixing Pool "+ aPool.getName());
			List<Vertex> vertices =  aPool.getVertices();
			
			for (Vertex v : vertices){
				if (v instanceof Activity) {
					if (!isValidString( ((Activity)v).getName() ) ){
						ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Activity with ID ["+v.getID()+"] do not have a Name ");
						setAutomaticActivityName((Activity)v);
						numberOfFixedErrors++;
					}
				}
			}
			
  
			for (Vertex v : vertices){
				if ((v instanceof Activity) &&  ((Activity)v).isLooping() &&  (((Activity)v) instanceof SubProcess)) {
					for (Vertex subVertex : ((SubProcess)v).getVertices()){
						if (!isValidString( ((Activity)subVertex).getName() )){
							setAutomaticActivityName(((Activity)subVertex));
							numberOfFixedErrors++;
						}
					}
				}
			}
			
			
			Vertex sourceVertex = null;
			Vertex targetVertex = null;
			for ( SequenceEdge seqEdge : aPool.getSequenceEdges()){
				
				sourceVertex = seqEdge.getSource();
				targetVertex = seqEdge.getTarget();
				
				if (sourceVertex == null){
					throw new BpmnImValidationException(" Transition with ID ["+seqEdge.getID()+"] has no source");
				}
				if (targetVertex == null){
					throw new BpmnImValidationException(" Transition with ID ["+seqEdge.getID()+"] has no target");
				}
				
				//If source is an exclusive gateway transition must be conditioned
				if (sourceVertex instanceof Activity){
					ActivityType at = ((Activity)sourceVertex).getActivityType();
					if (((at.equals(ActivityType.GATEWAY_DATA_BASED_EXCLUSIVE_LITERAL))
						|| (at.equals(at.equals(ActivityType.GATEWAY_EVENT_BASED_EXCLUSIVE_LITERAL)))) && 
						!(isEdgeConditioned(seqEdge))){
						
						ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Transition form exclusive based gateway ["+sourceVertex.getID()+"] must be conditioned");
						//throw new BpmnImValidationException(" Transition form exclusive based gateway ["+sourceVertex.getID()+"] must be conditioned");
					}
					
					if ((at.equals(ActivityType.GATEWAY_PARALLEL)) && 
							(isEdgeConditioned(seqEdge))){
							ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Transition form parallel gateway ["+sourceVertex.getID()+"] must not be conditioned");
							//throw new BpmnImValidationException(" Transition form parallel gateway ["+sourceVertex.getID()+"] must not be conditioned");
						}
				}
			}
			monitor.worked(10);
		}
		return numberOfFixedErrors;
	}

	public IFile getBpmnFile() {
		return bpmnFile;
	}

	public void setBpmnFile(IFile bpmnFile) {
		this.bpmnFile = bpmnFile;
	}
	
	
	
	
	
	
	public String normalizeStepName(String s){
		return s.replace(' ', '_');
	}
	
	public String getStepNameForActivity(Activity a){
		String name = a.getName();
		
		

		if (name  == null){
			ImLogger.info(Bpmn2ImActivator.PLUGIN_ID,"Activity ["+a.getID()+"] has no name id is used");
			name=a.getID();
		}
		return normalizeStepName(name);
	}
	
	public boolean isEdgeConditioned(SequenceEdge seqEdge){
		if (seqEdge.getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION) != null){
			return true;
		}else{
			return false;
		}
			
	}
	
	public boolean isValidString(String name){
		return name != null && name.trim().length() > 0;
	}
	
	public void setAutomaticActivityName(Activity a){
		ActivityType at = a.getActivityType();
		String name = null;
		switch (at.getValue()) {
			case ActivityType.EVENT_START_EMPTY:
			case ActivityType.EVENT_START_LINK:
			case ActivityType.EVENT_START_RULE:	
			case ActivityType.EVENT_START_MULTIPLE:
			case ActivityType.EVENT_START_MESSAGE:{
				this.startStepNamesCounter++;
				name = this.basicStartStep + startStepNamesCounter; 
				break;
			}	
			case ActivityType.EVENT_END_CANCEL:
			case ActivityType.EVENT_END_COMPENSATION:
			case ActivityType.EVENT_END_EMPTY:	
			case ActivityType.EVENT_END_ERROR:
			case ActivityType.EVENT_END_LINK:
			case ActivityType.EVENT_END_MESSAGE:
			case ActivityType.EVENT_END_MULTIPLE:
			case ActivityType.EVENT_END_TERMINATE:{
				this.endStepNamesCounter++;
				name = this.basicEndStep + endStepNamesCounter; 
				break;

			}
			
			case ActivityType.GATEWAY_DATA_BASED_EXCLUSIVE:
			case ActivityType.GATEWAY_EVENT_BASED_EXCLUSIVE:{
				
				this.exclusiveStepNamesCounter++;
				name = this.basicExclusiveStepName + exclusiveStepNamesCounter; 
				break;

			}
			
			case ActivityType.GATEWAY_PARALLEL:{
				this.parallelStepNamesCounter++;
				name = this.basicParallelStepName + parallelStepNamesCounter; 
				break;
			}
			case ActivityType.SUB_PROCESS:{
				if (a.isLooping()){
					this.loopStepNamesCounter++;
					name = this.basicLoopStepName + loopStepNamesCounter; 
					break;
				}else{
					this.subPocessNamesCounter++;
					name = this.basicSubProcessName + subPocessNamesCounter; 
					break;
				}
					
			}
			default:
				this.stepNamesCounter++;
				name = this.basicStepName + stepNamesCounter; 
				break;
			}
			a.setName(name);
		}//
	

		public int getNumberOfFixedError() {
			return numberOfFixedError;
		}

		public void setNumberOfFixedError(int numberOfFixedError) {
			this.numberOfFixedError = numberOfFixedError;
		}
}
