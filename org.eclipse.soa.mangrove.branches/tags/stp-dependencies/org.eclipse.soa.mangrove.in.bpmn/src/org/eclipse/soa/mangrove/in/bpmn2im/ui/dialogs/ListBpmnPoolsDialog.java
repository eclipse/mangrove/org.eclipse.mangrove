/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.in.bpmn2im.ui.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.soa.mangrove.in.bpmn2im.BpmnDiagramUtils;
import org.eclipse.soa.mangrove.in.bpmn2im.ui.providers.BpmnPoolContentProvider;
import org.eclipse.soa.mangrove.in.bpmn2im.ui.providers.BpmnPoolLabelProvider;
import org.eclipse.stp.bpmn.BpmnDiagram;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class ListBpmnPoolsDialog  extends TitleAreaDialog {

	

    private Composite aComposite = null;	
    private SashForm internalSash  = null; 
    
    private Table tablePools = null;
    private TableViewer tbvPools = null; 
    private List<Pool> thePools = null;
    private List<String> selectedPools = null;
    private int status = 0;
    private boolean separateImFiles = false;
		
	public boolean isSeparateImFiles() {
		return separateImFiles;
	}




	public void setSeparateImFiles(boolean separateImFiles) {
		this.separateImFiles = separateImFiles;
	}




	public ListBpmnPoolsDialog(Shell parentShell, IFile bpmnFile ) {
		
		super(parentShell);
		
		Resource resourceBpmn = BpmnDiagramUtils.getResourceFromIFile(bpmnFile);
		BpmnDiagram bpmnDiagram = BpmnDiagramUtils.getBpmnDiagramFromResource(resourceBpmn);
		
		this.thePools = bpmnDiagram.getPools();
		this.selectedPools = new ArrayList<String>();

	}
	
	
				

	/**
	   * @see org.eclipse.jface.window.Window#create() We complete the dialog with
	   *      a title and a message
	   */
	  public void create() {
		super.create();
		setTitle(" Bpmn Pools/Process ");
	    setMessage(" Select Bpmn Pools/Process  ");
	   
	  }

	 
	  

	@Override
	protected Control createContents(Composite parent) {
		Control ctrl = super.createContents(parent);
		return ctrl;
	}
	

	protected Control createDialogArea(Composite parent) {
		
        this.internalSash = new SashForm(parent,  SWT.VERTICAL | SWT.FLAT);
        this.internalSash.setLayoutData(new GridData(GridData.FILL_BOTH));
       	
    	final ListBpmnPoolsDialog theDialog = this;
    	
    	
    	
        this.tablePools = new Table(internalSash, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);
        this.tablePools.setLinesVisible(true);
        this.tablePools.setHeaderVisible(true);
        TableColumn column = null;
		column = new TableColumn(tablePools, SWT.LEFT);
		column.setText("Name");
		column.setWidth(100);
		column = new TableColumn(tablePools, SWT.LEFT);
		column.setText("Technology");
		column.setWidth(200);
		
		
        this.tbvPools = new TableViewer(tablePools);
        tbvPools.setContentProvider(new BpmnPoolContentProvider());
        tbvPools.setLabelProvider(new BpmnPoolLabelProvider());
        tbvPools.setInput(this.thePools);    
    	
      
        aComposite = new Composite(internalSash, SWT.NONE);
		final GridLayout layout = new GridLayout();
    	layout.marginWidth = 15;
    	layout.marginHeight = 10;
    	layout.numColumns = 3;
    	aComposite.setLayout(layout);
    	aComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    	
    	// 1 - row
    	Label label = new Label(aComposite, SWT.NULL);
    	label.setText("IM Generation Options: ");
    	GridData data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	label.setLayoutData(data);
    	  
    	Button oneImFileButton = new Button(aComposite, SWT.RADIO);
    	oneImFileButton.setText("One IM File");
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	oneImFileButton.setLayoutData(data);
    	oneImFileButton.setSelection(true);
    	separateImFiles = false;
    	
    	    
    	Button separateFilesButton = new Button(aComposite, SWT.RADIO);
    	separateFilesButton.addSelectionListener(new SelectionListener() {
			
			
			public void widgetSelected(SelectionEvent e) {
				
				//MessageDialog.openConfirm(getShell(), "SeparateImFileChecked", "SeparateFile = ["+((Button)e.getSource()).getSelection()+"]");
				separateImFiles = ((Button)e.getSource()).getSelection();
				
			}
			
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
				
			}
		});
    	separateFilesButton.setText("Separate IM Files for Pools ");
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	separateFilesButton.setLayoutData(data);
    	
    	
    	// 1 - row
    	Button btnSelectAll = new Button(aComposite, SWT.FLAT);
    	btnSelectAll.setText(" Select All ");
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	btnSelectAll.setLayoutData(data);
    	
    	Button btnSelect = new Button(aComposite, SWT.FLAT);
    	btnSelect.setText(" OK ");
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	btnSelect.setLayoutData(data);
    	
    	Button btnCancel = new Button(aComposite, SWT.FLAT);
    	//btnAddSpagicDatasource.setImage(addDatasorceImage);
    	btnCancel.setText(" Cancel ");
    	data = new GridData(GridData.FILL_HORIZONTAL);
    	data.horizontalSpan = 1;
    	btnCancel.setLayoutData(data);
    	
    	

    	btnSelect.addListener(SWT.Selection,new Listener() {
        	public void handleEvent(Event event) {
        		
        		TableItem[] selectedItems = theDialog.tablePools.getSelection();
        		selectedPools = new ArrayList<String>();
        		if (selectedItems == null || selectedItems.length == 0){
        			MessageDialog.openInformation(getShell(), "Bpmn Pools/Processes", " No Pools Selected ");
        			return;
        		}
        		for (int i = 0; i < selectedItems.length; i++) {
        			selectedPools.add(((Pool)selectedItems[i].getData()).getID());
        		}
        		close();
        	
        	}
    	});
    	
    	btnSelectAll.addListener(SWT.Selection,new Listener() {
    		
    		public void handleEvent(Event event) {
    			theDialog.tablePools.selectAll();
    			theDialog.tbvPools.refresh(true);
    		}
    	});
    	btnCancel.addListener(SWT.Selection,new Listener() {
    		
    		public void handleEvent(Event event) {
    			theDialog.tablePools.deselectAll();
    			theDialog.tbvPools.refresh(true);
    			selectedPools = null;
    			theDialog.status = 1;
    			close();
    		}
    	});
    	
    	internalSash.setWeights(new int[] {60,40});
    	this.tablePools.selectAll();
        return parent;
	}

	
	protected void okPressed() {
		super.okPressed();
		
	}
	



	protected void createButtonsForButtonBar(Composite parent) {
		
		
	}




	public List<String> getSelectedPools() {
		return selectedPools;
	}




	public void setSelectedPools(List<String> selectedPools) {
		this.selectedPools = selectedPools;
	}




	public int getStatus() {
		return status;
	}




	public void setStatus(int status) {
		this.status = status;
	}
	
}
