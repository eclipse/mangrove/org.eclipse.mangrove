/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.in.bpmn2im;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.soa.mangrove.util.ImLogger;

public class BPMNProcessorProxy {

	private static final String ATT_ID = "id";
	private static final String ATT_NAME = "name";
	private static final String ATT_CLASS = "class";
	
	
	private final IConfigurationElement configElement;

	private final String id;
	private final String name;
	

	public BPMNProcessorProxy(IConfigurationElement configElement) {
		this.configElement = configElement;

		id = getAttribute(configElement, ATT_ID, null);
		name = getAttribute(configElement, ATT_NAME, id);
		
		// Make sure that class is defined,
		// but don't load it
		getAttribute(configElement, ATT_CLASS, null);
		
	}

	private static String getAttribute(IConfigurationElement configElem,
			String name, String defaultValue) {
		String value = configElem.getAttribute(name);
		if (value != null)
			return value;
		if (defaultValue != null)
			return defaultValue;
		throw new IllegalArgumentException("Missing " + name + " attribute");
	}
	
	public IBPMNProcessor getBPMNProcessor(){
		try{
			IBPMNProcessor runtime = (IBPMNProcessor) configElement.createExecutableExtension(ATT_CLASS);
			return runtime;
		}catch (Exception e) {
			ImLogger.error(Bpmn2ImActivator.PLUGIN_ID, e.getMessage(), e);
			return null;
		}
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	

}
