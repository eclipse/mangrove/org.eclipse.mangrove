/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.in.bpmn2im.ui.providers;



import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.swt.graphics.Image;

public class BpmnPoolLabelProvider implements ITableLabelProvider
{
	
	public BpmnPoolLabelProvider(){
	}
	
	public String getColumnText(Object element, int column_index) {
		if (element instanceof Pool) {
			 Pool pool =(Pool)element;
			 
			 if (column_index == 0 ){
				 return ( pool.getName() != null ? pool.getName() : pool.getID());
			 }
			 if (column_index == 1){
				 String result = " No Technology ";
				 EAnnotation technologyAnnotation = pool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
				 if (technologyAnnotation != null){
					 String runtimeId  = technologyAnnotation.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
					 String runtimeName  = technologyAnnotation.getDetails().get(ImConstants.TECHNOLOGY_NAME);
					 result = (runtimeName != null ? runtimeName : runtimeId);
				 }
				 return result;
			 }
			 return "";
		 }else{
			 return "";
		 }	 
			 
  }

  public void addListener(ILabelProviderListener ilabelproviderlistener)
  {
  }

  public void dispose()
  {
  }

  public boolean isLabelProperty(Object obj, String s)
  {
    return false;
  }

  public void removeListener(ILabelProviderListener ilabelproviderlistener)
  {
  }

  public Image getColumnImage(Object element, int column_index)
  {
      return null;
    
  }
}

