/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.in.bpmn2im.ui.providers;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.stp.bpmn.Pool;


	public class BpmnPoolContentProvider  implements IStructuredContentProvider {
		
			
			
			public BpmnPoolContentProvider(){
				 super(); 
				
			}
		  
		
			public Object[] getElements(Object element){
				List<Pool> c = (List<Pool>) (element); 
				Object[] arr = c.toArray();
				return arr;
			}	

			public void dispose()
			{
				
			}

			public void inputChanged(Viewer viewer, Object old_object, Object new_object)
			{
				
			}
		  
		  
	}

