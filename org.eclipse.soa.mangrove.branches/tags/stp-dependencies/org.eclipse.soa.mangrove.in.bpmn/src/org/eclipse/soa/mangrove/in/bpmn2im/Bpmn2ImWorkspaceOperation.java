package org.eclipse.soa.mangrove.in.bpmn2im;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.soa.mangrove.BasicProperty;
import org.eclipse.soa.mangrove.Condition;
import org.eclipse.soa.mangrove.ExpressionCondition;
import org.eclipse.soa.mangrove.ExtractDataRule;
import org.eclipse.soa.mangrove.ImFactory;
import org.eclipse.soa.mangrove.MapProperty;
import org.eclipse.soa.mangrove.ObservableAttribute;
import org.eclipse.soa.mangrove.Process;
import org.eclipse.soa.mangrove.ProcessCollection;
import org.eclipse.soa.mangrove.Property;
import org.eclipse.soa.mangrove.PropertyCondition;
import org.eclipse.soa.mangrove.Service;
import org.eclipse.soa.mangrove.ServiceBinding;
import org.eclipse.soa.mangrove.ServiceCollection;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.Transition;
import org.eclipse.soa.mangrove.TransitionUnderCondition;
import org.eclipse.soa.mangrove.Variable;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.IService;
import org.eclipse.soa.mangrove.runtime.IServiceBinding;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.ActivityType;
import org.eclipse.stp.bpmn.Artifact;
import org.eclipse.stp.bpmn.Association;
import org.eclipse.stp.bpmn.AssociationTarget;
import org.eclipse.stp.bpmn.BpmnDiagram;
import org.eclipse.stp.bpmn.DataObject;
import org.eclipse.stp.bpmn.Lane;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.stp.bpmn.SequenceEdge;
import org.eclipse.stp.bpmn.SubProcess;
import org.eclipse.stp.bpmn.Vertex;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

public class Bpmn2ImWorkspaceOperation extends WorkspaceModifyOperation {

	private static String IS_STEP_INTERMEDIATE_EVENT = "im.isStepIntermediateEvent";

	private IFile bpmnFile = null;
	List<String> selectedPools = null;
	private List<IFile> generatedFiles = null;
	
	public List<IFile> getGeneratedFiles() {
		return generatedFiles;
	}

	public void setGeneratedFiles(List<IFile> generatedFiles) {
		this.generatedFiles = generatedFiles;
	}

	private boolean separateImForEachPool = false;

	public boolean isSeparateImForEachPool() {
		return separateImForEachPool;
	}

	public void setSeparateImForEachPool(boolean separateImForEachPool) {
		this.separateImForEachPool = separateImForEachPool;
	}

	
	public Bpmn2ImWorkspaceOperation(IFile bpmnFile) {
		this.bpmnFile = bpmnFile;
		this.generatedFiles = new ArrayList<IFile>();
	}

	public Bpmn2ImWorkspaceOperation(IFile bpmnFile, List<String> selectedPools) {

		this.bpmnFile = bpmnFile;
		this.selectedPools = selectedPools;
		this.generatedFiles = new ArrayList<IFile>();

	}

	protected void execute(IProgressMonitor monitor) {
		try {
			Resource resourceBpmn = BpmnDiagramUtils.getResourceFromIFile(bpmnFile);
			BpmnDiagram bpmnDiagram = BpmnDiagramUtils.getBpmnDiagramFromResource(resourceBpmn);

			List<IBPMNProcessor> processors = Bpmn2ImActivator.getBPMNProcessors();

			if (processors != null) {
				for (IBPMNProcessor processor : processors) {
					if (processor.isApplicable(bpmnDiagram))
						bpmnDiagram = processor.process(bpmnDiagram);
					List<String> addToSelection = processor.selectPools(bpmnDiagram);
					if (addToSelection != null)
						this.selectedPools.addAll(addToSelection);

				}
			}

			int workingUnits = (10 * selectedPools.size()) + 20;
			monitor.beginTask(" Creating Intermediate Model", workingUnits);
			if (separateImForEachPool){
				for ( String poolID : selectedPools ){
					List<String> toUsePoolIDs = new ArrayList<String>();
					toUsePoolIDs.add(poolID);
					StpIntermediateModel stpIm = createIntermediateModelFromBpmn(bpmnDiagram, toUsePoolIDs, monitor);
					
					writeStpIntermediateModel(stpIm, monitor, retrievePoolName(bpmnDiagram, poolID));
				}
			}else{
				StpIntermediateModel stpIm = createIntermediateModelFromBpmn(bpmnDiagram, selectedPools, monitor);
				writeStpIntermediateModel(stpIm, monitor, null);

			}
		} catch (Throwable x) {
			ImLogger.error(x.getMessage(), x);
			throw new RuntimeException(" Error Generating IM From BPMN", x);
		} finally {
			if (monitor != null)
				monitor.done();
		}
	}
	
	public String retrievePoolName(BpmnDiagram bpmn, String poolID){
		for (Pool p : bpmn.getPools()){
			if (p.getID().equals(poolID))
				return p.getName();
		}
		return "pool";
	}

	/**
	 * Copy process parameters from BPMN pool to IM process
	 * 
	 * @param aPool
	 *          Source BPMN pool
	 * @param process
	 *          Target IM process
	 * @param stpModelfactory
	 *          Factory for creating properties
	 */
	private void createProcessParameters(Pool aPool, Process process, ImFactory stpModelfactory) {
		EAnnotation poolParametersAnnotation = aPool.getEAnnotation(ImConstants.PARAMETERS_ANNOTATION);

		if (poolParametersAnnotation != null) {
			// Annotation existing: read all contained parameters
			EMap<String, String> details = poolParametersAnnotation.getDetails();
			EMap<String, Property> propertyMap = null;

			// Create property map for storing all parameters
			MapProperty processParametersMapProperty = stpModelfactory.createMapProperty();
			processParametersMapProperty.setKey(ImConstants.IM_PROCESS_PARAMETERS);
			propertyMap = processParametersMapProperty.getValue();
			process.getProperties().put(ImConstants.IM_PROCESS_PARAMETERS, processParametersMapProperty);

			BasicProperty processParameterBasicProperty = null;

			// Store all parameters in the map
			for (String key : details.keySet()) {
				String propertyValue = details.get(key);

				processParameterBasicProperty = stpModelfactory.createBasicProperty();
				processParameterBasicProperty.setKey(key);
				processParameterBasicProperty.setValue(propertyValue);
				propertyMap.put(key, processParameterBasicProperty);
			}
		}
	}

	public StpIntermediateModel createIntermediateModelFromBpmn(BpmnDiagram bpmnDiagram, List<String> selectedPools,
			IProgressMonitor monitor) {

		ImFactory stpModelfactory = ImFactory.eINSTANCE;
		StpIntermediateModel stpIm = stpModelfactory.createStpIntermediateModel();

		// Working Units : 10 - Iter Configuration
		// 10 - Service Service Binding Generation
		String currentVersion = "0";
		EAnnotation versionAnnotation = bpmnDiagram.getEAnnotation(ImConstants.IM_VERSION_ANNOTATION);
		if (versionAnnotation != null) {
			currentVersion = versionAnnotation.getDetails().get(ImConstants.PROCESS_VERSION);
		}

		BasicProperty iterVersionBasicProperty = stpModelfactory.createBasicProperty();
		iterVersionBasicProperty.setKey(ImConstants.ITER_VERSION);
		iterVersionBasicProperty.setValue(currentVersion);
		stpIm.getProperties().put(ImConstants.ITER_VERSION, iterVersionBasicProperty);

		monitor.subTask("Generating Intermediate Model");
		// -- Iter Configuration
		BasicProperty bp = null;
		MapProperty mp = null;
		monitor.subTask("Iter Configuration");
		if (bpmnDiagram.getEAnnotation(ImConstants.IM_ITER_ANNOTATION) != null) {
			EAnnotation iterAnnotation = bpmnDiagram.getEAnnotation(ImConstants.IM_ITER_ANNOTATION);

			EMap<String, String> iterAnnotationDetails = iterAnnotation.getDetails();

			bp = stpModelfactory.createBasicProperty();
			bp.setKey(ImConstants.IM_ITER_NAME);
			bp.setValue(iterAnnotationDetails.get(ImConstants.IM_ITER_NAME));
			stpIm.getProperties().put(bp.getKey(), bp);

			bp = stpModelfactory.createBasicProperty();
			bp.setKey(ImConstants.IM_ITER_FIRST_PROCESS_NAME);
			bp.setValue(iterAnnotationDetails.get(ImConstants.IM_ITER_FIRST_PROCESS_NAME));
			stpIm.getProperties().put(bp.getKey(), bp);

			String ruleExpr = "";
			// String ruleExprLanguage = "";
			ruleExpr = iterAnnotationDetails.get(ImConstants.IM_ITER_RULE_FOR_DETERMINATION);

			if (ruleExpr != null && ruleExpr.trim().length() > 0) {
				bp = stpModelfactory.createBasicProperty();
				bp.setKey(ImConstants.IM_ITER_RULE_FOR_DETERMINATION);
				bp.setValue(iterAnnotationDetails.get(ImConstants.IM_ITER_RULE_FOR_DETERMINATION));
				stpIm.getProperties().put(bp.getKey(), bp);

				bp = stpModelfactory.createBasicProperty();
				bp.setKey(ImConstants.IM_ITER_RULE_FOR_DETERMINATION_LANGUAGE);
				bp.setValue(iterAnnotationDetails.get(ImConstants.IM_ITER_RULE_FOR_DETERMINATION_LANGUAGE));
				stpIm.getProperties().put(bp.getKey(), bp);
			}

			bp = stpModelfactory.createBasicProperty();
			bp.setKey(ImConstants.IM_ITER_OBSERVABLE_ATTRIBUTE_LIST_SIZE);
			bp.setValue(iterAnnotationDetails.get(ImConstants.IM_ITER_OBSERVABLE_ATTRIBUTE_LIST_SIZE));
			stpIm.getProperties().put(bp.getKey(), bp);

			Integer size = Integer.valueOf(iterAnnotationDetails.get(ImConstants.IM_ITER_OBSERVABLE_ATTRIBUTE_LIST_SIZE));

			mp = stpModelfactory.createMapProperty();
			mp.setKey(ImConstants.IM_ITER_OBSERVABLE_ATTRIBUTE_MAP);

			for (int j = 0; j < size; j++) {
				// Save attribute id
				bp = stpModelfactory.createBasicProperty();
				bp.setKey(MessageFormat.format(ImConstants.IM_ITER_OBSERVABLE_ATTRIBUTE_ID, j));
				bp.setValue(iterAnnotationDetails.get(MessageFormat.format(ImConstants.IM_ITER_OBSERVABLE_ATTRIBUTE_ID, j)));
				mp.getValue().put(bp.getKey(), bp);

				// Save attribute name
				bp = stpModelfactory.createBasicProperty();
				bp.setKey(MessageFormat.format(ImConstants.IM_ITER_OBSERVABLE_ATTRIBUTE_NAME, j));
				bp.setValue(iterAnnotationDetails.get(MessageFormat.format(ImConstants.IM_ITER_OBSERVABLE_ATTRIBUTE_NAME, j)));
				mp.getValue().put(bp.getKey(), bp);
			}

			stpIm.getProperties().put(mp.getKey(), mp);

		}
		monitor.worked(10);
		ProcessCollection processCollection = stpModelfactory.createProcessCollection();
		ServiceCollection serviceCollection = stpModelfactory.createServiceCollection();

		Map<String, Service> imServicesMap = new HashMap<String, Service>();
		Map<String, ServiceBinding> imServiceBindingsMap = new HashMap<String, ServiceBinding>();

		List<IRuntime> usedRuntimes = new ArrayList<IRuntime>();
		EAnnotation technologyAnnotation = null;
		String runtimeId = null;

		//
		// -- Check all runtimes for which we need to define services and service
		// binding
		//
		for (Pool aPool : bpmnDiagram.getPools()) {

			if (selectedPools != null && !selectedPools.contains(aPool.getID())) {
				continue;
			}

			technologyAnnotation = aPool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
			if (technologyAnnotation != null) {
				runtimeId = technologyAnnotation.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
				if (runtimeId == null) {
					// --
				} else {
					usedRuntimes.add(ImRuntimeActivator.getRuntime(runtimeId));
				}
			}

		}
		monitor.subTask("Generating Services And Service Bindings ");
		BasicProperty runtimeIdProperty = null;
		for (IRuntime aRuntime : usedRuntimes) {
			// For each runtime -- create the bindings
			Map<String, IServiceBinding> runtimeServiceBinding = aRuntime.getServiceBindings();
			ServiceBinding aServiceBinding = null;
			runtimeId = aRuntime.getId();
			for (IServiceBinding serviceBinding : runtimeServiceBinding.values()) {
				if (!(imServiceBindingsMap.containsKey(serviceBinding.getName()))) {
					aServiceBinding = stpModelfactory.createServiceBinding();
					aServiceBinding.setName(serviceBinding.getName());
					runtimeIdProperty = stpModelfactory.createBasicProperty();
					runtimeIdProperty.setKey(ImConstants.IM_STEP_BINDING_RUNTIME_ID);
					runtimeIdProperty.setValue(runtimeId);
					aServiceBinding.getProperties().put(runtimeIdProperty.getKey(), runtimeIdProperty);
					stpIm.getServicebindings().add(aServiceBinding);
					imServiceBindingsMap.put(serviceBinding.getName(), aServiceBinding);
				} else {
					ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " ServiceBinding  [ " + serviceBinding.getName()
							+ "] already Created in IM");
				}
			}

			Service service = null;
			String serviceEntity = null;
			for (IService aService : aRuntime.getServices()) {
				if (!(imServicesMap.containsKey(aService.getName()))) {
					serviceEntity = aService.getEmfServiceEntity();
					if (serviceEntity == null)
						service = stpModelfactory.createService();
					else if (serviceEntity.equalsIgnoreCase(ImConstants.SERVICE_SPLIT_CONTROL))
						service = stpModelfactory.createSplitControl();
					else if (serviceEntity.equalsIgnoreCase(ImConstants.SERVICE_JOIN_CONTROL))
						service = stpModelfactory.createJoinControl();
					else if (serviceEntity.equalsIgnoreCase(ImConstants.SERVICE_ROUTER_CONTROL))
						service = stpModelfactory.createRouterControl();
					else if (serviceEntity.equalsIgnoreCase(ImConstants.SERVICE_ITERATION_CONTROL))
						service = stpModelfactory.createIterationControl();
					else
						service = stpModelfactory.createService();

					service.setServiceName(aService.getName());
					service.setServiceType("BPMN-Service");
					// System.out.println(" service ["+service.getServiceName()+"]");
					for (String bindingNameForService : aService.getServiceBindingsName()) {
						// System.out.println(" bindingNameForService ["+bindingNameForService+"]");
						// System.out.println(" imServiceBindingsMap.get(bindingNameForService) "
						// + imServiceBindingsMap.get(bindingNameForService));
						service.getBindings().add(imServiceBindingsMap.get(bindingNameForService));
					}

					serviceCollection.getServices().add(service);
					imServicesMap.put(aService.getName(), service);
				} else {
					ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Service [ " + aService.getName() + "] already Created in IM");
				}
			}
		}

		monitor.worked(10);
		Process process = null;

		Map<String, Step> stepMapForPool = new HashMap<String, Step>();
		for (Pool aPool : bpmnDiagram.getPools()) {
			if (selectedPools != null && !selectedPools.contains(aPool.getID())) {
				continue;
			}
			aPool.setName(aPool.getName());
			monitor.subTask("Generating IM Process for Pool " + aPool.getName());
			stepMapForPool.clear();
			process = stpModelfactory.createProcess();
			process.setName(aPool.getName());

			// add lanes to the process

			BasicProperty processProperty = null;
			EList<Lane> laneList = aPool.getLanes();
			for (Lane lane : laneList) {
				String laneName = lane.getName();
				String prop = "swimlane";
				if (process.getProperties().containsKey(prop)) {
					processProperty = (BasicProperty) process.getProperties().get(prop);
					laneName = processProperty.getValue() + "#" + laneName;
				} else {
					processProperty = stpModelfactory.createBasicProperty();
					processProperty.setKey(prop);
				}
				processProperty.setValue(laneName);
				process.getProperties().put(prop, processProperty);
			}

			process.setDescription(aPool.getDocumentation());
			BasicProperty versionBasicProperty = stpModelfactory.createBasicProperty();
			versionBasicProperty.setKey(ImConstants.PROCESS_VERSION);
			versionBasicProperty.setValue(currentVersion);
			process.getProperties().put(ImConstants.PROCESS_VERSION, versionBasicProperty);

			technologyAnnotation = aPool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
			if (technologyAnnotation != null) {

				// Annotation existing: read all contained parameters
				EMap<String, String> details = technologyAnnotation.getDetails();
				BasicProperty processParameterBasicProperty = null;

				// Store all parameters in the map
				for (String key : details.keySet()) {
					String propertyValue = details.get(key);

					processParameterBasicProperty = stpModelfactory.createBasicProperty();
					processParameterBasicProperty.setKey(key);
					processParameterBasicProperty.setValue(propertyValue);
					process.getProperties().put(key, processParameterBasicProperty);
				}

			}

			// Copy process parameters from BPMN pool to IM process
			createProcessParameters(aPool, process, stpModelfactory);

			List<Vertex> vertices = aPool.getVertices();

			Variable var = null;
			// VARIABLE ARE DATA OBJECTS WITH NO ANNOTATION AND THE NAME SETTED
			for (Artifact anArtifact : aPool.getArtifacts()) {
				if ((anArtifact instanceof DataObject) && (anArtifact.getEAnnotations().size() == 0)) {
					EList<Association> associations = anArtifact.getAssociations();
					if (associations == null || associations.size() == 0) {
						var = stpModelfactory.createVariable();
						var.setName(anArtifact.getName());
						process.getProcessVariables().add(var);
					}
				}
			}// for

			for (Vertex v : vertices) {

				if (v instanceof Activity) {

					Activity a = (Activity) v;

					Step aStep = createStepForActivity(a, stpModelfactory, imServicesMap, imServiceBindingsMap);
					process.getSteps().add(aStep);
					stepMapForPool.put(aStep.getName(), aStep);
					if (a.isLooping() || a.isSetLooping()) {
						// System.out.println(" Is Looping "+ a);
						if (a instanceof SubProcess) {
							// Generate step for each loop node
							for (Vertex vertexSubProcess : ((SubProcess) a).getVertices()) {

								aStep = createStepForActivity((Activity) vertexSubProcess, stpModelfactory, imServicesMap,
										imServiceBindingsMap);
								process.getSteps().add(aStep);
								stepMapForPool.put(aStep.getName(), aStep);

							}

							Step endLoopStep = stpModelfactory.createStep();
							endLoopStep.setName("End." + getStepNameForActivity(a));
							endLoopStep.setDescription(" Description for " + a.getName());
							process.getSteps().add(endLoopStep);
							stepMapForPool.put(endLoopStep.getName(), endLoopStep);
						}
					}

					// -- DB Begin

					for (EAnnotation annotation : a.getEAnnotations()) {
						if (annotation.getSource().equals("genericFile")) {
							EMap<String, String> details = annotation.getDetails();
							if (details != null) {
								String filepath = details.get("projectRelativePath");
								if (filepath != null && filepath.trim().length() > 0) {
									BasicProperty property = ImFactory.eINSTANCE.createBasicProperty();
									property.setKey("documentationRelativePath");
									property.setValue(filepath);
									aStep.getProperties().put(property.getKey(), property);
								}
							}
						}

						createTaskConfigProp(annotation, aStep, stpModelfactory);
					}
					// -- DB End
				} else {
					ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Not Recognized v [" + v.getID() + "] as Activity");
				}

			}

			Vertex sourceVertex = null;
			Vertex targetVertex = null;

			for (SequenceEdge seqEdge : aPool.getSequenceEdges()) {

				sourceVertex = seqEdge.getSource();
				targetVertex = seqEdge.getTarget();

				if ((sourceVertex instanceof Activity) && (targetVertex instanceof Activity)) {
					Activity sourceActivity = (Activity) sourceVertex;
					Activity targetActivity = (Activity) targetVertex;

					// Aggiungo le Transizioni del Loop
					if (sourceActivity.isLooping() && sourceActivity.isSetLooping()) {

						if (sourceActivity instanceof SubProcess) {

							for (SequenceEdge seqEdgeSubPool : ((SubProcess) sourceActivity).getSequenceEdges()) {
								Vertex sourceVertexSubProcess = seqEdgeSubPool.getSource();
								Vertex targetVertexSubProcess = seqEdgeSubPool.getTarget();

								if ((sourceVertexSubProcess instanceof Activity) && (sourceVertexSubProcess instanceof Activity)) {

									Activity sourceActivitySubProcess = (Activity) sourceVertexSubProcess;
									Activity targetActivitySubProcess = (Activity) targetVertexSubProcess;

									Transition aTransition = createTransition(sourceActivitySubProcess, targetActivitySubProcess,
											seqEdge, stpModelfactory, stepMapForPool);
									if (aTransition != null) {
										process.getTransitions().add(aTransition);
									}
								}
							}
							// Found the firstLoopActivity
							// The first Loop Activity is an activity that is only a source
							// activity
							Activity firstLoopActivity = null;
							for (Vertex vertexSubProcess : ((SubProcess) sourceActivity).getVertices()) {
								if ((vertexSubProcess instanceof Activity) && (vertexSubProcess.getIncomingEdges().size() == 0)) {
									firstLoopActivity = (Activity) vertexSubProcess;

								}
							}

							// Add Transition to get from loop step to loop first step
							Transition firstLoopTransition = stpModelfactory.createTransition();

							firstLoopTransition.setSource(stepMapForPool.get(getStepNameForActivity(sourceActivity)));
							firstLoopTransition.setTarget(stepMapForPool.get(getStepNameForActivity(firstLoopActivity)));

							process.getTransitions().add(firstLoopTransition);
							Step endLoopStep = stepMapForPool.get("End." + getStepNameForActivity(sourceActivity));

							for (Vertex vertexSubProcess : ((SubProcess) sourceActivity).getVertices()) {
								if ((vertexSubProcess instanceof Activity) && (vertexSubProcess.getOutgoingEdges().size() == 0)) {
									{
										Transition endLoopTransition = stpModelfactory.createTransition();

										endLoopTransition
												.setSource(stepMapForPool.get(getStepNameForActivity((Activity) vertexSubProcess)));
										endLoopTransition.setTarget(endLoopStep);
										process.getTransitions().add(endLoopTransition);
									}
								}
							}

							Transition endLoopOriginalTargetTransition = stpModelfactory.createTransition();

							endLoopOriginalTargetTransition.setSource(endLoopStep);
							endLoopOriginalTargetTransition.setTarget(stepMapForPool.get(getStepNameForActivity(targetActivity)));

							process.getTransitions().add(endLoopOriginalTargetTransition);

						}
					} else {
						Transition aTransition = createTransition(sourceActivity, targetActivity, seqEdge, stpModelfactory,
								stepMapForPool);
						if (aTransition != null) {
							process.getTransitions().add(aTransition);
						}
					}
				} else {
					ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, " Not Recognized Activity in Edge Analysis");
				}

			}

			processCollection.getProcesses().add(process);
			monitor.worked(10);

			createTaskVariables(process, aPool, stpModelfactory);

		}

		stpIm.setServiceCollection(serviceCollection);
		stpIm.setProcessCollection(processCollection);

		return stpIm;

	}

	public Transition createTransition(Activity sourceActivity, Activity targetActivity, SequenceEdge seqEdge,
			ImFactory stpModelfactory, Map<String, Step> stepMapForPool) {

		String sourceStepName = null;
		String targetStepName = null;
		Transition aTransition = null;
		boolean isConditioned = false;
		Condition condition = null;

		sourceStepName = getStepNameForActivity(sourceActivity);
		targetStepName = getStepNameForActivity(targetActivity);
		isConditioned = isEdgeConditioned(seqEdge);
		if (isConditioned) {
			aTransition = stpModelfactory.createTransitionUnderCondition();
		} else {
			aTransition = stpModelfactory.createTransition();

			if (seqEdge.getName() != null && seqEdge.getName().trim().length() > 0) {
				BasicProperty transitionProperty = stpModelfactory.createBasicProperty();
				transitionProperty.setKey("TransitionLabel");
				transitionProperty.setValue(seqEdge.getName());
				aTransition.getProperties().put(transitionProperty.getKey(), transitionProperty);
			}
		}

		aTransition.setSource(stepMapForPool.get(sourceStepName));
		aTransition.setTarget(stepMapForPool.get(targetStepName));

		if (isConditioned) {

			condition = createConditionForTransition(stpModelfactory, seqEdge, aTransition);
			if (condition != null)
				((TransitionUnderCondition) aTransition).setCondition(condition);

		}

		return aTransition;
	}

	public Step createStepForActivity(Activity a, ImFactory stpModelfactory, Map<String, Service> imServicesMap,
			Map<String, ServiceBinding> imServiceBindingsMap) {

		Step aStep = stpModelfactory.createStep();

		String stepServiceName = null;
		String stepServiceBindingName = null;
		String stepRuntimeID = null;
		EAnnotation imAnnotation = null;
		BasicProperty stepProperty = null;
		aStep.setName(getStepNameForActivity(a));
		aStep.setDescription(" Description for " + a.getName());

		// add lanes to the step

		EList<Lane> laneList = a.getLanes();
		for (Lane lane : laneList) {
			String laneName = lane.getName();
			String prop = "swimlane";
			if (aStep.getProperties().containsKey(prop)) {
				stepProperty = (BasicProperty) aStep.getProperties().get(prop);
				laneName = stepProperty.getValue() + "#" + laneName;
			} else {
				stepProperty = stpModelfactory.createBasicProperty();
				stepProperty.setKey(prop);
			}
			stepProperty.setValue(laneName);
			aStep.getProperties().put(prop, stepProperty);
		}

		checkStartEndEvent(aStep, a, stpModelfactory);
		checkGatewayElement(aStep, a, stpModelfactory);
		checkIntermediateEvent(aStep, a, stpModelfactory);
		imAnnotation = a.getEAnnotation(ImConstants.IM_ANNOTATION);

		if (imAnnotation != null) {

			stepServiceName = imAnnotation.getDetails().get(ImConstants.IM_SERVICE_NAME);

			stepServiceBindingName = imAnnotation.getDetails().get(ImConstants.IM_SERVICE_BINDING_NAME);

			for (String prop : imAnnotation.getDetails().keySet()) {

				if (prop.equalsIgnoreCase(ImConstants.IM_SERVICE_NAME)) {
					stepServiceName = imAnnotation.getDetails().get(prop);
					continue;
				}

				if (prop.equalsIgnoreCase(ImConstants.IM_SERVICE_BINDING_NAME)) {
					stepServiceBindingName = imAnnotation.getDetails().get(prop);
					continue;
				}

				if (prop.equalsIgnoreCase(ImConstants.IM_POOL_RUNTIME_ID)) {
					stepRuntimeID = imAnnotation.getDetails().get(prop);
					continue;
				}

				stepProperty = stpModelfactory.createBasicProperty();
				stepProperty.setKey(prop);
				stepProperty.setValue(imAnnotation.getDetails().get(prop));
				aStep.getProperties().put(stepProperty.getKey(), stepProperty);
			}

			ImLogger.info(Bpmn2ImActivator.PLUGIN_ID, "Setting service [" + stepServiceName + "] as Service on this step");
			ImLogger.info(Bpmn2ImActivator.PLUGIN_ID, "Setting serviceBinding [" + stepServiceBindingName
					+ "] as Service on this step");
			Service stepService = imServicesMap.get(stepServiceName);
			ServiceBinding stepServiceBinding = imServiceBindingsMap.get(stepServiceBindingName);

			aStep.setServiceModel(stepService);
			if (stepServiceBinding != null) {
				aStep.getStepbindings().add(stepServiceBinding);
			} else {
				ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, "stepServiceBinding is null ");
			}

			Artifact anArtifact = null;
			EAnnotation relDataAnnotation = null;
			for (Association anAssociation : a.getAssociations()) {
				anArtifact = anAssociation.getSource();
				relDataAnnotation = anArtifact.getEAnnotation(ImConstants.IM_OBSERVABLE_ATTRIBUTE);
				if (relDataAnnotation != null) {

					EMap<String, String> relDataDetails = relDataAnnotation.getDetails();

					Integer size = Integer.valueOf(relDataDetails.get(ImConstants.IM_OBSERVABLE_ATTRIBUTE_LIST_SIZE));

					String attributeId = null;
					String attributeName = null;
					String ruleId = null;
					String ruleName = null;
					String ruleExpression = null;
					String ruleLanguage = null;
					String ruleSubjectToCondition = null;
					String conditionExpression = null;
					String conditionExpressionLanguage = null;

					ObservableAttribute obsAttribute = null;
					ExtractDataRule extractDataRule = null;
					for (int i = 1; i <= size; i++) {

						attributeId = relDataDetails.get(MessageFormat.format(ImConstants.IM_OBSERVABLE_ATTRIBUTE_ID, i));
						attributeName = relDataDetails.get(MessageFormat.format(ImConstants.IM_OBSERVABLE_ATTRIBUTE_NAME, i));
						ruleId = relDataDetails.get(MessageFormat.format(ImConstants.IM_OBSERVABLE_ATTRIBUTE_RULE_ID, i));
						ruleName = relDataDetails.get(MessageFormat.format(ImConstants.IM_OBSERVABLE_ATTRIBUTE_RULE_NAME, i));

						ruleExpression = relDataDetails.get(MessageFormat.format(
								ImConstants.IM_OBSERVABLE_ATTRIBUTE_RULE_EXPRESSION, i));

						ruleLanguage = relDataDetails.get(MessageFormat
								.format(ImConstants.IM_OBSERVABLE_ATTRIBUTE_RULE_LANGUAGE, i));

						ruleSubjectToCondition = relDataDetails.get(MessageFormat.format(
								ImConstants.IM_OBSERVABLE_ATTRIBUTE_RULE_SUBJECT_TO_CONDITION, i));

						conditionExpression = relDataDetails.get(MessageFormat.format(
								ImConstants.IM_OBSERVABLE_ATTRIBUTE_CONDITION_EXPRESSION, i));

						conditionExpressionLanguage = relDataDetails.get(MessageFormat.format(
								ImConstants.IM_OBSERVABLE_ATTRIBUTE_CONDITION_EXPRESSION_LANGUAGE, i));

						extractDataRule = stpModelfactory.createExtractDataRule();
						extractDataRule.setIdRule(ruleId);
						extractDataRule.setName(ruleName);
						extractDataRule.setExpression(ruleExpression);
						extractDataRule.setExpressionLanguage(ruleLanguage);

						if (Boolean.valueOf(ruleSubjectToCondition)) {

							ExpressionCondition exprCondition = stpModelfactory.createExpressionCondition();
							exprCondition.setExpression(conditionExpression);
							exprCondition.setExpressionLanguage(conditionExpressionLanguage);
							extractDataRule.setCondition(exprCondition);

						}

						obsAttribute = stpModelfactory.createObservableAttribute();
						obsAttribute.setIdAttribute(attributeId);
						obsAttribute.setName(attributeName);
						obsAttribute.setObservableAttributeExtractRule(extractDataRule);
						aStep.getObservableAttributes().add(obsAttribute);
					}
				}//
			}//
		} else {
			ImLogger.warning(Bpmn2ImActivator.PLUGIN_ID, "Activity [" + a.getName() + "] does not have annotation");
		}
		return aStep;
	}

	public String normalizeName(String s) {
		return s.replace(' ', '_');
	}

	public String getStepNameForActivity(Activity a) {
		String name = a.getName();

		if (name == null) {
			ImLogger.info(Bpmn2ImActivator.PLUGIN_ID, "Activity [" + a.getID() + "] has no name id is used");
			name = a.getID();
		}
		return name;
		// return normalizeName(name);
	}

	public boolean isEdgeConditioned(SequenceEdge seqEdge) {
		if (seqEdge.getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION) != null) {
			return true;
		} else {
			return false;
		}

	}

	public Condition createConditionForTransition(ImFactory stpModelFactory, SequenceEdge seqEdge, Transition aTransition) {
		EAnnotation transitionAnnotation = seqEdge.getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION);
		EMap<String, String> transitionAnnotationDetails = transitionAnnotation.getDetails();
		Condition condition = null;
		String transitionType = transitionAnnotationDetails.get(ImConstants.IM_TRANSITION_TYPE);
		if (transitionType.equalsIgnoreCase(ImConstants.IM_TRANSITION_TYPE_EXPRESSION_CONDITION)) {
			condition = stpModelFactory.createExpressionCondition();
			((ExpressionCondition) condition).setExpressionLanguage("XPath");
			((ExpressionCondition) condition).setExpression(transitionAnnotationDetails.get("Expression"));
		} else if (transitionType.equalsIgnoreCase(ImConstants.IM_TRANSITION_TYPE_PROPERTY_CONDITION)) {
			condition = stpModelFactory.createPropertyCondition();
			((PropertyCondition) condition).setOperator(transitionAnnotationDetails.get("Operator"));
			((PropertyCondition) condition).setPropertyName(transitionAnnotationDetails.get("PropertyName"));
			((PropertyCondition) condition).setPropertyValue(transitionAnnotationDetails.get("PropertyValue"));
		} else {
			for (String prop : transitionAnnotationDetails.keySet()) {
				BasicProperty transitionProperty = stpModelFactory.createBasicProperty();
				transitionProperty.setKey(prop);
				transitionProperty.setValue(transitionAnnotationDetails.get(prop));
				aTransition.getProperties().put(transitionProperty.getKey(), transitionProperty);
			}
		}
		return condition;

	}

	public boolean isValidString(String name) {
		return name != null && name.trim().length() > 0;
	}

	public void writeStpIntermediateModel(StpIntermediateModel aStpIntermediateModel, IProgressMonitor monitor, String forcedImFileName)
			throws Exception {
		IContainer container = bpmnFile.getParent();

		// Create a resource set.
		ResourceSet resourceSetStpIm = new ResourceSetImpl();
		// Get the URI of the model file.
		String destinationStpFileName = bpmnFile.getName();
		
		if (forcedImFileName == null){
			destinationStpFileName = destinationStpFileName.substring(0, destinationStpFileName.indexOf("."));
			destinationStpFileName = destinationStpFileName + ".im";
		}else{
			destinationStpFileName = forcedImFileName + ".im";
		}

		IFile destinationStpFile = container.getFile(new Path(destinationStpFileName));
		URI destinationStpFileURI = URI.createPlatformResourceURI(destinationStpFile.getFullPath().toString(), true);

		// Create a resource for this file.
		Resource resourceStpIm = resourceSetStpIm.createResource(destinationStpFileURI);
		resourceStpIm.getContents().add(aStpIntermediateModel);

		resourceStpIm.save(Collections.EMPTY_MAP);
		
		this.generatedFiles.add(destinationStpFile);
		

	}

	private void checkIntermediateEvent(Step aStep, Activity a, ImFactory stpModelFactory) {
		ActivityType at = a.getActivityType();

		switch (at.getValue()) {
		case ActivityType.EVENT_INTERMEDIATE_EMPTY:
		case ActivityType.EVENT_INTERMEDIATE_MESSAGE:
		case ActivityType.EVENT_INTERMEDIATE_TIMER:
		case ActivityType.EVENT_INTERMEDIATE_ERROR:
		case ActivityType.EVENT_INTERMEDIATE_COMPENSATION:
		case ActivityType.EVENT_INTERMEDIATE_RULE:
		case ActivityType.EVENT_INTERMEDIATE_LINK:
		case ActivityType.EVENT_INTERMEDIATE_MULTIPLE:
		case ActivityType.EVENT_INTERMEDIATE_CANCEL: {
			BasicProperty isIntermediateEventProperty = stpModelFactory.createBasicProperty();
			isIntermediateEventProperty.setKey(IS_STEP_INTERMEDIATE_EVENT);
			isIntermediateEventProperty.setValue("true");
			aStep.getProperties().put(IS_STEP_INTERMEDIATE_EVENT, isIntermediateEventProperty);
			break;
		}
		default:
			break;
		}
		return;
	}

	private void checkStartEndEvent(Step aStep, Activity a, ImFactory stpModelFactory) {
		ActivityType at = a.getActivityType();

		switch (at.getValue()) {
		case ActivityType.EVENT_START_EMPTY:
			BasicProperty isStartEventProperty = stpModelFactory.createBasicProperty();
			isStartEventProperty.setKey(ImConstants.IS_STEP_START_EVENT);
			isStartEventProperty.setValue("true");
			aStep.getProperties().put(ImConstants.IS_STEP_START_EVENT, isStartEventProperty);
			break;
		case ActivityType.EVENT_END_EMPTY: {
			BasicProperty isEndEventProperty = stpModelFactory.createBasicProperty();
			isEndEventProperty.setKey(ImConstants.IS_STEP_END_EVENT);
			isEndEventProperty.setValue("true");
			aStep.getProperties().put(ImConstants.IS_STEP_END_EVENT, isEndEventProperty);
			break;
		}
		default:
			break;
		}
		return;
	}

	private void checkGatewayElement(Step aStep, Activity a, ImFactory stpModelFactory) {
		ActivityType at = a.getActivityType();

		switch (at.getValue()) {
		case ActivityType.GATEWAY_EVENT_BASED_EXCLUSIVE:
			BasicProperty isExclusiveGateway = stpModelFactory.createBasicProperty();
			isExclusiveGateway.setKey(ImConstants.IS_EXCLUSIVE_GATEWAY);
			isExclusiveGateway.setValue("true");
			aStep.getProperties().put(ImConstants.IS_EXCLUSIVE_GATEWAY, isExclusiveGateway);
			break;
		case ActivityType.GATEWAY_DATA_BASED_EXCLUSIVE:
			BasicProperty isExclusiveGateway1 = stpModelFactory.createBasicProperty();
			isExclusiveGateway1.setKey(ImConstants.IS_EXCLUSIVE_GATEWAY);
			isExclusiveGateway1.setValue("true");
			aStep.getProperties().put(ImConstants.IS_EXCLUSIVE_GATEWAY, isExclusiveGateway1);
			break;
		case ActivityType.GATEWAY_PARALLEL: {
			BasicProperty isParallelGateway = stpModelFactory.createBasicProperty();
			isParallelGateway.setKey(ImConstants.IS_PARALLEL_GATEWAY);
			isParallelGateway.setValue("true");
			aStep.getProperties().put(ImConstants.IS_PARALLEL_GATEWAY, isParallelGateway);
			break;
		}
		default:
			break;
		}
		return;
	}

	// -DB

	protected void createTaskConfigProp(EAnnotation annotation, Step step, ImFactory stpModelfactory) {
	}

	private void createTaskVariables(Process process, Pool aPool, ImFactory stpModelfactory) {
		EList<Step> steps = process.getSteps();
		for (Artifact anArtifact : aPool.getArtifacts()) {
			if ((anArtifact instanceof DataObject) && (anArtifact.getEAnnotations().size() == 0)) {
				DataObject dataObject = (DataObject) anArtifact;
				EList<Association> associations = dataObject.getAssociations();
				if (associations != null && associations.size() > 0) {
					for (Association association : associations) {
						AssociationTarget associationTarget = association.getTarget();
						if (associationTarget instanceof Activity) {
							Activity target = (Activity) associationTarget;
							String stepName = target.getName();
							Step step = null;
							if (stepName != null && stepName.trim().length() > 0) {
								for (Step astep : steps) {
									if (astep.getName().equals(stepName)) {
										step = astep;
										break;
									}
								}
							}
							if (step != null) {
								EMap<String, Property> props = step.getProperties();
								MapProperty variables = (MapProperty) props.get("taskVariables");
								if (variables == null) {
									variables = stpModelfactory.createMapProperty();
								}
								BasicProperty variable = stpModelfactory.createBasicProperty();
								String varName = dataObject.getName();
								variable.setKey(varName);
								variable.setValue(varName);
								variables.getValue().put(varName, variable);
								props.put("taskVariables", variables);
							}
						}
					}
				}
			}
		}// for

	}
}
