/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.in.bpmn2im.popup.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.soa.mangrove.in.bpmn2im.Bpmn2ImActivator;
import org.eclipse.soa.mangrove.in.bpmn2im.Bpmn2ImWorkspaceOperation;
import org.eclipse.soa.mangrove.in.bpmn2im.FixBpmnDiagramAction;
import org.eclipse.soa.mangrove.in.bpmn2im.ui.dialogs.ListBpmnPoolsDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

public class Bpmn2ImAction implements IObjectActionDelegate {

	private IFile bpmnFile = null;

	/**
	 * Constructor for Action1.
	 */
	public Bpmn2ImAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		try {
			
			ListBpmnPoolsDialog selectPoolsDialog = new ListBpmnPoolsDialog(shell,bpmnFile);
			int status = selectPoolsDialog.open();
			
			if ((status == 1) || ( selectPoolsDialog.getStatus() == 1)){
			
				MessageDialog.openInformation(shell,
						" Information",
						" Operation Cancelled");
				return;
			
			}
			
			List<String> idSelectedPools = selectPoolsDialog.getSelectedPools();
			
			if ((idSelectedPools == null) || (idSelectedPools.isEmpty())) {
				MessageDialog.openInformation(shell,
						" Information",
						" No Pool Selected ");
				return;
			}
			
			boolean sepIm = selectPoolsDialog.isSeparateImFiles();
			
			WorkspaceModifyOperation operationFixBpmnDiagram = new FixBpmnDiagramAction(bpmnFile, idSelectedPools);
			PlatformUI.getWorkbench().getProgressService().run(false, false, operationFixBpmnDiagram);
			
			WorkspaceModifyOperation createIMOperation = new Bpmn2ImWorkspaceOperation(bpmnFile, idSelectedPools);
			((Bpmn2ImWorkspaceOperation)createIMOperation).setSeparateImForEachPool(sepIm);
			PlatformUI.getWorkbench().getProgressService().run(false, false, createIMOperation);
			
			MessageDialog.openInformation(shell,
					"Information",
					"Export BPMN To Intermediate Model Completed");
			
		} catch (InvocationTargetException x) {
			reportError(shell, x);
		} catch (InterruptedException x) {
			reportError(shell, x);
		} catch (Throwable e) {
			reportError(shell, e);
		}
	}

	/**
	 * Displays an error that occured during the project creation.
	 * 
	 * @param x
	 *            details on the error
	 */
	private void reportError(Shell shell, Throwable x) {
		ErrorDialog.openError(shell, "Error", "Error Occurred", makeStatus(x));
	}

	public static IStatus makeStatus(Throwable x) {

		return new Status(IStatus.ERROR, Bpmn2ImActivator.PLUGIN_ID, IStatus.ERROR,
				(x.getMessage() != null) ? x.getMessage()
				: (x.getCause() != null) ? x.getCause().getMessage() : "", null);


	}
	
	public final void selectionChanged(final IAction action,
			final ISelection selection) {
		
		setBpmnFile((IFile) ((StructuredSelection) selection)
				.getFirstElement());
	}
	public IFile getBpmnFile() {
		return bpmnFile;
	}

	public void setBpmnFile(IFile bpmnFile) {
		this.bpmnFile = bpmnFile;
	}
}
