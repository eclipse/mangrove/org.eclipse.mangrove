/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.in.bpmn2im;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.runtime.RuntimeDescriptorProxy;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Bpmn2ImActivator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.soa.mangrove.in.bpmn";

	// The shared instance
	private static Bpmn2ImActivator plugin;
	
	private static List<IBPMNProcessor> bpmnProcessors = null;

	public static final String BPMN_PROCESSOR_TAG= "bpmn-processor";
	
	/**
	 * The constructor
	 */
	public Bpmn2ImActivator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Bpmn2ImActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static void initBPMNProcessors() {
		
		try{
			IExtensionRegistry extensionPointRegistry   = Platform.getExtensionRegistry ();
			IExtensionPoint    extensionPoint = extensionPointRegistry.getExtensionPoint (PLUGIN_ID, "bpmnprocessor");

			IExtension[] extensions = extensionPoint.getExtensions();
			
			bpmnProcessors = new ArrayList<IBPMNProcessor>();
			for (int i = 0; i < extensions.length; i++) {
				IConfigurationElement[] configElements = extensions[i]
					.getConfigurationElements();
				for (int j = 0; j < configElements.length; j++) {
					BPMNProcessorProxy proxy = parseBPMNDescriptorProxy(
						configElements[j]);
					if (proxy != null){
						bpmnProcessors.add(proxy.getBPMNProcessor());
					}
				}
			}
			return;
		}catch (Throwable t) {
			ImLogger.error(ImRuntimeActivator.PLUGIN_ID, t.getMessage(), t);
			bpmnProcessors = null;
			
		}
	}
	
	private static BPMNProcessorProxy parseBPMNDescriptorProxy(IConfigurationElement configElement) {
		if (!configElement.getName().equals(BPMN_PROCESSOR_TAG))
			return null;
		try {
			return new BPMNProcessorProxy(configElement);
		}
		catch (Exception e) {
			ImLogger.error(Bpmn2ImActivator.PLUGIN_ID, e.getMessage(), e);
			return null;
			
		}
	}
	
	public static List<IBPMNProcessor> getBPMNProcessors(){
		
		if (bpmnProcessors == null)
			initBPMNProcessors();
		
		return bpmnProcessors;
	}
}
