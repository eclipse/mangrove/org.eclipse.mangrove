/*******************************************************************************
 * Copyright (c) {2008} INRIA
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (INRIA) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.in.sca.transform;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.DocumentRoot;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.Service;
import org.eclipse.stp.sca.util.ScaResourceFactoryImpl;

public class SCAModelReader {

    /**
     * Loads the SCA model instance contained by the file
     * @param uri the URI of the file to load
     */
    public DocumentRoot loadSCAModel(URI uri) {
        
        DocumentRoot docRoot = null; //the SCA document root to be returned
        
        //System.out.println("Loading SCA model from " + uri.toFileString());

        // Create a resource set to hold the resources.
        ResourceSet resourceSet = new ResourceSetImpl();

        // Register the appropriate resource factory to handle all file extensions.
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
                Resource.Factory.Registry.DEFAULT_EXTENSION, new ScaResourceFactoryImpl());

        // Register the package to ensure it is available during loading.
        resourceSet.getPackageRegistry().put(ScaPackage.eNS_URI, ScaPackage.eINSTANCE);

        //load the resource for the file
        Resource resource = null;
        try {
            resource = resourceSet.getResource(uri, true);
            //System.out.println("Loaded " + uri);
        } catch (RuntimeException exception) {
            System.out.println("Problem loading " + uri);
            exception.printStackTrace();
        }

            //printEMFResourceContents(resource);
            
            //get the Document Root
            docRoot = (DocumentRoot) resource.getContents().get(0);
            
            //printSCADocumentContents(docRoot);

        return docRoot;
    }


    /**
     * test method so that we can get a quick view of the SCA composite's contents
     * @param docRoot the SCA DocumentRoot element
     */
    private void printSCADocumentContents(DocumentRoot docRoot) {
        //get the composite
        Composite composite = docRoot.getComposite();
        System.out.println("Composite: " + composite.getName());
        //get all the services it declares
        for (Service service : composite.getService()) System.out.println("  service: " + service.getName());
        //get all the references it declares
        for (Reference ref : composite.getReference()) System.out.println("  ref: " + ref.getName());
        //get all the components it contains
        for (Component comp : composite.getComponent()) {
            System.out.println("  * component: " + comp.getName());
            //get all the services it declares
            for (ComponentService service : comp.getService()) System.out.println("  * \tservice: " + service.getName());
            //get all the references it declares
            for (ComponentReference ref : comp.getReference()) System.out.println("  * \tref: " + ref.getName());
        }
        
        System.out.println("DONE.");
    }

    
    /**
     * Print out the contents of the given resource
     * @param resource an EMF resource
     */
    private void printEMFResourceContents(Resource resource) {
        TreeIterator<EObject> iterator = resource.getAllContents();
        while (iterator.hasNext()) {
            EObject obj = iterator.next();
            System.out.println("EMF Object: " + obj.eClass().getName() + " : " + obj.eContents());
        }
    }
    
}
