/*******************************************************************************
 * Copyright (c) {2008} INRIA
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (INRIA) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.in.sca.transform;

import java.io.IOException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.soa.mangrove.Process;
import org.eclipse.soa.mangrove.Service;
import org.eclipse.soa.mangrove.Step;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.in.sca.Activator;
import org.eclipse.soa.mangrove.util.IMHandler;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.DocumentRoot;
import org.eclipse.swt.widgets.Shell;

/**
 * The main class driving the transformation process from SCA to the
 * Intermediate Model
 * 
 * @author Adrian Mos
 */
public class SCA2IMTransformer {
    private SCAModelReader scaReader = new SCAModelReader(); // helper object
                                                                // for reading
                                                                // SCA files
    private Shell shell; // the SWT shell that will be the parent of
                            // different widgets created here

    public SCA2IMTransformer(Shell shell) {
        super();
        this.shell = shell;
    }

    /**
     * Creates an IM instance from the SCA model instance contained by the file
     * @param uri the URI of the SCA composite file to load   
     * @param container the Eclipse resource container (typically the workspace)
     */
    public synchronized void createIMfromSCA(URI uri, IContainer container) {

        // get the SCA Document Root and the Composite
        DocumentRoot scaRoot = null;
        Composite composite = null;
        try {
            scaRoot = scaReader.loadSCAModel(uri);
            composite = scaRoot.getComposite();
        } catch (Throwable t) {
            displayIMError("Could not obtain the SCA contents", t);
            return;
        }
        
        //create the IM helper object and obtain the IM EMF instance
        IMHandler imHandler = new IMHandler();
        StpIntermediateModel im = imHandler.getStpIM();
        
        //for each component service, must create a service in the IM
        for (Component comp : composite.getComponent()) { //get all the components
            //get all the services it declares
            for (ComponentService service : comp.getService()) {
                Service serv = imHandler.createService(service.getName());
            }
            //get all the references it declares
            //for (ComponentReference ref : comp.getReference()) System.out.println("  * \tref: " + ref.getName());
        }
        
        //for each SCA Composite Service, must create an IM process
        for(org.eclipse.stp.sca.Service compService : composite.getService()){
            Process proc = imHandler.createProcess(compService.getName());
            //if the composite service is a promoted service from a component (as it should)
            //the promoted component service must be added as a first step to the process
            Step firstStep = imHandler.createStep(compService.getPromote().getName(), proc);
            //Service serv = imHandler.createService(compService.getPromote2().getName());
        }
                
        //to finish up, the IM must be persisted in the workspace 
        //(using an identical file name to the SCA one, with a changed extension)
        try {
            URI imURI = uri.trimFileExtension().appendFileExtension("im");
            imHandler.persistIM(imURI);
            container.refreshLocal(2, null); //refreshes the workspace so that the saved file is visible
        } catch (IOException e) {
            displayIMError("Could not save the IM file", e);
        } catch (CoreException e) {
            displayIMError("Could not refresh the workspace", e);
        }
        
    }

    /**
     * Display an error message window for IM-related operations
     * @param msg the error message
     */
    private void displayIMError(String msg, Throwable t) {
        IStatus iStatus = null;
        if (null != t)
            iStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, t.getLocalizedMessage(), t);
        else
            iStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, msg);
        ErrorDialog.openError(shell, "Intermediate Model Generation Error", msg, iStatus);

    }
}
