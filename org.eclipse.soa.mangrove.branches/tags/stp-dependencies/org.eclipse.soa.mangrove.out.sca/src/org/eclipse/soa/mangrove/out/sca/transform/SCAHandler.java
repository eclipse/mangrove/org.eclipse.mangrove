/*******************************************************************************
 * Copyright (c) {2008} INRIA
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (INRIA) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.out.sca.transform;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.DocumentRoot;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.Service;

/**
 * Contains operations for creating elements in the SCA model and writing it to disk
 * It is oriented towards IM-related elements, so will handle a similar subset of SCA
 * An instance of this class corresponds to one SCA composite file
 * @author Adrian Mos
 */
public class SCAHandler {
    private DocumentRoot scaRoot = null; //the top-level element of the SCA meta-model
    private ScaFactory scaFactory = null;
    private Composite composite = null; //the only composite in the SCA model

    /**
     * Instantiates the SCA meta-model and performs set-up operations
     */
    public SCAHandler() {
        scaFactory = ScaFactory.eINSTANCE;
        scaRoot = scaFactory.createDocumentRoot();
        composite = scaFactory.createComposite();
        scaRoot.setComposite(composite);
    }

    public Composite getComposite() {
        return composite;
    }

    
    /**
     * Creates a Service for the SCA composite
     * @param name the names of the service
     * @return the created service
     */
    public Service createCompositeService (String name){
        //create the service
        Service service = scaFactory.createService();
        service.setName(name);
        //add the created service to the composite
        composite.getService().add(service);
        return service;
    }

    /**
     * Creates a component
     * @param name the name of the component
     * @return the created component
     */
    public Component createComponent(String name) {
        //create the component
        Component component = scaFactory.createComponent();
        component.setName(name);
        //add the component to the composite
        composite.getComponent().add(component);
        return component;
    }

    /**
     * Creates a component service
     * @param parent the component that will expose this service
     * @param name the name of the component service
     * @return
     */
    public ComponentService createComponentService(Component parent, String name) {
        //create the service
        ComponentService compServ = scaFactory.createComponentService();
        compServ.setName(name);
        //add the service to its containing component
        parent.getService().add(compServ);
        return compServ;
    }


    /**
     * Creates a component reference
     * @param parent the component that will expose this reference
     * @param name the name of the component reference
     * @return
     */
    public ComponentReference createComponentRef(Component parent, String name) {
        //create the reference
        ComponentReference compRef = scaFactory.createComponentReference();
        compRef.setName(name);
        //add the reference to its containing component
        parent.getReference().add(compRef);
        return compRef;
    }
    
    /**
     * will create a file and save the SCA composite in it
     * @param uri the URI of the file to save to
     * @throws IOException if the save operation does not succeed
     */
    public void persistSCA(URI uri) throws IOException {

        ResourceSet rs = new ResourceSetImpl();

        Resource resource = rs.createResource(uri);
        resource.getContents().add(this.scaRoot);
        
        resource.save(Collections.EMPTY_MAP);
    }
    
}
