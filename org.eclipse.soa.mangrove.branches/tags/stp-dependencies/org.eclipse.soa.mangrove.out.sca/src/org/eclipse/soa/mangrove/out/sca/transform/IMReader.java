/*******************************************************************************
 * Copyright (c) {2008} INRIA
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (INRIA) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.out.sca.transform;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.soa.mangrove.StpIntermediateModel;

/**
 * Helper class for reading an Intermediate Model instance
 * @author Adrian Mos
 */
public class IMReader {

    /**
     * Loads an Intermediate Model instance from a file on the disk
     * @param uri the file URI containing the serialized instance of the IM
     * @return the top-level element of the IM instance
     */
    public StpIntermediateModel loadIM (URI uri){
        StpIntermediateModel im = null; //the IM top level element

        // Create a resource set to hold the resources.
        ResourceSet resourceSet = new ResourceSetImpl();

        //load the resource for the file
        Resource resource = null;
        try {
            resource = resourceSet.getResource(uri, true);
            //System.out.println("Loaded IM from " + uri);
        } catch (RuntimeException exception) {
            System.out.println("Problem loading " + uri);
            exception.printStackTrace();
        }

            //printEMFResourceContents(resource);
            
            //get the IM root
            im = (StpIntermediateModel) resource.getContents().get(0);

        return im;
    }
    
    /**
     * Print out the contents of the given resource
     * @param resource an EMF resource
     */
    private void printEMFResourceContents(Resource resource) {
        TreeIterator<EObject> iterator = resource.getAllContents();
        while (iterator.hasNext()) {
            EObject obj = iterator.next();
            System.out.println("EMF Object: " + obj.eClass().getName() + " : " + obj.eContents());
        }
    }
    
}
