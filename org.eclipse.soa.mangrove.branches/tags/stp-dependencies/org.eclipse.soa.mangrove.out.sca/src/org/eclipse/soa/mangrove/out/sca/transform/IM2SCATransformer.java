/*******************************************************************************
 * Copyright (c) {2008} INRIA
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (INRIA) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.out.sca.transform;

import java.io.IOException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.out.sca.Activator;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.swt.widgets.Shell;

/**
 * The main class driving the transformation process from the
 * Intermediate Model to SCA
 * 
 * @author Adrian Mos
 */
public class IM2SCATransformer {
    
    private IMReader imReader = new IMReader(); //helper object for reading IM serialized instances
    private Shell shell; // the SWT shell that will be the parent of different widgets created here
 
    /**
     * @param shell
     */
    public IM2SCATransformer(Shell shell) {
        super();
        this.shell = shell;
    }
    
    /**
     * Creates an SCA model instance from the IM instance contained by the file
     * @param uri the URI of the IM serialized instance file to load   
     * @param container the Eclipse resource container (typically the workspace)
     */
    public synchronized void createSCAfromIM(URI uri, IContainer container){
        StpIntermediateModel im = null;
        
        //get the root element of the IM
        try {
            im = imReader.loadIM(uri);
        } catch (Throwable t) {
            displayIMError("Could not obtain the IM instance contents", t);
            return;
        }
        
        SCAHandler scaHandler = new SCAHandler();
        Composite composite = scaHandler.getComposite();
        
        //for each process in the IM, create a composite service and the inner components containing 
        //the services taking part in the process (orchestrated by a component)
        for (org.eclipse.soa.mangrove.Process proc : im.getProcessCollection().getProcesses()) {
            org.eclipse.stp.sca.Service compositeService = scaHandler.createCompositeService(proc.getName());
            // create an orchestration component exposing the same service and
            // connects the composite service to the orchestration service
            // (which has the same name as it is the SAME service)
            Component orchestrComponent = scaHandler.createComponent(proc.getName() + "_orchestrationComponent");
            ComponentService orchestrService = scaHandler.createComponentService(orchestrComponent, proc.getName());
            compositeService.setPromote(orchestrService);

            // for each of the services invoked by this process, create
            // components with the respective services
            // and connect the orchestration component to these services
            for (org.eclipse.soa.mangrove.Step step : proc.getSteps()) {
                // if the process step relates to a service, get it and create
                // the appropriate artefacts
                // if the process step does not CURRENTLY relate to a service,
                // CREATE a mockup service
                // to give the architect the option for creating one or not in
                // an easy manner
                // this policy could easily change when we switch to declarative
                // rules
                org.eclipse.soa.mangrove.Service procService = step.getServiceModel();
                String pServiceName = null;
                if (null == procService) pServiceName = step.getName() + "_genService";
                else pServiceName = procService.getServiceName();
                // create a SCA component with the service
                Component scaComponent = scaHandler.createComponent(step.getName() + "_component");
                ComponentService scaService = scaHandler.createComponentService(scaComponent, pServiceName);
                // create a service reference in the orchestration component for
                // this service
                // and connect it to the newly created component service
                ComponentReference scaRef = scaHandler.createComponentRef(orchestrComponent, pServiceName + "_ref");
                scaRef.getTarget().add(scaService);

            }
        }
        
        //to finish up, the SCA composite must be persisted in the workspace 
        //(using an identical file name to the SCA one, with a changed extension)
        try {
            URI scaURI = uri.trimFileExtension().appendFileExtension("composite");
            scaHandler.persistSCA(scaURI);
            container.refreshLocal(2, null); //refreshes the workspace so that the saved file is visible
        } catch (IOException e) {
            displayIMError("Could not save the SCA Composite file", e);
        } catch (CoreException e) {
            displayIMError("Could not refresh the workspace", e);
        }
    }
    
    /**
     * Display an error message window for IM-related operations
     * @param msg the error message
     */
    private void displayIMError(String msg, Throwable t) {
        IStatus iStatus = null;
        if (null != t)
            iStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, t.getLocalizedMessage(), t);
        else
            iStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, msg);
        ErrorDialog.openError(shell, "Intermediate Model Generation Error", msg, iStatus);

    }

}
