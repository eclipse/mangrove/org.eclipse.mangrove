/*******************************************************************************
 * Copyright (c) {2008} INRIA
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Adrian Mos (INRIA) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.out.sca.popup.actions;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.soa.mangrove.out.sca.transform.IM2SCATransformer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class GenerateSCAAction implements IObjectActionDelegate {

    private IFile selectedIMFile = null;
    private IM2SCATransformer scaTransformer;
    private Shell shell;

	public GenerateSCAAction() {
		super();
        shell = new Shell();
        this.scaTransformer = new IM2SCATransformer(shell);
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
        if (null != this.selectedIMFile) {
            //load the IM from the selected file (full path includes the workspace location)
            IPath location = Platform.getLocation();
            //System.out.println("Workspace Location: " + location.toFile().getAbsolutePath());
            URI uri = URI.createFileURI(location.toString() + this.selectedIMFile.getFullPath().toString());
            IContainer container = this.selectedIMFile.getParent();
            this.scaTransformer.createSCAfromIM(uri, container);
        }
	}

    /**
     * @see IActionDelegate#selectionChanged(IAction, ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
        this.selectedIMFile = ((IFile) ((StructuredSelection) selection).getFirstElement());
    }

}
