package org.eclipse.soa.mangrove.governance.propview.views;


import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.sirius.diagram.ui.edit.api.part.AbstractDiagramNodeEditPart;
import org.eclipse.sirius.diagram.ui.internal.edit.parts.DNode3EditPart;
import org.eclipse.sirius.diagram.ui.internal.edit.parts.DNodeContainer2EditPart;
import org.eclipse.sirius.diagram.ui.internal.edit.parts.DNodeContainerViewNodeContainerCompartment2EditPart;
import org.eclipse.sirius.diagram.ui.internal.edit.parts.DNodeNameEditPart;
import org.eclipse.sirius.diagram.ui.part.SiriusDiagramEditor;
import org.eclipse.sirius.diagram.ui.tools.api.figure.DBorderedNodeFigure;
import org.eclipse.sirius.diagram.ui.tools.api.figure.SiriusWrapLabel;
import org.eclipse.soa.mangrove.diagram.edit.parts.ProcessEditPart;
import org.eclipse.soa.mangrove.diagram.edit.parts.StepEditPart;
import org.eclipse.soa.mangrove.diagram.part.IntermediateModelDiagramEditor;
import org.eclipse.soa.mangrove.governance.view.GovernanceView;
import org.eclipse.soa.mangrove.monitoring.ActivityMonitoringData;
import org.eclipse.soa.mangrove.monitoring.BPMSMonitoringHandler;
import org.eclipse.soa.mangrove.monitoring.DSLActivityandServiceMap;
import org.eclipse.soa.mangrove.monitoring.ProcessMonitoringData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.IAxisSet;
import org.swtchart.IBarSeries;
import org.swtchart.ISeries;
import org.swtchart.ISeriesSet;
import org.swtchart.ISeries.SeriesType;


public class MangroveDetailsView extends ViewPart {

	private BPMSMonitoringHandler monitoringHandler;
	
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.eclipse.soa.mangrove.governance.propview.views.MangroveDetailsView";

	/**
	 * ensure that the Mangrove details view displays the information about the selected mangrove element
	 */
	private ISelectionListener mangroveElementListener = new ISelectionListener() {
        public void selectionChanged(IWorkbenchPart sourcepart, ISelection selection) {
        	//System.out.println(">>>> DETAILS VIEW >>>> sourcepart: " + sourcepart + " >>> selection: " + selection);
        	if ( (sourcepart instanceof GovernanceView) || (sourcepart instanceof IntermediateModelDiagramEditor) ){
				//System.out.println(">>>> Mangrove element selected: " + sourcepart	+ " >>> selection: " + selection.getClass() + " >>> " + selection);
				if (selection instanceof TreeSelection) { //this originates in a tree based view such as the Governance View or some Mangrove tree view
					TreeSelection ts = (TreeSelection) selection;
					String selectedItem = null;
					String parentItem = null;
					if (null!=ts && null!=ts.getFirstElement()){
						selectedItem = ts.getFirstElement().toString();
						if( (ts.getPaths() != null) && (ts.getPaths().length > 0) ){
							TreePath p = ts.getPaths()[0];
							if (null!=p && null!=p.getParentPath() && null!=p.getParentPath().getLastSegment()){
								parentItem = p.getParentPath().getLastSegment().toString();
								//must check to see the activity name actually points to an activity name (i.e. it's top-level parent is "Processes")
								if (p.getParentPath().getFirstSegment().toString().equalsIgnoreCase("Processes")){
									activityName = selectedItem.split(" <")[0];
									processName = parentItem.split(" <")[0];
									//if the activity name is in fact a process (i.e. its top-level parent is "Processes" but this is also its direct parent)
									//this means the selection in fact pertains to a process (the user selected a process)
									//so the activity is set to null
									if (processName.equalsIgnoreCase("Processes")){
										processName = activityName;
										activityName = null;
										handleProcessSelection(processName);
									}
									else {
										handleActivitySelection(processName, activityName);
									}
								}
								else {
									activityName = null;
									processName = null;
											
								}
	
							}
							
						}
					}
				} else if (selection instanceof StructuredSelection){
					StructuredSelection sel = (StructuredSelection)selection;
					Object element = sel.getFirstElement();
					if(element instanceof ProcessEditPart) { //the user clicked on a Mangrove Process
						ProcessEditPart procPart = (ProcessEditPart)element;
						processName = procPart.getContentPane().getChildren().get(0).toString();
						//System.out.println("PROCESS NAME: " + processName);
						handleProcessSelection(processName);
					}
					else if(element instanceof StepEditPart){ //the user clicked on a Mangrove Step
						StepEditPart stepPart = (StepEditPart)element;
						activityName = stepPart.getContentPane().getChildren().get(0).toString();
						processName = ((ProcessEditPart)(stepPart.getParent())).getContentPane().getChildren().get(0).toString();
						//System.out.println("PROCESS NAME: " + processName + ":: ACTIVITY NAME: " + activityName);
						handleActivitySelection(processName, activityName);
					}
					
				}
			}
        	//for SIRIUS-based Graphical DSL diagram editors
        	else if (sourcepart instanceof SiriusDiagramEditor){
				StructuredSelection sel = (StructuredSelection)selection;
				Object element = sel.getFirstElement();
				//System.out.println("\t Sirius ELEMENT: " + element + " CLASS: " + element.getClass());
				//System.out.println("\t\t Children: "+ ((AbstractDiagramNodeEditPart)element).getChildren());
				//System.out.println("\t\t Content: "+ ((AbstractDiagramNodeEditPart)element).getContentPane());
				//System.out.println("\t\t Edit Text: "+ ((DNodeNameEditPart)(((AbstractDiagramNodeEditPart)element).getChildren().get(0))).getEditText());
				if(element instanceof DNodeContainer2EditPart) { //the user clicked on a Sirius DSL Process
					DNodeContainer2EditPart procPart = (DNodeContainer2EditPart)element;
					processName = ((SiriusWrapLabel)(procPart.getContentPane().getChildren().get(0))).getText();
					//System.out.println("\t\t SIRIUS PROCESS NAME: " + processName);
					handleProcessSelection(processName);
				}
				else if(element instanceof DNode3EditPart){ //the user clicked on a Sirius DSL Activity
					DNode3EditPart stepPart = (DNode3EditPart)element;
					activityName = ((DNodeNameEditPart)(stepPart.getChildren().get(0))).getEditText();
					//System.out.println("\t\t SIRIUS ACTIVITY NAME: " + activityName);
					DNodeContainer2EditPart procP = (DNodeContainer2EditPart)(stepPart.getParent().getParent());
					processName = ((SiriusWrapLabel)(procP.getContentPane().getChildren().get(0))).getText();
					//System.out.println("PROCESS NAME: " + processName + ":: ACTIVITY NAME: " + activityName);
					handleDSLActivitySelection(processName, activityName);
				}
				
			}
        }
   
    };

	private String activityName = null;
	private String processName = null;
	private Composite compositeParent;

	private Chart chart;
	private Table detailsTable;
	private Table activitiesTable;

	private ISeries chartSeries;

	private Label activity;
	private Label process;
	private Label concept;
	private Label activityNames;

	private Composite infoBox;

	private ScrolledComposite sc;

	private Composite chartHolder;
	
	/**
	 * @param processName
	 * @param activityName
	 * Handles the selection somewhere in the workspace of an item that corresponds to a DSL activity
	 * It will display all the monitoring elements required for it (this may include several BPMS activities as well as SOA data
	 */
	private void handleDSLActivitySelection(String processName,	String conceptName) {
		DSLActivityandServiceMap conceptMap = null;

		conceptMap = monitoringHandler.getConceptActivitiesAndServiceMap(conceptName);

		setUpInfoBox();
		RowLayout boxLayout = new RowLayout();
		boxLayout.type = SWT.VERTICAL;
		infoBox.setLayout(boxLayout);
		
		if(null!=concept) concept.dispose();
		concept = new Label(infoBox, PROP_TITLE);
		concept.setText("Concept: " + conceptName);
		FontData[] fD = concept.getFont().getFontData();
		fD[0].setHeight(16);
		concept.setFont( new Font(Display.getCurrent(),fD[0]));

		Vector<String> actNames = new Vector<String>();	
		Map<String, String> dataMap = null;
		Vector<Long> sumsOfExecTimes = new Vector<Long>(); //contains all the execution times of the concept summed from the individual activities
		if (null!=conceptMap){
			dataMap = new HashMap<String, String>();
			actNames = conceptMap.getActivityNames();
			for(int i=0; i< 5; i++) sumsOfExecTimes.add(new Long(0));
			long lastExecTime = 0; //the sum of the last execution times of the related activities
			long avgExecTime = 0; //the sum of the avg execution times of the related activities
			
			if (null!=actNames) for(String a : actNames){
				ActivityMonitoringData d;
				try {
					d = monitoringHandler.findLatestActivityMonitoringData(processName, a);
					if(null!=d){
						lastExecTime += d.getLastExecTime();
						avgExecTime += d.getAvgExecTime();
					}
					Vector<Long> aTimes = monitoringHandler.getActivityExecutionTimes(processName, a);
					if(null!=aTimes) for(int i=0; i<aTimes.size(); i++){
						sumsOfExecTimes.set(i, sumsOfExecTimes.get(i) + aTimes.get(i));
					}
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
			dataMap.put("Service associated to the concept: ", conceptMap.getServiceName() );
			Long sT = new Long(-1);
			if (null!=monitoringHandler.getServiceLastExecTime().get(conceptMap.getServiceName())) sT = monitoringHandler.getServiceLastExecTime().get(conceptMap.getServiceName())/1000;
			dataMap.put("Average execution time of the service: ",sT.toString() + "ms");
			dataMap.put("The cumulated (human tasks associated) duration of the last execution was: ", lastExecTime + "ms");
			dataMap.put("The average cumulated (human tasks associated) execution time for the concept is: ", avgExecTime + "ms");
		}
		
		this.updateTable(dataMap);
		
		if(null!=activityNames) activityNames.dispose();
		activityNames = new Label(infoBox, PROP_TITLE);
		activityNames.setText("Activities executed by the concept " + conceptName);
		FontData[] fD2 = activityNames.getFont().getFontData();
		fD2[0].setHeight(12);
		activityNames.setFont( new Font(Display.getCurrent(),fD2[0]));
		
		this.updateActivitiesTable(actNames);
		infoBox.layout();
		
		updateChart(conceptName, sumsOfExecTimes);
		compositeParent.layout();
		
	}
	
	/**
	 * @param processName
	 * @param activityName
	 * Handles the selection somewhere in the workspace of an item that corresponds to a BPMS activity
	 * It will display all the monitoring elements required for it
	 */
	private void handleActivitySelection(String processName, String activityName){
		ActivityMonitoringData data = null;
		try {
			data = monitoringHandler.findLatestActivityMonitoringData(processName, activityName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setUpInfoBox();
		RowLayout boxLayout = new RowLayout();
		boxLayout.type = SWT.VERTICAL;
		infoBox.setLayout(boxLayout);
		
		if(null!=activity) activity.dispose();
		if(null!=process) process.dispose();
		
		activity = new Label(infoBox, PROP_TITLE);
		activity.setText("Activity Name: " + activityName);
		FontData[] fD = activity.getFont().getFontData();
		fD[0].setHeight(16);
		activity.setFont( new Font(Display.getCurrent(),fD[0]));
		
		process = new Label(infoBox, PROP_TITLE);
		process.setText("Process: " + processName);
		
		Map<String, String> dataMap = null;
		
		if (null!=data){
			dataMap = new HashMap<String, String>();
			
			process.setText("Process: " + processName + " | ID: " + data.getProcID());
			
			dataMap.put("User executing the activity: ", data.getUser() );
			dataMap.put("Role executing the activity: ", data.getGrantedRole());
			dataMap.put("The duration of the last execution was: ", String.valueOf(data.getLastExecTime()) + "ms");
			dataMap.put("The average execution time for the activity is: ", String.valueOf(data.getAvgExecTime()) + "ms");
			
		}
		this.updateTable(dataMap);
		infoBox.layout();
		updateChart(activityName, monitoringHandler.getActivityExecutionTimes(processName, activityName));
		compositeParent.layout();
	}
	
	
	/**
	 * @param processName
	 * Handles the selection somewhere in the workspace of an item that corresponds to a BPMS process
	 * It will display all the monitoring elements required for it
	 */
	protected void handleProcessSelection(String processName) {
		ProcessMonitoringData data = null;
		try {
			data = monitoringHandler.findLatestProcessMonitoringData(processName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setUpInfoBox();
		RowLayout boxLayout = new RowLayout();
		boxLayout.type = SWT.VERTICAL;
		infoBox.setLayout(boxLayout);
		
		if(null!=process) process.dispose();
		process = new Label(infoBox, PROP_TITLE);
		process.setText("Process: " + processName);
		FontData[] fD = process.getFont().getFontData();
		fD[0].setHeight(16);
		process.setFont( new Font(Display.getCurrent(),fD[0]));

		Vector<String> actNames = null;
		String procID = "";		
		Map<String, String> dataMap = null;
		if (null!=data){
			dataMap = new HashMap<String, String>();
			dataMap.put("User executing the process: ", data.getUser() );
			dataMap.put("The duration of the last execution was: ", String.valueOf(data.getLastExecTime()) + "ms");
			dataMap.put("The average execution time for the activity is: ", String.valueOf(data.getAvgExecTime()) + "ms");
			procID = data.getProcID();
			actNames = data.getPerformedActivityNames();
		}
		
		
		this.updateTable(dataMap);
		
		if(null!=activityNames) activityNames.dispose();
		activityNames = new Label(infoBox, PROP_TITLE);
		activityNames.setText("Activities executed by last instance " + procID);
		FontData[] fD2 = activityNames.getFont().getFontData();
		fD2[0].setHeight(12);
		activityNames.setFont( new Font(Display.getCurrent(),fD2[0]));
		
		this.updateActivitiesTable(actNames);
		infoBox.layout();
		
		updateChart(processName, monitoringHandler.getProcessExecutionTimes(processName));
		compositeParent.layout();
		
	}


	private void setUpInfoBox() {
		if(null!=infoBox) infoBox.dispose();
		infoBox = new Composite(compositeParent, SWT.BORDER);
		//sc.setContent(infoBox);
	}

	/**
	 * The constructor.
	 */
	public MangroveDetailsView() {
		monitoringHandler = new BPMSMonitoringHandler();
	}

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		
		FillLayout flayout = new FillLayout();
		//flayout.type = SWT.VERTICAL;
		parent.setLayout(flayout);
		this.compositeParent = parent;

		//add the listener for the any changes, the idea is that when the user selects a Mangrove element, the governance view gets updated
		//System.out.println("ADDING Governance Details LISTENER");
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this.mangroveElementListener);
	}


	/**
	 * updates the SWT Table component with the details about the activity or the process selected
	 */
	private void updateTable(Map<String, String> tableContents){
		if (null!=detailsTable) detailsTable.dispose();
		detailsTable = new Table(infoBox, SWT.BORDER);
		detailsTable.setSize(450, 200);
		TableColumn titlesColumn = new TableColumn(detailsTable, SWT.LEFT);
		titlesColumn.setWidth(300);
		titlesColumn.setText("Parameter");
		TableColumn valuesColumn = new TableColumn(detailsTable, SWT.RIGHT);
		valuesColumn.setText("Value");
		valuesColumn.setWidth(150);
		detailsTable.setHeaderVisible(true);
		detailsTable.setLinesVisible(true);
		if(null!=tableContents){
			for(String key : tableContents.keySet()){
				//System.out.println("Adding line: " + key + " :: " + tableContents.get(key));
				TableItem line = new TableItem(detailsTable, SWT.NONE);
				line.setText(new String[] {key, tableContents.get(key)});
			}
		}
		detailsTable.layout();
	}
	
	private void updateActivitiesTable(Vector<String> actNames){
		if (null!=activitiesTable) activitiesTable.dispose();
		activitiesTable = new Table(infoBox, SWT.BORDER);
		activitiesTable.setSize(250, 100);
		TableColumn titlesColumn = new TableColumn(activitiesTable, SWT.CENTER);
		titlesColumn.setWidth(250);
		titlesColumn.setText("Activity");
		activitiesTable.setHeaderVisible(true);
		activitiesTable.setLinesVisible(true);
		if(null!=actNames){
			for(String value : actNames){
				//System.out.println("Adding line: " + key + " :: " + tableContents.get(key));
				TableItem line = new TableItem(activitiesTable, SWT.NONE);
				line.setText(new String[] {value});
			}
		}
		activitiesTable.layout();
	}

	/**
	 * updates the SWT Chart component with the relevant execution history data for the process or activity selected
	 */
	private void updateChart(String title, Vector<Long> values) {
		
		if (null!=chart) chart.dispose();
				
		chart = new Chart(compositeParent, SWT.BORDER);
		chart.getAxisSet().getXAxis(0).getTitle().setText("Execution instance");
		chart.getAxisSet().getYAxis(0).getTitle().setText("Execution Time (ms)");

		ISeriesSet seriesSet = chart.getSeriesSet();
		chartSeries = seriesSet.createSeries (SeriesType.LINE, "execution history");
		chart.getTitle().setText("Execution History for " + title);
		chartSeries.setDescription(title);
		chart.getTitle().setText("Execution History for " + title);
		
			//System.out.println("Looking for historical data for " + activityName + " in process " + processName);
			if (null != values) {
				double[] doubleValues = new double[values.size()];
				for (int i = 0; i < values.size(); i++)	doubleValues[i] = values.get(i).doubleValue();
				chartSeries.setYSeries(doubleValues);
				IAxisSet axisSet = chart.getAxisSet();
				axisSet.adjustRange();
			}
		
		chart.layout();
		
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		
	}
}