/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.handler;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.DataObject;
import org.eclipse.stp.bpmn.MessagingEdge;
import org.eclipse.stp.bpmn.NamedBpmnObject;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.stp.bpmn.SequenceEdge;
import org.eclipse.stp.bpmn.TextAnnotation;
import org.eclipse.stp.bpmn.dnd.file.FileDnDHandler;
import org.eclipse.stp.bpmn.properties.ModifyBpmnEAnnotationCommand;
import org.eclipse.ui.PlatformUI;



public class WorkbenchFileDndHandler extends FileDnDHandler {

	
	// This could be avoided but if in the base class is declared protected
	// But now it's private
	protected IResource myFile = null;

	public WorkbenchFileDndHandler(IResource f) {
		super(f);
		myFile = f;
	}
	
	/**
	 * this method returns a command that will create an annotation
	 * on the semantic element of the edit part passed in parameter.
	 */
	public Command getDropCommand(IGraphicalEditPart hoverPart, int index, 
			Point dropLocation) {
		Map<String, String> details = new HashMap<String, String>();
		CompoundCommand co = new CompoundCommand("Setting the pool to be executable");
		EModelElement eModelElement = (EModelElement) hoverPart.resolveSemanticElement();
		final String nameOfPropertyMatch = getNameOfPropertyMatch(eModelElement);
		if ( nameOfPropertyMatch != null){
			final EAnnotation ea = eModelElement.getEAnnotations().get(0);
			ModifyBpmnEAnnotationCommand modifyCommand = 
				new ModifyBpmnEAnnotationCommand(ea,"Modifying BPMN property") {

				@Override
				protected CommandResult doExecuteWithResult(
						IProgressMonitor arg0, 
						IAdaptable arg1) throws ExecutionException {
							ea.getDetails().put(nameOfPropertyMatch,myFile.getProjectRelativePath().toString());
					
					return CommandResult.newOKCommandResult();
				}};
				co.add(new ICommandProxy(modifyCommand));
		}
		return co;
		
		
		
	}

	public String getNameOfPropertyMatch(EModelElement eModelElement){
		if ( eModelElement.getEAnnotations().size() > 0){
			final EAnnotation ea = eModelElement.getEAnnotations().get(0);
			if (ea.getSource().equalsIgnoreCase(ImConstants.IM_ANNOTATION)){
				String runtimeId = ea.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
				IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeId);
				Map<String, String> matchingDnDProperties = runtime.getDnDMatchingProperties();
				final String filePath = myFile.getProjectRelativePath().toString();
				final String fileExt = filePath.substring(filePath.lastIndexOf(".")+1);
				final String destinationPropertyNameForFileExt = matchingDnDProperties.get(fileExt);
				if (ea.getDetails().get(destinationPropertyNameForFileExt) != null){
					
			        return destinationPropertyNameForFileExt;
				}
			}
		}
		return null;
	}
	
	public String getMenuItemLabel(IGraphicalEditPart hoverPart, int index) {

		EModelElement eModelElement = (EModelElement) hoverPart.resolveSemanticElement();
		String path = myFile.getProjectRelativePath().lastSegment().toString();
		String nameOfPopertyMatch = getNameOfPropertyMatch(eModelElement);
		return ( nameOfPopertyMatch != null ? "Fill property ("+nameOfPopertyMatch+") with this file (" + path + ") on Property " :  "No Property Matching");
		
		
	}

	/**
	 * the file drop should have the lowest priority.
	 */
	public int getPriority() {
		return 1;
	}

	
	

    @Override
    public boolean isEnabled(IGraphicalEditPart hoverPart, int index) {
    	EModelElement eModelElement = (EModelElement) hoverPart.resolveSemanticElement();
		
		String nameOfPopertyMatch = getNameOfPropertyMatch(eModelElement);
		return ( nameOfPopertyMatch != null ? true  :  false);
    }
}
