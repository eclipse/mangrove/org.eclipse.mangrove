/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.decorator;


import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget.Direction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.IServiceBinding;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.ActivityType;
import org.eclipse.stp.bpmn.dnd.IEAnnotationDecorator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class BindingDecorator implements IEAnnotationDecorator {

	public ImageDescriptor getImageDescriptor(EditPart part,
			EModelElement element, EAnnotation annotation) {
		EAnnotation imAnnotation = element.getEAnnotation(ImConstants.IM_ANNOTATION);
		
		ImageDescriptor desc = BpmnEditorExtensionActivator.getImageDescriptor("icons/spagic.gif");
		
		if (imAnnotation != null){
			String runtimeID = imAnnotation.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
			String serviceName = imAnnotation.getDetails().get(ImConstants.IM_SERVICE_NAME);
			String serviceBindingName = imAnnotation.getDetails().get(ImConstants.IM_SERVICE_BINDING_NAME);
			
			IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeID);
			IServiceBinding serviceBinding = runtime.getServiceBindings().get(serviceBindingName);
		
			String bundleId = serviceBinding.getRegisteringBundleId();
			String iconPath = serviceBinding.getIconPath();
		
	
			if ( 	(bundleId != null) 
				&& (bundleId.trim().length() > 0) 
				&& (iconPath != null) 
				&& (iconPath.trim().length() > 0))
				desc =  AbstractUIPlugin.imageDescriptorFromPlugin(bundleId, iconPath);
			}
		
			return desc;

	}

	public String getAssociatedAnnotationSource() {
		return ImConstants.IM_ANNOTATION;
		
	}

	public Direction getDirection(EditPart part, EModelElement element,
			EAnnotation annotation) {
		if (element instanceof Activity) {
			ActivityType at = ((Activity)element).getActivityType();

			switch (at.getValue()) {
				case ActivityType.EVENT_START_EMPTY:
				case ActivityType.EVENT_START_LINK:
				case ActivityType.EVENT_START_RULE:	
				case ActivityType.EVENT_START_MULTIPLE:
				case ActivityType.EVENT_START_MESSAGE:
				case ActivityType.EVENT_END_CANCEL:
				case ActivityType.EVENT_END_COMPENSATION:
				case ActivityType.EVENT_END_EMPTY:	
				case ActivityType.EVENT_END_ERROR:
				case ActivityType.EVENT_END_LINK:
				case ActivityType.EVENT_END_MESSAGE:
				case ActivityType.EVENT_END_MULTIPLE:
				case ActivityType.EVENT_END_TERMINATE:{
					return Direction.CENTER;
				}
				default:
					return Direction.SOUTH;
				}
			
		}
		return Direction.SOUTH;
	}

	public Image getImage(EditPart part, EModelElement element, EAnnotation annotation) {
		ImageDescriptor desc = getImageDescriptor(part, element, annotation);
	
		return desc == null ? null : desc.createImage();
	}


	public IFigure getToolTip(EditPart part, EModelElement element, EAnnotation annotation) {
		
		String serviceName = (String) annotation.getDetails().get(ImConstants.IM_SERVICE_NAME);
		String bindingName = (String) annotation.getDetails().get(ImConstants.IM_SERVICE_BINDING_NAME);
        return new org.eclipse.draw2d.Label("Service " +serviceName+ " -> Service Binding - "+bindingName);
	
	}

}
