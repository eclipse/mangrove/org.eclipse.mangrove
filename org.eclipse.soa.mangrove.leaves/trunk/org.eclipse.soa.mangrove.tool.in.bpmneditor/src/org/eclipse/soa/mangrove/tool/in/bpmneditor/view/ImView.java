/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.view;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IExpression;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.IService;
import org.eclipse.soa.mangrove.runtime.IServiceBinding;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IPageListener;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class ImView extends ViewPart implements ISelectionListener {
	private static Object[] EMPTY_ARRAY = new Object[0];
	private TreeViewer tree = null;
	private IWorkbenchPage page = null;
	
	public ImView(){
		super();
		System.out.println(" ---> ( - ImView - ) " );
		if (PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage() != null) {
			//then the properties one
			this.page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			page.addSelectionListener(this);

			System.out.println(" ---> ( - Added Bindings Views as Selction Listener on Workbench - )" );
		}
		System.out.println(" ---> ( - End Constructor - )" );
	}
	
	@Override
	public void dispose() {
		System.out.println(" ---> ( - ImView - ) disposed " );
		if (page != null){
			page.removeSelectionListener(this);
		}	
		super.dispose();
		
	}

	@Override
	public void createPartControl(Composite parent) {
		

		System.out.println(" Bindings View Create Part Control");
		tree = new TreeViewer(parent);
		tree.setContentProvider(new ImViewContentProvider());
		tree.setLabelProvider(new ImViewLabelProvider());
		int ops = DND.DROP_COPY | DND.DROP_MOVE;
	    Transfer[] transfers = new Transfer[] {LocalSelectionTransfer.getTransfer()};
	    ImDragAdapter drag = new ImDragAdapter();
	    tree.addDragSupport(ops, transfers, drag);
	    
	    
	    
	}

	public ServiceTreeElement getInputForRuntime(IRuntime runtime){
		 
		 ServiceTreeElement root = new ServiceTreeElement();
		 
		 if (runtime == null){
			 ServiceTreeElement noRuntimeNode = new ServiceTreeElement("No Runtime Selected");
			 root.addServiceTreeElement(noRuntimeNode);
			 root.addServiceTreeElement(new RubberTreeElement("Delete Annotations"));
			 return root;
		 }
		 Map<String, IServiceBinding > mapServiceBindings = runtime.getServiceBindings();
		 
		 ServiceTreeElement runtimeNode = new ServiceTreeElement("Runtime - "+ runtime.getName());
		 for ( IService iService : runtime.getServices()){
			 ServiceTreeElement aServiceTreeElement = new ServiceTreeElement(iService.getName());
			 aServiceTreeElement.setBundleId(iService.getRegisteringBundleId());
			 aServiceTreeElement.setIcon(iService.getIconPath());
			 
			 ServiceBindingTreeElement aServiceBindingTreeElement = null;
			 IServiceBinding sb = null;
			 for(String bindingStr : iService.getServiceBindingsName()){
				 
				 sb = mapServiceBindings.get(bindingStr);
				 
				 aServiceBindingTreeElement = new ServiceBindingTreeElement(bindingStr);
				 aServiceBindingTreeElement.setBundleId(sb.getRegisteringBundleId());
				 aServiceBindingTreeElement.setIcon(sb.getIconPath());
				 aServiceBindingTreeElement.setRuntimeId(runtime.getId());
				 aServiceTreeElement.addServiceBindingTreeElement(aServiceBindingTreeElement);
				 
				 
			 }
			 runtimeNode.addServiceTreeElement(aServiceTreeElement);
		 }
		 ConditionsTreeElement conditions = new ConditionsTreeElement("Expressions");
		 
		 for ( IExpression expression : runtime.getExpressions()){
			 ExpressionTreeElement expressionTreeElement = new ExpressionTreeElement(expression.getName(), expression.getType());
			 expressionTreeElement.setBundleId(expression.getRegisteringBundleId());
			 expressionTreeElement.setIcon(expression.getIconPath());
			 expressionTreeElement.setRuntimeId(runtime.getId());
			 conditions.addExpression(expressionTreeElement);
		 }
		 
		 //conditions.addExpression(new ExpressionTreeElement("XPath Expression"));
		 //conditions.addExpression(new ExpressionTreeElement("Message Header Expression"));
		 runtimeNode.addServiceTreeElement(conditions);
		 root.addServiceTreeElement(runtimeNode);
		 root.addServiceTreeElement(new RubberTreeElement("Delete Annotations"));
		 
		 return root;
	}
	@Override
	public void setFocus() {
	}
	
	
	private class ImViewLabelProvider extends LabelProvider {	
		private Map<ImageDescriptor, Image> imageCache = new HashMap<ImageDescriptor, Image>(11);
		
		/*
		 * @see ILabelProvider#getImage(Object)
		 */
		
		
		public Image getImage(Object element) {
			ImageDescriptor descriptor = null;
			
			if ( element instanceof ServiceTreeModel){
				String bundleId = ((ServiceTreeModel)element).getBundleId();
				String iconPath = ((ServiceTreeModel)element).getIcon();
				
				if ( 	(bundleId != null) 
						&& (bundleId.trim().length() > 0) 
						&& (iconPath != null) 
						&& (iconPath.trim().length() > 0)){
					
					descriptor = AbstractUIPlugin.imageDescriptorFromPlugin(bundleId, iconPath);
				
				}else{
				
					if (element instanceof ConditionsTreeElement) {
						descriptor = BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_condition.gif");	
					} else if (element instanceof RubberTreeElement) {
						descriptor = BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_disconnect.gif");
					} else if (element instanceof ServiceTreeElement) {
						descriptor = BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_service.gif");
					} else if (element instanceof ServiceBindingTreeElement) {
						descriptor = BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_servicebinding.gif");
					} else if (element instanceof ExpressionTreeElement) {
						descriptor = BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_expression.gif");
					} else {
						throw unknownElement(element);
					}
				
				}
			}else{
				throw unknownElement(element);
			}
			

			//obtain the cached image corresponding to the descriptor
			Image image = (Image)imageCache.get(descriptor);
			if (image == null) {
				image = descriptor.createImage();
				imageCache.put(descriptor, image);
			}
			return image;
		}

		/*
		 * @see ILabelProvider#getText(Object)
		 */
		public String getText(Object element) {
			if (element instanceof ServiceTreeElement) {
				if(((ServiceTreeElement)element).getName() == null) {
					return "Service";
				} else {
					return ((ServiceTreeElement)element).getName();
				}
			} else if (element instanceof ServiceBindingTreeElement) {
				return ((ServiceBindingTreeElement)element).getName();
			} else if (element instanceof ExpressionTreeElement) {
				return ((ExpressionTreeElement)element).getName();
			} else {
				throw unknownElement(element);
			}
		}

		public void dispose() {
			for (Iterator<Image> i = imageCache.values().iterator(); i.hasNext();) {
				((Image) i.next()).dispose();
			}
			imageCache.clear();
		}

		protected RuntimeException unknownElement(Object element) {
			return new RuntimeException("Unknown type of element in tree of type " + element.getClass().getName());
		}
		
	}
	private class ImViewContentProvider implements ITreeContentProvider {
		
		public Object[] getChildren(Object parentElement) {
			    if(parentElement instanceof ServiceTreeElement) {
			    	if (parentElement instanceof ConditionsTreeElement) {
						ConditionsTreeElement ct =  (ConditionsTreeElement)parentElement;
						return ct.getExpressions().toArray();
					}else{
						ServiceTreeElement st = (ServiceTreeElement)parentElement;
						return concat(st.getServices().toArray(), st.getBindings().toArray());
					}
			    }
			    return EMPTY_ARRAY;
			}
		
		protected Object[] concat(Object[] object, Object[] more) {
			Object[] both = new Object[object.length + more.length];
			System.arraycopy(object, 0, both, 0, object.length);
			System.arraycopy(more, 0, both, object.length, more.length);	
			return both;
		}

		public Object getParent(Object element) {
		    if(element instanceof ServiceTreeModel) {
		        return ((ServiceTreeModel)element).getParent();
		    }
		    return null;
		}

	
		public boolean hasChildren(Object element) {
			    return getChildren(element).length > 0;
		}
		

		public Object[] getElements(Object inputElement) {
		    return getChildren(inputElement);
		}

		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
    /**
     * Simple drag adapter that fills the LocalSelectionTransfer with the selection
     */
     private class ImDragAdapter extends DragSourceAdapter {
              @Override
              public void dragStart(org.eclipse.swt.dnd.DragSourceEvent event) {
            	  	
            	  	System.out.println("**************************** Drag Start");
            	  	
            	  	
                	 LocalSelectionTransfer.getTransfer().setSelection(tree.getSelection());
              }
     }

	public void selectionChanged(IWorkbenchPart workbenchPart, ISelection selection) {
		
		
		System.out.println("Workbench Part -> "+workbenchPart + "Selection -> " + selection);
		 
		if (!(selection instanceof IStructuredSelection)) {
        	 return;
         }else{
        	 if (!((IStructuredSelection) selection).isEmpty()){
        		 Object unknownInput = ((IStructuredSelection) selection).getFirstElement();
        		 Object newInput = transformSelection(unknownInput);
        		 //System.out.println(" Unknow Input " + unknownInput.getClass().getName());
        		 //System.out.println(" New Input " + newInput.getClass().getName());
        		 // super.setInput(part, new StructuredSelection(newInput));  	
        		 if (newInput instanceof Pool) {
        			 Pool pool = (Pool)newInput;
        			 EAnnotation ea = pool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
        			 if (ea != null){
        				 String runtimeID = ea.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID );
        				 System.out.println(" Runtime ID ["+runtimeID+"]");
        				 IRuntime selectedRuntime = ImRuntimeActivator.getRuntime(runtimeID);
        				 this.setTitle(" Bindings for Runtime " + selectedRuntime.getName());
        				 this.setContentDescription(" Bindings for Runtime " + selectedRuntime.getName());
        				 tree.setInput(getInputForRuntime(selectedRuntime));
        				 tree.refresh(true);
        			 }
        		 }
        	 }else{
        		 System.out.println(" Structured Selection Is Empty");
        	 }
         
         }
	}

	protected Object transformSelection(Object selected) {
		if (selected instanceof EditPart) {
			Object model = ((EditPart) selected).getModel();
			return model instanceof View ? ((View) model).getElement() : null;
		}
		if (selected instanceof View) {
			return ((View) selected).getElement();
		}
		if (selected instanceof IAdaptable) {
			View view = (View) ((IAdaptable) selected).getAdapter(View.class);
			if (view != null) {
				return view.getElement();
			}
		}
		return selected;
	}
	
	
	

}
