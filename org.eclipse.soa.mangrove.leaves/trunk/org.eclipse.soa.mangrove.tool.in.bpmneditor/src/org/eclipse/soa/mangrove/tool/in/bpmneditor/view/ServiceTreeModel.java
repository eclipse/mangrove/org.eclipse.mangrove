/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.view;


import java.util.ArrayList;
import java.util.List;

public abstract class ServiceTreeModel {
	
	protected  ServiceTreeElement parent = null;
	protected String runtimeId = null;
	
	private String icon = null;
	private String bundleId = null;
	
	public ServiceTreeModel(){
		
	}

	public ServiceTreeElement getParent() {
		return parent;
	}

	public void setParent(ServiceTreeElement parent) {
		this.parent = parent;
	}

	public String getRuntimeId() {
		return runtimeId;
	}

	public void setRuntimeId(String runtimeId) {
		this.runtimeId = runtimeId;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}
}
