/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.handler;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IProperty;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.IServiceBinding;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.runtime.comboproviders.ComboEntries;
import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;
import org.eclipse.soa.mangrove.runtime.comboproviders.ValuesProvidedByInContextExtractorComboProvider;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.extractor.IBpmnModelExtractor;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.view.ServiceBindingTreeElement;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.DataObject;
import org.eclipse.stp.bpmn.Graph;
import org.eclipse.stp.bpmn.MessagingEdge;
import org.eclipse.stp.bpmn.NamedBpmnObject;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.stp.bpmn.SequenceEdge;
import org.eclipse.stp.bpmn.TextAnnotation;
import org.eclipse.stp.bpmn.dnd.AbstractEAnnotationDnDHandler;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.plugin.AbstractUIPlugin;



public class ServiceWithServiceBindingDndHandler extends AbstractEAnnotationDnDHandler {

		private String serviceName =  null;
		private String runtimeID =  null;
		private String serviceBindingName =  null;
		
		public ServiceWithServiceBindingDndHandler(ServiceBindingTreeElement serviceBindingTreeElement) {
			this.runtimeID =  serviceBindingTreeElement.getRuntimeId();
			this.serviceName = serviceBindingTreeElement.getParent().getName();
			this.serviceBindingName = serviceBindingTreeElement.getName();
		}
		
		/**
		 * this method returns a command that will create an annotation
		 * on the semantic element of the edit part passed in parameter.
		 */
		public Command getDropCommand(IGraphicalEditPart hoverPart, int index, Point dropLocation) {
			
			String propertyName = null;
			
		try{
			Map<String, String> details = new HashMap<String, String>();

			details.put(ImConstants.IM_POOL_RUNTIME_ID,runtimeID);
			details.put(ImConstants.IM_SERVICE_NAME,serviceName);
			details.put(ImConstants.IM_SERVICE_BINDING_NAME,serviceBindingName);

			
			IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeID);
			IServiceBinding serviceBinding = runtime.getServiceBindings().get(serviceBindingName);
				
			for (IProperty property : serviceBinding.getDefinedProperties()){
				propertyName = property.getName();
				if ((!property.isMap()) && (!property.isVisibleUnderCondition())){
					String value = (property.getDefaultValue() != null ? property.getDefaultValue() : ""); 
					
					if ((value == null) || (value.trim().length() == 0)){  
						if (property.getPropertyEditor().equalsIgnoreCase("combo")){
							IComboProvider comboProvider = property.getComboProvider();
							if (comboProvider == null){
								comboProvider = runtime.getNamedComboProvider(property.getComboProviderName());
							}
							
							// Initialize the combo, if necessary
							initializeCombo(comboProvider, hoverPart);
							
							ComboEntries ce = comboProvider.getComboEntries();
							value = ce.getValues()[0];
						}
					}
					details.put(property.getName(),value);
				}
			}
			
			// Must add also the properties that are visible under condition if the default
			// state of the annotation make this visible 
			for (IProperty property : serviceBinding.getDefinedProperties()){
				propertyName = property.getName();
				if (property.isVisibleUnderCondition() && evaluateVisibilityCondition(property.getVisibleCondition(), details)){
					String value = (property.getDefaultValue() != null ? property.getDefaultValue() : ""); 
					if ((value == null) || (value.trim().length() == 0)){  
						if (property.getPropertyEditor().equalsIgnoreCase("combo")){
							IComboProvider comboProvider = property.getComboProvider();
							if (comboProvider == null){
								comboProvider = runtime.getNamedComboProvider(property.getComboProviderName());
							}
							// Initialize the combo, if necessary
							initializeCombo(comboProvider, hoverPart);
							
								ComboEntries ce = comboProvider.getComboEntries();
								value = ce.getValues()[0];						
						}
					}
					details.put(property.getName(),value);
				}
			}
	 		return createEAnnotationDropCommand(
					createAnnotation(ImConstants.IM_ANNOTATION, details),
					(EModelElement) hoverPart.resolveSemanticElement());
		}catch (ArrayIndexOutOfBoundsException e) {
			MessageDialog.openError(new Shell(), "Error", "Error while retrieving property: " + propertyName);
			throw new RuntimeException(e);
		}

		}
		
		/**
		 * this method initialize the combo if it's a dynamic combo.
		 */
		protected void initializeCombo(IComboProvider comboProvider, IGraphicalEditPart hoverPart) {
			if (comboProvider.getComboEntries().getValues() != null)
				// Already initialized
				return;
			
			if (comboProvider instanceof ValuesProvidedByInContextExtractorComboProvider) {
				//
				// Extract the class that must be instantiated here because he need org.eclipse.stp.bpm
				//
				String extractorClassName = comboProvider.getParams().get("inContextExtractorClassName");
				try{
					IBpmnModelExtractor bpmnModelExtractor = (IBpmnModelExtractor)Class.forName(extractorClassName).newInstance();
					
					EObject element = hoverPart.resolveSemanticElement();

					bpmnModelExtractor.setContextObject(element);
					((ValuesProvidedByInContextExtractorComboProvider)comboProvider).setValues(bpmnModelExtractor.getValues());
					((ValuesProvidedByInContextExtractorComboProvider)comboProvider).setLabels(bpmnModelExtractor.getLabels());
				}catch(Throwable t){
					t.printStackTrace();
					ImLogger.error(BpmnEditorExtensionActivator.PLUGIN_ID, t.getMessage(), t);
				}
			}
		}
		
		protected boolean evaluateVisibilityCondition(String visibleCondition, Map<String, String> details){
			if (visibleCondition.startsWith("if-is-true")){
				String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(")"));
				String value = details.get(propName);
				return Boolean.valueOf(value);
			}
			if (visibleCondition.startsWith("if-is-false")){
					String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(")"));
					String value = details.get(propName);
					return !Boolean.valueOf(value);
			}
			if (visibleCondition.startsWith("if-has-value")){
				String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(","));
				String valueToEnableVisibility=visibleCondition.substring(visibleCondition.indexOf(",") + 1, visibleCondition.indexOf(")"));
				String value = details.get(propName);
				return valueToEnableVisibility.equalsIgnoreCase(value);
			}
			if (visibleCondition.startsWith("if-isone-of")){
				String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(","));
				String valueToEnableVisibility=visibleCondition.substring(visibleCondition.indexOf(",") + 1, visibleCondition.indexOf(")"));
				// remove [ and "]" syimbols
				String arrValues = valueToEnableVisibility.substring(1, valueToEnableVisibility.length() -1 );
//				System.out.println("values -> "+ arrValues);
				String[] splitterArr = arrValues.split(";");
				List<String> splittedValues = Arrays.asList(splitterArr);
				String value = details.get(propName);
				return splittedValues.contains(value);
			}
			return false;
		}

		/**
		 * the file DnD only enables the drop of one element.
		 */
		public int getItemCount() {
			return 1;
		}

		public Image getMenuItemImage(IGraphicalEditPart hoverPart, int index) {
			IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeID);
			IServiceBinding serviceBinding = runtime.getServiceBindings().get(serviceBindingName);
			
			String bundleId = serviceBinding.getRegisteringBundleId();
			String iconPath = serviceBinding.getIconPath();
			
			ImageDescriptor descriptor = null;
			if ( 	(bundleId != null) 
					&& (bundleId.trim().length() > 0) 
					&& (iconPath != null) 
					&& (iconPath.trim().length() > 0))
				descriptor =  AbstractUIPlugin.imageDescriptorFromPlugin(bundleId, iconPath);
			else 
				descriptor = BpmnEditorExtensionActivator.getImageDescriptor("icons/spagic.gif");
			
			return descriptor.createImage();
		}

		public String getMenuItemLabel(IGraphicalEditPart hoverPart, int index) {
			EObject element = hoverPart.resolveSemanticElement();
			String suffix = " on " + getShapeLabel((NamedBpmnObject) element);
	        return "Attach Service["+serviceName+"] with Binding (" + serviceBindingName + ") on " + suffix;
		}

		/**
		 * the file drop should have the lowest priority.
		 */
		public int getPriority() {
			return 0;
		}

		
		/**
	     * @param namedElement
	     * @return a label for a bpmn shape
	     */
	    protected String getShapeLabel(NamedBpmnObject namedElement) {
	        String name = namedElement.getName() != null && 
	            namedElement.getName().length() > 0 ? namedElement.getName() : "";
	        if (name.length() > 18) {
	            name = name.substring(0, 12) + "...";
	        }
	        if (namedElement instanceof Pool) {
	            if (name.indexOf("pool") == -1) {
	                return "pool " + name;
	            } else {
	                return name;
	            }
	        } else if (namedElement instanceof Activity) {
	            String shape = ((Activity)namedElement).getActivityType().getLiteral();
	            if (name.indexOf(shape) == -1) {
	                return shape + " " + name;
	            } else {
	                return name;
	            }
	        } else if (namedElement instanceof MessagingEdge) {
	            String shape = "message";
	            if (name.indexOf(shape) == -1) {
	                return shape + " " + name;
	            } else {
	                return name;
	            }
	        } else if (namedElement instanceof SequenceEdge) {
	            String shape = "sequence";
	            if (name.indexOf(shape) == -1) {
	                return shape + " " + name;
	            } else {
	                return name;
	            }
	        } else if (namedElement instanceof Diagram) {
	            String shape = "diagram";
	            if (name.indexOf(shape) == -1) {
	                return shape + " " + name;
	            } else {
	                return name;
	            }
	        } else if (namedElement instanceof TextAnnotation) {
	            String shape = "annotation";
	            if (name.indexOf(shape) == -1) {
	                return "text " + shape + " " + name;
	            } else {
	                return name;
	            }
	        } else if (namedElement instanceof DataObject) {
	            String shape = "object";
	            if (name.indexOf(shape) == -1) {
	                return "data " + shape + " " + name;
	            } else {
	                return name;
	            }
	        }
	        return name;
	    }

	    @Override
	    public boolean isEnabled(IGraphicalEditPart hoverPart, int index) {
	    	EModelElement emodelElement = (EModelElement) hoverPart.resolveSemanticElement();
	    	if ( emodelElement instanceof Pool) {
				return false;		
			}if ( emodelElement instanceof Activity) {
				Graph g = ((Activity)emodelElement).getGraph();
				EAnnotation technologyAnnotation = g.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
				if ((g instanceof Pool) && (technologyAnnotation == null)) {
					return false;	
				}
			}
	    	return true;
	    }
	    
	    
	
}
