/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.section;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IProperty;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.runtime.comboproviders.ComboEntries;
import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;
import org.eclipse.soa.mangrove.runtime.comboproviders.ValuesProvidedByInContextExtractorComboProvider;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.extractor.IBpmnModelExtractor;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.Graph;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

public class TechnologySection  extends AbstractPropertySection implements SelectionListener {
	/**
     * Creates the UI of the section.
     */
	 protected CLabel poolLabel = null;
	 protected CLabel runtimeLabel = null;
	 protected CCombo  technologyText = null;
	 protected Button btnSetTechnology = null;
	 protected Pool pool = null;
	 protected String[] data = null;
	 protected TabbedPropertySheetPage page = null;

	 @Override
     public void createControls(Composite parent, 
             TabbedPropertySheetPage aTabbedPropertySheetPage) {
             
		 	 super.createControls(parent, aTabbedPropertySheetPage);
             
		 	 FontData fTextFontData = new FontData( );

		 	 fTextFontData.setStyle(SWT.BOLD);
		 	 fTextFontData.setName("Tahoma");
		 	 fTextFontData.setHeight(8);
		 	 Font boldText = new Font(PlatformUI.getWorkbench().getDisplay(),fTextFontData);

		 	
             org.eclipse.swt.layout.GridLayout layout = new GridLayout(2, false);
             parent.setLayout(layout);
             
             page = aTabbedPropertySheetPage;
             GridData gd = new GridData(SWT.FILL);
             gd.minimumWidth = 500;
             gd.widthHint = 500;
             
             
             // Row Containing "Pool : <POOL_NAME> "
             CLabel label2 = getWidgetFactory().createCLabel(parent, "Pool");
             label2.setFont(boldText);
             
             poolLabel = getWidgetFactory().createCLabel(parent, "");
             poolLabel.setLayoutData(gd);
             
             // Row Containing "Runtime  : <POOL_NAME> "
             CLabel label3 = getWidgetFactory().createCLabel(parent, "Runtime");
             label3.setFont(boldText);
             
             runtimeLabel = getWidgetFactory().createCLabel(parent, "Not Defined");
             runtimeLabel.setLayoutData(gd);
             
          
             this.technologyText = getWidgetFactory().createCCombo(parent, SWT.DROP_DOWN | SWT.SINGLE |SWT.V_SCROLL | SWT.H_SCROLL | SWT.READ_ONLY );
             //this.technologyText.addSelectionListener(this);
             this.data = ImRuntimeActivator.getRuntimeNames();
                
             this.technologyText.setItems(this.data);
             this.technologyText.select(0);
             
             GridData gd4 = new GridData(SWT.FILL);
             gd4.minimumWidth = 100;
             gd4.widthHint = 100;
             this.btnSetTechnology = getWidgetFactory().createButton(parent, " Bind to Runtime ", SWT.PUSH);
             this.btnSetTechnology.setLayoutData(gd4);
             this.btnSetTechnology.addSelectionListener(this);
     }
	 
	 
     
     
     /**
      * Manages the input.
      */
      @Override
      public void setInput(IWorkbenchPart part, ISelection selection) {
      
              super.setInput(part, selection);
              if(selection instanceof IStructuredSelection) {
                      Object unknownInput = 
                             ((IStructuredSelection) selection).getFirstElement();
                      if (unknownInput instanceof IGraphicalEditPart && 
                              (((IGraphicalEditPart) unknownInput).
                                      resolveSemanticElement() != null)) {
                               unknownInput = ((IGraphicalEditPart) unknownInput).resolveSemanticElement();
                      }
                      if (unknownInput instanceof Pool) {
                           pool = (Pool)unknownInput;	 
                           this.technologyText.setEnabled(true);
                           this.btnSetTechnology.setEnabled(true);
                     	   EAnnotation technologyAnnotation = pool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
                           if ( technologyAnnotation != null){
                          		String technologyName = technologyAnnotation.getDetails().get(ImConstants.TECHNOLOGY_NAME);
                          		runtimeLabel.setText(technologyName);
                          		this.technologyText.select(findSelectionIndex( technologyName));
                            }
                          	return;
                      }else if (unknownInput instanceof Activity) {
                    	  Activity activity  = (Activity)unknownInput;
                    	  Graph graph = activity.getGraph();
                    	  if ((graph instanceof Pool) && (graph.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION) != null )) {
                    		  this.technologyText.setEnabled(false);
                    		  this.btnSetTechnology.setEnabled(false);
                              this.technologyText.select(findSelectionIndex( graph.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION).getDetails().get(ImConstants.TECHNOLOGY_NAME)));
                              return;
                    	  }
                      }else{   
                    	  
                      }
              }     

              
              this.technologyText.setEnabled(false);
              this.btnSetTechnology.setEnabled(false);

      }
      
      /**
  	 * Creates an EAnnotation with a source parameter and
  	 * a details map.
  	 * @param source
  	 * @param details
  	 * @return
  	 */
  	protected EAnnotation createAnnotation(String source, Map<String, String> details) {
  		EAnnotation ea = EcoreFactory.eINSTANCE.createEAnnotation();
  		ea.setSource(source);
  		ea.getDetails().putAll(details);
  		return ea;
  	}
  	
  	private abstract class CreateOrUpdateTechnologyEAnnotationCommand extends AbstractTransactionalCommand {
        
        public CreateOrUpdateTechnologyEAnnotationCommand(Pool pool, String label) {
                 super((TransactionalEditingDomain) AdapterFactoryEditingDomain.getEditingDomainFor(pool), label, getWorkspaceFiles(pool));
        }
  	}

  	




	public void widgetDefaultSelected(SelectionEvent ev) {
		widgetSelected(ev);
	}




	public void widgetSelected(SelectionEvent ev) {
		
		
		 if (ev.getSource() instanceof Button) {

			 	int selectionIndex = this.technologyText.getSelectionIndex();
				
				final String technologyName = data[selectionIndex];
				final String technologyId = ImRuntimeActivator.getIdForRuntimeName(technologyName);
				String previousTechnologyId = null;
				String previousTechnologyName = null;
				if (pool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION) != null){
					previousTechnologyId = pool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION).getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
					previousTechnologyName = pool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION).getDetails().get(ImConstants.TECHNOLOGY_NAME);
				}
				
				boolean safeUpdate = false;
				
			
				if (technologyId.equalsIgnoreCase(previousTechnologyId)){
					safeUpdate = true;
				}else if ( previousTechnologyId == null || previousTechnologyId.equalsIgnoreCase("AbstractModel")){
					//
					// Changing from technology Abstract to specific technology
					//
					safeUpdate = true;
				}
				
				System.out.println(" Technology Change ["+previousTechnologyId+"] --> ["+technologyId+"] Update Safe ["+safeUpdate+"]");
				
				if (safeUpdate){
					CreateOrUpdateTechnologyEAnnotationCommand command = new CreateOrUpdateTechnologyEAnnotationCommand(pool, "Modifying Technology") {
		                     
						@Override
						protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1) throws ExecutionException {
		            	 	EAnnotation poolTechnologyAnnotation = pool.getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
		            	 	String runtimeID = ImRuntimeActivator.getIdForRuntimeName(technologyName);
		            	 	if (poolTechnologyAnnotation == null){ 
		            	 		Map<String, String> details = new HashMap<String, String>();
		            	 		
		            	 		details.put(ImConstants.IM_POOL_RUNTIME_ID, runtimeID);
		            	 		details.put(ImConstants.TECHNOLOGY_NAME,technologyName);
		            	 		
		            	 		createProcessProperties(details,runtimeID);
              		   
		            	 		poolTechnologyAnnotation = createAnnotation(ImConstants.TECHNOLOGY_ANNOTATION, details);
		            	 		SetCommand.create(getEditingDomain(), poolTechnologyAnnotation, 
		            	 				EcorePackage.eINSTANCE.getEAnnotation_EModelElement(),pool).execute();
		            	 	} else{
		            	 		EMap<String, String> existDetails = poolTechnologyAnnotation.getDetails();
		            	 		existDetails.put(ImConstants.TECHNOLOGY_NAME,technologyName);   
		            	 		existDetails.put(ImConstants.IM_POOL_RUNTIME_ID,ImRuntimeActivator.getIdForRuntimeName(technologyName));
		            	 		Map<String, String> newDetails = new HashMap<String, String>();
		            	 		createProcessProperties(newDetails,runtimeID);
		            	 		
		            	 		for (String key : newDetails.keySet()){
		            	 			if (existDetails.get(key) == null){
		            	 				existDetails.put(key, newDetails.get(key));
		            	 			}
		            	 		}
		            	 	}
		            	 	return CommandResult.newOKCommandResult();
						}
						
						
						
						public void createProcessProperties(Map<String, String> details, String runtimeId){
							IRuntime imRuntime = ImRuntimeActivator.getRuntime(runtimeId);
							for (IProperty property : imRuntime.getProcessProperties().getDefinedProperties()){
								if ((!property.isMap()) && (!property.isVisibleUnderCondition())){
									String value = (property.getDefaultValue() != null ? property.getDefaultValue() : ""); 
									
									if ((value == null) || (value.trim().length() == 0)){  
										if (property.getPropertyEditor().equalsIgnoreCase("combo")){
											IComboProvider comboProvider = property.getComboProvider();
											if (comboProvider == null){
												comboProvider = imRuntime.getNamedComboProvider(property.getComboProviderName());
											}
											
											
											ComboEntries ce = comboProvider.getComboEntries();
											value = ce.getValues()[0];
										}
									}
									details.put(property.getName(),value);
								}
							}
							
							// Must add also the properties that are visible under condition if the default
							// state of the annotation make this visible 
							for (IProperty property : imRuntime.getProcessProperties().getDefinedProperties()){
								if (property.isVisibleUnderCondition() && evaluateVisibilityCondition(property.getVisibleCondition(), details)){
									String value = (property.getDefaultValue() != null ? property.getDefaultValue() : ""); 
									if ((value == null) || (value.trim().length() == 0)){  
										if (property.getPropertyEditor().equalsIgnoreCase("combo")){
											IComboProvider comboProvider = property.getComboProvider();
											if (comboProvider == null){
												comboProvider = imRuntime.getNamedComboProvider(property.getComboProviderName());
											}
											
											
											ComboEntries ce = comboProvider.getComboEntries();
											value = ce.getValues()[0];
										}
									}
									details.put(property.getName(),value);
								}
							}
						}
						
						
						protected boolean evaluateVisibilityCondition(String visibleCondition, Map<String, String> details){
							if (visibleCondition.startsWith("if-is-true")){
								String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(")"));
								String value = details.get(propName);
								return Boolean.valueOf(value);
							}
							if (visibleCondition.startsWith("if-is-false")){
									String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(")"));
									String value = details.get(propName);
									return !Boolean.valueOf(value);
							}
							if (visibleCondition.startsWith("if-has-value")){
								String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(","));
								String valueToEnableVisibility=visibleCondition.substring(visibleCondition.indexOf(",") + 1, visibleCondition.indexOf(")"));
								String value = details.get(propName);
								return valueToEnableVisibility.equalsIgnoreCase(value);
							}
							if (visibleCondition.startsWith("if-isone-of")){
								String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(","));
								String valueToEnableVisibility=visibleCondition.substring(visibleCondition.indexOf(",") + 1, visibleCondition.indexOf(")"));
								// remove [ and "]" syimbols
								String arrValues = valueToEnableVisibility.substring(1, valueToEnableVisibility.length() -1 );
//								System.out.println("values -> "+ arrValues);
								String[] splitterArr = arrValues.split(";");
								List<String> splittedValues = Arrays.asList(splitterArr);
								String value = details.get(propName);
								return splittedValues.contains(value);
							}
							return false;
						}
						
						
						
						
					};
		            try {
		                    command.execute(new NullProgressMonitor(), null);
		            } catch (ExecutionException exception) {
		            	BpmnEditorExtensionActivator.getDefault().getLog().log(new Status(IStatus.ERROR, BpmnEditorExtensionActivator.PLUGIN_ID, IStatus.ERROR, exception.getMessage(), exception));
		            }
		            this.runtimeLabel.setText(technologyName);
				}else{
					MessageDialog.openError(page.getSite().getShell(), " Error "," Technology Change ["+previousTechnologyId+"] --> ["+technologyId+"] Cannot be done");
					this.technologyText.select(findSelectionIndex(previousTechnologyName));
				}
			
		}

	}
	
	
	
	public int findSelectionIndex(String str){
		for (int i = 0; i < data.length; i++){
			if (data[i].equalsIgnoreCase(str))
				return i;
		}
		return 0;
	}

	
	
	
	
	
}
