/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.view;


import java.util.ArrayList;
import java.util.List;

public class ConditionsTreeElement extends ServiceTreeElement {

	protected List expressions;
	
	public ConditionsTreeElement() {
		super();
		expressions = new ArrayList();
	}
	
	public ConditionsTreeElement(String name) {
		this();
		this.name = name;
	}

	public List getExpressions() {
		return expressions;
	}

	public void setExpressions(List expressions) {
		this.expressions = expressions;
	}
	
	public void addExpression(ExpressionTreeElement expressionTreeElement){
		
		this.expressions.add(expressionTreeElement);
		expressionTreeElement.parent = this;	
	}
}
