/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.handler;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IExpression;
import org.eclipse.soa.mangrove.runtime.IProperty;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.IServiceBinding;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.view.ExpressionTreeElement;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.DataObject;
import org.eclipse.stp.bpmn.MessagingEdge;
import org.eclipse.stp.bpmn.NamedBpmnObject;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.stp.bpmn.SequenceEdge;
import org.eclipse.stp.bpmn.TextAnnotation;
import org.eclipse.stp.bpmn.dnd.AbstractEAnnotationDnDHandler;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;



public class ExpressionDndHandler extends AbstractEAnnotationDnDHandler {

	private String expressionName =  null;
	private String runtimeID =  null;
	private String expressionType = null;
	
	
	public ExpressionDndHandler(ExpressionTreeElement expressionTreeElement) {
		this.runtimeID =  expressionTreeElement.getRuntimeId();
		this.expressionName = expressionTreeElement.getName();
		this.expressionType = expressionTreeElement.getType();
		
	}
	
	/**
	 * this method returns a command that will create an annotation
	 * on the semantic element of the edit part passed in parameter.
	 */
	public Command getDropCommand(IGraphicalEditPart hoverPart, int index, 
			Point dropLocation) {
		
	
		Map<String, String> details = new HashMap<String, String>();

		details.put(ImConstants.IM_POOL_RUNTIME_ID,runtimeID);
		details.put(ImConstants.IM_TRANSITION_CONDITION_NAME,expressionName);
		details.put(ImConstants.IM_TRANSITION_TYPE, expressionType);

		
		IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeID);
		IExpression expression = runtime.getExpressionWithName(expressionName);
			
		for (IProperty property : expression.getDefinedProperties()){
			if (!property.isMap()){
				String value = (property.getDefaultValue() != null ? property.getDefaultValue() : ""); 
				details.put(property.getName(),value);
			}
		}
		
 		return createEAnnotationDropCommand(
				createAnnotation(ImConstants.IM_TRANSITION_ANNOTATION, details),
				(EModelElement) hoverPart.resolveSemanticElement());

	}

	/**
	 * the file DnD only enables the drop of one element.
	 */
	public int getItemCount() {
		return 1;
	}

	public Image getMenuItemImage(IGraphicalEditPart hoverPart, int index) {
		IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeID);
		IExpression expression = runtime.getExpressionWithName(expressionName);
		
		String bundleId = expression.getRegisteringBundleId();
		String iconPath = expression.getIconPath();
		
		ImageDescriptor descriptor = null;
		if ( 	(bundleId != null) 
				&& (bundleId.trim().length() > 0) 
				&& (iconPath != null) 
				&& (iconPath.trim().length() > 0))
			descriptor =  AbstractUIPlugin.imageDescriptorFromPlugin(bundleId, iconPath);
		else 
			descriptor = BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_condition.gif");
		
		return descriptor.createImage();

		
	}

	public String getMenuItemLabel(IGraphicalEditPart hoverPart, int index) {
		EObject element = hoverPart.resolveSemanticElement();
		String suffix = " on " + getShapeLabel((NamedBpmnObject) element);
        return "Attach Condition ["+expressionName+"] on " + suffix;
	}

	/**
	 * the file drop should have the lowest priority.
	 */
	public int getPriority() {
		return 0;
	}

	
	/**
     * @param namedElement
     * @return a label for a bpmn shape
     */
    protected String getShapeLabel(NamedBpmnObject namedElement) {
        String name = namedElement.getName() != null && 
            namedElement.getName().length() > 0 ? namedElement.getName() : "";
        if (name.length() > 18) {
            name = name.substring(0, 12) + "...";
        }
        if (namedElement instanceof Pool) {
            if (name.indexOf("pool") == -1) {
                return "pool " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof Activity) {
            String shape = ((Activity)namedElement).getActivityType().getLiteral();
            if (name.indexOf(shape) == -1) {
                return shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof MessagingEdge) {
            String shape = "message";
            if (name.indexOf(shape) == -1) {
                return shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof SequenceEdge) {
            String shape = "sequence";
            if (name.indexOf(shape) == -1) {
                return shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof Diagram) {
            String shape = "diagram";
            if (name.indexOf(shape) == -1) {
                return shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof TextAnnotation) {
            String shape = "annotation";
            if (name.indexOf(shape) == -1) {
                return "text " + shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof DataObject) {
            String shape = "object";
            if (name.indexOf(shape) == -1) {
                return "data " + shape + " " + name;
            } else {
                return name;
            }
        }
        return name;
    }

    @Override
    public boolean isEnabled(IGraphicalEditPart hoverPart, int index) {
    	EModelElement emodelElement = (EModelElement) hoverPart.resolveSemanticElement();
    	//System.out.println(" eModelElement "+ emodelElement.getClass().getName());
    	
    	if ((emodelElement instanceof SequenceEdge) || (emodelElement instanceof MessagingEdge))
			return true;
    	else
    		return false;
    
    }
	
}
