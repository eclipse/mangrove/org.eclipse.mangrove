/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.section;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.properties.sections.AdvancedPropertySection;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IElementDefiningProperties;
import org.eclipse.soa.mangrove.runtime.IProperty;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.runtime.comboproviders.AnnotationDependentComboProvider;
import org.eclipse.soa.mangrove.runtime.comboproviders.ComboEntries;
import org.eclipse.soa.mangrove.runtime.comboproviders.CustomComboPropertyDescriptor;
import org.eclipse.soa.mangrove.runtime.comboproviders.IComboProvider;
import org.eclipse.soa.mangrove.runtime.comboproviders.ValuesProvidedByInContextExtractorComboProvider;
import org.eclipse.soa.mangrove.runtime.property.listeners.IPropertyListener;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.extractor.IBpmnModelExtractor;
import org.eclipse.soa.mangrove.util.ImLogger;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.MessagingEdge;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.stp.bpmn.SequenceEdge;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;


public class IntermediateModelPropertySection extends AdvancedPropertySection implements IPropertySourceProvider {
		
		public IPropertySource getPropertySource(Object object) {
			EAnnotation ea = null;
			if (object instanceof Activity){
				ea = ((Activity)object).getEAnnotation(ImConstants.IM_ANNOTATION);
			}else if (object instanceof SequenceEdge){
				ea = ((SequenceEdge)object).getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION);
			}else if (object instanceof MessagingEdge){
				ea = ((MessagingEdge)object).getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION);
			}else if (object instanceof Pool){
				ea = ((Pool)object).getEAnnotation(ImConstants.TECHNOLOGY_ANNOTATION);
			}else{
				return null;
			}
			if (ea != null){
				return new IntermediateModelAnnotationPropertySource((EObject)object, ea);	
			}else{
				return null;
			}
			
		}
		

			
		/**
		 * Modify/unwrap selection.
		 * @generated
		 */
		protected Object transformSelection(Object selected) {
			if (selected instanceof EditPart) {
				Object model = ((EditPart) selected).getModel();
				return model instanceof View ? ((View) model).getElement() : null;
			}
			if (selected instanceof View) {
				return ((View) selected).getElement();
			}
			if (selected instanceof IAdaptable) {
				View view = (View) ((IAdaptable) selected).getAdapter(View.class);
				if (view != null) {
					return view.getElement();
				}
			}
			return selected;
		}
	

		/**
		 * @generated
		 */
		protected IPropertySourceProvider getPropertySourceProvider() {
			return this;
		}

		/**
		 * @generated
		 */
		public void setInput(IWorkbenchPart part, ISelection selection) { 
             if (!(selection instanceof IStructuredSelection)) {
            	 return;
				
             }else{
            	 Object unknownInput = ((IStructuredSelection) selection).getFirstElement();
            	 if (unknownInput == null)
            		 return;
            	 Object newInput = transformSelection(unknownInput);
            	 if (newInput == null)
            		 return;
            	 super.setInput(part, new StructuredSelection(newInput));  
             }
		}

		/**
		 * @generated
		 */
		protected AdapterFactory getAdapterFactory(Object object) {
			 
			if (getEditingDomain() instanceof AdapterFactoryEditingDomain) {
				return ((AdapterFactoryEditingDomain) getEditingDomain())
						.getAdapterFactory();
			}
			TransactionalEditingDomain editingDomain = TransactionUtil
					.getEditingDomain(object);
			if (editingDomain != null) {
				return ((AdapterFactoryEditingDomain) editingDomain)
						.getAdapterFactory();
			}
			return null;
		}
	
	

		public class IntermediateModelAnnotationPropertySource implements IPropertySource{
			/**The descriptor list.*/
			private transient List <IPropertyDescriptor> descriptorList = null;
			private static final String IM_CATEGORY = "00 - IntermediateModel";
			private static final String DEFAULT_PROP_CATEGORY = "01 - Step Properties";
			
			private static final String DEFAULT_PROP_PROCESS_CATEGORY = "01 - Process Properties";
			
			private EAnnotation ea = null;
			private EObject emfObject = null;
			private IRuntime imRuntime = null;
			private IElementDefiningProperties elementDefiningProperties = null;
		
			
			public IntermediateModelAnnotationPropertySource(EObject emfObject, EAnnotation ea){
				this.ea = ea;
				this.emfObject = emfObject;
				
				if (ea.getSource().equalsIgnoreCase(ImConstants.IM_ANNOTATION)){
					String runtimeId = ea.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
					String serviceBindingName = ea.getDetails().get(ImConstants.IM_SERVICE_BINDING_NAME);
					imRuntime = ImRuntimeActivator.getRuntime(runtimeId);
					this.elementDefiningProperties = imRuntime.getServiceBindings().get(serviceBindingName);
					if (this.elementDefiningProperties == null){
//						System.out.println(" Binding ["+serviceBindingName +"] Not found");
					}
				}else if (ea.getSource().equalsIgnoreCase(ImConstants.IM_TRANSITION_ANNOTATION)){
					String runtimeId = ea.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
					String expressionName = ea.getDetails().get(ImConstants.IM_TRANSITION_CONDITION_NAME);
					imRuntime = ImRuntimeActivator.getRuntime(runtimeId);
					this.elementDefiningProperties = imRuntime.getExpressionWithName(expressionName);
					if (this.elementDefiningProperties == null){
//						System.out.println(" Expression ["+expressionName +"] Not found");
					}
				}else if (ea.getSource().equalsIgnoreCase(ImConstants.TECHNOLOGY_ANNOTATION)){
					String runtimeId = ea.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
					imRuntime = ImRuntimeActivator.getRuntime(runtimeId);
					this.elementDefiningProperties = imRuntime.getProcessProperties();
					
				}
			}

			public Object getEditableValue() {
				return this;
			}

			public IPropertyDescriptor[] getPropertyDescriptors() {
				descriptorList = new ArrayList<IPropertyDescriptor>();
				createDescriptorForBindingAnnotation();
				IPropertyDescriptor [] myDescTable =
					new IPropertyDescriptor[descriptorList.size()];
				int indice = 0;
				
				for (IPropertyDescriptor it : descriptorList) {
					myDescTable[indice++] = it;
				}
				return myDescTable;
			}
			
			protected void createDescriptorForBindingAnnotation(){
				
				Map<String, Integer> counterForCategoryMap = new HashMap<String, Integer>();
				addReadOnlyTextDescriptor(ImConstants.IM_POOL_RUNTIME_ID, "01 - Runtime", IM_CATEGORY);
				if (this.ea.getSource().equalsIgnoreCase(ImConstants.IM_ANNOTATION)){
					addReadOnlyTextDescriptor(ImConstants.IM_SERVICE_NAME, "02 - Service", IM_CATEGORY);
					addReadOnlyTextDescriptor(ImConstants.IM_SERVICE_BINDING_NAME, "03 - Service Binding", IM_CATEGORY);
				}if (this.ea.getSource().equalsIgnoreCase(ImConstants.IM_TRANSITION_ANNOTATION)){
					addReadOnlyTextDescriptor(ImConstants.IM_TRANSITION_CONDITION_NAME, "02 - Condition Expression", IM_CATEGORY);
				}
				
				
			    //int counter = 0;
			    //String counterString = null;
				counterForCategoryMap.clear();
				Integer counter = null;
				String counterString  = null;
				String category = DEFAULT_PROP_CATEGORY;
				counterForCategoryMap.put(DEFAULT_PROP_CATEGORY, 0);
				if (this.ea.getSource().equalsIgnoreCase(ImConstants.TECHNOLOGY_ANNOTATION)){
					category = DEFAULT_PROP_PROCESS_CATEGORY;
					counterForCategoryMap.put(DEFAULT_PROP_PROCESS_CATEGORY, 0);
				}
			    
			    for (IProperty prop : elementDefiningProperties.getDefinedProperties()){
					if (prop.getUiCategoratory() != null){
						category = prop.getUiCategoratory();	
					}
					if ( isVisible(prop) ){
						if (counterForCategoryMap.containsKey(category)){
							counter = counterForCategoryMap.get(category);
						}else{
							counter = 0;
						}
						counter++;
						counterForCategoryMap.put(category, counter);
						counterString = getCounterString(counter);
						if (prop.isText()){
							if (prop.isEditable()){
								addTextDescriptor(prop.getName(), counterString + " - " + prop.getLabel(), category);
							}else{
								addReadOnlyTextDescriptor(prop.getName(), counterString + " - " + prop.getLabel(), category);
							}
						}else if (prop.isCombo()){
							IComboProvider comboProvider = prop.getComboProvider();
							if (comboProvider == null){
								comboProvider = imRuntime.getNamedComboProvider(prop.getComboProviderName());
							}
							
							
							
							if (comboProvider instanceof ValuesProvidedByInContextExtractorComboProvider) {
								//
								// Extract the class that must be instantiated here beacuse he need org.eclipse.stp.bpm
								//
								String extractorClassName = comboProvider.getParams().get("inContextExtractorClassName");
								try{
									IBpmnModelExtractor bpmnModelExtractor = (IBpmnModelExtractor)Class.forName(extractorClassName).newInstance();
									bpmnModelExtractor.setContextObject(emfObject);
									((ValuesProvidedByInContextExtractorComboProvider)comboProvider).setValues(bpmnModelExtractor.getValues());
									((ValuesProvidedByInContextExtractorComboProvider)comboProvider).setLabels(bpmnModelExtractor.getLabels());
								}catch(Throwable t){
									t.printStackTrace();
									ImLogger.error(BpmnEditorExtensionActivator.PLUGIN_ID, t.getMessage(), t);
								}
							}else 	if (comboProvider instanceof AnnotationDependentComboProvider) {
									((AnnotationDependentComboProvider)comboProvider).setAnnotation(this.ea);
							}
							
							ComboEntries ce = comboProvider.getComboEntries();
							addComboBoxDescriptor(prop.getName(), counterString + " - " + prop.getLabel(), ce, category);
						}
					}
				}
				
				category = null;
				String idxMapPropertyString = null;
				String displayName = null;
				IProperty mapProp = null;
				Map<String, String> mapPropertiesFieldsEditorMap = null;
				String mapFieldEditor = null;
				String mapPropertyName = null;
				
				
				for (String s : ea.getDetails().keySet()){
					//System.out.println( "Annotation KEY [" + s + "]" );
					if (s.indexOf("[") > 0){
						
						category =  s.substring(0, s.indexOf("["));
						mapPropertyName = category;
						//System.out.println(" Map Property Name "+ mapPropertyName );
						
						displayName = s.substring(s.indexOf("]") + 2);
						// -- NEW 
						idxMapPropertyString = s.substring(s.indexOf("[") +1, s.indexOf("]"));
						category += " - " + getCounterString(idxMapPropertyString);
						mapProp = elementDefiningProperties.getDefinedPropertyByName(mapPropertyName);
						mapPropertiesFieldsEditorMap = mapProp.getMapFieldsEditorsMap();
						
//						System.out.println(" Display Time  "+ displayName );
						if (mapPropertiesFieldsEditorMap != null 
							&& mapPropertiesFieldsEditorMap.containsKey(displayName)){
							mapFieldEditor = mapPropertiesFieldsEditorMap.get(displayName);
							
//							System.out.println(" Field Editor ["+mapFieldEditor+"]");
							if (mapFieldEditor.startsWith("combo")){ 
								String comboName = mapFieldEditor.substring(mapFieldEditor.indexOf("(") + 1, mapFieldEditor.indexOf(")"));
								IComboProvider comboProvider = imRuntime.getNamedComboProvider(comboName);
								if (comboProvider instanceof ValuesProvidedByInContextExtractorComboProvider) {
									//
									// Extract the class that must be instantiated here beacuse he need org.eclipse.stp.bpm
									//
									String extractorClassName = comboProvider.getParams().get("inContextExtractorClassName");
									try{
										IBpmnModelExtractor bpmnModelExtractor = (IBpmnModelExtractor)Class.forName(extractorClassName).newInstance();
										bpmnModelExtractor.setContextObject(emfObject);
										((ValuesProvidedByInContextExtractorComboProvider)comboProvider).setValues(bpmnModelExtractor.getValues());
										((ValuesProvidedByInContextExtractorComboProvider)comboProvider).setLabels(bpmnModelExtractor.getLabels());
									}catch(Throwable t){
										t.printStackTrace();
										ImLogger.error(BpmnEditorExtensionActivator.PLUGIN_ID, t.getMessage(), t);
									}
								}else 	if (comboProvider instanceof AnnotationDependentComboProvider) {
									((AnnotationDependentComboProvider)comboProvider).setAnnotation(this.ea);
								}
								ComboEntries ce = comboProvider.getComboEntries();
								Object value = ea.getDetails().get(s);
								
								
								addComboBoxDescriptor(s, displayName, ce, category);
							}else{
								addTextDescriptor(s, displayName, category);
							}
						}else{
							addTextDescriptor(s, displayName, category);
						}
					}
				}
				
			}
			/**
			 * add a descriptor in the descriptor model list.
			 * @param myDescr : the descriptor to add in the descriptor list.
			 */
			public final void addDescriptor(final IPropertyDescriptor myDescr) {
				descriptorList.add(myDescr);
			}

			/**
			 * add a text property descriptor in a property descriptor list.
			 * @param id : the id of this descriptor.
			 * @param displayName : the name of this descriptor.
			 */
			protected final void addReadOnlyTextDescriptor(final String id,
					final String displayName) {
				
				descriptorList.add(new PropertyDescriptor(id, displayName));
			}

			/**
			 * add a text property descriptor in a property descriptor list.
			 * @param id : the id of this descriptor.
			 * @param displayName : the name of this descriptor.
			 */
			protected final void addReadOnlyTextDescriptor(final String id,
					final String displayName, final String category) {
				PropertyDescriptor pDescr = new PropertyDescriptor(id, displayName);
				pDescr.setCategory(category);

				descriptorList.add(pDescr);
			}
			
			/**
			 * add a text property descriptor in a property descriptor list.
			 * @param id : the id of this descriptor.
			 * @param displayName : the name of this descriptor.
			 */
			protected final void addTextDescriptor(final String id,
					final String displayName) {
				descriptorList.add(new TextPropertyDescriptor(id, displayName));
			}

			/**
			 * add a text property descriptor in a property descriptor list.
			 * @param id : the id of this descriptor.
			 * @param displayName : the name of this descriptor.
			 */
			protected final void addTextDescriptor(final String id,
					final String displayName, final String category) {
				TextPropertyDescriptor pDescr = new TextPropertyDescriptor(id, displayName);
				((TextPropertyDescriptor)pDescr).setCategory(category);

				descriptorList.add(pDescr);
			}
			
			
			/**
			 * add a combobox  property descriptor in the descriptor component list.
			 * @param id : the id of this descriptor.
			 * @param displayName : the display name
			 * @param value : the table value of this combo.
			 */
			protected final void addComboBoxDescriptor(final String id,
					final String displayName, final ComboEntries entries) {
				descriptorList.add(new CustomComboPropertyDescriptor(
						id, displayName, entries, 0));
			}
			
			protected final void addComboBoxDescriptor(final String id,
					final String displayName, final ComboEntries entries, final String category) {
				CustomComboPropertyDescriptor pDescr = new CustomComboPropertyDescriptor(
						id, displayName, entries, 0);
				pDescr.setCategory(category);
				descriptorList.add(pDescr);
			}
			
			public Object getPropertyValue(Object propertyID) {
				System.out.println("getPropertyValue("+propertyID+")");
				/*if (notInitializedComboEntries.containsKey(propertyID)){
					setAnnotationProperty((String)propertyID, (String)notInitializedComboEntries.get(propertyID));
					notInitializedComboEntries.remove(propertyID);
				}*/
				return getAnnotationProperty((String)propertyID);
			}

			public boolean isPropertySet(Object propertyId) {
				System.out.println(" Is PropertySet("+propertyId+")");
				return true;
			}

			public void resetPropertyValue(Object propertyId) {
				System.out.println(" resetPropertyValue ("+propertyId+")" );
				
			}

			public void setPropertyValue(Object propertyID, Object propertyValue) {
				//
				//
				//
				String thePropertyID = (String)propertyID;
				if ((thePropertyID.equalsIgnoreCase(ImConstants.IM_POOL_RUNTIME_ID)) 
						|| (thePropertyID.equalsIgnoreCase(ImConstants.IM_SERVICE_BINDING_NAME))
						|| (thePropertyID.equalsIgnoreCase(ImConstants.IM_SERVICE_NAME))){
					
					setAnnotationProperty((String)propertyID, propertyValue);
					return;
				}
				
				if (!isMapEntry(thePropertyID)){
					IProperty propertyDef = this.elementDefiningProperties.getDefinedPropertyByName(thePropertyID);
					String dependentPropertyName = null;
					if (propertyDef != null){
						dependentPropertyName =  propertyDef.getDependentProperty();
					}else{
						ImLogger.warning(BpmnEditorExtensionActivator.PLUGIN_ID," Not found property with name ["+propertyID+"]");
					}
					
					//System.out.println(" Dependent PropertyName ");
					
					// ---------------------------------------------
					// Notifico i listener se c'e' ne bisogno
					// ---------------------------------------------
					
					if (dependentPropertyName != null){
						IProperty dependentProperty = this.elementDefiningProperties.getDefinedPropertyByName(dependentPropertyName);
						IPropertyListener listener = propertyDef.getPropertyListener();
						if ( listener != null){
							System.out.println(" Notifyng Listener ");
							listener.handleUpdate((String)propertyValue, propertyDef.getDependentProperty(),dependentProperty, ea );
				
						}
				
					}
				}
				
				setAnnotationProperty((String)propertyID, propertyValue);
				
				
			}
			
			protected Object getAnnotationProperty(String propertyName){
				
				return ea.getDetails().get(propertyName);
			}
			
			protected void setAnnotationProperty(String propertyName, Object value){
				ea.getDetails().put(propertyName, (String)value);
			}
	
			protected boolean isMapEntry(String propertyID){
				return propertyID.indexOf("[") > 0;
			}
			
			protected boolean isVisible(IProperty prop){
				if (!prop.isVisibleUnderCondition())
					return true;
				else
					return evaluateVisibilityCondition(prop.getVisibleCondition());
			}
			
			protected boolean evaluateVisibilityCondition(String visibleCondition){
				if (visibleCondition.startsWith("if-is-true")){
					String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(")"));
					String value = this.ea.getDetails().get(propName);
					return Boolean.valueOf(value);
				}
				if (visibleCondition.startsWith("if-is-false")){
						String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(")"));
						String value = this.ea.getDetails().get(propName);
						return !Boolean.valueOf(value);
				}
				if (visibleCondition.startsWith("if-has-value")){
					String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(","));
					String valueToEnableVisibility=visibleCondition.substring(visibleCondition.indexOf(",") + 1, visibleCondition.indexOf(")"));
					String value = this.ea.getDetails().get(propName);
					return valueToEnableVisibility.equalsIgnoreCase(value);
				}
				if (visibleCondition.startsWith("if-isone-of")){
					String propName = visibleCondition.substring(visibleCondition.indexOf("(") + 1, visibleCondition.indexOf(","));
					String valueToEnableVisibility=visibleCondition.substring(visibleCondition.indexOf(",") + 1, visibleCondition.indexOf(")"));
					String arrValues = valueToEnableVisibility.substring(1, valueToEnableVisibility.length() -1 );
					System.out.println("values -> "+ arrValues);
					String[] splitterArr = arrValues.split(";");
					List<String> splittedValues = Arrays.asList(splitterArr);
					String value = this.ea.getDetails().get(propName);
					return splittedValues.contains(value);
				}
				return false;
			}
			
			protected String getCounterString(int counter){
				if (counter > 9)
					return String.valueOf(counter);
				else
					return "0"+ String.valueOf(counter);
			}
			
			protected String getCounterString(String  counter){
				try{
					int value = Integer.valueOf(counter);
					return getCounterString(value);
				}catch (Exception e) {
					return counter;
				}
			}
			
		}
				

   	
}

