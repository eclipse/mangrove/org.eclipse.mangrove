/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.extractor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.Artifact;
import org.eclipse.stp.bpmn.DataObject;
import org.eclipse.stp.bpmn.Graph;
import org.eclipse.stp.bpmn.NamedBpmnObject;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.stp.bpmn.SubProcess;
import org.eclipse.stp.bpmn.Vertex;

public class DataObjectWithNoAnnotationExtractor implements IBpmnModelExtractor {

	private String[] labels = null;
	private String[] values = null;
	
	public String[] getLabels() {
		
		return labels;
	}

	public String[] getValues() {
		
		return values;
	}

	public void setContextObject(EObject emfObject) {
		this.labels = new String[0];
		this.values = new String[0];
		List<String> variables = new ArrayList<String>();
		
		if (emfObject instanceof Vertex) {
			Graph graph = ((Vertex)emfObject).getGraph();
			
			if (graph instanceof SubProcess) {
				graph = ((SubProcess)graph).getGraph();
			}
			if (graph instanceof Pool) {
				Pool aPool = (Pool)graph;
				// VARIABLE ARE DATA OBJECTS WITH NO ANNOTATION AND THE NAME SETTED
				for (Artifact anArtifact : aPool.getArtifacts()){
					if ((anArtifact instanceof DataObject) && (anArtifact.getEAnnotations().size() == 0)) {
						variables.add(anArtifact.getName());
					}
				}//for
				if (variables.size() == 0)
					variables.add(" -- No Variable Configured -- ");
				variables.add("input");
				variables.add("output");
				
				labels = variables.toArray(labels);
				values = variables.toArray(values);
			}// if
		}//if	
	}
	
	

}
