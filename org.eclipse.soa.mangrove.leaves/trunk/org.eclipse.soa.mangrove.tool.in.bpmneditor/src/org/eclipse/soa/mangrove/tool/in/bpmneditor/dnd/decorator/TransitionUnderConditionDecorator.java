/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.decorator;


import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget.Direction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.runtime.IExpression;
import org.eclipse.soa.mangrove.runtime.IRuntime;
import org.eclipse.soa.mangrove.runtime.ImRuntimeActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.stp.bpmn.dnd.IEAnnotationDecorator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class TransitionUnderConditionDecorator implements IEAnnotationDecorator {

	
	public String getAssociatedAnnotationSource() {
		return ImConstants.IM_TRANSITION_ANNOTATION;
		
	}

	public Direction getDirection(EditPart part, EModelElement element,
			EAnnotation annotation) {
		return Direction.CENTER;
	}
	
	/*
	public Image getImage(EditPart part, EModelElement element, EAnnotation annotation) {
		
		
		ImageDescriptor desc = BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_condition.gif");
		return desc == null ? null : desc.createImage();
		
	}
*/
	public IFigure getToolTip(EditPart part, EModelElement element, EAnnotation annotation) {
    
		String name = (String) annotation.getDetails().get(ImConstants.IM_TRANSITION_CONDITION_NAME);
        return new org.eclipse.draw2d.Label("Transition under condition  - " + name + " ");
	
	}
	
	public ImageDescriptor getImageDescriptor(EditPart part,
			EModelElement element, EAnnotation annotation) {
		
		EAnnotation imAnnotation = element.getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION);
		
		ImageDescriptor desc = BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_condition.gif");
		
		if (imAnnotation != null){
			String runtimeID = imAnnotation.getDetails().get(ImConstants.IM_POOL_RUNTIME_ID);
			String expressionName = imAnnotation.getDetails().get(ImConstants.IM_TRANSITION_CONDITION_NAME);
			
			IRuntime runtime = ImRuntimeActivator.getRuntime(runtimeID);
			IExpression expression = runtime.getExpressionWithName(expressionName);
		
			String bundleId = expression.getRegisteringBundleId();
			String iconPath = expression.getIconPath();
		
	
			if ( 	(bundleId != null) 
				&& (bundleId.trim().length() > 0) 
				&& (iconPath != null) 
				&& (iconPath.trim().length() > 0))
				desc =  AbstractUIPlugin.imageDescriptorFromPlugin(bundleId, iconPath);
			}
		
			return desc;
	
	}
	
	public Image getImage(EditPart part, EModelElement element, EAnnotation annotation) {
		ImageDescriptor desc = getImageDescriptor(part, element, annotation);
	
		return desc == null ? null : desc.createImage();
	}

}
