/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/

package org.eclipse.soa.mangrove.tool.in.bpmneditor.diagram;

import org.eclipse.gmf.runtime.common.ui.action.AbstractActionHandler;
import org.eclipse.gmf.runtime.common.ui.action.ActionMenuManager;
import org.eclipse.gmf.runtime.common.ui.util.IPartSelector;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.jface.action.IAction;
import org.eclipse.ui.IWorkbenchPart;


public class ImActionMenuManager extends ActionMenuManager {
	
    public static final String MENU_ID = "imMenu";
    private static final String MENU_TEXT = "IntermediateModel";

   

    

    /**
     * Creates a new instance of the group menu manager
     * 
     * @param action
     *            default action associated with this menu manager (should not
     *            be null)
     */
    public ImActionMenuManager(IAction action) {
        super(MENU_ID, action, true);

       

        ((AbstractActionHandler) getDefaultAction())
                .setPartSelector(new IPartSelector() {
                    public boolean selects(IWorkbenchPart p) {
                        return p instanceof IDiagramWorkbenchPart;
                    }
                });
    }
}

