/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.decorator;


import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget.Direction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.stp.bpmn.dnd.IEAnnotationDecorator;

import org.eclipse.swt.graphics.Image;

public class RuntimeDecorator implements IEAnnotationDecorator {

	
	public String getAssociatedAnnotationSource() {
		return ImConstants.TECHNOLOGY_ANNOTATION;
		
	}

	public Direction getDirection(EditPart part, EModelElement element,
			EAnnotation annotation) {
		return Direction.SOUTH_WEST;
	}

	public Image getImage(EditPart part, EModelElement element, EAnnotation annotation) {
		ImageDescriptor desc = BpmnEditorExtensionActivator.getImageDescriptor("icons/spagic.gif");
		return desc == null ? null : desc.createImage();
	}

	public IFigure getToolTip(EditPart part, EModelElement element, EAnnotation annotation) {
		if (annotation != null){
			String name = (String) annotation.getDetails().get(ImConstants.TECHNOLOGY_NAME);
			return new org.eclipse.draw2d.Label("This Pool is bound to  - "+name + " - Technology");
		}else{
			return new org.eclipse.draw2d.Label("");
		}
	}
	
	public ImageDescriptor getImageDescriptor(EditPart part,
			EModelElement element, EAnnotation annotation) {
		ImageDescriptor desc = BpmnEditorExtensionActivator.getImageDescriptor("icons/spagic.gif");
		return desc;
	}

}
