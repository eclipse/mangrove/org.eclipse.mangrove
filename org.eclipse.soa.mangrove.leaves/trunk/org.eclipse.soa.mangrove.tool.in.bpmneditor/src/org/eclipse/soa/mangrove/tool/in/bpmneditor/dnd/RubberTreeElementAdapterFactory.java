/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.handler.RubberDndHandler;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.handler.ServiceWithServiceBindingDndHandler;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.view.RubberTreeElement;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.view.ServiceBindingTreeElement;
import org.eclipse.stp.bpmn.dnd.IDnDHandler;

public class RubberTreeElementAdapterFactory implements IAdapterFactory {     

    @SuppressWarnings("unchecked")
 	public Object getAdapter(Object adaptableObject, Class adapterType) {
    	if (adapterType.equals(IDnDHandler.class)) {
 			if (adaptableObject instanceof RubberTreeElement) {
 				
 				
 				return new RubberDndHandler();
 			}
 		}
 		return null;
 	}

 	
 	/**
      * Returns the collection of adapter types handled by this
      * factory.
      * <p>
      * This method is generally used by an adapter manager
      * to discover which adapter types are supported, in advance
      * of dispatching any actual <code>getAdapter</code> requests.
      * </p>
      *
      * @return the collection of adapter types
      */
 	@SuppressWarnings("unchecked")
 	public Class[] getAdapterList() {
 		return new Class[] {IDnDHandler.class};
 	}
}
