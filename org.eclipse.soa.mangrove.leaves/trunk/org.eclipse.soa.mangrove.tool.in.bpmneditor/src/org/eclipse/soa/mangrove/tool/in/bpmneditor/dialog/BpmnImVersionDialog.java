/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dialog;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.stp.bpmn.BpmnDiagram;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class BpmnImVersionDialog extends TitleAreaDialog {

	// final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

	private Composite aComposite = null;
	private CLabel currentVersionLabelLabel = null;
	private CLabel currentVersionLabelText = null;

	private CLabel newVersionLabel = null;
	private Text newVersionText = null;

	private Group versioningTypeGroup = null;
	private Button versionOnCurrentDiagramButton = null;
	private Button versionCreateNewDiagram = null;
	private String currentVersion = null;
	private BpmnDiagram bpmnDiagram = null;
	
	public BpmnImVersionDialog(Shell parentShell, BpmnDiagram bpmnDiagram) {
		super(parentShell);
		this.bpmnDiagram = bpmnDiagram;
		EAnnotation versionAnnotation = bpmnDiagram.getEAnnotation(ImConstants.IM_VERSION_ANNOTATION);
		if (versionAnnotation != null){
			currentVersion = versionAnnotation.getDetails().get(ImConstants.PROCESS_VERSION);
		}
		
		
	}

	/**
	 * @see org.eclipse.jface.window.Window#create() We complete the dialog with
	 *      a title and a message
	 */
	public void create() {
		super.create();

		setTitle("Versioning of " + (bpmnDiagram != null ? ( bpmnDiagram.getName() != null ? bpmnDiagram.getName() : bpmnDiagram.getID()) : ""));
		setMessage("*** PLEASE ENSURE THAT THE DIAGRAM IS CLOSED BEFORE CHANGING THE VERSION ***");

	}

	@Override
	protected Control createContents(Composite parent) {
		Control ctrl = super.createContents(parent);
		return ctrl;
	}

	protected Control createDialogArea(Composite parent) {

		aComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.marginWidth = 15;
		layout.marginHeight = 10;
		layout.numColumns = 5;
		aComposite.setLayout(layout);
		aComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = 200;
		data.horizontalSpan = 4;

		// Riga 2
		currentVersionLabelLabel = new CLabel(aComposite, SWT.RIGHT);
		currentVersionLabelLabel.setText(" Current Version ");
		currentVersionLabelText = new CLabel(aComposite, SWT.BORDER | SWT.FLAT);
		currentVersionLabelText.setText(currentVersion != null ? currentVersion : "Not versioned (0)");
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = 200;
		data.horizontalSpan = 4;
		currentVersionLabelText.setLayoutData(data);

		// Riga 2
		newVersionLabel = new CLabel(aComposite, SWT.RIGHT);
		newVersionLabel.setText(" New Version ");
		newVersionText = new Text(aComposite, SWT.BORDER | SWT.FLAT);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = 200;
		data.horizontalSpan = 4;
		newVersionText.setLayoutData(data);

		
		versioningTypeGroup = new Group(aComposite, SWT.SHADOW_ETCHED_IN);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 5;
		versioningTypeGroup
				.setText(" Versioning ");
		versionOnCurrentDiagramButton = new Button(versioningTypeGroup,
				SWT.RADIO);
		versionOnCurrentDiagramButton.setText("On This File");
		versionOnCurrentDiagramButton.setBounds(5, 10, 100, 30);
		versionCreateNewDiagram = new Button(versioningTypeGroup,
				SWT.RADIO);
		versionCreateNewDiagram.setText("Create New File ");
		versionCreateNewDiagram.setBounds(130, 10, 100, 30);

		versionOnCurrentDiagramButton.setSelection(true);
		versionCreateNewDiagram.setSelection(false);
		versioningTypeGroup.setLayoutData(data);
		return parent;

	}

	protected void okPressed() {
		String newVersion = newVersionText.getText().trim();
		
		if (newVersion == null || newVersion.length() == 0){
			MessageDialog.openError(getShell(), "IntermediateModel",
			"Enter the new version");
			return;
		}
		
		if (newVersion != null && newVersion.equalsIgnoreCase(currentVersion)){
			MessageDialog.openError(getShell(), "IntermediateModel",
			"New Version and Old Version has the same value !");
			return;
		}
		if (versionCreateNewDiagram.getSelection()){
			MessageDialog.openError(getShell(), "IntermediateModel",
					"This mode is not supported yet");
			return;
		}
		
		
		if (currentVersion == null){
			EAnnotation ea = EcoreFactory.eINSTANCE.createEAnnotation();
			ea.setSource(ImConstants.IM_VERSION_ANNOTATION);
		
			Map<String, String> details = new HashMap<String, String>();
			details.put(ImConstants.PROCESS_VERSION, newVersionText.getText());
			ea.getDetails().putAll(details);
			bpmnDiagram.getEAnnotations().add(ea);
		}else{
			EAnnotation ea = bpmnDiagram.getEAnnotation(ImConstants.IM_VERSION_ANNOTATION);
			ea.getDetails().put(ImConstants.PROCESS_VERSION, newVersionText.getText());
		}
		super.okPressed();
		

	}

	@Override
	protected void buttonPressed(int buttonId) {

		super.buttonPressed(buttonId);
	}

}
