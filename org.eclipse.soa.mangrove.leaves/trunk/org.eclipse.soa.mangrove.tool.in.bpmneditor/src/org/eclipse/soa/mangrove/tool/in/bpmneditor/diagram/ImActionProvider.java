/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.diagram;

import org.eclipse.gmf.runtime.common.ui.services.action.contributionitem.AbstractContributionItemProvider;
import org.eclipse.gmf.runtime.common.ui.services.action.contributionitem.ActionRegistry;
import org.eclipse.gmf.runtime.common.ui.util.IWorkbenchPartDescriptor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.action.DeleteImAnnotationAction;
import org.eclipse.stp.bpmn.diagram.providers.BpmnDiagramActionProvider;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.actions.ActionGroup;


public class ImActionProvider extends AbstractContributionItemProvider {

    public ImActionProvider() {
		super();
	}
    /**
     * @return The menu manager
     */
    protected IMenuManager createMenuManager(String menuId,
            IWorkbenchPartDescriptor partDescriptor) {
        
    	if (ImActionMenuManager.MENU_ID.equals(menuId)) {
              return new ImActionMenuManager(getAction(DeleteImAnnotationAction.ID,
                      partDescriptor));
          }
        return super.createMenuManager(menuId, partDescriptor);
    }

    /**
     * 
     */
    protected IAction createAction(String actionId,
            IWorkbenchPartDescriptor partDescriptor) {

        IWorkbenchPage workbenchPage = partDescriptor.getPartPage();
        
        if (DeleteImAnnotationAction.ID.equals(actionId)) {
        	return new DeleteImAnnotationAction(workbenchPage);
        } 
        return super.createAction(actionId, partDescriptor);
    }
	@Override
	protected ActionGroup createActionGroup(String actionGroupId,
			IWorkbenchPartDescriptor partDescriptor) {
		// TODO Auto-generated method stub
		return super.createActionGroup(actionGroupId, partDescriptor);
	}
	@Override
	protected IContributionItem createCustomContributionItem(String customId,
			IWorkbenchPartDescriptor partDescriptor) {
		// TODO Auto-generated method stub
		return super.createCustomContributionItem(customId, partDescriptor);
	}
	@Override
	protected IAction getActionFromRegistry(String actionId,
			IWorkbenchPartDescriptor partDescriptor, ActionRegistry registry) {
		// TODO Auto-generated method stub
		return super.getActionFromRegistry(actionId, partDescriptor, registry);
	}
    
}

