/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/

package org.eclipse.soa.mangrove.tool.in.bpmneditor.action;

import java.util.List;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.stp.bpmn.diagram.actions.AbstractDeleteAnnotationAction;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;


public class DeleteImAnnotationAction extends AbstractDeleteAnnotationAction {

	public static final String ID = "deleteIntermediateModelAnnotationAction";
	
	public DeleteImAnnotationAction(IWorkbenchPage page) {
		super(page);
		setId(ID);
		setText("Remove Intermediate Model Annotation");
	}

	public DeleteImAnnotationAction(IWorkbenchPart part) {
		super(part);
		setId(ID);
		setText("Remove Intermediate Model Annotation");
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.stp.bpmn.diagram.actions.AbstractDeleteAnnotationAction#getAnnotationSource()
	 */
	@Override
	protected String getAnnotationSource() {
		return ImConstants.IM_ANNOTATION;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.stp.bpmn.diagram.actions.AbstractDeleteAnnotationAction#updateImage(org.eclipse.emf.ecore.EModelElement)
	 */
	@Override
	protected ImageDescriptor updateImage(EModelElement elt) {
		
		return PlatformUI.getWorkbench().
			getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJ_FILE);
		/*
		if (elt == null) {
			return PlatformUI.getWorkbench().
				getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJ_FILE);
		}
		EAnnotation annotation = elt.getEAnnotation(getAnnotationSource());
		if (annotation == null) {
			return PlatformUI.getWorkbench().
				getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJ_FILE);
		}
		String filePath = (String) annotation.getDetails().
		get(FileDnDConstants.PROJECT_RELATIVE_PATH);
		
		IFile ourFile = WorkspaceSynchronizer.getFile(elt.eResource()).
			getProject().getFile(filePath);
		IWorkbenchAdapter adapter = (IWorkbenchAdapter) 
		Platform.getAdapterManager().
			getAdapter(ourFile, IWorkbenchAdapter.class);
		if (adapter == null) {
			return PlatformUI.getWorkbench().
				getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJ_FILE);
		} else {
			return adapter.getImageDescriptor(ourFile);
		}
		*/
	}

	/* (non-Javadoc)
	 * @see org.eclipse.stp.bpmn.diagram.actions.AbstractDeleteAnnotationAction#updateText(org.eclipse.emf.ecore.EModelElement)
	 */
	@Override
	protected String updateText(EModelElement elt) {
		if (elt == null || elt.getEAnnotation(getAnnotationSource()) == null) {
			return "Remove Im Annotation";
		} else {
			EAnnotation annotation = elt.getEAnnotation(getAnnotationSource());
			String serviceName = annotation.getDetails().get(ImConstants.IM_SERVICE_NAME);
			String serviceBinding = annotation.getDetails().get(ImConstants.IM_SERVICE_BINDING_NAME);
			return "Remove Service ["+serviceName+"] with Binding ["+serviceBinding+"] Annotation" ;
		}
		
	}

	@Override
	protected String getDefaultLabel() {
		return "Remove Im Annotation";
	}
	
	@Override
	protected List<String> getAnnotationDetails() {
		// TODO Auto-generated method stub
		return null;
	}
}
