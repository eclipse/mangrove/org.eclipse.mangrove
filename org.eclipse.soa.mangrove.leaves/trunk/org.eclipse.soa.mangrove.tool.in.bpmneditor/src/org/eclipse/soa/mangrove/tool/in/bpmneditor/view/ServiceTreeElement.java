/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.view;


import java.util.ArrayList;
import java.util.List;

public class ServiceTreeElement extends ServiceTreeModel {
	protected List services;
	protected List bindings;

	
	protected String name = null;
	
	
	public ServiceTreeElement() {
		services = new ArrayList();
		bindings = new ArrayList();
	}
	
	public void addServiceTreeElement(ServiceTreeElement serviceTreeElement){
		
		this.services.add(serviceTreeElement);
		serviceTreeElement.parent = this;
	}
	
	public void addServiceBindingTreeElement(ServiceBindingTreeElement serviceBindingTreeElement){
		
		this.bindings.add(serviceBindingTreeElement);
		serviceBindingTreeElement.parent = this;	
	}
	
	public ServiceTreeElement(String name) {
		this();
		this.name = name;
	}
		
	
	/** Answer the total number of items the
	 * receiver contains. */
	public int size() {
		return getServices().size() + getBindings().size();
	}
	



	public List getServices() {
		return services;
	}



	public void setServices(List services) {
		this.services = services;
	}



	public List getBindings() {
		return bindings;
	}



	public void setBindings(List bindings) {
		this.bindings = bindings;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
