/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.action;

import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.dialog.BpmnImVersionDialog;
import org.eclipse.stp.bpmn.BpmnDiagram;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;


public class VersionBpmnDiagramAction implements IObjectActionDelegate {

	private IFile bpmnFile = null;
	
	
	
	/**
	 * Constructor for Action1.
	 */
	public VersionBpmnDiagramAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		final Shell shell = new Shell();

		        	 	try{
		        	 	
		        	 		versionBpmn(shell);
		        	 	
		        	 	}catch (Exception e) {
		        	 		reportError(shell,e);
		        	 		
		        	 	}
		        	 	
		      
	
		
	}
	public void versionBpmn(Shell shell) throws Exception {
		// Read the bpmn emf model
		ResourceSet resourceSetBpmn = new ResourceSetImpl();
		URI uri = URI.createPlatformResourceURI(bpmnFile.getFullPath().toString(), true);
	
		
		//
		Resource resourceBpmn = resourceSetBpmn.getResource(uri, true);	
		BpmnDiagram bpmnDiagram= (BpmnDiagram)resourceBpmn.getContents().get(0);
		
		//EAnnotation versionAnnotation = bpmnDiagram.getEAnnotation(ImConstants.IM_VERSION_ANNOTATION);
		
		
		
		BpmnImVersionDialog dialog = new BpmnImVersionDialog(shell, bpmnDiagram);
		int result = dialog.open();
		
		if (result == dialog.OK){
			resourceBpmn.save(Collections.EMPTY_MAP);
		}
		
	}
	
	public final void selectionChanged(final IAction action,
			final ISelection selection) {
		
		setBpmnFile((IFile) ((StructuredSelection) selection)
				.getFirstElement());
	}

	public IFile getBpmnFile() {
		return bpmnFile;
	}

	public void setBpmnFile(IFile bpmnFile) {
		this.bpmnFile = bpmnFile;
	}
	
	/**
	    * Displays an error that occured during the project creation.
	    * @param x details on the error
	    */
	   private void reportError(Shell shell, Throwable x)
	   {
	      ErrorDialog.openError(shell,
	                            "Error",
	                            "Error Occurred",
	                            makeStatus(x));
	   }
	   
	   
	   public static IStatus makeStatus(Throwable x)
	   {
		   		
		   return new Status(IStatus.ERROR, BpmnEditorExtensionActivator.PLUGIN_ID, IStatus.ERROR,
				   (x.getMessage() != null) ? x.getMessage()
				   : (x.getCause() != null) ? x.getCause().getMessage() : "", null);


	   }
}
	
	
