/*******************************************************************************
 * Copyright (c) {2007, 2008} Engineering Ingegneria Informatica S.p.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Andrea Zoppello (Engineering) - initial API and implementation
 *******************************************************************************/
package org.eclipse.soa.mangrove.tool.in.bpmneditor.dnd.handler;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.soa.mangrove.constants.ImConstants;
import org.eclipse.soa.mangrove.tool.in.bpmneditor.BpmnEditorExtensionActivator;
import org.eclipse.stp.bpmn.Activity;
import org.eclipse.stp.bpmn.Artifact;
import org.eclipse.stp.bpmn.DataObject;
import org.eclipse.stp.bpmn.MessagingEdge;
import org.eclipse.stp.bpmn.NamedBpmnObject;
import org.eclipse.stp.bpmn.Pool;
import org.eclipse.stp.bpmn.SequenceEdge;
import org.eclipse.stp.bpmn.TextAnnotation;
import org.eclipse.stp.bpmn.Vertex;
import org.eclipse.stp.bpmn.dnd.AbstractEAnnotationDnDHandler;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;



public class RubberDndHandler extends AbstractEAnnotationDnDHandler {

	
	
	
	public RubberDndHandler() {
		
		
	}
	
	/**
	 * this method returns a command that will create an annotation
	 * on the semantic element of the edit part passed in parameter.
	 */
	public Command getDropCommand(IGraphicalEditPart hoverPart, int index, 
			Point dropLocation) {
		
		EModelElement eModelElement = (EModelElement) hoverPart.resolveSemanticElement();
		EAnnotation ea = null;
		if ( eModelElement.getEAnnotations().size() > 0){
			
			ea = eModelElement.getEAnnotations().get(0);
		}
		CompoundCommand co = new CompoundCommand("Drop Annotation Command");
		if (ea != null){
			if (eModelElement instanceof Pool){
				if (checkIMAnnotationsOnPoolContent((Pool)eModelElement)){
					MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Intermediate Model", "Cannot Delete Pool Annotaion");
					return co;
				}
			}
			if (eModelElement.getEAnnotation(ea.getSource()) != null) {
				co.add(new ICommandProxy(deleteAnnotation(eModelElement, ea)));
			}
		}
		return co;
 		

	}
	
	 private ICommand deleteAnnotation(EModelElement selectedElt, EAnnotation ea) {
	    	if (selectedElt.getEAnnotation(ea.getSource()) != null) {
	    		DestroyElementRequest request = new DestroyElementRequest(
	    				selectedElt.getEAnnotation(ea.getSource()),
	    				true);
	    		DestroyElementCommand command = 
	    			new DestroyElementCommand(request);
	    		
	    		return command;
	    	}
	    	return null;
	    }

	
	public int getItemCount() {
		return 1;
	}

	public Image getMenuItemImage(IGraphicalEditPart hoverPart, int index) {
		return BpmnEditorExtensionActivator.getImageDescriptor("icons/ico_condition.gif").createImage();
		
	}

	public String getMenuItemLabel(IGraphicalEditPart hoverPart, int index) {
		EObject element = hoverPart.resolveSemanticElement();
		String suffix = " on " + getShapeLabel((NamedBpmnObject) element);
        return "Delete Annotation on " + suffix;
	}

	/**
	 * the file drop should have the lowest priority.
	 */
	public int getPriority() {
		return 0;
	}

	
	/**
     * @param namedElement
     * @return a label for a bpmn shape
     */
    protected String getShapeLabel(NamedBpmnObject namedElement) {
        String name = namedElement.getName() != null && 
            namedElement.getName().length() > 0 ? namedElement.getName() : "";
        if (name.length() > 18) {
            name = name.substring(0, 12) + "...";
        }
        if (namedElement instanceof Pool) {
            if (name.indexOf("pool") == -1) {
                return "pool " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof Activity) {
            String shape = ((Activity)namedElement).getActivityType().getLiteral();
            if (name.indexOf(shape) == -1) {
                return shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof MessagingEdge) {
            String shape = "message";
            if (name.indexOf(shape) == -1) {
                return shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof SequenceEdge) {
            String shape = "sequence";
            if (name.indexOf(shape) == -1) {
                return shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof Diagram) {
            String shape = "diagram";
            if (name.indexOf(shape) == -1) {
                return shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof TextAnnotation) {
            String shape = "annotation";
            if (name.indexOf(shape) == -1) {
                return "text " + shape + " " + name;
            } else {
                return name;
            }
        } else if (namedElement instanceof DataObject) {
            String shape = "object";
            if (name.indexOf(shape) == -1) {
                return "data " + shape + " " + name;
            } else {
                return name;
            }
        }
        return name;
    }

    @Override
    public boolean isEnabled(IGraphicalEditPart hoverPart, int index) {
    	EModelElement emodelElement = (EModelElement) hoverPart.resolveSemanticElement();
    	//System.out.println(" eModelElement "+ emodelElement.getClass().getName());
    	
    	
    	if ((emodelElement instanceof SequenceEdge) 
    		|| (emodelElement instanceof MessagingEdge)
    		|| (emodelElement instanceof Activity)
    		|| (emodelElement instanceof Pool))  {
		
			return true;
    	}else
    		return false;
    
    }
    
    
    public boolean checkIMAnnotationsOnPoolContent(Pool aPool){
    	
    	for (Vertex aVertex :  aPool.getVertices())
    		if ((aVertex.getEAnnotation(ImConstants.IM_ANNOTATION) != null)
    			|| (aVertex.getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION) != null)
    			|| (aVertex.getEAnnotation(ImConstants.IM_OBSERVABLE_ATTRIBUTE) != null))
    			return true;
 
		for ( SequenceEdge seqEdge : aPool.getSequenceEdges())
			if ((seqEdge.getEAnnotation(ImConstants.IM_ANNOTATION) != null)
				|| ( seqEdge.getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION) != null)
				|| ( seqEdge.getEAnnotation(ImConstants.IM_OBSERVABLE_ATTRIBUTE) != null))
				return true;
		
		
		for ( Artifact artifact : aPool.getArtifacts())
			if ((artifact.getEAnnotation(ImConstants.IM_ANNOTATION) != null)
				|| ( artifact.getEAnnotation(ImConstants.IM_TRANSITION_ANNOTATION) != null)
				|| ( artifact.getEAnnotation(ImConstants.IM_OBSERVABLE_ATTRIBUTE) != null))
				return true;
		
		return false;
    	}
}
