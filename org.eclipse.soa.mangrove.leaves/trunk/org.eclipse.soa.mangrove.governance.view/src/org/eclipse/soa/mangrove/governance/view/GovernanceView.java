package org.eclipse.soa.mangrove.governance.view;

import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.internal.ui.packageview.PackageExplorerPart;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.soa.mangrove.StpIntermediateModel;
import org.eclipse.soa.mangrove.monitoring.BPMSMonitoringHandler;
import org.eclipse.soa.mangrove.util.IMReader;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.navigator.CommonNavigator;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;


public class GovernanceView extends ViewPart implements ISelectionProvider {

	private org.eclipse.soa.mangrove.monitoring.BPMSMonitoringHandler monitoringHandler;
	
	private String selection = "";  
	  
	ListenerList listeners = new ListenerList();  
	
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.eclipse.soa.mangrove.governance.view.GovernanceView";

	/**
	 * ensure that the governance view displays the contents of the selected .mangrove file
	 */
	private ISelectionListener mangroveFileListener = new ISelectionListener() {
        public void selectionChanged(IWorkbenchPart sourcepart, ISelection selection) {
        	//System.out.println(">>>> sourcepart: " + sourcepart + " >>> selection: " + selection);
        	if ( (sourcepart instanceof CommonNavigator) || (sourcepart instanceof PackageExplorerPart) ){ //only if the selection is in the  Explorer
        		//System.out.println(">>>> GOV_VIEW: selection: " + selection);
				if (selection instanceof ITreeSelection) {
					if (((ITreeSelection) selection).toString().endsWith(".mangrove]")) {
						IFile mangroveFile = (IFile) ((StructuredSelection) selection).getFirstElement();
						IPath location = Platform.getLocation();
						URI uri = URI.createFileURI(location.toString()	+ mangroveFile.getFullPath().toString());
						//System.out.println("Calling SetInput on Viewer");
						viewer.setInput(uri);
					}
				}
        	}
        }
    };
	
	private TreeViewer viewer;
	private DrillDownAdapter drillDownAdapter;
	private Action action1;
	private Action action2;
	private Action doubleClickAction;

	/*
	 * The content provider class is responsible for
	 * providing objects to the view. It can wrap
	 * existing objects in adapters or simply return
	 * objects as-is. These objects may be sensitive
	 * to the current input of the view, or ignore
	 * it and always show the same content 
	 * (like Task List, for example).
	 */
	 
	class TreeObject implements IAdaptable {
		private String name;
		private TreeParent parent;
		
		public TreeObject(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
		public void setParent(TreeParent parent) {
			this.parent = parent;
		}
		public TreeParent getParent() {
			return parent;
		}
		public String toString() {
			return getName();
		}
		public Object getAdapter(Class key) {
			return null;
		}
	}
	
	class TreeParent extends TreeObject {
		private ArrayList children;
		public TreeParent(String name) {
			super(name);
			children = new ArrayList();
		}
		public void addChild(TreeObject child) {
			children.add(child);
			child.setParent(this);
		}
		public void removeChild(TreeObject child) {
			children.remove(child);
			child.setParent(null);
		}
		public TreeObject [] getChildren() {
			return (TreeObject [])children.toArray(new TreeObject[children.size()]);
		}
		public boolean hasChildren() {
			return children.size()>0;
		}
	}

	class ViewContentProvider implements IStructuredContentProvider, 
										   ITreeContentProvider {
		private TreeParent invisibleRoot;
		
		//to be called when the selection of the Mangrove file changes
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		
		public void dispose() {
		}
		
		public Object[] getElements(Object parent) {
			//System.out.println("GET ELEMENTS: ");
			if (parent.equals(getViewSite())) {
				if (invisibleRoot==null) initialize();
				return getChildren(invisibleRoot);
			}
			if(parent instanceof URI){
				URI uri = (URI)parent;
				this.initializeFromMangrove(uri);
				return getChildren(invisibleRoot);
			}
			return getChildren(parent);
		}
		public Object getParent(Object child) {
			if (child instanceof TreeObject) {
				return ((TreeObject)child).getParent();
			}
			return null;
		}
		public Object [] getChildren(Object parent) {
			//System.err.println("Get Children called with parent: " + parent);
			if (parent instanceof TreeParent) {
				return ((TreeParent)parent).getChildren();
			}
			return new Object[0];
		}
		public boolean hasChildren(Object parent) {
			if (parent instanceof TreeParent)
				return ((TreeParent)parent).hasChildren();
			return false;
		}
/*
 * We will set up a dummy model to initialize tree heararchy.
 * In a real code, you will connect to a real model and
 * expose its hierarchy.
 */
		private void initialize() {
			TreeParent root = new TreeParent("No Mangrove file selected");
			invisibleRoot = new TreeParent("");
			invisibleRoot.addChild(root);
		}
		
		/**
		 * Initialize the tree view from the contents of the selected Mangrove Core file
		 * @param uri the URI of the Mangrove file
		 */
		private void initializeFromMangrove(URI uri){
			//System.out.println("reading from " + uri);
			IMReader reader = new IMReader();
			StpIntermediateModel mangroveModel = reader.loadIM(uri);
			TreeParent procRoot = new TreeParent("Processes");
			TreeParent servicesRoot = new TreeParent("Services");
			for (org.eclipse.soa.mangrove.Process process : mangroveModel.getProcessCollection().getProcesses()){
				String pName = process.getName();
				Vector<Long> pTimes = monitoringHandler.getProcessExecutionTimes(pName);
				int pSize = 0;
				if(null!=pTimes) pSize =  pTimes.size();
				TreeParent procObject = new TreeParent(pName + " <" + pSize + ">" );
				procRoot.addChild(procObject);
				//System.out.println("Proc: " + process.getName());
				for (org.eclipse.soa.mangrove.Step step : process.getSteps()){
					String aName = step.getName();
					Vector<Long> aTimes = monitoringHandler.getActivityExecutionTimes(pName, aName);
					int aSize = 0;
					if(null!=aTimes) aSize =  aTimes.size();
					TreeObject stepObject = new TreeObject(aName + " <" + aSize + ">" );
					procObject.addChild(stepObject);
				}
				
			}
			for (org.eclipse.soa.mangrove.Service service : mangroveModel.getServiceCollection().getServices()){
				TreeObject serviceObject = new TreeObject(service.getServiceName());
				servicesRoot.addChild(serviceObject);
				//System.out.println("Service: " + service.getServiceName());
			}
			invisibleRoot = new TreeParent("");
			invisibleRoot.addChild(procRoot);
			invisibleRoot.addChild(servicesRoot);
		}
	}
	

	
	class ViewLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			return obj.toString();
		}
		public Image getImage(Object obj) {
			String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
			if (obj instanceof TreeParent)
			   imageKey = ISharedImages.IMG_OBJ_FOLDER;
			return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
		}
	}
	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public GovernanceView() {
		monitoringHandler = new BPMSMonitoringHandler();
	}

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(viewer);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());

		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "org.eclipse.soa.mangrove.governance.view.viewer");

		
		//make sure this tree view can fire selection events as well, used by the properties view which will display details about Mangrove elements
		//getSite().setSelectionProvider(this);  
		getSite().setSelectionProvider(viewer);
		
		//add the listener for the file changes, the idea is that when the user selects a .mangrove file, the governance view gets updated
		//System.out.println("ADDING LISTENER");
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this.mangroveFileListener);
	}



	private void makeActions() {

	}



	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	public void dispose() {
		// important: We need do unregister our listener when the view is disposed
		getSite().getWorkbenchWindow().getSelectionService().removeSelectionListener(mangroveFileListener);
		super.dispose();
	}

	@Override
	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		listeners.add(listener);  
		
	}

	@Override
	public ISelection getSelection() {
		return new StructuredSelection(selection);  
	}

	@Override
	public void removeSelectionChangedListener(
			ISelectionChangedListener listener) {  
		  listeners.remove(listener); 
		
	}

	@Override
	public void setSelection(ISelection select) {
		Object[] list = listeners.getListeners();  
		  for (int i = 0; i < list.length; i++) {  
		   ((ISelectionChangedListener) list[i])  
		     .selectionChanged(new SelectionChangedEvent(this, select));  
		  }
	}
	
}